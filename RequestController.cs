using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Task;

public class TestController : MonoBehaviour
{
    [ContextMenu("Get")]
    public async void Get()
    {
        string url = new string("/localhost:3000/user/61aa98e06ee5854b73687b21");
        using var www = UnityWebRequest.Get(url);

        www.SetRequestHeader("Content-Type", "application/json");
        var operation = www.SendWebRequest();
        while (!operation.isDone)
            await Task.Yield();

        var jsonResponse = www.downloadHandler.text;
        if (www.result != UnityWebRequest.Result.Success)
            Debug.LogError($"Failed: {www.error}");

        try
        {
            var result = JsonConvert.DeserializeObject<Player>(jsonResponse);
            Debug.Log($"Success: {www.downloadHandler.text}");
        }
        catch (Exception ex)
        {
            Debug.LogError($"{this} Could not parse response {jsonResponse}.{ex.message}");
        }
    }
}