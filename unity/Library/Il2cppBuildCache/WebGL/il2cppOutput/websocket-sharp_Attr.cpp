﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// System.Runtime.Versioning.TargetFrameworkAttribute
struct TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyConfigurationAttribute::m_configuration
	String_t* ___m_configuration_0;

public:
	inline static int32_t get_offset_of_m_configuration_0() { return static_cast<int32_t>(offsetof(AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C, ___m_configuration_0)); }
	inline String_t* get_m_configuration_0() const { return ___m_configuration_0; }
	inline String_t** get_address_of_m_configuration_0() { return &___m_configuration_0; }
	inline void set_m_configuration_0(String_t* value)
	{
		___m_configuration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_configuration_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyInformationalVersionAttribute::m_informationalVersion
	String_t* ___m_informationalVersion_0;

public:
	inline static int32_t get_offset_of_m_informationalVersion_0() { return static_cast<int32_t>(offsetof(AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0, ___m_informationalVersion_0)); }
	inline String_t* get_m_informationalVersion_0() const { return ___m_informationalVersion_0; }
	inline String_t** get_address_of_m_informationalVersion_0() { return &___m_informationalVersion_0; }
	inline void set_m_informationalVersion_0(String_t* value)
	{
		___m_informationalVersion_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_informationalVersion_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.InteropServices.ComVisibleAttribute::_val
	bool ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A, ____val_0)); }
	inline bool get__val_0() const { return ____val_0; }
	inline bool* get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(bool value)
	{
		____val_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Runtime.Versioning.TargetFrameworkAttribute
struct TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.Versioning.TargetFrameworkAttribute::_frameworkName
	String_t* ____frameworkName_0;
	// System.String System.Runtime.Versioning.TargetFrameworkAttribute::_frameworkDisplayName
	String_t* ____frameworkDisplayName_1;

public:
	inline static int32_t get_offset_of__frameworkName_0() { return static_cast<int32_t>(offsetof(TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517, ____frameworkName_0)); }
	inline String_t* get__frameworkName_0() const { return ____frameworkName_0; }
	inline String_t** get_address_of__frameworkName_0() { return &____frameworkName_0; }
	inline void set__frameworkName_0(String_t* value)
	{
		____frameworkName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameworkName_0), (void*)value);
	}

	inline static int32_t get_offset_of__frameworkDisplayName_1() { return static_cast<int32_t>(offsetof(TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517, ____frameworkDisplayName_1)); }
	inline String_t* get__frameworkDisplayName_1() const { return ____frameworkDisplayName_1; }
	inline String_t** get_address_of__frameworkDisplayName_1() { return &____frameworkDisplayName_1; }
	inline void set__frameworkDisplayName_1(String_t* value)
	{
		____frameworkDisplayName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameworkDisplayName_1), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyInformationalVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyInformationalVersionAttribute__ctor_m9BF349D8F980B0ABAB2A6312E422915285FA1678 (AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 * __this, String_t* ___informationalVersion0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757 (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * __this, String_t* ___configuration0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Runtime.Versioning.TargetFrameworkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetFrameworkAttribute__ctor_m0F8E5550F9199AC44F2CBCCD3E968EC26731187D (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___frameworkName0, const RuntimeMethod* method);
// System.Void System.Runtime.Versioning.TargetFrameworkAttribute::set_FrameworkDisplayName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.ComVisibleAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172 (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * __this, bool ___visibility0, const RuntimeMethod* method);
static void websocketU2Dsharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[0];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x77\x65\x62\x73\x6F\x63\x6B\x65\x74\x2D\x73\x68\x61\x72\x70"), NULL);
	}
	{
		AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 * tmp = (AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 *)cache->attributes[1];
		AssemblyInformationalVersionAttribute__ctor_m9BF349D8F980B0ABAB2A6312E422915285FA1678(tmp, il2cpp_codegen_string_new_wrapper("\x31\x2E\x30\x2E\x31"), NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[2];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x31\x2E\x30\x2E\x31\x2E\x30"), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[3];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper("\x77\x65\x62\x73\x6F\x63\x6B\x65\x74\x2D\x73\x68\x61\x72\x70\x20\x70\x72\x6F\x76\x69\x64\x65\x73\x20\x74\x68\x65\x20\x57\x65\x62\x53\x6F\x63\x6B\x65\x74\x20\x70\x72\x6F\x74\x6F\x63\x6F\x6C\x20\x63\x6C\x69\x65\x6E\x74\x20\x61\x6E\x64\x20\x73\x65\x72\x76\x65\x72\x2E\xD\xA\xD\xA\x49\x74\x20\x73\x75\x70\x70\x6F\x72\x74\x73\x3A\xD\xA\x2D\x20\x52\x46\x43\x20\x36\x34\x35\x35\xD\xA\x2D\x20\x57\x65\x62\x53\x6F\x63\x6B\x65\x74\x20\x43\x6C\x69\x65\x6E\x74\x20\x61\x6E\x64\x20\x53\x65\x72\x76\x65\x72\xD\xA\x2D\x20\x50\x65\x72\x2D\x6D\x65\x73\x73\x61\x67\x65\x20\x43\x6F\x6D\x70\x72\x65\x73\x73\x69\x6F\x6E\x20\x65\x78\x74\x65\x6E\x73\x69\x6F\x6E\xD\xA\x2D\x20\x53\x65\x63\x75\x72\x65\x20\x43\x6F\x6E\x6E\x65\x63\x74\x69\x6F\x6E\xD\xA\x2D\x20\x48\x54\x54\x50\x20\x41\x75\x74\x68\x65\x6E\x74\x69\x63\x61\x74\x69\x6F\x6E\x20\x28\x42\x61\x73\x69\x63\x2F\x44\x69\x67\x65\x73\x74\x29\xD\xA\x2D\x20\x51\x75\x65\x72\x79\x20\x53\x74\x72\x69\x6E\x67\x2C\x20\x4F\x72\x69\x67\x69\x6E\x20\x68\x65\x61\x64\x65\x72\x20\x61\x6E\x64\x20\x43\x6F\x6F\x6B\x69\x65\x73\xD\xA\x2D\x20\x43\x6F\x6E\x6E\x65\x63\x74\x69\x6E\x67\x20\x74\x68\x72\x6F\x75\x67\x68\x20\x74\x68\x65\x20\x48\x54\x54\x50\x20\x50\x72\x6F\x78\x79\x20\x73\x65\x72\x76\x65\x72\xD\xA\x2D\x20\x2E\x4E\x45\x54\x20\x33\x2E\x35\x20\x6F\x72\x20\x6C\x61\x74\x65\x72\x20\x28\x69\x6E\x63\x6C\x75\x64\x65\x73\x20\x63\x6F\x6D\x70\x61\x74\x69\x62\x6C\x65\x29"), NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[4];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x73\x74\x61\x2E\x62\x6C\x6F\x63\x6B\x68\x65\x61\x64"), NULL);
	}
	{
		AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * tmp = (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C *)cache->attributes[5];
		AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6C\x65\x61\x73\x65"), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[6];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x73\x74\x61"), NULL);
	}
	{
		TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * tmp = (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 *)cache->attributes[7];
		TargetFrameworkAttribute__ctor_m0F8E5550F9199AC44F2CBCCD3E968EC26731187D(tmp, il2cpp_codegen_string_new_wrapper("\x2E\x4E\x45\x54\x53\x74\x61\x6E\x64\x61\x72\x64\x2C\x56\x65\x72\x73\x69\x6F\x6E\x3D\x76\x32\x2E\x30"), NULL);
		TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[8];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[9];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[10];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[11];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[12];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x77\x65\x62\x73\x6F\x63\x6B\x65\x74\x2D\x73\x68\x61\x72\x70"), NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_compress_mD4C2796D2FBC070550D2CD75DDA51C83E9BD2987(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_decompress_m427E057F05587ED24C6E248E6832A6D2E5112726(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_decompress_mABE502281B3E26E33B4C2637C9E7D24CDC52F967(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_decompressToArray_m1F9B4046E694DCB4D999DA8B47B6DC0ED5399523(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Append_mF73F4C045B980F22589FA584EB26759B9C5CCA95(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Compress_m4DE120E660E355838429CCBC9837D16427D916C9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Contains_m4BC4D1850B91EB5543352563FBE48FD03E79A169(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ContainsTwice_mAB075799CC2EF175AD23C89F94B0EAD00E85C875(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Decompress_m1ED72371D40BA922178A5130E1A0527D8F6FE557(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_DecompressToArray_mA52D8B0CA81216D2051623836AD21CDCC5E8C022(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_EqualsWith_m602C3F302D0F9AAE130D9014F970884E471109E7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_GetAbsolutePath_m941CECC02205794D2954367E1359313F5E3E83C6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_GetMessage_m509016B4BFA0E92FDB9084A58D2276CE738E342F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_GetValue_m57696A3768779B421AF2926C38554B548B8433BA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_GetValue_m0AEA039DB43D2305C9C4ADDAF663AD8BCCA158CD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_InternalToByteArray_mF1C42D893EC79B4750C85D794AADCD4A41BB3E5C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_InternalToByteArray_m5E229B80AEBD6E9EA6D7C93A763386C68F03209A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsCompressionExtension_m28F55DD02E9B4FF10BD9B2BAB623B1B5AB46AE03(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsControl_mC3D6A201173127C93DD76E8FA6DF7C321E2CF73A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsData_mAFFBD230031406EA01E187C28D84823FEC0D3A97(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsData_mE05AE2DBC078B9A47A8CAAEA9B4320585340557A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsReserved_m47B52E0CA0FC872C775CCF9A9650B6BABAD98350(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsSupported_m7A0754B21CBA9C9697E5099A47BDB6C60BCA7472(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsText_mCFD72F9EEE8AD25B018A0E6373E7477369A879DB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsToken_m97D7912B9DAE4BB8855044EA5A0684014CD79AA0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ReadBytes_m746C3CBBF792074ADF3F3C23CAB4BED5CDBD8B7A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ReadBytes_m0AE12D583213668EAA64B597F03AE1A61FB70D78(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ReadBytesAsync_m16D801AC0B450703AB6A3F0D4EAEFD30B1156814(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ReadBytesAsync_mADCDA9C10789215294D374FD43220DE6EB47F0B6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Reverse_m66F9F899730F2F861776B303423A63DE4398BA05(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_SplitHeaderValue_m214C4CDEF09FDCA6EC0A8F2964607820BE77A2EB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_0_0_0_var), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_SplitHeaderValue_m214C4CDEF09FDCA6EC0A8F2964607820BE77A2EB____separators1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToByteArray_mE2EF1061C8DAFCAB1F802BEC6EDE6AD39B7EC9A4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToExtensionString_m4419B0E5FB8CAA7DE4947DA8FD366DD73FF2F118(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToExtensionString_m4419B0E5FB8CAA7DE4947DA8FD366DD73FF2F118____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToUInt16_mEA0762C274353DC4130C43DF43078BBFDDD7BEBE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToUInt64_m44C63692BA27C38B6EFEB3CE8861A3448BEA9C41(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_TryCreateWebSocketUri_m8B34AC7F5E019DD2BF9E2F491435B6D288493F1B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_TryGetUTF8EncodedBytes_m57A1F100366F6B8808A4E67AAC39EBC07287F406(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Unquote_m1360A078A746E1AA119C3F86618A3144A5360103(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_UTF8Decode_m3EB4CA65516EE6D3585EA310EEEDDBBE261430B4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_UTF8Encode_m3F64D495F861D010952DBE3DC48C7D54DDBC5C71(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_WriteBytes_mA70DD162AF0633BD51AABBCEE4BF9A9833CF631E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Contains_mED3E63D3389C2DF33CD407F751C3EB06C9AC4D03(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Contains_mED3E63D3389C2DF33CD407F751C3EB06C9AC4D03____chars1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Contains_mDC75764AAB1DA8B2D81FB2522DE4E9AF267DF906(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Contains_m881701E2ECE7D8C155C47197A9E9890CCCA318CB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Emit_m31FDD032A4BE024BBD0B74012EEB8424FF55F8DF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Emit_m2A7919429CBA68AA58393B433AD4400A7EF45342(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_GetCookies_mC4F7262B4912A08960C4EBCF42672898E1CC40CE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsEnclosedIn_m73DC5AD84C7CA776AE088D81F15E540CD1783649(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsHostOrder_m27DD21DC7DB6BBD6991D1E8E9C2DE77A18BC9324(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsNullOrEmpty_m654B58C78D471F8590E04A1391A16073EEF7D99A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsPredefinedScheme_mAE6C3157BAF10BC07F334A7E6A996561E26C682E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_MaybeUri_m862204170F3AD7AC5346239341C0EC6546CFBB5A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_SubArray_m5D29CC577848A8CE32F812C5569DAE2EBA8B1375(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_SubArray_mAB3DBE0A2DCF1AD5A9FADDDA9839A55AD455350D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Times_m51922047D256B1F5AA71BEF7AC9FAF016D514BD1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToHostOrder_m980E3470B526CC55E4BEBF850C7C7C851800FEE0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToString_m78ABAA7F855F99FCD7D785C6B110A28790135088(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToUri_m0375123E8A1876CA4EADB8744AD6DAA0902BEA1F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_UrlDecode_mECBC25F781F7303754C50655582A1DD968028DB0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_t06E3969F5C29A952E754FC417BA4BA11213E1FB3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass48_0_t89D86E6C2BFC8E33970139B92490EBDC2035064D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass49_0_t806E4BF3AB8D26AE47A82E95E546CE99CB8A2F42_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass49_1_tEF00AA77E07D4B6B6F5F8214B0C8CC85D0BD9A85_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__52__ctor_mEEEF5C290CC8DAE912EE4213AA0124EFC70AA711(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__52_System_IDisposable_Dispose_m5568AFC7A25D4DFAF5D77935CF2BC0D4CF8534A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m28C4CE000A42289D2A12502FCBE95363C55B3D8E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__52_System_Collections_IEnumerator_Reset_m3A8605E28BA677079F200B3F2F4446DBD6DCC330(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__52_System_Collections_IEnumerator_get_Current_m7A30EF478B56FFEDD2CC9DD22CAA2AD613EF00D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__52_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_mF15E97C6108FA75DAA8BE8CB081C885F020EC7FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__52_System_Collections_IEnumerable_GetEnumerator_mCACDDAEAC36B9140023F81001E76691A7624F61D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass101_0_1_t846176E9984C32DE33645364F0E0D19967A35C25_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_tCA450B810B44E67E5830087B3933C5A9195603B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass14_0_1_tB51E13DED4D3590F86DE178ED24B684BFBF7D89E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PayloadData_tA66104D0BADFF925F28E6A25EA0B0139B4ABFDE7_CustomAttributesCacheGenerator_PayloadData_GetEnumerator_m55F82176CFD8B96DEE1F16FA6C287A9DFE2AFC6B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_0_0_0_var), NULL);
	}
}
static void U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__30__ctor_m335EF37C1CFAE5315B2F71A5BD50284DA5FC5B51(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__30_System_IDisposable_Dispose_m6F8626D33DFE696C5767F3F7070C9177362363F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mC12FF44109AF7A21477128A4D49F2AD34ADBF26D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__30_System_Collections_IEnumerator_Reset_m19951350A8D870881344C8AD1EE42C0A114D141D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__30_System_Collections_IEnumerator_get_Current_m40BB1BAEB424A447D2B4255058E140312277F51F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_OnClose(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_OnError(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_OnMessage(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_OnOpen(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_WebSocket__ctor_m175FB52D7114C1BADE9B416A1A8FEA68DDB598D2____protocols1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_WebSocket_add_OnMessage_mF0B54AE34AAFCE31A08400721B0A5E090917A217(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_WebSocket_remove_OnMessage_m935642FBC23EC0857A2AA9DE7F71D4BE581D6C60(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_WebSocket_U3CopenU3Eb__149_0_m9BEF4D4159EDBA5F03CA42219E36E40ECFA2B702(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_WebSocket_U3CstartReceivingU3Eb__176_2_mCD6FC6C5B650FC5B3D855EE5D11C162718353A2E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tC1C888ACB61CD06B14F8AC3C19D391A5A249A096_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass176_0_tAB7678AFC0836378C7D03FB7DB5ECC853074DB21_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass179_0_tCD2CD0CB950FE4445E97AACEDEB5D0D9C715B0B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass182_0_t096D6E0C57C16203E43EA447EB9BACCF2D26E9CE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketFrame_t6A14FB912F013E1CC58712AEB8A53B4408D86374_CustomAttributesCacheGenerator_WebSocketFrame_GetEnumerator_m9D52C4ADD5619EEC50A9D022597512CFA217B09D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass67_0_t0754558F084C851F4AE3CA7FA09FA57F536138C6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass67_1_t6BDC11423E6E40F8F3FE3179F402C0860C596939_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass71_0_t3F3AD50430BAC683EABD44B9456250428522D48B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass73_0_t5DD8587F0CADC8671ED19483030B7BCE8EF4D97B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass75_0_t8EDE3104728F59DD9DA9473BE0D79D3424640230_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass77_0_tC8DA7856C26D7EB32CB1235733E46DA95160FAE8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass83_0_t1F340B1A6034E254D9306EF84EB6F93210292DAA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__85__ctor_mB81744CE82D2D830E6E3271167B58BBEEEA122B8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__85_System_IDisposable_Dispose_mCE929B1D1E5378276B9CEC19201A079022860EA0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__85_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mA058D4C46AC01F3ED57D8F130C576D2A0450B147(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__85_System_Collections_IEnumerator_Reset_m88AFA5520518B5E282A8DD1F5AC7F8DE991EAA3D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__85_System_Collections_IEnumerator_get_Current_mC46248ABC03D68B751A1165D6D1801CA942A28F1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t07C90999738667A3837650D03A6D719288578304_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Cookie_tD943A255D5ED2BDAC2E3D9F151402B56091EF692_CustomAttributesCacheGenerator_U3CExactDomainU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Cookie_tD943A255D5ED2BDAC2E3D9F151402B56091EF692_CustomAttributesCacheGenerator_Cookie_set_ExactDomain_mBDC214FF8F6EDE24455AE63CB7975DC36358B99F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CookieCollection_t89ECD07A71921A03F3EB5B9AB70096E59F9495EC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void HttpHeaderType_t082C7B499E97951252EC6063B73D3DB991A43CED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void WebHeaderCollection_tB28780450E885DDA72DFA8EC0D53036F09C26F59_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * tmp = (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A *)cache->attributes[1];
		ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172(tmp, true, NULL);
	}
}
static void U3CU3Ec__DisplayClass59_0_t73141F1E0E124BF648A9B2F2113B62A61F494E49_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass70_0_t221525622012D6A60329B009355C9BC615E67882_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t8704722ABB9B14E4CBA9B88B1853EA7EC759AB14_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_websocketU2Dsharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_websocketU2Dsharp_AttributeGenerators[122] = 
{
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_t06E3969F5C29A952E754FC417BA4BA11213E1FB3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass48_0_t89D86E6C2BFC8E33970139B92490EBDC2035064D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass49_0_t806E4BF3AB8D26AE47A82E95E546CE99CB8A2F42_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass49_1_tEF00AA77E07D4B6B6F5F8214B0C8CC85D0BD9A85_CustomAttributesCacheGenerator,
	U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass101_0_1_t846176E9984C32DE33645364F0E0D19967A35C25_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_tCA450B810B44E67E5830087B3933C5A9195603B0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass14_0_1_tB51E13DED4D3590F86DE178ED24B684BFBF7D89E_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_CustomAttributesCacheGenerator,
	U3CU3Ec_tC1C888ACB61CD06B14F8AC3C19D391A5A249A096_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass176_0_tAB7678AFC0836378C7D03FB7DB5ECC853074DB21_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass179_0_tCD2CD0CB950FE4445E97AACEDEB5D0D9C715B0B0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass182_0_t096D6E0C57C16203E43EA447EB9BACCF2D26E9CE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass67_0_t0754558F084C851F4AE3CA7FA09FA57F536138C6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass67_1_t6BDC11423E6E40F8F3FE3179F402C0860C596939_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass71_0_t3F3AD50430BAC683EABD44B9456250428522D48B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass73_0_t5DD8587F0CADC8671ED19483030B7BCE8EF4D97B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass75_0_t8EDE3104728F59DD9DA9473BE0D79D3424640230_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass77_0_tC8DA7856C26D7EB32CB1235733E46DA95160FAE8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass83_0_t1F340B1A6034E254D9306EF84EB6F93210292DAA_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_CustomAttributesCacheGenerator,
	U3CU3Ec_t07C90999738667A3837650D03A6D719288578304_CustomAttributesCacheGenerator,
	CookieCollection_t89ECD07A71921A03F3EB5B9AB70096E59F9495EC_CustomAttributesCacheGenerator,
	HttpHeaderType_t082C7B499E97951252EC6063B73D3DB991A43CED_CustomAttributesCacheGenerator,
	WebHeaderCollection_tB28780450E885DDA72DFA8EC0D53036F09C26F59_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass59_0_t73141F1E0E124BF648A9B2F2113B62A61F494E49_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass70_0_t221525622012D6A60329B009355C9BC615E67882_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t8704722ABB9B14E4CBA9B88B1853EA7EC759AB14_CustomAttributesCacheGenerator,
	WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_OnClose,
	WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_OnError,
	WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_OnMessage,
	WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_OnOpen,
	Cookie_tD943A255D5ED2BDAC2E3D9F151402B56091EF692_CustomAttributesCacheGenerator_U3CExactDomainU3Ek__BackingField,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_compress_mD4C2796D2FBC070550D2CD75DDA51C83E9BD2987,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_decompress_m427E057F05587ED24C6E248E6832A6D2E5112726,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_decompress_mABE502281B3E26E33B4C2637C9E7D24CDC52F967,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_decompressToArray_m1F9B4046E694DCB4D999DA8B47B6DC0ED5399523,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Append_mF73F4C045B980F22589FA584EB26759B9C5CCA95,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Compress_m4DE120E660E355838429CCBC9837D16427D916C9,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Contains_m4BC4D1850B91EB5543352563FBE48FD03E79A169,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ContainsTwice_mAB075799CC2EF175AD23C89F94B0EAD00E85C875,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Decompress_m1ED72371D40BA922178A5130E1A0527D8F6FE557,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_DecompressToArray_mA52D8B0CA81216D2051623836AD21CDCC5E8C022,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_EqualsWith_m602C3F302D0F9AAE130D9014F970884E471109E7,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_GetAbsolutePath_m941CECC02205794D2954367E1359313F5E3E83C6,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_GetMessage_m509016B4BFA0E92FDB9084A58D2276CE738E342F,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_GetValue_m57696A3768779B421AF2926C38554B548B8433BA,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_GetValue_m0AEA039DB43D2305C9C4ADDAF663AD8BCCA158CD,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_InternalToByteArray_mF1C42D893EC79B4750C85D794AADCD4A41BB3E5C,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_InternalToByteArray_m5E229B80AEBD6E9EA6D7C93A763386C68F03209A,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsCompressionExtension_m28F55DD02E9B4FF10BD9B2BAB623B1B5AB46AE03,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsControl_mC3D6A201173127C93DD76E8FA6DF7C321E2CF73A,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsData_mAFFBD230031406EA01E187C28D84823FEC0D3A97,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsData_mE05AE2DBC078B9A47A8CAAEA9B4320585340557A,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsReserved_m47B52E0CA0FC872C775CCF9A9650B6BABAD98350,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsSupported_m7A0754B21CBA9C9697E5099A47BDB6C60BCA7472,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsText_mCFD72F9EEE8AD25B018A0E6373E7477369A879DB,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsToken_m97D7912B9DAE4BB8855044EA5A0684014CD79AA0,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ReadBytes_m746C3CBBF792074ADF3F3C23CAB4BED5CDBD8B7A,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ReadBytes_m0AE12D583213668EAA64B597F03AE1A61FB70D78,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ReadBytesAsync_m16D801AC0B450703AB6A3F0D4EAEFD30B1156814,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ReadBytesAsync_mADCDA9C10789215294D374FD43220DE6EB47F0B6,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Reverse_m66F9F899730F2F861776B303423A63DE4398BA05,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_SplitHeaderValue_m214C4CDEF09FDCA6EC0A8F2964607820BE77A2EB,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToByteArray_mE2EF1061C8DAFCAB1F802BEC6EDE6AD39B7EC9A4,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToExtensionString_m4419B0E5FB8CAA7DE4947DA8FD366DD73FF2F118,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToUInt16_mEA0762C274353DC4130C43DF43078BBFDDD7BEBE,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToUInt64_m44C63692BA27C38B6EFEB3CE8861A3448BEA9C41,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_TryCreateWebSocketUri_m8B34AC7F5E019DD2BF9E2F491435B6D288493F1B,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_TryGetUTF8EncodedBytes_m57A1F100366F6B8808A4E67AAC39EBC07287F406,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Unquote_m1360A078A746E1AA119C3F86618A3144A5360103,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_UTF8Decode_m3EB4CA65516EE6D3585EA310EEEDDBBE261430B4,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_UTF8Encode_m3F64D495F861D010952DBE3DC48C7D54DDBC5C71,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_WriteBytes_mA70DD162AF0633BD51AABBCEE4BF9A9833CF631E,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Contains_mED3E63D3389C2DF33CD407F751C3EB06C9AC4D03,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Contains_mDC75764AAB1DA8B2D81FB2522DE4E9AF267DF906,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Contains_m881701E2ECE7D8C155C47197A9E9890CCCA318CB,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Emit_m31FDD032A4BE024BBD0B74012EEB8424FF55F8DF,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Emit_m2A7919429CBA68AA58393B433AD4400A7EF45342,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_GetCookies_mC4F7262B4912A08960C4EBCF42672898E1CC40CE,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsEnclosedIn_m73DC5AD84C7CA776AE088D81F15E540CD1783649,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsHostOrder_m27DD21DC7DB6BBD6991D1E8E9C2DE77A18BC9324,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsNullOrEmpty_m654B58C78D471F8590E04A1391A16073EEF7D99A,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_IsPredefinedScheme_mAE6C3157BAF10BC07F334A7E6A996561E26C682E,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_MaybeUri_m862204170F3AD7AC5346239341C0EC6546CFBB5A,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_SubArray_m5D29CC577848A8CE32F812C5569DAE2EBA8B1375,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_SubArray_mAB3DBE0A2DCF1AD5A9FADDDA9839A55AD455350D,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Times_m51922047D256B1F5AA71BEF7AC9FAF016D514BD1,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToHostOrder_m980E3470B526CC55E4BEBF850C7C7C851800FEE0,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToString_m78ABAA7F855F99FCD7D785C6B110A28790135088,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToUri_m0375123E8A1876CA4EADB8744AD6DAA0902BEA1F,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_UrlDecode_mECBC25F781F7303754C50655582A1DD968028DB0,
	U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__52__ctor_mEEEF5C290CC8DAE912EE4213AA0124EFC70AA711,
	U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__52_System_IDisposable_Dispose_m5568AFC7A25D4DFAF5D77935CF2BC0D4CF8534A8,
	U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m28C4CE000A42289D2A12502FCBE95363C55B3D8E,
	U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__52_System_Collections_IEnumerator_Reset_m3A8605E28BA677079F200B3F2F4446DBD6DCC330,
	U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__52_System_Collections_IEnumerator_get_Current_m7A30EF478B56FFEDD2CC9DD22CAA2AD613EF00D1,
	U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__52_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_mF15E97C6108FA75DAA8BE8CB081C885F020EC7FC,
	U3CSplitHeaderValueU3Ed__52_t0317920C7AB2F3B899C4F413FDD09EAC977BA716_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__52_System_Collections_IEnumerable_GetEnumerator_mCACDDAEAC36B9140023F81001E76691A7624F61D,
	PayloadData_tA66104D0BADFF925F28E6A25EA0B0139B4ABFDE7_CustomAttributesCacheGenerator_PayloadData_GetEnumerator_m55F82176CFD8B96DEE1F16FA6C287A9DFE2AFC6B,
	U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__30__ctor_m335EF37C1CFAE5315B2F71A5BD50284DA5FC5B51,
	U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__30_System_IDisposable_Dispose_m6F8626D33DFE696C5767F3F7070C9177362363F6,
	U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mC12FF44109AF7A21477128A4D49F2AD34ADBF26D,
	U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__30_System_Collections_IEnumerator_Reset_m19951350A8D870881344C8AD1EE42C0A114D141D,
	U3CGetEnumeratorU3Ed__30_tFF61E7E48C0DE0A368892823F5293C2DB6000AD4_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__30_System_Collections_IEnumerator_get_Current_m40BB1BAEB424A447D2B4255058E140312277F51F,
	WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_WebSocket_add_OnMessage_mF0B54AE34AAFCE31A08400721B0A5E090917A217,
	WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_WebSocket_remove_OnMessage_m935642FBC23EC0857A2AA9DE7F71D4BE581D6C60,
	WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_WebSocket_U3CopenU3Eb__149_0_m9BEF4D4159EDBA5F03CA42219E36E40ECFA2B702,
	WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_WebSocket_U3CstartReceivingU3Eb__176_2_mCD6FC6C5B650FC5B3D855EE5D11C162718353A2E,
	WebSocketFrame_t6A14FB912F013E1CC58712AEB8A53B4408D86374_CustomAttributesCacheGenerator_WebSocketFrame_GetEnumerator_m9D52C4ADD5619EEC50A9D022597512CFA217B09D,
	U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__85__ctor_mB81744CE82D2D830E6E3271167B58BBEEEA122B8,
	U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__85_System_IDisposable_Dispose_mCE929B1D1E5378276B9CEC19201A079022860EA0,
	U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__85_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mA058D4C46AC01F3ED57D8F130C576D2A0450B147,
	U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__85_System_Collections_IEnumerator_Reset_m88AFA5520518B5E282A8DD1F5AC7F8DE991EAA3D,
	U3CGetEnumeratorU3Ed__85_tC2A3C8F05BFEFA36F8EBEF2E12F233F14864272E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__85_System_Collections_IEnumerator_get_Current_mC46248ABC03D68B751A1165D6D1801CA942A28F1,
	Cookie_tD943A255D5ED2BDAC2E3D9F151402B56091EF692_CustomAttributesCacheGenerator_Cookie_set_ExactDomain_mBDC214FF8F6EDE24455AE63CB7975DC36358B99F,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_SplitHeaderValue_m214C4CDEF09FDCA6EC0A8F2964607820BE77A2EB____separators1,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_ToExtensionString_m4419B0E5FB8CAA7DE4947DA8FD366DD73FF2F118____parameters1,
	Ext_t3770DC47BCEE5907544D84E6674E04A22425A00D_CustomAttributesCacheGenerator_Ext_Contains_mED3E63D3389C2DF33CD407F751C3EB06C9AC4D03____chars1,
	WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_CustomAttributesCacheGenerator_WebSocket__ctor_m175FB52D7114C1BADE9B416A1A8FEA68DDB598D2____protocols1,
	websocketU2Dsharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__frameworkDisplayName_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
