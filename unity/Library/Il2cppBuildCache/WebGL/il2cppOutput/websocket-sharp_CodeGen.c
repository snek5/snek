﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void WebSocketSharp.CloseEventArgs::.ctor(WebSocketSharp.PayloadData)
extern void CloseEventArgs__ctor_mDC321F70447E0467C047F72D4F53350562907F28 (void);
// 0x00000002 System.Void WebSocketSharp.CloseEventArgs::set_WasClean(System.Boolean)
extern void CloseEventArgs_set_WasClean_m4361EF1BC86CBDB6ABDE42E560324AC0054795AD (void);
// 0x00000003 System.Void WebSocketSharp.ErrorEventArgs::.ctor(System.String,System.Exception)
extern void ErrorEventArgs__ctor_mE691F8047B9A0E5EA872F48B5FF369CC584B37B1 (void);
// 0x00000004 System.IO.MemoryStream WebSocketSharp.Ext::compress(System.IO.Stream)
extern void Ext_compress_mD4C2796D2FBC070550D2CD75DDA51C83E9BD2987 (void);
// 0x00000005 System.Byte[] WebSocketSharp.Ext::decompress(System.Byte[])
extern void Ext_decompress_m427E057F05587ED24C6E248E6832A6D2E5112726 (void);
// 0x00000006 System.IO.MemoryStream WebSocketSharp.Ext::decompress(System.IO.Stream)
extern void Ext_decompress_mABE502281B3E26E33B4C2637C9E7D24CDC52F967 (void);
// 0x00000007 System.Byte[] WebSocketSharp.Ext::decompressToArray(System.IO.Stream)
extern void Ext_decompressToArray_m1F9B4046E694DCB4D999DA8B47B6DC0ED5399523 (void);
// 0x00000008 System.Byte[] WebSocketSharp.Ext::Append(System.UInt16,System.String)
extern void Ext_Append_mF73F4C045B980F22589FA584EB26759B9C5CCA95 (void);
// 0x00000009 System.IO.Stream WebSocketSharp.Ext::Compress(System.IO.Stream,WebSocketSharp.CompressionMethod)
extern void Ext_Compress_m4DE120E660E355838429CCBC9837D16427D916C9 (void);
// 0x0000000A System.Boolean WebSocketSharp.Ext::Contains(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<T,System.Boolean>)
// 0x0000000B System.Boolean WebSocketSharp.Ext::ContainsTwice(System.String[])
extern void Ext_ContainsTwice_mAB075799CC2EF175AD23C89F94B0EAD00E85C875 (void);
// 0x0000000C System.Byte[] WebSocketSharp.Ext::Decompress(System.Byte[],WebSocketSharp.CompressionMethod)
extern void Ext_Decompress_m1ED72371D40BA922178A5130E1A0527D8F6FE557 (void);
// 0x0000000D System.Byte[] WebSocketSharp.Ext::DecompressToArray(System.IO.Stream,WebSocketSharp.CompressionMethod)
extern void Ext_DecompressToArray_mA52D8B0CA81216D2051623836AD21CDCC5E8C022 (void);
// 0x0000000E System.Boolean WebSocketSharp.Ext::EqualsWith(System.Int32,System.Char,System.Action`1<System.Int32>)
extern void Ext_EqualsWith_m602C3F302D0F9AAE130D9014F970884E471109E7 (void);
// 0x0000000F System.String WebSocketSharp.Ext::GetAbsolutePath(System.Uri)
extern void Ext_GetAbsolutePath_m941CECC02205794D2954367E1359313F5E3E83C6 (void);
// 0x00000010 System.String WebSocketSharp.Ext::GetMessage(WebSocketSharp.CloseStatusCode)
extern void Ext_GetMessage_m509016B4BFA0E92FDB9084A58D2276CE738E342F (void);
// 0x00000011 System.String WebSocketSharp.Ext::GetValue(System.String,System.Char)
extern void Ext_GetValue_m57696A3768779B421AF2926C38554B548B8433BA (void);
// 0x00000012 System.String WebSocketSharp.Ext::GetValue(System.String,System.Char,System.Boolean)
extern void Ext_GetValue_m0AEA039DB43D2305C9C4ADDAF663AD8BCCA158CD (void);
// 0x00000013 System.Byte[] WebSocketSharp.Ext::InternalToByteArray(System.UInt16,WebSocketSharp.ByteOrder)
extern void Ext_InternalToByteArray_mF1C42D893EC79B4750C85D794AADCD4A41BB3E5C (void);
// 0x00000014 System.Byte[] WebSocketSharp.Ext::InternalToByteArray(System.UInt64,WebSocketSharp.ByteOrder)
extern void Ext_InternalToByteArray_m5E229B80AEBD6E9EA6D7C93A763386C68F03209A (void);
// 0x00000015 System.Boolean WebSocketSharp.Ext::IsCompressionExtension(System.String,WebSocketSharp.CompressionMethod)
extern void Ext_IsCompressionExtension_m28F55DD02E9B4FF10BD9B2BAB623B1B5AB46AE03 (void);
// 0x00000016 System.Boolean WebSocketSharp.Ext::IsControl(System.Byte)
extern void Ext_IsControl_mC3D6A201173127C93DD76E8FA6DF7C321E2CF73A (void);
// 0x00000017 System.Boolean WebSocketSharp.Ext::IsData(System.Byte)
extern void Ext_IsData_mAFFBD230031406EA01E187C28D84823FEC0D3A97 (void);
// 0x00000018 System.Boolean WebSocketSharp.Ext::IsData(WebSocketSharp.Opcode)
extern void Ext_IsData_mE05AE2DBC078B9A47A8CAAEA9B4320585340557A (void);
// 0x00000019 System.Boolean WebSocketSharp.Ext::IsReserved(System.UInt16)
extern void Ext_IsReserved_m47B52E0CA0FC872C775CCF9A9650B6BABAD98350 (void);
// 0x0000001A System.Boolean WebSocketSharp.Ext::IsSupported(System.Byte)
extern void Ext_IsSupported_m7A0754B21CBA9C9697E5099A47BDB6C60BCA7472 (void);
// 0x0000001B System.Boolean WebSocketSharp.Ext::IsText(System.String)
extern void Ext_IsText_mCFD72F9EEE8AD25B018A0E6373E7477369A879DB (void);
// 0x0000001C System.Boolean WebSocketSharp.Ext::IsToken(System.String)
extern void Ext_IsToken_m97D7912B9DAE4BB8855044EA5A0684014CD79AA0 (void);
// 0x0000001D System.Byte[] WebSocketSharp.Ext::ReadBytes(System.IO.Stream,System.Int32)
extern void Ext_ReadBytes_m746C3CBBF792074ADF3F3C23CAB4BED5CDBD8B7A (void);
// 0x0000001E System.Byte[] WebSocketSharp.Ext::ReadBytes(System.IO.Stream,System.Int64,System.Int32)
extern void Ext_ReadBytes_m0AE12D583213668EAA64B597F03AE1A61FB70D78 (void);
// 0x0000001F System.Void WebSocketSharp.Ext::ReadBytesAsync(System.IO.Stream,System.Int32,System.Action`1<System.Byte[]>,System.Action`1<System.Exception>)
extern void Ext_ReadBytesAsync_m16D801AC0B450703AB6A3F0D4EAEFD30B1156814 (void);
// 0x00000020 System.Void WebSocketSharp.Ext::ReadBytesAsync(System.IO.Stream,System.Int64,System.Int32,System.Action`1<System.Byte[]>,System.Action`1<System.Exception>)
extern void Ext_ReadBytesAsync_mADCDA9C10789215294D374FD43220DE6EB47F0B6 (void);
// 0x00000021 T[] WebSocketSharp.Ext::Reverse(T[])
// 0x00000022 System.Collections.Generic.IEnumerable`1<System.String> WebSocketSharp.Ext::SplitHeaderValue(System.String,System.Char[])
extern void Ext_SplitHeaderValue_m214C4CDEF09FDCA6EC0A8F2964607820BE77A2EB (void);
// 0x00000023 System.Byte[] WebSocketSharp.Ext::ToByteArray(System.IO.Stream)
extern void Ext_ToByteArray_mE2EF1061C8DAFCAB1F802BEC6EDE6AD39B7EC9A4 (void);
// 0x00000024 System.String WebSocketSharp.Ext::ToExtensionString(WebSocketSharp.CompressionMethod,System.String[])
extern void Ext_ToExtensionString_m4419B0E5FB8CAA7DE4947DA8FD366DD73FF2F118 (void);
// 0x00000025 System.UInt16 WebSocketSharp.Ext::ToUInt16(System.Byte[],WebSocketSharp.ByteOrder)
extern void Ext_ToUInt16_mEA0762C274353DC4130C43DF43078BBFDDD7BEBE (void);
// 0x00000026 System.UInt64 WebSocketSharp.Ext::ToUInt64(System.Byte[],WebSocketSharp.ByteOrder)
extern void Ext_ToUInt64_m44C63692BA27C38B6EFEB3CE8861A3448BEA9C41 (void);
// 0x00000027 System.Boolean WebSocketSharp.Ext::TryCreateWebSocketUri(System.String,System.Uri&,System.String&)
extern void Ext_TryCreateWebSocketUri_m8B34AC7F5E019DD2BF9E2F491435B6D288493F1B (void);
// 0x00000028 System.Boolean WebSocketSharp.Ext::TryGetUTF8EncodedBytes(System.String,System.Byte[]&)
extern void Ext_TryGetUTF8EncodedBytes_m57A1F100366F6B8808A4E67AAC39EBC07287F406 (void);
// 0x00000029 System.String WebSocketSharp.Ext::Unquote(System.String)
extern void Ext_Unquote_m1360A078A746E1AA119C3F86618A3144A5360103 (void);
// 0x0000002A System.String WebSocketSharp.Ext::UTF8Decode(System.Byte[])
extern void Ext_UTF8Decode_m3EB4CA65516EE6D3585EA310EEEDDBBE261430B4 (void);
// 0x0000002B System.Byte[] WebSocketSharp.Ext::UTF8Encode(System.String)
extern void Ext_UTF8Encode_m3F64D495F861D010952DBE3DC48C7D54DDBC5C71 (void);
// 0x0000002C System.Void WebSocketSharp.Ext::WriteBytes(System.IO.Stream,System.Byte[],System.Int32)
extern void Ext_WriteBytes_mA70DD162AF0633BD51AABBCEE4BF9A9833CF631E (void);
// 0x0000002D System.Boolean WebSocketSharp.Ext::Contains(System.String,System.Char[])
extern void Ext_Contains_mED3E63D3389C2DF33CD407F751C3EB06C9AC4D03 (void);
// 0x0000002E System.Boolean WebSocketSharp.Ext::Contains(System.Collections.Specialized.NameValueCollection,System.String)
extern void Ext_Contains_mDC75764AAB1DA8B2D81FB2522DE4E9AF267DF906 (void);
// 0x0000002F System.Boolean WebSocketSharp.Ext::Contains(System.Collections.Specialized.NameValueCollection,System.String,System.String)
extern void Ext_Contains_m881701E2ECE7D8C155C47197A9E9890CCCA318CB (void);
// 0x00000030 System.Void WebSocketSharp.Ext::Emit(System.EventHandler,System.Object,System.EventArgs)
extern void Ext_Emit_m31FDD032A4BE024BBD0B74012EEB8424FF55F8DF (void);
// 0x00000031 System.Void WebSocketSharp.Ext::Emit(System.EventHandler`1<TEventArgs>,System.Object,TEventArgs)
// 0x00000032 WebSocketSharp.Net.CookieCollection WebSocketSharp.Ext::GetCookies(System.Collections.Specialized.NameValueCollection,System.Boolean)
extern void Ext_GetCookies_mC4F7262B4912A08960C4EBCF42672898E1CC40CE (void);
// 0x00000033 System.Boolean WebSocketSharp.Ext::IsEnclosedIn(System.String,System.Char)
extern void Ext_IsEnclosedIn_m73DC5AD84C7CA776AE088D81F15E540CD1783649 (void);
// 0x00000034 System.Boolean WebSocketSharp.Ext::IsHostOrder(WebSocketSharp.ByteOrder)
extern void Ext_IsHostOrder_m27DD21DC7DB6BBD6991D1E8E9C2DE77A18BC9324 (void);
// 0x00000035 System.Boolean WebSocketSharp.Ext::IsNullOrEmpty(System.String)
extern void Ext_IsNullOrEmpty_m654B58C78D471F8590E04A1391A16073EEF7D99A (void);
// 0x00000036 System.Boolean WebSocketSharp.Ext::IsPredefinedScheme(System.String)
extern void Ext_IsPredefinedScheme_mAE6C3157BAF10BC07F334A7E6A996561E26C682E (void);
// 0x00000037 System.Boolean WebSocketSharp.Ext::MaybeUri(System.String)
extern void Ext_MaybeUri_m862204170F3AD7AC5346239341C0EC6546CFBB5A (void);
// 0x00000038 T[] WebSocketSharp.Ext::SubArray(T[],System.Int32,System.Int32)
// 0x00000039 T[] WebSocketSharp.Ext::SubArray(T[],System.Int64,System.Int64)
// 0x0000003A System.Void WebSocketSharp.Ext::Times(System.Int32,System.Action`1<System.Int32>)
extern void Ext_Times_m51922047D256B1F5AA71BEF7AC9FAF016D514BD1 (void);
// 0x0000003B System.Byte[] WebSocketSharp.Ext::ToHostOrder(System.Byte[],WebSocketSharp.ByteOrder)
extern void Ext_ToHostOrder_m980E3470B526CC55E4BEBF850C7C7C851800FEE0 (void);
// 0x0000003C System.String WebSocketSharp.Ext::ToString(T[],System.String)
// 0x0000003D System.Uri WebSocketSharp.Ext::ToUri(System.String)
extern void Ext_ToUri_m0375123E8A1876CA4EADB8744AD6DAA0902BEA1F (void);
// 0x0000003E System.String WebSocketSharp.Ext::UrlDecode(System.String)
extern void Ext_UrlDecode_mECBC25F781F7303754C50655582A1DD968028DB0 (void);
// 0x0000003F System.Void WebSocketSharp.Ext::.cctor()
extern void Ext__cctor_m18E1AA1A549C62FBCEB1E058DB12B53792465C8A (void);
// 0x00000040 System.Void WebSocketSharp.Ext/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_mF0FAF9A2BE249E49400A40158D2F6C542E9151DB (void);
// 0x00000041 System.Boolean WebSocketSharp.Ext/<>c__DisplayClass17_0::<ContainsTwice>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass17_0_U3CContainsTwiceU3Eb__0_mB75C137661CE3DD30CD73F8C5BD64A2B2D7DF955 (void);
// 0x00000042 System.Void WebSocketSharp.Ext/<>c__DisplayClass48_0::.ctor()
extern void U3CU3Ec__DisplayClass48_0__ctor_m3240EBA2FC44B1CF2D82F99BEBF64B497AEDA021 (void);
// 0x00000043 System.Void WebSocketSharp.Ext/<>c__DisplayClass48_0::<ReadBytesAsync>b__0(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass48_0_U3CReadBytesAsyncU3Eb__0_m8F47055EF367B407891F4D19B30ADF6832BBC151 (void);
// 0x00000044 System.Void WebSocketSharp.Ext/<>c__DisplayClass49_0::.ctor()
extern void U3CU3Ec__DisplayClass49_0__ctor_m06606A62C40C2667F40ED16FD9B785264F9D2236 (void);
// 0x00000045 System.Void WebSocketSharp.Ext/<>c__DisplayClass49_0::<ReadBytesAsync>b__0(System.Int64)
extern void U3CU3Ec__DisplayClass49_0_U3CReadBytesAsyncU3Eb__0_m743493B2C077508720674A8FB050F1789FA6848B (void);
// 0x00000046 System.Void WebSocketSharp.Ext/<>c__DisplayClass49_1::.ctor()
extern void U3CU3Ec__DisplayClass49_1__ctor_mF41FED9907D163525810B5FC87DB594C6896227F (void);
// 0x00000047 System.Void WebSocketSharp.Ext/<>c__DisplayClass49_1::<ReadBytesAsync>b__1(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass49_1_U3CReadBytesAsyncU3Eb__1_mDB4AE547D2D5E45E53BC72DA100F350DBD94092C (void);
// 0x00000048 System.Void WebSocketSharp.Ext/<SplitHeaderValue>d__52::.ctor(System.Int32)
extern void U3CSplitHeaderValueU3Ed__52__ctor_mEEEF5C290CC8DAE912EE4213AA0124EFC70AA711 (void);
// 0x00000049 System.Void WebSocketSharp.Ext/<SplitHeaderValue>d__52::System.IDisposable.Dispose()
extern void U3CSplitHeaderValueU3Ed__52_System_IDisposable_Dispose_m5568AFC7A25D4DFAF5D77935CF2BC0D4CF8534A8 (void);
// 0x0000004A System.Boolean WebSocketSharp.Ext/<SplitHeaderValue>d__52::MoveNext()
extern void U3CSplitHeaderValueU3Ed__52_MoveNext_mEB0076C482A64006D0ACDCA2D7A595DDFF1ACFB5 (void);
// 0x0000004B System.String WebSocketSharp.Ext/<SplitHeaderValue>d__52::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3CSplitHeaderValueU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m28C4CE000A42289D2A12502FCBE95363C55B3D8E (void);
// 0x0000004C System.Void WebSocketSharp.Ext/<SplitHeaderValue>d__52::System.Collections.IEnumerator.Reset()
extern void U3CSplitHeaderValueU3Ed__52_System_Collections_IEnumerator_Reset_m3A8605E28BA677079F200B3F2F4446DBD6DCC330 (void);
// 0x0000004D System.Object WebSocketSharp.Ext/<SplitHeaderValue>d__52::System.Collections.IEnumerator.get_Current()
extern void U3CSplitHeaderValueU3Ed__52_System_Collections_IEnumerator_get_Current_m7A30EF478B56FFEDD2CC9DD22CAA2AD613EF00D1 (void);
// 0x0000004E System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharp.Ext/<SplitHeaderValue>d__52::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void U3CSplitHeaderValueU3Ed__52_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_mF15E97C6108FA75DAA8BE8CB081C885F020EC7FC (void);
// 0x0000004F System.Collections.IEnumerator WebSocketSharp.Ext/<SplitHeaderValue>d__52::System.Collections.IEnumerable.GetEnumerator()
extern void U3CSplitHeaderValueU3Ed__52_System_Collections_IEnumerable_GetEnumerator_mCACDDAEAC36B9140023F81001E76691A7624F61D (void);
// 0x00000050 System.Void WebSocketSharp.Ext/<>c__DisplayClass101_0`1::.ctor()
// 0x00000051 System.Void WebSocketSharp.Ext/<>c__DisplayClass101_0`1::<ToString>b__0(System.Int32)
// 0x00000052 System.Void WebSocketSharp.HttpBase::.ctor(System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpBase__ctor_m6BDD2ED980EC8084B9094686433BB7941BE3BB36 (void);
// 0x00000053 System.String WebSocketSharp.HttpBase::get_EntityBody()
extern void HttpBase_get_EntityBody_m56BFF1A7DA71E8FA8750E8A701A82DD400B0E194 (void);
// 0x00000054 System.Collections.Specialized.NameValueCollection WebSocketSharp.HttpBase::get_Headers()
extern void HttpBase_get_Headers_m24461D7C004DB2F04E7C5EC6B9A907AA2F836A70 (void);
// 0x00000055 System.Version WebSocketSharp.HttpBase::get_ProtocolVersion()
extern void HttpBase_get_ProtocolVersion_mF582A1C4D8FBA34837B3F5B094C8FCF542A88A2E (void);
// 0x00000056 System.Byte[] WebSocketSharp.HttpBase::readEntityBody(System.IO.Stream,System.String)
extern void HttpBase_readEntityBody_mB991152177BFF56FE57E41EDFB670EAE57513EBE (void);
// 0x00000057 System.String[] WebSocketSharp.HttpBase::readHeaders(System.IO.Stream,System.Int32)
extern void HttpBase_readHeaders_m4FB768CF58F79E65D882ADC06D7A131920D7059F (void);
// 0x00000058 T WebSocketSharp.HttpBase::Read(System.IO.Stream,System.Func`2<System.String[],T>,System.Int32)
// 0x00000059 System.Byte[] WebSocketSharp.HttpBase::ToByteArray()
extern void HttpBase_ToByteArray_m0C0BC34C6B074EE47F6674A58069B30B75BAD423 (void);
// 0x0000005A System.Void WebSocketSharp.HttpBase/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mD747743FF8C3ECA76CCD36DBEA62A461EEA2A4AF (void);
// 0x0000005B System.Void WebSocketSharp.HttpBase/<>c__DisplayClass13_0::<readHeaders>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass13_0_U3CreadHeadersU3Eb__0_m340D5883AAE76D4F1386D4F741747FBA448A9316 (void);
// 0x0000005C System.Void WebSocketSharp.HttpBase/<>c__DisplayClass14_0`1::.ctor()
// 0x0000005D System.Void WebSocketSharp.HttpBase/<>c__DisplayClass14_0`1::<Read>b__0(System.Object)
// 0x0000005E System.Void WebSocketSharp.HttpRequest::.ctor(System.String,System.String,System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpRequest__ctor_m9D7958E1424351848E11A4E2041B0EBC78F82E3C (void);
// 0x0000005F System.Void WebSocketSharp.HttpRequest::.ctor(System.String,System.String)
extern void HttpRequest__ctor_m3EA5DB4C024E712057BDCE0D65AAE9B6FDFFE604 (void);
// 0x00000060 WebSocketSharp.HttpRequest WebSocketSharp.HttpRequest::CreateConnectRequest(System.Uri)
extern void HttpRequest_CreateConnectRequest_m44EB2C0A38B77E7E24D78FB203246C51726F8B3E (void);
// 0x00000061 WebSocketSharp.HttpRequest WebSocketSharp.HttpRequest::CreateWebSocketRequest(System.Uri)
extern void HttpRequest_CreateWebSocketRequest_m483EDB8FCC75CC12AE33E73AF291B967778C9296 (void);
// 0x00000062 WebSocketSharp.HttpResponse WebSocketSharp.HttpRequest::GetResponse(System.IO.Stream,System.Int32)
extern void HttpRequest_GetResponse_m7F34D114B8A9644C8EBAE0FF12C817C400E6DE43 (void);
// 0x00000063 System.Void WebSocketSharp.HttpRequest::SetCookies(WebSocketSharp.Net.CookieCollection)
extern void HttpRequest_SetCookies_mA0AFFEDF2E96A61215588CD9BC7EFC53D7F334F2 (void);
// 0x00000064 System.String WebSocketSharp.HttpRequest::ToString()
extern void HttpRequest_ToString_m9660D7F7F8B52FAF155BA00B2B0776E1FA4A2FD9 (void);
// 0x00000065 System.Void WebSocketSharp.HttpResponse::.ctor(System.String,System.String,System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpResponse__ctor_mD21EB7871945A19605382BB2729E7A974DDBC27A (void);
// 0x00000066 WebSocketSharp.Net.CookieCollection WebSocketSharp.HttpResponse::get_Cookies()
extern void HttpResponse_get_Cookies_m44E63E077EFE4AE09881484C9D9BE70FEAB27172 (void);
// 0x00000067 System.Boolean WebSocketSharp.HttpResponse::get_HasConnectionClose()
extern void HttpResponse_get_HasConnectionClose_m513633428FB48F21748338AF26A8BEC95039D7CF (void);
// 0x00000068 System.Boolean WebSocketSharp.HttpResponse::get_IsProxyAuthenticationRequired()
extern void HttpResponse_get_IsProxyAuthenticationRequired_m5075CC07905D4DBF42C3CB057EB8FD4794839841 (void);
// 0x00000069 System.Boolean WebSocketSharp.HttpResponse::get_IsRedirect()
extern void HttpResponse_get_IsRedirect_m9EC1FC46F378A7E4351B3C06BACC613913366BE8 (void);
// 0x0000006A System.Boolean WebSocketSharp.HttpResponse::get_IsUnauthorized()
extern void HttpResponse_get_IsUnauthorized_mEF9EE7E22A77025DA1932059F6B6A417E20590BB (void);
// 0x0000006B System.Boolean WebSocketSharp.HttpResponse::get_IsWebSocketResponse()
extern void HttpResponse_get_IsWebSocketResponse_m1818EA031CE2C0409C19AFA175618977633E9745 (void);
// 0x0000006C System.String WebSocketSharp.HttpResponse::get_StatusCode()
extern void HttpResponse_get_StatusCode_mE68937073584FEEEA2916174687747D9915897AD (void);
// 0x0000006D WebSocketSharp.HttpResponse WebSocketSharp.HttpResponse::Parse(System.String[])
extern void HttpResponse_Parse_mA6D6C652C8B4916979640B7219A1746673309634 (void);
// 0x0000006E System.String WebSocketSharp.HttpResponse::ToString()
extern void HttpResponse_ToString_m18847537B4222F77D5DEE509A289B0DDB1BCFDC2 (void);
// 0x0000006F System.Void WebSocketSharp.LogData::.ctor(WebSocketSharp.LogLevel,System.Diagnostics.StackFrame,System.String)
extern void LogData__ctor_mF8E1C53E4A1D3503D2D297E465ED9F1C491743F7 (void);
// 0x00000070 System.String WebSocketSharp.LogData::ToString()
extern void LogData_ToString_mF1E28365092C73BD30800104CC8741F0F28661F4 (void);
// 0x00000071 System.Void WebSocketSharp.Logger::.ctor()
extern void Logger__ctor_mCA34E16009AD89A47D17AE0E990697958DBB9162 (void);
// 0x00000072 System.Void WebSocketSharp.Logger::.ctor(WebSocketSharp.LogLevel,System.String,System.Action`2<WebSocketSharp.LogData,System.String>)
extern void Logger__ctor_m95D1A5E9F9039DB13CE2BA2C2FC319D9618DEBE3 (void);
// 0x00000073 System.Void WebSocketSharp.Logger::defaultOutput(WebSocketSharp.LogData,System.String)
extern void Logger_defaultOutput_m8FF965E6D941AEEF705872667743418C4404500E (void);
// 0x00000074 System.Void WebSocketSharp.Logger::output(System.String,WebSocketSharp.LogLevel)
extern void Logger_output_mE6682E3961EB0EB91B61EEE510A85CDA85C43E3D (void);
// 0x00000075 System.Void WebSocketSharp.Logger::writeToFile(System.String,System.String)
extern void Logger_writeToFile_m9F98007E158236794F49313D8C8C1C3829DF484E (void);
// 0x00000076 System.Void WebSocketSharp.Logger::Debug(System.String)
extern void Logger_Debug_mE12768ACB86A12ACEDB976A9438162F7D7310C9D (void);
// 0x00000077 System.Void WebSocketSharp.Logger::Error(System.String)
extern void Logger_Error_m66745DDD974C80951E1CF20813099E74770B91B7 (void);
// 0x00000078 System.Void WebSocketSharp.Logger::Fatal(System.String)
extern void Logger_Fatal_mC1AA7BD00C477F0272C9066D7A864ADDF85A5A11 (void);
// 0x00000079 System.Void WebSocketSharp.Logger::Info(System.String)
extern void Logger_Info_mD332A7B1B39D26D825CD435275A4DAD2B2402036 (void);
// 0x0000007A System.Void WebSocketSharp.Logger::Trace(System.String)
extern void Logger_Trace_m1CF54FD67E3B0D173D2055BB03C910DD0B723296 (void);
// 0x0000007B System.Void WebSocketSharp.Logger::Warn(System.String)
extern void Logger_Warn_m0122EA54266E4E0C3143914DBF1E24EBBEB79B01 (void);
// 0x0000007C System.Void WebSocketSharp.MessageEventArgs::.ctor(WebSocketSharp.WebSocketFrame)
extern void MessageEventArgs__ctor_mD8454957F0042F9420106AB2A94695B4B063CCDF (void);
// 0x0000007D System.Void WebSocketSharp.MessageEventArgs::.ctor(WebSocketSharp.Opcode,System.Byte[])
extern void MessageEventArgs__ctor_m8237A1352361CCAEB53DAB653B9D5A4E6104CDF5 (void);
// 0x0000007E System.String WebSocketSharp.MessageEventArgs::get_Data()
extern void MessageEventArgs_get_Data_mD9D53F8250BB093F649A6279D1EB19D408901A52 (void);
// 0x0000007F System.Void WebSocketSharp.MessageEventArgs::setData()
extern void MessageEventArgs_setData_m4C24C252B72BE15F3DA1D3A5F1D155B6E9836BC5 (void);
// 0x00000080 System.Void WebSocketSharp.PayloadData::.cctor()
extern void PayloadData__cctor_mB7644CD570FE157F463FCCFF3415C1C395CF5805 (void);
// 0x00000081 System.Void WebSocketSharp.PayloadData::.ctor()
extern void PayloadData__ctor_mF5D87D4121219352F25929034C788CED603D7D35 (void);
// 0x00000082 System.Void WebSocketSharp.PayloadData::.ctor(System.Byte[])
extern void PayloadData__ctor_m88C22FADB90110886E2BA24C2F1FC51A0EED5669 (void);
// 0x00000083 System.Void WebSocketSharp.PayloadData::.ctor(System.Byte[],System.Int64)
extern void PayloadData__ctor_m81EA7F817427C6A708309C6F5FC0E873527842D5 (void);
// 0x00000084 System.Void WebSocketSharp.PayloadData::.ctor(System.UInt16,System.String)
extern void PayloadData__ctor_m3F3A9E5163178992AA8BDB7188133415C9C05C89 (void);
// 0x00000085 System.UInt16 WebSocketSharp.PayloadData::get_Code()
extern void PayloadData_get_Code_m7E00D5AEA04FFC02A963AF98E4A9C6BB3CE0E41D (void);
// 0x00000086 System.Boolean WebSocketSharp.PayloadData::get_HasReservedCode()
extern void PayloadData_get_HasReservedCode_mC85FE15F2BD39856FF54260BC018F6EF162EBFB2 (void);
// 0x00000087 System.Byte[] WebSocketSharp.PayloadData::get_ApplicationData()
extern void PayloadData_get_ApplicationData_mA92610537E1DAEDD250EAEAA6FB4F665AD04995E (void);
// 0x00000088 System.UInt64 WebSocketSharp.PayloadData::get_Length()
extern void PayloadData_get_Length_m6F53F71D7C887F3B9488504F4FF841A6071F7123 (void);
// 0x00000089 System.Void WebSocketSharp.PayloadData::Mask(System.Byte[])
extern void PayloadData_Mask_mF87463FB968657CA88936F16495F21FE6D6DA49C (void);
// 0x0000008A System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharp.PayloadData::GetEnumerator()
extern void PayloadData_GetEnumerator_m55F82176CFD8B96DEE1F16FA6C287A9DFE2AFC6B (void);
// 0x0000008B System.Byte[] WebSocketSharp.PayloadData::ToArray()
extern void PayloadData_ToArray_m167BA73C74B5037261484E0FA744238A90798EE8 (void);
// 0x0000008C System.String WebSocketSharp.PayloadData::ToString()
extern void PayloadData_ToString_mB8A2E015AA4EEA408BF622542272148262643427 (void);
// 0x0000008D System.Collections.IEnumerator WebSocketSharp.PayloadData::System.Collections.IEnumerable.GetEnumerator()
extern void PayloadData_System_Collections_IEnumerable_GetEnumerator_m11977F44DD5D507C2221979B1DF435568B2056EE (void);
// 0x0000008E System.Void WebSocketSharp.PayloadData/<GetEnumerator>d__30::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__30__ctor_m335EF37C1CFAE5315B2F71A5BD50284DA5FC5B51 (void);
// 0x0000008F System.Void WebSocketSharp.PayloadData/<GetEnumerator>d__30::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__30_System_IDisposable_Dispose_m6F8626D33DFE696C5767F3F7070C9177362363F6 (void);
// 0x00000090 System.Boolean WebSocketSharp.PayloadData/<GetEnumerator>d__30::MoveNext()
extern void U3CGetEnumeratorU3Ed__30_MoveNext_m23A12FEF8820EF8F6A863E5F3B1305F4D7B88BCC (void);
// 0x00000091 System.Byte WebSocketSharp.PayloadData/<GetEnumerator>d__30::System.Collections.Generic.IEnumerator<System.Byte>.get_Current()
extern void U3CGetEnumeratorU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mC12FF44109AF7A21477128A4D49F2AD34ADBF26D (void);
// 0x00000092 System.Void WebSocketSharp.PayloadData/<GetEnumerator>d__30::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__30_System_Collections_IEnumerator_Reset_m19951350A8D870881344C8AD1EE42C0A114D141D (void);
// 0x00000093 System.Object WebSocketSharp.PayloadData/<GetEnumerator>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__30_System_Collections_IEnumerator_get_Current_m40BB1BAEB424A447D2B4255058E140312277F51F (void);
// 0x00000094 System.Void WebSocketSharp.WebSocket::.cctor()
extern void WebSocket__cctor_mA9D3ECD1DF4667D62001FBC74E00272DCB039B6D (void);
// 0x00000095 System.Void WebSocketSharp.WebSocket::.ctor(System.String,System.String[])
extern void WebSocket__ctor_m175FB52D7114C1BADE9B416A1A8FEA68DDB598D2 (void);
// 0x00000096 System.Boolean WebSocketSharp.WebSocket::get_HasMessage()
extern void WebSocket_get_HasMessage_mF77D6A257D1D5DDE2F67190947551E32C7F18EB1 (void);
// 0x00000097 WebSocketSharp.Net.ClientSslConfiguration WebSocketSharp.WebSocket::get_SslConfiguration()
extern void WebSocket_get_SslConfiguration_mB99EC9B00CF3D62366DD58767720F976EB1CAEC7 (void);
// 0x00000098 System.Uri WebSocketSharp.WebSocket::get_Url()
extern void WebSocket_get_Url_mBC9629B05E6D0F63B87A76E8E079DFDD78DFC788 (void);
// 0x00000099 System.Void WebSocketSharp.WebSocket::add_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
extern void WebSocket_add_OnMessage_mF0B54AE34AAFCE31A08400721B0A5E090917A217 (void);
// 0x0000009A System.Void WebSocketSharp.WebSocket::remove_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
extern void WebSocket_remove_OnMessage_m935642FBC23EC0857A2AA9DE7F71D4BE581D6C60 (void);
// 0x0000009B System.Boolean WebSocketSharp.WebSocket::checkHandshakeResponse(WebSocketSharp.HttpResponse,System.String&)
extern void WebSocket_checkHandshakeResponse_m170E0888DED27765C357293FAC3B56FD6EE9A53F (void);
// 0x0000009C System.Boolean WebSocketSharp.WebSocket::checkIfAvailable(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String&)
extern void WebSocket_checkIfAvailable_mF8F4AC5040D11ACB449D959669D73C02D442C43A (void);
// 0x0000009D System.Boolean WebSocketSharp.WebSocket::checkIfAvailable(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String&)
extern void WebSocket_checkIfAvailable_m426F7C62840EA4143E2C8FAB09B509B7D116BD88 (void);
// 0x0000009E System.Boolean WebSocketSharp.WebSocket::checkProtocols(System.String[],System.String&)
extern void WebSocket_checkProtocols_mC185E421CB2FCD24D977AD47AAD926E47D63895F (void);
// 0x0000009F System.Boolean WebSocketSharp.WebSocket::checkReceivedFrame(WebSocketSharp.WebSocketFrame,System.String&)
extern void WebSocket_checkReceivedFrame_m4AB3496468C150ADCDF4293CA7900BE3E86FB4CF (void);
// 0x000000A0 System.Void WebSocketSharp.WebSocket::close(System.UInt16,System.String)
extern void WebSocket_close_mD946E9F019F843AB6F170BA067F725267C2D7A26 (void);
// 0x000000A1 System.Void WebSocketSharp.WebSocket::close(WebSocketSharp.PayloadData,System.Boolean,System.Boolean,System.Boolean)
extern void WebSocket_close_m7FE622A1A45AC84AF8E625148EE0DD8A3DA30CD3 (void);
// 0x000000A2 System.Boolean WebSocketSharp.WebSocket::closeHandshake(WebSocketSharp.PayloadData,System.Boolean,System.Boolean,System.Boolean)
extern void WebSocket_closeHandshake_m422725140F42BDB53F472CB90DF2A79AEF7F4C4D (void);
// 0x000000A3 System.Boolean WebSocketSharp.WebSocket::connect()
extern void WebSocket_connect_mF175F8BB4C098E5705E64A8A1E36DC349042B575 (void);
// 0x000000A4 System.String WebSocketSharp.WebSocket::createExtensions()
extern void WebSocket_createExtensions_mE2F71B48DE46105E19F9258F54E1D6A81DF47F45 (void);
// 0x000000A5 WebSocketSharp.HttpRequest WebSocketSharp.WebSocket::createHandshakeRequest()
extern void WebSocket_createHandshakeRequest_mE075DFD36540293F547E9A8E36498EB94C32316E (void);
// 0x000000A6 System.Void WebSocketSharp.WebSocket::doHandshake()
extern void WebSocket_doHandshake_m4FAFC1594C76F86ADB4A5D33CA10F8688833D072 (void);
// 0x000000A7 System.Void WebSocketSharp.WebSocket::enqueueToMessageEventQueue(WebSocketSharp.MessageEventArgs)
extern void WebSocket_enqueueToMessageEventQueue_m9C7AD2244BAF1ED734B6597C39E79B0D7AB2462C (void);
// 0x000000A8 System.Void WebSocketSharp.WebSocket::error(System.String,System.Exception)
extern void WebSocket_error_m06A1332197237509DC3B3D2EF205A56BAC4313AF (void);
// 0x000000A9 System.Void WebSocketSharp.WebSocket::fatal(System.String,System.Exception)
extern void WebSocket_fatal_m97B2E2B7C8F7DE278EF374B568C1A62DE3895D46 (void);
// 0x000000AA System.Void WebSocketSharp.WebSocket::fatal(System.String,System.UInt16)
extern void WebSocket_fatal_m5BB9146E82E925264BDDBA02DA67D4FD1EFF6B2A (void);
// 0x000000AB System.Void WebSocketSharp.WebSocket::fatal(System.String,WebSocketSharp.CloseStatusCode)
extern void WebSocket_fatal_mF98585EFFB8AB899C2AF5459A22AA23F6797E03C (void);
// 0x000000AC System.Void WebSocketSharp.WebSocket::init()
extern void WebSocket_init_mFD12CBCE8D217388D44A27A7F234CF56DF1CC6B7 (void);
// 0x000000AD System.Void WebSocketSharp.WebSocket::message()
extern void WebSocket_message_m31EEE103C6A3E5278CEBE338EE5DE7B6B2D55B52 (void);
// 0x000000AE System.Void WebSocketSharp.WebSocket::messagec(WebSocketSharp.MessageEventArgs)
extern void WebSocket_messagec_mBC045F7784E3468CC4F9423E26643C6B313BED63 (void);
// 0x000000AF System.Void WebSocketSharp.WebSocket::open()
extern void WebSocket_open_m4CB54D45FD3E493826FF901F0EEE6E78303096D7 (void);
// 0x000000B0 System.Boolean WebSocketSharp.WebSocket::processCloseFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processCloseFrame_m5A3991F8CB8017E064AC346EB064ACC19ACC6025 (void);
// 0x000000B1 System.Void WebSocketSharp.WebSocket::processCookies(WebSocketSharp.Net.CookieCollection)
extern void WebSocket_processCookies_m5F78334D163E93DD7175B3160F1B7EFC48B9DD54 (void);
// 0x000000B2 System.Boolean WebSocketSharp.WebSocket::processDataFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processDataFrame_m5F66F8A0593FA2D4A8649E76AE58CC702DE7FFDA (void);
// 0x000000B3 System.Boolean WebSocketSharp.WebSocket::processFragmentFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processFragmentFrame_m74C560E6355A9D1707196E7E6D5DB3B291D1AFBA (void);
// 0x000000B4 System.Boolean WebSocketSharp.WebSocket::processPingFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processPingFrame_m7003688EBA6B3215B3C8932069A3AFDF2A61FEE5 (void);
// 0x000000B5 System.Boolean WebSocketSharp.WebSocket::processPongFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processPongFrame_m070CB41EF461443887634D095A475CC7F367F56B (void);
// 0x000000B6 System.Boolean WebSocketSharp.WebSocket::processReceivedFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processReceivedFrame_mF83C54F25258F2561D4C06FBFBC04665F9DE43DC (void);
// 0x000000B7 System.Void WebSocketSharp.WebSocket::processSecWebSocketExtensionsServerHeader(System.String)
extern void WebSocket_processSecWebSocketExtensionsServerHeader_mB10BA3FF5AF0D50D328D8A09444BABE4A5F91696 (void);
// 0x000000B8 System.Boolean WebSocketSharp.WebSocket::processUnsupportedFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processUnsupportedFrame_m9DCA729B9AE7ACBB60C91FEFF40A4B22ED2462E6 (void);
// 0x000000B9 System.Void WebSocketSharp.WebSocket::releaseClientResources()
extern void WebSocket_releaseClientResources_m51DC67496F684EDCE811B0F9D950184B0CBD67A9 (void);
// 0x000000BA System.Void WebSocketSharp.WebSocket::releaseCommonResources()
extern void WebSocket_releaseCommonResources_mB51780C83634F7617C529E03390C32A6B460E5BC (void);
// 0x000000BB System.Void WebSocketSharp.WebSocket::releaseResources()
extern void WebSocket_releaseResources_mFA3D35D608E8AC4D7B39D39351A92A7F560AE26E (void);
// 0x000000BC System.Void WebSocketSharp.WebSocket::releaseServerResources()
extern void WebSocket_releaseServerResources_m9B663B1EDA7676F497FD4AC87E27E5654BCDD63D (void);
// 0x000000BD System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Opcode,System.IO.Stream)
extern void WebSocket_send_m27C189CB1E3A5EC38FDBA2168F1D24F6DF143ADC (void);
// 0x000000BE System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Opcode,System.IO.Stream,System.Boolean)
extern void WebSocket_send_mC77F9DCE16017C0265F61FD288D53CD4B718892E (void);
// 0x000000BF System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Fin,WebSocketSharp.Opcode,System.Byte[],System.Boolean)
extern void WebSocket_send_m66F67DEC9942B6701D4E9B13BDBFC5B03042DD01 (void);
// 0x000000C0 System.Boolean WebSocketSharp.WebSocket::sendBytes(System.Byte[])
extern void WebSocket_sendBytes_mCF575A0B62BFBE18DCD19B10E71B96D97EA49D7C (void);
// 0x000000C1 WebSocketSharp.HttpResponse WebSocketSharp.WebSocket::sendHandshakeRequest()
extern void WebSocket_sendHandshakeRequest_m0E09CE106174C6F947943FEACE61AFB07C862964 (void);
// 0x000000C2 WebSocketSharp.HttpResponse WebSocketSharp.WebSocket::sendHttpRequest(WebSocketSharp.HttpRequest,System.Int32)
extern void WebSocket_sendHttpRequest_m50769E4EE9523AB957CE0038A47DCE3606ABCC53 (void);
// 0x000000C3 System.Void WebSocketSharp.WebSocket::sendProxyConnectRequest()
extern void WebSocket_sendProxyConnectRequest_mAB74241CF1009C925CB5FA2EDCCAF70BC233EE16 (void);
// 0x000000C4 System.Void WebSocketSharp.WebSocket::setClientStream()
extern void WebSocket_setClientStream_mF279DC34FB82585814FFDDD9D83EFE12E023640D (void);
// 0x000000C5 System.Void WebSocketSharp.WebSocket::startReceiving()
extern void WebSocket_startReceiving_mB0FB7FF506E5C804E4B8E27ECA6FCB32091C36D9 (void);
// 0x000000C6 System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketAcceptHeader(System.String)
extern void WebSocket_validateSecWebSocketAcceptHeader_mAA6E9F3A8DE35B5A6E93E36657FC321A501C5091 (void);
// 0x000000C7 System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketExtensionsServerHeader(System.String)
extern void WebSocket_validateSecWebSocketExtensionsServerHeader_m9884B0DCE956DE8261CAEE46914090B997588655 (void);
// 0x000000C8 System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketProtocolServerHeader(System.String)
extern void WebSocket_validateSecWebSocketProtocolServerHeader_m04C0FF21F3878BD399E04E2CA2CE615055225A33 (void);
// 0x000000C9 System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketVersionServerHeader(System.String)
extern void WebSocket_validateSecWebSocketVersionServerHeader_mB29F9D54B7722A00B9A250E3BFA83B41C2B61E9F (void);
// 0x000000CA System.String WebSocketSharp.WebSocket::CreateBase64Key()
extern void WebSocket_CreateBase64Key_m4E4C5A9BFB2AAB8FFAB759F3798DA589825784E4 (void);
// 0x000000CB System.String WebSocketSharp.WebSocket::CreateResponseKey(System.String)
extern void WebSocket_CreateResponseKey_m55035FB709DB40706532B2143096C403B460B2B6 (void);
// 0x000000CC System.Void WebSocketSharp.WebSocket::Connect()
extern void WebSocket_Connect_m786CF51F671040752A008871832C29E4D9E416A5 (void);
// 0x000000CD System.Void WebSocketSharp.WebSocket::Send(System.String)
extern void WebSocket_Send_mB2A549377FA705334E19181213272F70DB8C0786 (void);
// 0x000000CE System.Void WebSocketSharp.WebSocket::System.IDisposable.Dispose()
extern void WebSocket_System_IDisposable_Dispose_m930A970B3B5AE1090F71D02362C5B5DC88D287D9 (void);
// 0x000000CF System.Void WebSocketSharp.WebSocket::<open>b__149_0(System.IAsyncResult)
extern void WebSocket_U3CopenU3Eb__149_0_m9BEF4D4159EDBA5F03CA42219E36E40ECFA2B702 (void);
// 0x000000D0 System.Void WebSocketSharp.WebSocket::<startReceiving>b__176_2(System.Exception)
extern void WebSocket_U3CstartReceivingU3Eb__176_2_mCD6FC6C5B650FC5B3D855EE5D11C162718353A2E (void);
// 0x000000D1 System.Void WebSocketSharp.WebSocket/<>c::.cctor()
extern void U3CU3Ec__cctor_mCB8A692B08D8D4D4BB64C642689AE7DE82B03404 (void);
// 0x000000D2 System.Void WebSocketSharp.WebSocket/<>c::.ctor()
extern void U3CU3Ec__ctor_m3CEED560674A4945BC8FDC2C14ABCC989AC85E18 (void);
// 0x000000D3 System.Boolean WebSocketSharp.WebSocket/<>c::<checkProtocols>b__124_0(System.String)
extern void U3CU3Ec_U3CcheckProtocolsU3Eb__124_0_m8F0DAEAF49E98730466C8E5167277B8F4272AF12 (void);
// 0x000000D4 System.Void WebSocketSharp.WebSocket/<>c__DisplayClass176_0::.ctor()
extern void U3CU3Ec__DisplayClass176_0__ctor_mC0A30D69AAFCD5AA00C18C1195442779D79FE1D8 (void);
// 0x000000D5 System.Void WebSocketSharp.WebSocket/<>c__DisplayClass176_0::<startReceiving>b__0()
extern void U3CU3Ec__DisplayClass176_0_U3CstartReceivingU3Eb__0_m2A7B03FB79CCE4E2FB47EB171ED7A9B2785EEE4E (void);
// 0x000000D6 System.Void WebSocketSharp.WebSocket/<>c__DisplayClass176_0::<startReceiving>b__1(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass176_0_U3CstartReceivingU3Eb__1_mA4F4CFC0B0A15C3E84AF593A9C0ABB84C2D9AE50 (void);
// 0x000000D7 System.Void WebSocketSharp.WebSocket/<>c__DisplayClass179_0::.ctor()
extern void U3CU3Ec__DisplayClass179_0__ctor_m55D9990B23F8828EB72CB398491DCA20C7EBEE62 (void);
// 0x000000D8 System.Boolean WebSocketSharp.WebSocket/<>c__DisplayClass179_0::<validateSecWebSocketExtensionsServerHeader>b__0(System.String)
extern void U3CU3Ec__DisplayClass179_0_U3CvalidateSecWebSocketExtensionsServerHeaderU3Eb__0_m956CE07189AC34D6261A3A4D70B86E60A0F31BB5 (void);
// 0x000000D9 System.Void WebSocketSharp.WebSocket/<>c__DisplayClass182_0::.ctor()
extern void U3CU3Ec__DisplayClass182_0__ctor_m7861E4F74431DDF555C8CB2ECF075B9CC09473CF (void);
// 0x000000DA System.Boolean WebSocketSharp.WebSocket/<>c__DisplayClass182_0::<validateSecWebSocketProtocolServerHeader>b__0(System.String)
extern void U3CU3Ec__DisplayClass182_0_U3CvalidateSecWebSocketProtocolServerHeaderU3Eb__0_mC0C9397A11B72D9077A8A518EF2733CA505F3906 (void);
// 0x000000DB System.Void WebSocketSharp.WebSocketException::.ctor(System.String)
extern void WebSocketException__ctor_m4F8C44D0ED6DC238B95A38ABC4F62555B82A63CB (void);
// 0x000000DC System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode)
extern void WebSocketException__ctor_m37104D159DA9FEE7697CF1E5ECAF805A3FE820EC (void);
// 0x000000DD System.Void WebSocketSharp.WebSocketException::.ctor(System.String,System.Exception)
extern void WebSocketException__ctor_m80571970407102805C654203762A5B31F2DED4A7 (void);
// 0x000000DE System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode,System.Exception)
extern void WebSocketException__ctor_mC63F8BAE7E2A2C273CD212592D6AB89CCA08F27D (void);
// 0x000000DF System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode,System.String)
extern void WebSocketException__ctor_m70A4596E77190F38BF2B735E2DCAD7EDD8121804 (void);
// 0x000000E0 System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode,System.String,System.Exception)
extern void WebSocketException__ctor_m7F957748B4B8EFCFE830977D00F3736998F968B3 (void);
// 0x000000E1 WebSocketSharp.CloseStatusCode WebSocketSharp.WebSocketException::get_Code()
extern void WebSocketException_get_Code_m46B3849A5E32C752095251E7874010B9A5BFE81F (void);
// 0x000000E2 System.Void WebSocketSharp.WebSocketFrame::.cctor()
extern void WebSocketFrame__cctor_mB6D71665F14DFA4F06CB436BCB71D0CCFF52BEF1 (void);
// 0x000000E3 System.Void WebSocketSharp.WebSocketFrame::.ctor()
extern void WebSocketFrame__ctor_mE209F195A26C50E298EFB4A8764815DCE9CF63B2 (void);
// 0x000000E4 System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Fin,WebSocketSharp.Opcode,System.Byte[],System.Boolean,System.Boolean)
extern void WebSocketFrame__ctor_m6E9B8868182548333E3A0133ED67D3BD335DC7B6 (void);
// 0x000000E5 System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Fin,WebSocketSharp.Opcode,WebSocketSharp.PayloadData,System.Boolean,System.Boolean)
extern void WebSocketFrame__ctor_mA84A65CA4022F3073A21FEE99E9A6F1AC023B1AC (void);
// 0x000000E6 System.Int32 WebSocketSharp.WebSocketFrame::get_ExtendedPayloadLengthCount()
extern void WebSocketFrame_get_ExtendedPayloadLengthCount_m1ACF6915EC1017DD8F8B98D4DCC936ADA43623C5 (void);
// 0x000000E7 System.UInt64 WebSocketSharp.WebSocketFrame::get_FullPayloadLength()
extern void WebSocketFrame_get_FullPayloadLength_m2DAD5F51ED33EF716E9702A40B9AF4444E748F47 (void);
// 0x000000E8 System.Boolean WebSocketSharp.WebSocketFrame::get_IsClose()
extern void WebSocketFrame_get_IsClose_m7F7DBE4FBC1D2BF69A9AB43023D0D6467D999B55 (void);
// 0x000000E9 System.Boolean WebSocketSharp.WebSocketFrame::get_IsCompressed()
extern void WebSocketFrame_get_IsCompressed_m8B9D766DD81ADB4B836C3E062DE4FE9E26B21192 (void);
// 0x000000EA System.Boolean WebSocketSharp.WebSocketFrame::get_IsContinuation()
extern void WebSocketFrame_get_IsContinuation_m206BB01BAC8521C8722318558E984A569CC57BF4 (void);
// 0x000000EB System.Boolean WebSocketSharp.WebSocketFrame::get_IsData()
extern void WebSocketFrame_get_IsData_mA98924C3282582F2E40E57A9162DA1A5257A4CE8 (void);
// 0x000000EC System.Boolean WebSocketSharp.WebSocketFrame::get_IsFinal()
extern void WebSocketFrame_get_IsFinal_mAA4AD52428C2A68F461134146697359B03996FA3 (void);
// 0x000000ED System.Boolean WebSocketSharp.WebSocketFrame::get_IsFragment()
extern void WebSocketFrame_get_IsFragment_mE3455E58FE4F83726C50D55C8EDB1A275BD46911 (void);
// 0x000000EE System.Boolean WebSocketSharp.WebSocketFrame::get_IsMasked()
extern void WebSocketFrame_get_IsMasked_m9E3573456D57EDD5328551A4CBDB3483FFB8A996 (void);
// 0x000000EF System.Boolean WebSocketSharp.WebSocketFrame::get_IsPing()
extern void WebSocketFrame_get_IsPing_m499420E36209571E635E8FCEC18160415D1E5F32 (void);
// 0x000000F0 System.Boolean WebSocketSharp.WebSocketFrame::get_IsPong()
extern void WebSocketFrame_get_IsPong_m616E7D2C0E77D904A773B58E8EF04795B21A51C5 (void);
// 0x000000F1 System.Boolean WebSocketSharp.WebSocketFrame::get_IsText()
extern void WebSocketFrame_get_IsText_m25E0B1A7D499D821FB9971833D4D7864FA9247CE (void);
// 0x000000F2 System.UInt64 WebSocketSharp.WebSocketFrame::get_Length()
extern void WebSocketFrame_get_Length_m5723A0CEA5186A0B2EB2D566802E1E35F98AE5B8 (void);
// 0x000000F3 WebSocketSharp.Opcode WebSocketSharp.WebSocketFrame::get_Opcode()
extern void WebSocketFrame_get_Opcode_m3707296DE36CE71A0199AD5435F9EB2E8015D755 (void);
// 0x000000F4 WebSocketSharp.PayloadData WebSocketSharp.WebSocketFrame::get_PayloadData()
extern void WebSocketFrame_get_PayloadData_mC6233DB9BB3F2A3187E24F1AD2BF341537801F28 (void);
// 0x000000F5 WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv2()
extern void WebSocketFrame_get_Rsv2_m6E8CA8B8AD9A78B4D2A123761B9E5E7C573EE0DC (void);
// 0x000000F6 WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv3()
extern void WebSocketFrame_get_Rsv3_mC64EDD0EBB6EE850F6905AC20A48189518CE352C (void);
// 0x000000F7 System.Byte[] WebSocketSharp.WebSocketFrame::createMaskingKey()
extern void WebSocketFrame_createMaskingKey_mB940C8D6FCAE9757C996FE0301EE29DED0B777DF (void);
// 0x000000F8 System.String WebSocketSharp.WebSocketFrame::dump(WebSocketSharp.WebSocketFrame)
extern void WebSocketFrame_dump_m2F236F320597ADCC03D24DC5B45AD13DC79CDF88 (void);
// 0x000000F9 System.String WebSocketSharp.WebSocketFrame::print(WebSocketSharp.WebSocketFrame)
extern void WebSocketFrame_print_m3379755B719537AEF2AB74CE05649D4DFB0096E4 (void);
// 0x000000FA WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::processHeader(System.Byte[])
extern void WebSocketFrame_processHeader_m6EA4C248356902DC7D4E865478D78EF456B5EAAF (void);
// 0x000000FB System.Void WebSocketSharp.WebSocketFrame::readExtendedPayloadLengthAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readExtendedPayloadLengthAsync_mBB0342CEBDE6CB01686C93CC56F92AAC0EB4676D (void);
// 0x000000FC System.Void WebSocketSharp.WebSocketFrame::readHeaderAsync(System.IO.Stream,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readHeaderAsync_mC12797D8F0B59E0245880478AD79192351512BAE (void);
// 0x000000FD System.Void WebSocketSharp.WebSocketFrame::readMaskingKeyAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readMaskingKeyAsync_m55556B927ED46A8B6DBF4120FF246FEC9F899EEC (void);
// 0x000000FE System.Void WebSocketSharp.WebSocketFrame::readPayloadDataAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readPayloadDataAsync_m46FD6BD9FBF1AE3F99692D6372FA29D2A7A56855 (void);
// 0x000000FF WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreateCloseFrame(WebSocketSharp.PayloadData,System.Boolean)
extern void WebSocketFrame_CreateCloseFrame_mA6E4B1350A7988A2AB79E1ACFD5D8B23304A958F (void);
// 0x00000100 WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePingFrame(System.Boolean)
extern void WebSocketFrame_CreatePingFrame_m3FB0CE5FF9D1CCCE2F7A8472EE4C088E6C52E58A (void);
// 0x00000101 WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePongFrame(WebSocketSharp.PayloadData,System.Boolean)
extern void WebSocketFrame_CreatePongFrame_m00D2B6F42FDD53B9351E8C50DF9EBF5AD96D565B (void);
// 0x00000102 System.Void WebSocketSharp.WebSocketFrame::ReadFrameAsync(System.IO.Stream,System.Boolean,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_ReadFrameAsync_mA06C978446F5D203FF1AE5C2C962E3CEE0896936 (void);
// 0x00000103 System.Void WebSocketSharp.WebSocketFrame::Unmask()
extern void WebSocketFrame_Unmask_m0EA4C16389BA326A4142C7E17F9BD7085FAC687D (void);
// 0x00000104 System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharp.WebSocketFrame::GetEnumerator()
extern void WebSocketFrame_GetEnumerator_m9D52C4ADD5619EEC50A9D022597512CFA217B09D (void);
// 0x00000105 System.String WebSocketSharp.WebSocketFrame::PrintToString(System.Boolean)
extern void WebSocketFrame_PrintToString_m5F1FCCBAB22F279510375C499ED9EE8C1EFFBC53 (void);
// 0x00000106 System.Byte[] WebSocketSharp.WebSocketFrame::ToArray()
extern void WebSocketFrame_ToArray_m2AE8E02852C05805AEE803976DE8925BD0200216 (void);
// 0x00000107 System.String WebSocketSharp.WebSocketFrame::ToString()
extern void WebSocketFrame_ToString_mAE49EC095A893DF67E98500D155483A5B7555D7F (void);
// 0x00000108 System.Collections.IEnumerator WebSocketSharp.WebSocketFrame::System.Collections.IEnumerable.GetEnumerator()
extern void WebSocketFrame_System_Collections_IEnumerable_GetEnumerator_mF06569C6B3356E840353E6D0A642287012C3AE27 (void);
// 0x00000109 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass67_0::.ctor()
extern void U3CU3Ec__DisplayClass67_0__ctor_mE00BB4C75CFC4B069EB56EC123723D08BDFE0109 (void);
// 0x0000010A System.Action`4<System.String,System.String,System.String,System.String> WebSocketSharp.WebSocketFrame/<>c__DisplayClass67_0::<dump>b__0()
extern void U3CU3Ec__DisplayClass67_0_U3CdumpU3Eb__0_m6C28C82464A5DCA553F618A7CF04A0D6284F2E8B (void);
// 0x0000010B System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass67_1::.ctor()
extern void U3CU3Ec__DisplayClass67_1__ctor_m6CE0C0F6A75EAD4B74FAB16EB64697D2B3B61960 (void);
// 0x0000010C System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass67_1::<dump>b__1(System.String,System.String,System.String,System.String)
extern void U3CU3Ec__DisplayClass67_1_U3CdumpU3Eb__1_mFAF2D2813869DA529C60C068F2454B276E99154B (void);
// 0x0000010D System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass71_0::.ctor()
extern void U3CU3Ec__DisplayClass71_0__ctor_mD3AA75D4732FC00F7D5B4EC2857B2834616A441E (void);
// 0x0000010E System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass71_0::<readExtendedPayloadLengthAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass71_0_U3CreadExtendedPayloadLengthAsyncU3Eb__0_m2A4D9D5BDCC2549492E68DB69AE431B742080B79 (void);
// 0x0000010F System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass73_0::.ctor()
extern void U3CU3Ec__DisplayClass73_0__ctor_m10DEB3715DC0E3041BF6917B38FDF2ABA5CCCBA1 (void);
// 0x00000110 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass73_0::<readHeaderAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass73_0_U3CreadHeaderAsyncU3Eb__0_m8ADEB89BC5F24E2BA37202E2F2C66663760C5F7F (void);
// 0x00000111 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass75_0::.ctor()
extern void U3CU3Ec__DisplayClass75_0__ctor_m722882ABF39E1C9C32A0AD8F955CE15B71585BF6 (void);
// 0x00000112 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass75_0::<readMaskingKeyAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass75_0_U3CreadMaskingKeyAsyncU3Eb__0_m1FA8526D4569F4CDBF68E22E49E976DFD6DEF74B (void);
// 0x00000113 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass77_0::.ctor()
extern void U3CU3Ec__DisplayClass77_0__ctor_m71633A74BC77F4805788F9B7EA2C503C2EA135FD (void);
// 0x00000114 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass77_0::<readPayloadDataAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass77_0_U3CreadPayloadDataAsyncU3Eb__0_mEF050408B66A26DF43426F5197C3D8755367F8CA (void);
// 0x00000115 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass83_0::.ctor()
extern void U3CU3Ec__DisplayClass83_0__ctor_m3FE3BF4B184324E2BD408DCA69536D055E015DCE (void);
// 0x00000116 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass83_0::<ReadFrameAsync>b__0(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass83_0_U3CReadFrameAsyncU3Eb__0_mBAB123EE861B5E23364AFF45A884317A61636DE7 (void);
// 0x00000117 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass83_0::<ReadFrameAsync>b__1(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass83_0_U3CReadFrameAsyncU3Eb__1_m70888344F2C292E44292A1CA350BC89432147EE2 (void);
// 0x00000118 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass83_0::<ReadFrameAsync>b__2(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass83_0_U3CReadFrameAsyncU3Eb__2_m7E2A49A965E3FE851420B1F90A7DB240E819B61C (void);
// 0x00000119 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass83_0::<ReadFrameAsync>b__3(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass83_0_U3CReadFrameAsyncU3Eb__3_mCB0E3FB6164F7E98AD566D2CF8868E4145C2B2B8 (void);
// 0x0000011A System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>d__85::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__85__ctor_mB81744CE82D2D830E6E3271167B58BBEEEA122B8 (void);
// 0x0000011B System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>d__85::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__85_System_IDisposable_Dispose_mCE929B1D1E5378276B9CEC19201A079022860EA0 (void);
// 0x0000011C System.Boolean WebSocketSharp.WebSocketFrame/<GetEnumerator>d__85::MoveNext()
extern void U3CGetEnumeratorU3Ed__85_MoveNext_mD7591509CEE1E5E2622D3D8F14F867519AC79F00 (void);
// 0x0000011D System.Byte WebSocketSharp.WebSocketFrame/<GetEnumerator>d__85::System.Collections.Generic.IEnumerator<System.Byte>.get_Current()
extern void U3CGetEnumeratorU3Ed__85_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mA058D4C46AC01F3ED57D8F130C576D2A0450B147 (void);
// 0x0000011E System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>d__85::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__85_System_Collections_IEnumerator_Reset_m88AFA5520518B5E282A8DD1F5AC7F8DE991EAA3D (void);
// 0x0000011F System.Object WebSocketSharp.WebSocketFrame/<GetEnumerator>d__85::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__85_System_Collections_IEnumerator_get_Current_mC46248ABC03D68B751A1165D6D1801CA942A28F1 (void);
// 0x00000120 System.Void WebSocketSharp.Net.AuthenticationBase::.ctor(WebSocketSharp.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection)
extern void AuthenticationBase__ctor_m719CF21F3BFF3F429E7025B7C2049ABA596D0C8B (void);
// 0x00000121 WebSocketSharp.Net.AuthenticationSchemes WebSocketSharp.Net.AuthenticationBase::get_Scheme()
extern void AuthenticationBase_get_Scheme_m26C6B857FB79FC8C2A74C9B52E804AF9D03CCC4D (void);
// 0x00000122 System.String WebSocketSharp.Net.AuthenticationBase::CreateNonceValue()
extern void AuthenticationBase_CreateNonceValue_m0473BFEC6705EB7EFA0F70FB4C8193BF17727365 (void);
// 0x00000123 System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.AuthenticationBase::ParseParameters(System.String)
extern void AuthenticationBase_ParseParameters_m2A84314878B8DBA4E1C35E0BEA353E1C78474D56 (void);
// 0x00000124 System.String WebSocketSharp.Net.AuthenticationBase::ToBasicString()
// 0x00000125 System.String WebSocketSharp.Net.AuthenticationBase::ToDigestString()
// 0x00000126 System.String WebSocketSharp.Net.AuthenticationBase::ToString()
extern void AuthenticationBase_ToString_m5689DDBCA3030BF5BA04D25A318F4D74522D56D1 (void);
// 0x00000127 System.Void WebSocketSharp.Net.AuthenticationChallenge::.ctor(WebSocketSharp.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection)
extern void AuthenticationChallenge__ctor_m07FAA064832B53A14F56160443981C820CA61D14 (void);
// 0x00000128 WebSocketSharp.Net.AuthenticationChallenge WebSocketSharp.Net.AuthenticationChallenge::Parse(System.String)
extern void AuthenticationChallenge_Parse_mE6BE547CF0858C03CCC274B002D657F388207107 (void);
// 0x00000129 System.String WebSocketSharp.Net.AuthenticationChallenge::ToBasicString()
extern void AuthenticationChallenge_ToBasicString_mACF66C3A24E93DD8DB7E3D86A7F3E66D56839038 (void);
// 0x0000012A System.String WebSocketSharp.Net.AuthenticationChallenge::ToDigestString()
extern void AuthenticationChallenge_ToDigestString_mBF05247679713904FDD81AB25530955D9C96071C (void);
// 0x0000012B System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.NetworkCredential)
extern void AuthenticationResponse__ctor_m672EC81772111AFA6E4B978CA97684ABBED3CFB0 (void);
// 0x0000012C System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.AuthenticationChallenge,WebSocketSharp.Net.NetworkCredential,System.UInt32)
extern void AuthenticationResponse__ctor_m869489970AE4E949A1660C51F5943831BB35BDC5 (void);
// 0x0000012D System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection,WebSocketSharp.Net.NetworkCredential,System.UInt32)
extern void AuthenticationResponse__ctor_m817F13011DD037624BAA38C3A28A9F64B4E66D2A (void);
// 0x0000012E System.UInt32 WebSocketSharp.Net.AuthenticationResponse::get_NonceCount()
extern void AuthenticationResponse_get_NonceCount_mC37856627D8AC0034866D7B5873504E3155151E0 (void);
// 0x0000012F System.String WebSocketSharp.Net.AuthenticationResponse::createA1(System.String,System.String,System.String)
extern void AuthenticationResponse_createA1_m930598A2558EE9057AE7E4148BEF6F44D9D14E9E (void);
// 0x00000130 System.String WebSocketSharp.Net.AuthenticationResponse::createA1(System.String,System.String,System.String,System.String,System.String)
extern void AuthenticationResponse_createA1_m0A12270C9B70954CD3BE27BD1CD24D7DD9C7790F (void);
// 0x00000131 System.String WebSocketSharp.Net.AuthenticationResponse::createA2(System.String,System.String)
extern void AuthenticationResponse_createA2_m2CF18A05359D77F2E5C26D15D490451520DF549B (void);
// 0x00000132 System.String WebSocketSharp.Net.AuthenticationResponse::createA2(System.String,System.String,System.String)
extern void AuthenticationResponse_createA2_m101DEB56DCD6A94A5F06F3390292726F4B5ADC27 (void);
// 0x00000133 System.String WebSocketSharp.Net.AuthenticationResponse::hash(System.String)
extern void AuthenticationResponse_hash_m5DA157091F6578CBDBF643C6736886500973C6FD (void);
// 0x00000134 System.Void WebSocketSharp.Net.AuthenticationResponse::initAsDigest()
extern void AuthenticationResponse_initAsDigest_m281CF0A2A62561E3D5336AFCEE676C803103FB7D (void);
// 0x00000135 System.String WebSocketSharp.Net.AuthenticationResponse::CreateRequestDigest(System.Collections.Specialized.NameValueCollection)
extern void AuthenticationResponse_CreateRequestDigest_m8072EFE98268C79EDCF8B7A84BBF94FDB0D90FF3 (void);
// 0x00000136 System.String WebSocketSharp.Net.AuthenticationResponse::ToBasicString()
extern void AuthenticationResponse_ToBasicString_m642B92065B362155EF35A93C8F0EB7678906F411 (void);
// 0x00000137 System.String WebSocketSharp.Net.AuthenticationResponse::ToDigestString()
extern void AuthenticationResponse_ToDigestString_m91A757B5671A3AD26450B53DBD481DA694FCEEB0 (void);
// 0x00000138 System.Void WebSocketSharp.Net.AuthenticationResponse/<>c::.cctor()
extern void U3CU3Ec__cctor_m5980C2748EA6FDEAEF14D1C9E4925FEA20C47D4F (void);
// 0x00000139 System.Void WebSocketSharp.Net.AuthenticationResponse/<>c::.ctor()
extern void U3CU3Ec__ctor_m1BED1BD78197F1961B86C40A23C67D503937476B (void);
// 0x0000013A System.Boolean WebSocketSharp.Net.AuthenticationResponse/<>c::<initAsDigest>b__24_0(System.String)
extern void U3CU3Ec_U3CinitAsDigestU3Eb__24_0_m152DA1BDDC55720E7B906D5F2AAF46ED55F8E2F6 (void);
// 0x0000013B System.Void WebSocketSharp.Net.ClientSslConfiguration::.ctor(System.String)
extern void ClientSslConfiguration__ctor_mCBF26DE444D5AD2FA4CBE5E81060582DE780C6AE (void);
// 0x0000013C System.Boolean WebSocketSharp.Net.ClientSslConfiguration::get_CheckCertificateRevocation()
extern void ClientSslConfiguration_get_CheckCertificateRevocation_m8ECD5FE571D91965243EB13B194A7F59894E7205 (void);
// 0x0000013D System.Security.Cryptography.X509Certificates.X509CertificateCollection WebSocketSharp.Net.ClientSslConfiguration::get_ClientCertificates()
extern void ClientSslConfiguration_get_ClientCertificates_m60D8F8C2A4AE8CFEE04C1EE887E5DB56F38B99C0 (void);
// 0x0000013E System.Net.Security.LocalCertificateSelectionCallback WebSocketSharp.Net.ClientSslConfiguration::get_ClientCertificateSelectionCallback()
extern void ClientSslConfiguration_get_ClientCertificateSelectionCallback_mC3E0B75D22C38C7E5AB279CD387D447E259CA643 (void);
// 0x0000013F System.Security.Authentication.SslProtocols WebSocketSharp.Net.ClientSslConfiguration::get_EnabledSslProtocols()
extern void ClientSslConfiguration_get_EnabledSslProtocols_m3035221E5891CAF90533EB9808EF6E548BAA6DA3 (void);
// 0x00000140 System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.Net.ClientSslConfiguration::get_ServerCertificateValidationCallback()
extern void ClientSslConfiguration_get_ServerCertificateValidationCallback_m82BDB7652E43B49B8F5844327651744522CE2C68 (void);
// 0x00000141 System.String WebSocketSharp.Net.ClientSslConfiguration::get_TargetHost()
extern void ClientSslConfiguration_get_TargetHost_m1C551257F6A289C9727180CF19AA9376AAA32481 (void);
// 0x00000142 System.Security.Cryptography.X509Certificates.X509Certificate WebSocketSharp.Net.ClientSslConfiguration::defaultSelectClientCertificate(System.Object,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[])
extern void ClientSslConfiguration_defaultSelectClientCertificate_m236368F92729EB43A653E24B55A735F3580F91FE (void);
// 0x00000143 System.Boolean WebSocketSharp.Net.ClientSslConfiguration::defaultValidateServerCertificate(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void ClientSslConfiguration_defaultValidateServerCertificate_m91920AAAE8CC6001430DC0727FD09292F4B25892 (void);
// 0x00000144 System.Void WebSocketSharp.Net.Cookie::.cctor()
extern void Cookie__cctor_m1ED7F429EA40CF260CBC3964497393D0C79FACB1 (void);
// 0x00000145 System.Void WebSocketSharp.Net.Cookie::.ctor()
extern void Cookie__ctor_m1A4A428AA9EC21F5CE57BA58FD167E420363ADDF (void);
// 0x00000146 System.Void WebSocketSharp.Net.Cookie::.ctor(System.String,System.String)
extern void Cookie__ctor_m4F2DA31492A07608B56F4066A7A15FA8D14D83C0 (void);
// 0x00000147 System.Void WebSocketSharp.Net.Cookie::set_ExactDomain(System.Boolean)
extern void Cookie_set_ExactDomain_mBDC214FF8F6EDE24455AE63CB7975DC36358B99F (void);
// 0x00000148 System.Void WebSocketSharp.Net.Cookie::set_Comment(System.String)
extern void Cookie_set_Comment_mEA8412196AD3F76BD12B8D315C85593A313AA9AB (void);
// 0x00000149 System.Void WebSocketSharp.Net.Cookie::set_CommentUri(System.Uri)
extern void Cookie_set_CommentUri_m9985739E7B2B898DBC6464F08311039E709B9B34 (void);
// 0x0000014A System.Void WebSocketSharp.Net.Cookie::set_Discard(System.Boolean)
extern void Cookie_set_Discard_m5910E5B4923BB88B011B2F02A16D14CAAD1CF56A (void);
// 0x0000014B System.String WebSocketSharp.Net.Cookie::get_Domain()
extern void Cookie_get_Domain_mD086A052BD15CE6D8F68F1F2477ED90327A87BA0 (void);
// 0x0000014C System.Void WebSocketSharp.Net.Cookie::set_Domain(System.String)
extern void Cookie_set_Domain_mBDBBF96CA4907CF1FE17F86E9B523118CF4D85E4 (void);
// 0x0000014D System.Boolean WebSocketSharp.Net.Cookie::get_Expired()
extern void Cookie_get_Expired_m05B70D70DFCA4169F62BAA8342A3833E4663E088 (void);
// 0x0000014E System.DateTime WebSocketSharp.Net.Cookie::get_Expires()
extern void Cookie_get_Expires_m1FC39B8F37FF57E8736E90CD196D4768786B0B3F (void);
// 0x0000014F System.Void WebSocketSharp.Net.Cookie::set_Expires(System.DateTime)
extern void Cookie_set_Expires_mB26AFCBD09F82EFF3ECFDC7F00C0BDB48BA42413 (void);
// 0x00000150 System.Void WebSocketSharp.Net.Cookie::set_HttpOnly(System.Boolean)
extern void Cookie_set_HttpOnly_m34EA7082993D5BFBFB6EED23906B3C05AF3FA325 (void);
// 0x00000151 System.String WebSocketSharp.Net.Cookie::get_Name()
extern void Cookie_get_Name_m26CDC1525FEDA4582056E7F9DDB1329A615377F5 (void);
// 0x00000152 System.Void WebSocketSharp.Net.Cookie::set_Name(System.String)
extern void Cookie_set_Name_m3F1720C9C2815DCCE9FEFA76E5021028AC95A4EC (void);
// 0x00000153 System.String WebSocketSharp.Net.Cookie::get_Path()
extern void Cookie_get_Path_m0B0081B0F45D35027D572A59D5BFD564D1E590E6 (void);
// 0x00000154 System.Void WebSocketSharp.Net.Cookie::set_Path(System.String)
extern void Cookie_set_Path_m7255F98171BBB11FC7BA978C4BF47D4C3CA9EF25 (void);
// 0x00000155 System.Void WebSocketSharp.Net.Cookie::set_Port(System.String)
extern void Cookie_set_Port_mE6F01D55B2C5FFA0AAAD4CBE6DDB44A98AC5E3AC (void);
// 0x00000156 System.Void WebSocketSharp.Net.Cookie::set_Secure(System.Boolean)
extern void Cookie_set_Secure_mB1ADA1037764FAC4D6118C836CEB1B4241DFECD3 (void);
// 0x00000157 System.String WebSocketSharp.Net.Cookie::get_Value()
extern void Cookie_get_Value_mD41B70CD874C713867836E2405CA0452BEE6E4B6 (void);
// 0x00000158 System.Void WebSocketSharp.Net.Cookie::set_Value(System.String)
extern void Cookie_set_Value_mB7BF66CBC56CC23725B2221D96309F3EB7587160 (void);
// 0x00000159 System.Int32 WebSocketSharp.Net.Cookie::get_Version()
extern void Cookie_get_Version_m2E8EB6E47B18C879A9FEB64ED40A4F7AC990B6C8 (void);
// 0x0000015A System.Void WebSocketSharp.Net.Cookie::set_Version(System.Int32)
extern void Cookie_set_Version_m833E55458B950054D119DB63612D5A2D264D8AFC (void);
// 0x0000015B System.Boolean WebSocketSharp.Net.Cookie::canSetName(System.String,System.String&)
extern void Cookie_canSetName_mBF6A452767AFDEEB274D5C2BEB4F1E9BD725DB89 (void);
// 0x0000015C System.Boolean WebSocketSharp.Net.Cookie::canSetValue(System.String,System.String&)
extern void Cookie_canSetValue_m1175576929984276131C29C45F6483BFF307DFBD (void);
// 0x0000015D System.Int32 WebSocketSharp.Net.Cookie::hash(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Cookie_hash_m9A6FBE2BBC3024545B03B14C1F1C925C5BD47635 (void);
// 0x0000015E System.Boolean WebSocketSharp.Net.Cookie::tryCreatePorts(System.String,System.Int32[]&,System.String&)
extern void Cookie_tryCreatePorts_mEA2441AAC5688489AEDC1CA968E1300610E5359F (void);
// 0x0000015F System.String WebSocketSharp.Net.Cookie::ToRequestString(System.Uri)
extern void Cookie_ToRequestString_m81ECADAD3DEF47AE5E4FDA7B3906D111DDF2D101 (void);
// 0x00000160 System.Boolean WebSocketSharp.Net.Cookie::Equals(System.Object)
extern void Cookie_Equals_m03D9CA4051EEE118E81C41BEFC27F94803050F12 (void);
// 0x00000161 System.Int32 WebSocketSharp.Net.Cookie::GetHashCode()
extern void Cookie_GetHashCode_mF28D50CEE926E77A1541859EDE028D4A8E42ECC2 (void);
// 0x00000162 System.String WebSocketSharp.Net.Cookie::ToString()
extern void Cookie_ToString_mF559BDD9263682D1D22813987E6249E38F8EC514 (void);
// 0x00000163 System.Void WebSocketSharp.Net.CookieCollection::.ctor()
extern void CookieCollection__ctor_m54C41243018D62688D7C0CADBBC5DA43750ACC02 (void);
// 0x00000164 System.Collections.Generic.IEnumerable`1<WebSocketSharp.Net.Cookie> WebSocketSharp.Net.CookieCollection::get_Sorted()
extern void CookieCollection_get_Sorted_m071DAFCE9CC929A22F344E6A47B99C5BAB7BABFC (void);
// 0x00000165 System.Int32 WebSocketSharp.Net.CookieCollection::get_Count()
extern void CookieCollection_get_Count_m2ACD93FB3ABD4E56726DA01971277F5667E2ABD6 (void);
// 0x00000166 System.Object WebSocketSharp.Net.CookieCollection::get_SyncRoot()
extern void CookieCollection_get_SyncRoot_mACAC8073C3A621504C6BF3C7498E707DA89306C1 (void);
// 0x00000167 System.Int32 WebSocketSharp.Net.CookieCollection::compareCookieWithinSorted(WebSocketSharp.Net.Cookie,WebSocketSharp.Net.Cookie)
extern void CookieCollection_compareCookieWithinSorted_m923F73F9A8DE6E3E91AD74B6D4FBDD69F617286A (void);
// 0x00000168 WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.CookieCollection::parseRequest(System.String)
extern void CookieCollection_parseRequest_m757769653163B49268F0667169DB0DF0176042E9 (void);
// 0x00000169 WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.CookieCollection::parseResponse(System.String)
extern void CookieCollection_parseResponse_m9B3863F46E2FCB99E33632F69A3E27A43DF88248 (void);
// 0x0000016A System.Int32 WebSocketSharp.Net.CookieCollection::searchCookie(WebSocketSharp.Net.Cookie)
extern void CookieCollection_searchCookie_m27A468721EAD0DDB9A92EACD8AF88B7A4F69424E (void);
// 0x0000016B System.String[] WebSocketSharp.Net.CookieCollection::splitCookieHeaderValue(System.String)
extern void CookieCollection_splitCookieHeaderValue_mCD642353A8FB66CDC0BEFE384AB3A762BAECC54A (void);
// 0x0000016C WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.CookieCollection::Parse(System.String,System.Boolean)
extern void CookieCollection_Parse_m4E8976C034E31989C92C1F17018F52DF01BB1E28 (void);
// 0x0000016D System.Void WebSocketSharp.Net.CookieCollection::SetOrRemove(WebSocketSharp.Net.Cookie)
extern void CookieCollection_SetOrRemove_m9B84ED23BABADA2098478C5CD74570FDEB370D91 (void);
// 0x0000016E System.Void WebSocketSharp.Net.CookieCollection::SetOrRemove(WebSocketSharp.Net.CookieCollection)
extern void CookieCollection_SetOrRemove_mA5E6FEDE87E5A8ECA670031ADF052321D94921E7 (void);
// 0x0000016F System.Void WebSocketSharp.Net.CookieCollection::Add(WebSocketSharp.Net.Cookie)
extern void CookieCollection_Add_m2C3DD1525813F20BF04541B38665A6EFE2FA5D28 (void);
// 0x00000170 System.Void WebSocketSharp.Net.CookieCollection::CopyTo(System.Array,System.Int32)
extern void CookieCollection_CopyTo_mCE61EC5F2D2F4953DC1A050B2FB75C60E66C7630 (void);
// 0x00000171 System.Collections.IEnumerator WebSocketSharp.Net.CookieCollection::GetEnumerator()
extern void CookieCollection_GetEnumerator_m22C09F58301BAC307E8C427E6E949BA4E39FF1DA (void);
// 0x00000172 System.Void WebSocketSharp.Net.CookieException::.ctor(System.String)
extern void CookieException__ctor_m01D86763ECD655F7EEE609A04ACCB4CC7F9A2DF0 (void);
// 0x00000173 System.Void WebSocketSharp.Net.CookieException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException__ctor_m305757AEC6FA4420F4687DC5EC0B38254E1D722D (void);
// 0x00000174 System.Void WebSocketSharp.Net.CookieException::.ctor()
extern void CookieException__ctor_m531ED4C19F012C2399E614EAD6D2F50352978706 (void);
// 0x00000175 System.Void WebSocketSharp.Net.CookieException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException_GetObjectData_m85443CA6F016E458870E9FD6FB01755211C54093 (void);
// 0x00000176 System.Void WebSocketSharp.Net.CookieException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException_System_Runtime_Serialization_ISerializable_GetObjectData_m337280E4451AB47F40D2986753935482F74A988A (void);
// 0x00000177 System.Void WebSocketSharp.Net.HttpHeaderInfo::.ctor(System.String,WebSocketSharp.Net.HttpHeaderType)
extern void HttpHeaderInfo__ctor_m281F160CF393469778DF205B99F12A36D93D7C23 (void);
// 0x00000178 System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsMultiValueInRequest()
extern void HttpHeaderInfo_get_IsMultiValueInRequest_mA69B870D55ED1FA381163B5323D4B74B17DF8AB3 (void);
// 0x00000179 System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsMultiValueInResponse()
extern void HttpHeaderInfo_get_IsMultiValueInResponse_mF20B7AC4DDD5B994AEBFF800D937AC1488707D04 (void);
// 0x0000017A System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsRequest()
extern void HttpHeaderInfo_get_IsRequest_m030F743EBE63A850212C177D4A54303C24B0CD0F (void);
// 0x0000017B System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsResponse()
extern void HttpHeaderInfo_get_IsResponse_m54DA601AA4A3AFDFEEAE177F53C8AB29CFD31AFC (void);
// 0x0000017C System.String WebSocketSharp.Net.HttpHeaderInfo::get_Name()
extern void HttpHeaderInfo_get_Name_mE7BBDFE2CA445921165E316011BC4C061F16789E (void);
// 0x0000017D System.Boolean WebSocketSharp.Net.HttpHeaderInfo::IsMultiValue(System.Boolean)
extern void HttpHeaderInfo_IsMultiValue_m4093D0DB9B9416C938652D48FE3F2EB8CEBF4F33 (void);
// 0x0000017E System.Boolean WebSocketSharp.Net.HttpHeaderInfo::IsRestricted(System.Boolean)
extern void HttpHeaderInfo_IsRestricted_m0C0708DFCA70E14A25CC9C599C94B753A409540C (void);
// 0x0000017F System.Int32 WebSocketSharp.Net.HttpUtility::getChar(System.String,System.Int32,System.Int32)
extern void HttpUtility_getChar_m5389D4953093A1CB95AC68F2CCFE448122A33EB5 (void);
// 0x00000180 System.Int32 WebSocketSharp.Net.HttpUtility::getInt(System.Byte)
extern void HttpUtility_getInt_m1DC3CA2F12A9D3691233B30E523CAA7835C2C7FA (void);
// 0x00000181 System.Void WebSocketSharp.Net.HttpUtility::writeCharBytes(System.Char,System.Collections.IList,System.Text.Encoding)
extern void HttpUtility_writeCharBytes_m31B40192FF9FC1A1D3025B341BF6F278BA3DEB82 (void);
// 0x00000182 System.Text.Encoding WebSocketSharp.Net.HttpUtility::GetEncoding(System.String)
extern void HttpUtility_GetEncoding_m2CCFD337B16C286FE96D825EB9278C1729DCBCD6 (void);
// 0x00000183 System.String WebSocketSharp.Net.HttpUtility::UrlDecode(System.String)
extern void HttpUtility_UrlDecode_m2CE9422EC86118AADFD271F37D16B10E544A5322 (void);
// 0x00000184 System.String WebSocketSharp.Net.HttpUtility::UrlDecode(System.String,System.Text.Encoding)
extern void HttpUtility_UrlDecode_m059F125D5096947C004415B3D2A7C06BC05D086B (void);
// 0x00000185 System.Void WebSocketSharp.Net.HttpUtility::.cctor()
extern void HttpUtility__cctor_mC4CE0809D18F4E44087560E4E62CF2AF2A263204 (void);
// 0x00000186 System.Void WebSocketSharp.Net.HttpVersion::.cctor()
extern void HttpVersion__cctor_mDFA7D3B3DABB6036002A7965092C012D45E4BA02 (void);
// 0x00000187 System.Void WebSocketSharp.Net.NetworkCredential::.cctor()
extern void NetworkCredential__cctor_m60EA31C6A3158B7ED95B5352FD519132B35BE787 (void);
// 0x00000188 System.String WebSocketSharp.Net.NetworkCredential::get_Domain()
extern void NetworkCredential_get_Domain_mABA9B27319E30196C9A90B3605B7020FC83E12C7 (void);
// 0x00000189 System.String WebSocketSharp.Net.NetworkCredential::get_Password()
extern void NetworkCredential_get_Password_m6140B2E28F58FBE6E56D0DA042DC62CAFE1CE0E5 (void);
// 0x0000018A System.String WebSocketSharp.Net.NetworkCredential::get_Username()
extern void NetworkCredential_get_Username_mCB3221FA0B0B5DCAB661439D479BB730691B86EE (void);
// 0x0000018B System.Void WebSocketSharp.Net.WebHeaderCollection::.cctor()
extern void WebHeaderCollection__cctor_mF98F246DC0339E465B8A668D8CB00C14CFCA8036 (void);
// 0x0000018C System.Void WebSocketSharp.Net.WebHeaderCollection::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection__ctor_m67BF55950F010C692FFE161BDCA6E24FBB618931 (void);
// 0x0000018D System.Void WebSocketSharp.Net.WebHeaderCollection::.ctor()
extern void WebHeaderCollection__ctor_m6FC350D811657D0C4745A6197D82D52B589C499F (void);
// 0x0000018E System.String[] WebSocketSharp.Net.WebHeaderCollection::get_AllKeys()
extern void WebHeaderCollection_get_AllKeys_m9EA04ADEE79DBA67A4D5D0BD38631720479D5AEA (void);
// 0x0000018F System.Int32 WebSocketSharp.Net.WebHeaderCollection::get_Count()
extern void WebHeaderCollection_get_Count_m642E490EA9B73C2437BB9E10C3995F7D66E67F29 (void);
// 0x00000190 System.Void WebSocketSharp.Net.WebHeaderCollection::add(System.String,System.String,System.Boolean)
extern void WebHeaderCollection_add_mEE83B961C22C63F59C26A0680AE661F95C2AB213 (void);
// 0x00000191 System.Void WebSocketSharp.Net.WebHeaderCollection::addWithoutCheckingName(System.String,System.String)
extern void WebHeaderCollection_addWithoutCheckingName_m11D6BDC48B12E3F4E8FDAD452CD478205A5D5AC2 (void);
// 0x00000192 System.Void WebSocketSharp.Net.WebHeaderCollection::addWithoutCheckingNameAndRestricted(System.String,System.String)
extern void WebHeaderCollection_addWithoutCheckingNameAndRestricted_mF8233AE8E7A9BC7297992835CD4253996AA3CB3D (void);
// 0x00000193 System.Int32 WebSocketSharp.Net.WebHeaderCollection::checkColonSeparated(System.String)
extern void WebHeaderCollection_checkColonSeparated_m42FFF81C337EE815DDD91FD897B7D6E09BC5531C (void);
// 0x00000194 WebSocketSharp.Net.HttpHeaderType WebSocketSharp.Net.WebHeaderCollection::checkHeaderType(System.String)
extern void WebHeaderCollection_checkHeaderType_m0F9B78FD24EFDC8A609E1A1DF4264E0E2F838395 (void);
// 0x00000195 System.String WebSocketSharp.Net.WebHeaderCollection::checkName(System.String)
extern void WebHeaderCollection_checkName_m577CC8D9A41FC3CBAB7EB122A4A0E72F5B92B51A (void);
// 0x00000196 System.Void WebSocketSharp.Net.WebHeaderCollection::checkRestricted(System.String)
extern void WebHeaderCollection_checkRestricted_mC7134DB53200F1CD60CAB9204A50F3BA8F068535 (void);
// 0x00000197 System.Void WebSocketSharp.Net.WebHeaderCollection::checkState(System.Boolean)
extern void WebHeaderCollection_checkState_m8C5FB8A4481804E040E7DA7606859102824DD916 (void);
// 0x00000198 System.String WebSocketSharp.Net.WebHeaderCollection::checkValue(System.String)
extern void WebHeaderCollection_checkValue_m75012EA09FD616151FD055730AF41417719ED6D3 (void);
// 0x00000199 System.Void WebSocketSharp.Net.WebHeaderCollection::doWithCheckingState(System.Action`2<System.String,System.String>,System.String,System.String,System.Boolean)
extern void WebHeaderCollection_doWithCheckingState_mC1F8EB9DCC0BF6B138E3BBA0B5D089AD1D8943A5 (void);
// 0x0000019A System.Void WebSocketSharp.Net.WebHeaderCollection::doWithCheckingState(System.Action`2<System.String,System.String>,System.String,System.String,System.Boolean,System.Boolean)
extern void WebHeaderCollection_doWithCheckingState_mB22F01202D9C55DF7BCD2362A9B77DB556B61876 (void);
// 0x0000019B System.Void WebSocketSharp.Net.WebHeaderCollection::doWithoutCheckingName(System.Action`2<System.String,System.String>,System.String,System.String)
extern void WebHeaderCollection_doWithoutCheckingName_mF2CA5BFC7D0598F2802B30AAE8CC9157392B87C5 (void);
// 0x0000019C WebSocketSharp.Net.HttpHeaderInfo WebSocketSharp.Net.WebHeaderCollection::getHeaderInfo(System.String)
extern void WebHeaderCollection_getHeaderInfo_m5AFB4EDBEF6BA047A5384C0B1A89FA16C67750DD (void);
// 0x0000019D System.Boolean WebSocketSharp.Net.WebHeaderCollection::isRestricted(System.String,System.Boolean)
extern void WebHeaderCollection_isRestricted_mCE08BE9CE40B1C72CB02134FE5C7A3BACDD1A0F5 (void);
// 0x0000019E System.Void WebSocketSharp.Net.WebHeaderCollection::setWithoutCheckingName(System.String,System.String)
extern void WebHeaderCollection_setWithoutCheckingName_mDFF9B915832FB9923D64207DC621FA3CEA55A3B5 (void);
// 0x0000019F System.Void WebSocketSharp.Net.WebHeaderCollection::InternalSet(System.String,System.Boolean)
extern void WebHeaderCollection_InternalSet_m684592CCF23E05E13CCD792AE7B9AC61BEC0E0A1 (void);
// 0x000001A0 System.Void WebSocketSharp.Net.WebHeaderCollection::InternalSet(System.String,System.String,System.Boolean)
extern void WebHeaderCollection_InternalSet_m96EA3C273D0C1EE76BFEC690B9D8F7E5103B23BB (void);
// 0x000001A1 System.Boolean WebSocketSharp.Net.WebHeaderCollection::IsHeaderName(System.String)
extern void WebHeaderCollection_IsHeaderName_mA7C3CDC84B2CBAB462C925EE3DFB1342E98C1112 (void);
// 0x000001A2 System.Boolean WebSocketSharp.Net.WebHeaderCollection::IsHeaderValue(System.String)
extern void WebHeaderCollection_IsHeaderValue_mDDE6E6EBFE0CD0F634BCC650EC92798902D15668 (void);
// 0x000001A3 System.Boolean WebSocketSharp.Net.WebHeaderCollection::IsMultiValue(System.String,System.Boolean)
extern void WebHeaderCollection_IsMultiValue_m1F005BD3A897E0949BBCBF4436E89B1322F60CD9 (void);
// 0x000001A4 System.Void WebSocketSharp.Net.WebHeaderCollection::Add(System.String,System.String)
extern void WebHeaderCollection_Add_m37F68EDB6B2B64D2816BD6B27F85E3F1D614B69C (void);
// 0x000001A5 System.String WebSocketSharp.Net.WebHeaderCollection::Get(System.Int32)
extern void WebHeaderCollection_Get_m0943472FCD6EF1F9115F62694E28A6E9ABA8125A (void);
// 0x000001A6 System.String WebSocketSharp.Net.WebHeaderCollection::Get(System.String)
extern void WebHeaderCollection_Get_mA611FABEBAEF99224BD73593ACFB49F05297A13A (void);
// 0x000001A7 System.Collections.IEnumerator WebSocketSharp.Net.WebHeaderCollection::GetEnumerator()
extern void WebHeaderCollection_GetEnumerator_m66EBC0FD0E1AE790C9D757033708635FBA49D8ED (void);
// 0x000001A8 System.String WebSocketSharp.Net.WebHeaderCollection::GetKey(System.Int32)
extern void WebHeaderCollection_GetKey_mB29F5963D092F4639B1928E249F4AA224646EFDA (void);
// 0x000001A9 System.Void WebSocketSharp.Net.WebHeaderCollection::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection_GetObjectData_mBA6849A9370646667CFDA41F9EBF4817E299C3C7 (void);
// 0x000001AA System.Void WebSocketSharp.Net.WebHeaderCollection::OnDeserialization(System.Object)
extern void WebHeaderCollection_OnDeserialization_m8E0BC14512788D22F637388F1EC9C57DB6ED98B5 (void);
// 0x000001AB System.Void WebSocketSharp.Net.WebHeaderCollection::Set(System.String,System.String)
extern void WebHeaderCollection_Set_m6B784E717DFECE6E8917FFEC9A13306B3F4D94BD (void);
// 0x000001AC System.String WebSocketSharp.Net.WebHeaderCollection::ToString()
extern void WebHeaderCollection_ToString_mFDE4C43EE1C9A223B99761A3BF7782941AE8B07A (void);
// 0x000001AD System.Void WebSocketSharp.Net.WebHeaderCollection::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_mC4315258B3EFB94DFB7AC77963C8EF930BC8302A (void);
// 0x000001AE System.Void WebSocketSharp.Net.WebHeaderCollection/<>c__DisplayClass59_0::.ctor()
extern void U3CU3Ec__DisplayClass59_0__ctor_mC9D3705C68754C36CDF371645A46281B1DB3BE40 (void);
// 0x000001AF System.Void WebSocketSharp.Net.WebHeaderCollection/<>c__DisplayClass59_0::<GetObjectData>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass59_0_U3CGetObjectDataU3Eb__0_m29762E3C0845575A73F823664B4085129DD93C1B (void);
// 0x000001B0 System.Void WebSocketSharp.Net.WebHeaderCollection/<>c__DisplayClass70_0::.ctor()
extern void U3CU3Ec__DisplayClass70_0__ctor_m8580E1DD36158EA301972891D06AC3468CD111AC (void);
// 0x000001B1 System.Void WebSocketSharp.Net.WebHeaderCollection/<>c__DisplayClass70_0::<ToString>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass70_0_U3CToStringU3Eb__0_mA775B88B7A20B424EDAC37F083ED08B9241FA713 (void);
// 0x000001B2 System.Uri WebSocketSharp.Net.WebSockets.WebSocketContext::get_RequestUri()
static Il2CppMethodPointer s_methodPointers[434] = 
{
	CloseEventArgs__ctor_mDC321F70447E0467C047F72D4F53350562907F28,
	CloseEventArgs_set_WasClean_m4361EF1BC86CBDB6ABDE42E560324AC0054795AD,
	ErrorEventArgs__ctor_mE691F8047B9A0E5EA872F48B5FF369CC584B37B1,
	Ext_compress_mD4C2796D2FBC070550D2CD75DDA51C83E9BD2987,
	Ext_decompress_m427E057F05587ED24C6E248E6832A6D2E5112726,
	Ext_decompress_mABE502281B3E26E33B4C2637C9E7D24CDC52F967,
	Ext_decompressToArray_m1F9B4046E694DCB4D999DA8B47B6DC0ED5399523,
	Ext_Append_mF73F4C045B980F22589FA584EB26759B9C5CCA95,
	Ext_Compress_m4DE120E660E355838429CCBC9837D16427D916C9,
	NULL,
	Ext_ContainsTwice_mAB075799CC2EF175AD23C89F94B0EAD00E85C875,
	Ext_Decompress_m1ED72371D40BA922178A5130E1A0527D8F6FE557,
	Ext_DecompressToArray_mA52D8B0CA81216D2051623836AD21CDCC5E8C022,
	Ext_EqualsWith_m602C3F302D0F9AAE130D9014F970884E471109E7,
	Ext_GetAbsolutePath_m941CECC02205794D2954367E1359313F5E3E83C6,
	Ext_GetMessage_m509016B4BFA0E92FDB9084A58D2276CE738E342F,
	Ext_GetValue_m57696A3768779B421AF2926C38554B548B8433BA,
	Ext_GetValue_m0AEA039DB43D2305C9C4ADDAF663AD8BCCA158CD,
	Ext_InternalToByteArray_mF1C42D893EC79B4750C85D794AADCD4A41BB3E5C,
	Ext_InternalToByteArray_m5E229B80AEBD6E9EA6D7C93A763386C68F03209A,
	Ext_IsCompressionExtension_m28F55DD02E9B4FF10BD9B2BAB623B1B5AB46AE03,
	Ext_IsControl_mC3D6A201173127C93DD76E8FA6DF7C321E2CF73A,
	Ext_IsData_mAFFBD230031406EA01E187C28D84823FEC0D3A97,
	Ext_IsData_mE05AE2DBC078B9A47A8CAAEA9B4320585340557A,
	Ext_IsReserved_m47B52E0CA0FC872C775CCF9A9650B6BABAD98350,
	Ext_IsSupported_m7A0754B21CBA9C9697E5099A47BDB6C60BCA7472,
	Ext_IsText_mCFD72F9EEE8AD25B018A0E6373E7477369A879DB,
	Ext_IsToken_m97D7912B9DAE4BB8855044EA5A0684014CD79AA0,
	Ext_ReadBytes_m746C3CBBF792074ADF3F3C23CAB4BED5CDBD8B7A,
	Ext_ReadBytes_m0AE12D583213668EAA64B597F03AE1A61FB70D78,
	Ext_ReadBytesAsync_m16D801AC0B450703AB6A3F0D4EAEFD30B1156814,
	Ext_ReadBytesAsync_mADCDA9C10789215294D374FD43220DE6EB47F0B6,
	NULL,
	Ext_SplitHeaderValue_m214C4CDEF09FDCA6EC0A8F2964607820BE77A2EB,
	Ext_ToByteArray_mE2EF1061C8DAFCAB1F802BEC6EDE6AD39B7EC9A4,
	Ext_ToExtensionString_m4419B0E5FB8CAA7DE4947DA8FD366DD73FF2F118,
	Ext_ToUInt16_mEA0762C274353DC4130C43DF43078BBFDDD7BEBE,
	Ext_ToUInt64_m44C63692BA27C38B6EFEB3CE8861A3448BEA9C41,
	Ext_TryCreateWebSocketUri_m8B34AC7F5E019DD2BF9E2F491435B6D288493F1B,
	Ext_TryGetUTF8EncodedBytes_m57A1F100366F6B8808A4E67AAC39EBC07287F406,
	Ext_Unquote_m1360A078A746E1AA119C3F86618A3144A5360103,
	Ext_UTF8Decode_m3EB4CA65516EE6D3585EA310EEEDDBBE261430B4,
	Ext_UTF8Encode_m3F64D495F861D010952DBE3DC48C7D54DDBC5C71,
	Ext_WriteBytes_mA70DD162AF0633BD51AABBCEE4BF9A9833CF631E,
	Ext_Contains_mED3E63D3389C2DF33CD407F751C3EB06C9AC4D03,
	Ext_Contains_mDC75764AAB1DA8B2D81FB2522DE4E9AF267DF906,
	Ext_Contains_m881701E2ECE7D8C155C47197A9E9890CCCA318CB,
	Ext_Emit_m31FDD032A4BE024BBD0B74012EEB8424FF55F8DF,
	NULL,
	Ext_GetCookies_mC4F7262B4912A08960C4EBCF42672898E1CC40CE,
	Ext_IsEnclosedIn_m73DC5AD84C7CA776AE088D81F15E540CD1783649,
	Ext_IsHostOrder_m27DD21DC7DB6BBD6991D1E8E9C2DE77A18BC9324,
	Ext_IsNullOrEmpty_m654B58C78D471F8590E04A1391A16073EEF7D99A,
	Ext_IsPredefinedScheme_mAE6C3157BAF10BC07F334A7E6A996561E26C682E,
	Ext_MaybeUri_m862204170F3AD7AC5346239341C0EC6546CFBB5A,
	NULL,
	NULL,
	Ext_Times_m51922047D256B1F5AA71BEF7AC9FAF016D514BD1,
	Ext_ToHostOrder_m980E3470B526CC55E4BEBF850C7C7C851800FEE0,
	NULL,
	Ext_ToUri_m0375123E8A1876CA4EADB8744AD6DAA0902BEA1F,
	Ext_UrlDecode_mECBC25F781F7303754C50655582A1DD968028DB0,
	Ext__cctor_m18E1AA1A549C62FBCEB1E058DB12B53792465C8A,
	U3CU3Ec__DisplayClass17_0__ctor_mF0FAF9A2BE249E49400A40158D2F6C542E9151DB,
	U3CU3Ec__DisplayClass17_0_U3CContainsTwiceU3Eb__0_mB75C137661CE3DD30CD73F8C5BD64A2B2D7DF955,
	U3CU3Ec__DisplayClass48_0__ctor_m3240EBA2FC44B1CF2D82F99BEBF64B497AEDA021,
	U3CU3Ec__DisplayClass48_0_U3CReadBytesAsyncU3Eb__0_m8F47055EF367B407891F4D19B30ADF6832BBC151,
	U3CU3Ec__DisplayClass49_0__ctor_m06606A62C40C2667F40ED16FD9B785264F9D2236,
	U3CU3Ec__DisplayClass49_0_U3CReadBytesAsyncU3Eb__0_m743493B2C077508720674A8FB050F1789FA6848B,
	U3CU3Ec__DisplayClass49_1__ctor_mF41FED9907D163525810B5FC87DB594C6896227F,
	U3CU3Ec__DisplayClass49_1_U3CReadBytesAsyncU3Eb__1_mDB4AE547D2D5E45E53BC72DA100F350DBD94092C,
	U3CSplitHeaderValueU3Ed__52__ctor_mEEEF5C290CC8DAE912EE4213AA0124EFC70AA711,
	U3CSplitHeaderValueU3Ed__52_System_IDisposable_Dispose_m5568AFC7A25D4DFAF5D77935CF2BC0D4CF8534A8,
	U3CSplitHeaderValueU3Ed__52_MoveNext_mEB0076C482A64006D0ACDCA2D7A595DDFF1ACFB5,
	U3CSplitHeaderValueU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m28C4CE000A42289D2A12502FCBE95363C55B3D8E,
	U3CSplitHeaderValueU3Ed__52_System_Collections_IEnumerator_Reset_m3A8605E28BA677079F200B3F2F4446DBD6DCC330,
	U3CSplitHeaderValueU3Ed__52_System_Collections_IEnumerator_get_Current_m7A30EF478B56FFEDD2CC9DD22CAA2AD613EF00D1,
	U3CSplitHeaderValueU3Ed__52_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_mF15E97C6108FA75DAA8BE8CB081C885F020EC7FC,
	U3CSplitHeaderValueU3Ed__52_System_Collections_IEnumerable_GetEnumerator_mCACDDAEAC36B9140023F81001E76691A7624F61D,
	NULL,
	NULL,
	HttpBase__ctor_m6BDD2ED980EC8084B9094686433BB7941BE3BB36,
	HttpBase_get_EntityBody_m56BFF1A7DA71E8FA8750E8A701A82DD400B0E194,
	HttpBase_get_Headers_m24461D7C004DB2F04E7C5EC6B9A907AA2F836A70,
	HttpBase_get_ProtocolVersion_mF582A1C4D8FBA34837B3F5B094C8FCF542A88A2E,
	HttpBase_readEntityBody_mB991152177BFF56FE57E41EDFB670EAE57513EBE,
	HttpBase_readHeaders_m4FB768CF58F79E65D882ADC06D7A131920D7059F,
	NULL,
	HttpBase_ToByteArray_m0C0BC34C6B074EE47F6674A58069B30B75BAD423,
	U3CU3Ec__DisplayClass13_0__ctor_mD747743FF8C3ECA76CCD36DBEA62A461EEA2A4AF,
	U3CU3Ec__DisplayClass13_0_U3CreadHeadersU3Eb__0_m340D5883AAE76D4F1386D4F741747FBA448A9316,
	NULL,
	NULL,
	HttpRequest__ctor_m9D7958E1424351848E11A4E2041B0EBC78F82E3C,
	HttpRequest__ctor_m3EA5DB4C024E712057BDCE0D65AAE9B6FDFFE604,
	HttpRequest_CreateConnectRequest_m44EB2C0A38B77E7E24D78FB203246C51726F8B3E,
	HttpRequest_CreateWebSocketRequest_m483EDB8FCC75CC12AE33E73AF291B967778C9296,
	HttpRequest_GetResponse_m7F34D114B8A9644C8EBAE0FF12C817C400E6DE43,
	HttpRequest_SetCookies_mA0AFFEDF2E96A61215588CD9BC7EFC53D7F334F2,
	HttpRequest_ToString_m9660D7F7F8B52FAF155BA00B2B0776E1FA4A2FD9,
	HttpResponse__ctor_mD21EB7871945A19605382BB2729E7A974DDBC27A,
	HttpResponse_get_Cookies_m44E63E077EFE4AE09881484C9D9BE70FEAB27172,
	HttpResponse_get_HasConnectionClose_m513633428FB48F21748338AF26A8BEC95039D7CF,
	HttpResponse_get_IsProxyAuthenticationRequired_m5075CC07905D4DBF42C3CB057EB8FD4794839841,
	HttpResponse_get_IsRedirect_m9EC1FC46F378A7E4351B3C06BACC613913366BE8,
	HttpResponse_get_IsUnauthorized_mEF9EE7E22A77025DA1932059F6B6A417E20590BB,
	HttpResponse_get_IsWebSocketResponse_m1818EA031CE2C0409C19AFA175618977633E9745,
	HttpResponse_get_StatusCode_mE68937073584FEEEA2916174687747D9915897AD,
	HttpResponse_Parse_mA6D6C652C8B4916979640B7219A1746673309634,
	HttpResponse_ToString_m18847537B4222F77D5DEE509A289B0DDB1BCFDC2,
	LogData__ctor_mF8E1C53E4A1D3503D2D297E465ED9F1C491743F7,
	LogData_ToString_mF1E28365092C73BD30800104CC8741F0F28661F4,
	Logger__ctor_mCA34E16009AD89A47D17AE0E990697958DBB9162,
	Logger__ctor_m95D1A5E9F9039DB13CE2BA2C2FC319D9618DEBE3,
	Logger_defaultOutput_m8FF965E6D941AEEF705872667743418C4404500E,
	Logger_output_mE6682E3961EB0EB91B61EEE510A85CDA85C43E3D,
	Logger_writeToFile_m9F98007E158236794F49313D8C8C1C3829DF484E,
	Logger_Debug_mE12768ACB86A12ACEDB976A9438162F7D7310C9D,
	Logger_Error_m66745DDD974C80951E1CF20813099E74770B91B7,
	Logger_Fatal_mC1AA7BD00C477F0272C9066D7A864ADDF85A5A11,
	Logger_Info_mD332A7B1B39D26D825CD435275A4DAD2B2402036,
	Logger_Trace_m1CF54FD67E3B0D173D2055BB03C910DD0B723296,
	Logger_Warn_m0122EA54266E4E0C3143914DBF1E24EBBEB79B01,
	MessageEventArgs__ctor_mD8454957F0042F9420106AB2A94695B4B063CCDF,
	MessageEventArgs__ctor_m8237A1352361CCAEB53DAB653B9D5A4E6104CDF5,
	MessageEventArgs_get_Data_mD9D53F8250BB093F649A6279D1EB19D408901A52,
	MessageEventArgs_setData_m4C24C252B72BE15F3DA1D3A5F1D155B6E9836BC5,
	PayloadData__cctor_mB7644CD570FE157F463FCCFF3415C1C395CF5805,
	PayloadData__ctor_mF5D87D4121219352F25929034C788CED603D7D35,
	PayloadData__ctor_m88C22FADB90110886E2BA24C2F1FC51A0EED5669,
	PayloadData__ctor_m81EA7F817427C6A708309C6F5FC0E873527842D5,
	PayloadData__ctor_m3F3A9E5163178992AA8BDB7188133415C9C05C89,
	PayloadData_get_Code_m7E00D5AEA04FFC02A963AF98E4A9C6BB3CE0E41D,
	PayloadData_get_HasReservedCode_mC85FE15F2BD39856FF54260BC018F6EF162EBFB2,
	PayloadData_get_ApplicationData_mA92610537E1DAEDD250EAEAA6FB4F665AD04995E,
	PayloadData_get_Length_m6F53F71D7C887F3B9488504F4FF841A6071F7123,
	PayloadData_Mask_mF87463FB968657CA88936F16495F21FE6D6DA49C,
	PayloadData_GetEnumerator_m55F82176CFD8B96DEE1F16FA6C287A9DFE2AFC6B,
	PayloadData_ToArray_m167BA73C74B5037261484E0FA744238A90798EE8,
	PayloadData_ToString_mB8A2E015AA4EEA408BF622542272148262643427,
	PayloadData_System_Collections_IEnumerable_GetEnumerator_m11977F44DD5D507C2221979B1DF435568B2056EE,
	U3CGetEnumeratorU3Ed__30__ctor_m335EF37C1CFAE5315B2F71A5BD50284DA5FC5B51,
	U3CGetEnumeratorU3Ed__30_System_IDisposable_Dispose_m6F8626D33DFE696C5767F3F7070C9177362363F6,
	U3CGetEnumeratorU3Ed__30_MoveNext_m23A12FEF8820EF8F6A863E5F3B1305F4D7B88BCC,
	U3CGetEnumeratorU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mC12FF44109AF7A21477128A4D49F2AD34ADBF26D,
	U3CGetEnumeratorU3Ed__30_System_Collections_IEnumerator_Reset_m19951350A8D870881344C8AD1EE42C0A114D141D,
	U3CGetEnumeratorU3Ed__30_System_Collections_IEnumerator_get_Current_m40BB1BAEB424A447D2B4255058E140312277F51F,
	WebSocket__cctor_mA9D3ECD1DF4667D62001FBC74E00272DCB039B6D,
	WebSocket__ctor_m175FB52D7114C1BADE9B416A1A8FEA68DDB598D2,
	WebSocket_get_HasMessage_mF77D6A257D1D5DDE2F67190947551E32C7F18EB1,
	WebSocket_get_SslConfiguration_mB99EC9B00CF3D62366DD58767720F976EB1CAEC7,
	WebSocket_get_Url_mBC9629B05E6D0F63B87A76E8E079DFDD78DFC788,
	WebSocket_add_OnMessage_mF0B54AE34AAFCE31A08400721B0A5E090917A217,
	WebSocket_remove_OnMessage_m935642FBC23EC0857A2AA9DE7F71D4BE581D6C60,
	WebSocket_checkHandshakeResponse_m170E0888DED27765C357293FAC3B56FD6EE9A53F,
	WebSocket_checkIfAvailable_mF8F4AC5040D11ACB449D959669D73C02D442C43A,
	WebSocket_checkIfAvailable_m426F7C62840EA4143E2C8FAB09B509B7D116BD88,
	WebSocket_checkProtocols_mC185E421CB2FCD24D977AD47AAD926E47D63895F,
	WebSocket_checkReceivedFrame_m4AB3496468C150ADCDF4293CA7900BE3E86FB4CF,
	WebSocket_close_mD946E9F019F843AB6F170BA067F725267C2D7A26,
	WebSocket_close_m7FE622A1A45AC84AF8E625148EE0DD8A3DA30CD3,
	WebSocket_closeHandshake_m422725140F42BDB53F472CB90DF2A79AEF7F4C4D,
	WebSocket_connect_mF175F8BB4C098E5705E64A8A1E36DC349042B575,
	WebSocket_createExtensions_mE2F71B48DE46105E19F9258F54E1D6A81DF47F45,
	WebSocket_createHandshakeRequest_mE075DFD36540293F547E9A8E36498EB94C32316E,
	WebSocket_doHandshake_m4FAFC1594C76F86ADB4A5D33CA10F8688833D072,
	WebSocket_enqueueToMessageEventQueue_m9C7AD2244BAF1ED734B6597C39E79B0D7AB2462C,
	WebSocket_error_m06A1332197237509DC3B3D2EF205A56BAC4313AF,
	WebSocket_fatal_m97B2E2B7C8F7DE278EF374B568C1A62DE3895D46,
	WebSocket_fatal_m5BB9146E82E925264BDDBA02DA67D4FD1EFF6B2A,
	WebSocket_fatal_mF98585EFFB8AB899C2AF5459A22AA23F6797E03C,
	WebSocket_init_mFD12CBCE8D217388D44A27A7F234CF56DF1CC6B7,
	WebSocket_message_m31EEE103C6A3E5278CEBE338EE5DE7B6B2D55B52,
	WebSocket_messagec_mBC045F7784E3468CC4F9423E26643C6B313BED63,
	WebSocket_open_m4CB54D45FD3E493826FF901F0EEE6E78303096D7,
	WebSocket_processCloseFrame_m5A3991F8CB8017E064AC346EB064ACC19ACC6025,
	WebSocket_processCookies_m5F78334D163E93DD7175B3160F1B7EFC48B9DD54,
	WebSocket_processDataFrame_m5F66F8A0593FA2D4A8649E76AE58CC702DE7FFDA,
	WebSocket_processFragmentFrame_m74C560E6355A9D1707196E7E6D5DB3B291D1AFBA,
	WebSocket_processPingFrame_m7003688EBA6B3215B3C8932069A3AFDF2A61FEE5,
	WebSocket_processPongFrame_m070CB41EF461443887634D095A475CC7F367F56B,
	WebSocket_processReceivedFrame_mF83C54F25258F2561D4C06FBFBC04665F9DE43DC,
	WebSocket_processSecWebSocketExtensionsServerHeader_mB10BA3FF5AF0D50D328D8A09444BABE4A5F91696,
	WebSocket_processUnsupportedFrame_m9DCA729B9AE7ACBB60C91FEFF40A4B22ED2462E6,
	WebSocket_releaseClientResources_m51DC67496F684EDCE811B0F9D950184B0CBD67A9,
	WebSocket_releaseCommonResources_mB51780C83634F7617C529E03390C32A6B460E5BC,
	WebSocket_releaseResources_mFA3D35D608E8AC4D7B39D39351A92A7F560AE26E,
	WebSocket_releaseServerResources_m9B663B1EDA7676F497FD4AC87E27E5654BCDD63D,
	WebSocket_send_m27C189CB1E3A5EC38FDBA2168F1D24F6DF143ADC,
	WebSocket_send_mC77F9DCE16017C0265F61FD288D53CD4B718892E,
	WebSocket_send_m66F67DEC9942B6701D4E9B13BDBFC5B03042DD01,
	WebSocket_sendBytes_mCF575A0B62BFBE18DCD19B10E71B96D97EA49D7C,
	WebSocket_sendHandshakeRequest_m0E09CE106174C6F947943FEACE61AFB07C862964,
	WebSocket_sendHttpRequest_m50769E4EE9523AB957CE0038A47DCE3606ABCC53,
	WebSocket_sendProxyConnectRequest_mAB74241CF1009C925CB5FA2EDCCAF70BC233EE16,
	WebSocket_setClientStream_mF279DC34FB82585814FFDDD9D83EFE12E023640D,
	WebSocket_startReceiving_mB0FB7FF506E5C804E4B8E27ECA6FCB32091C36D9,
	WebSocket_validateSecWebSocketAcceptHeader_mAA6E9F3A8DE35B5A6E93E36657FC321A501C5091,
	WebSocket_validateSecWebSocketExtensionsServerHeader_m9884B0DCE956DE8261CAEE46914090B997588655,
	WebSocket_validateSecWebSocketProtocolServerHeader_m04C0FF21F3878BD399E04E2CA2CE615055225A33,
	WebSocket_validateSecWebSocketVersionServerHeader_mB29F9D54B7722A00B9A250E3BFA83B41C2B61E9F,
	WebSocket_CreateBase64Key_m4E4C5A9BFB2AAB8FFAB759F3798DA589825784E4,
	WebSocket_CreateResponseKey_m55035FB709DB40706532B2143096C403B460B2B6,
	WebSocket_Connect_m786CF51F671040752A008871832C29E4D9E416A5,
	WebSocket_Send_mB2A549377FA705334E19181213272F70DB8C0786,
	WebSocket_System_IDisposable_Dispose_m930A970B3B5AE1090F71D02362C5B5DC88D287D9,
	WebSocket_U3CopenU3Eb__149_0_m9BEF4D4159EDBA5F03CA42219E36E40ECFA2B702,
	WebSocket_U3CstartReceivingU3Eb__176_2_mCD6FC6C5B650FC5B3D855EE5D11C162718353A2E,
	U3CU3Ec__cctor_mCB8A692B08D8D4D4BB64C642689AE7DE82B03404,
	U3CU3Ec__ctor_m3CEED560674A4945BC8FDC2C14ABCC989AC85E18,
	U3CU3Ec_U3CcheckProtocolsU3Eb__124_0_m8F0DAEAF49E98730466C8E5167277B8F4272AF12,
	U3CU3Ec__DisplayClass176_0__ctor_mC0A30D69AAFCD5AA00C18C1195442779D79FE1D8,
	U3CU3Ec__DisplayClass176_0_U3CstartReceivingU3Eb__0_m2A7B03FB79CCE4E2FB47EB171ED7A9B2785EEE4E,
	U3CU3Ec__DisplayClass176_0_U3CstartReceivingU3Eb__1_mA4F4CFC0B0A15C3E84AF593A9C0ABB84C2D9AE50,
	U3CU3Ec__DisplayClass179_0__ctor_m55D9990B23F8828EB72CB398491DCA20C7EBEE62,
	U3CU3Ec__DisplayClass179_0_U3CvalidateSecWebSocketExtensionsServerHeaderU3Eb__0_m956CE07189AC34D6261A3A4D70B86E60A0F31BB5,
	U3CU3Ec__DisplayClass182_0__ctor_m7861E4F74431DDF555C8CB2ECF075B9CC09473CF,
	U3CU3Ec__DisplayClass182_0_U3CvalidateSecWebSocketProtocolServerHeaderU3Eb__0_mC0C9397A11B72D9077A8A518EF2733CA505F3906,
	WebSocketException__ctor_m4F8C44D0ED6DC238B95A38ABC4F62555B82A63CB,
	WebSocketException__ctor_m37104D159DA9FEE7697CF1E5ECAF805A3FE820EC,
	WebSocketException__ctor_m80571970407102805C654203762A5B31F2DED4A7,
	WebSocketException__ctor_mC63F8BAE7E2A2C273CD212592D6AB89CCA08F27D,
	WebSocketException__ctor_m70A4596E77190F38BF2B735E2DCAD7EDD8121804,
	WebSocketException__ctor_m7F957748B4B8EFCFE830977D00F3736998F968B3,
	WebSocketException_get_Code_m46B3849A5E32C752095251E7874010B9A5BFE81F,
	WebSocketFrame__cctor_mB6D71665F14DFA4F06CB436BCB71D0CCFF52BEF1,
	WebSocketFrame__ctor_mE209F195A26C50E298EFB4A8764815DCE9CF63B2,
	WebSocketFrame__ctor_m6E9B8868182548333E3A0133ED67D3BD335DC7B6,
	WebSocketFrame__ctor_mA84A65CA4022F3073A21FEE99E9A6F1AC023B1AC,
	WebSocketFrame_get_ExtendedPayloadLengthCount_m1ACF6915EC1017DD8F8B98D4DCC936ADA43623C5,
	WebSocketFrame_get_FullPayloadLength_m2DAD5F51ED33EF716E9702A40B9AF4444E748F47,
	WebSocketFrame_get_IsClose_m7F7DBE4FBC1D2BF69A9AB43023D0D6467D999B55,
	WebSocketFrame_get_IsCompressed_m8B9D766DD81ADB4B836C3E062DE4FE9E26B21192,
	WebSocketFrame_get_IsContinuation_m206BB01BAC8521C8722318558E984A569CC57BF4,
	WebSocketFrame_get_IsData_mA98924C3282582F2E40E57A9162DA1A5257A4CE8,
	WebSocketFrame_get_IsFinal_mAA4AD52428C2A68F461134146697359B03996FA3,
	WebSocketFrame_get_IsFragment_mE3455E58FE4F83726C50D55C8EDB1A275BD46911,
	WebSocketFrame_get_IsMasked_m9E3573456D57EDD5328551A4CBDB3483FFB8A996,
	WebSocketFrame_get_IsPing_m499420E36209571E635E8FCEC18160415D1E5F32,
	WebSocketFrame_get_IsPong_m616E7D2C0E77D904A773B58E8EF04795B21A51C5,
	WebSocketFrame_get_IsText_m25E0B1A7D499D821FB9971833D4D7864FA9247CE,
	WebSocketFrame_get_Length_m5723A0CEA5186A0B2EB2D566802E1E35F98AE5B8,
	WebSocketFrame_get_Opcode_m3707296DE36CE71A0199AD5435F9EB2E8015D755,
	WebSocketFrame_get_PayloadData_mC6233DB9BB3F2A3187E24F1AD2BF341537801F28,
	WebSocketFrame_get_Rsv2_m6E8CA8B8AD9A78B4D2A123761B9E5E7C573EE0DC,
	WebSocketFrame_get_Rsv3_mC64EDD0EBB6EE850F6905AC20A48189518CE352C,
	WebSocketFrame_createMaskingKey_mB940C8D6FCAE9757C996FE0301EE29DED0B777DF,
	WebSocketFrame_dump_m2F236F320597ADCC03D24DC5B45AD13DC79CDF88,
	WebSocketFrame_print_m3379755B719537AEF2AB74CE05649D4DFB0096E4,
	WebSocketFrame_processHeader_m6EA4C248356902DC7D4E865478D78EF456B5EAAF,
	WebSocketFrame_readExtendedPayloadLengthAsync_mBB0342CEBDE6CB01686C93CC56F92AAC0EB4676D,
	WebSocketFrame_readHeaderAsync_mC12797D8F0B59E0245880478AD79192351512BAE,
	WebSocketFrame_readMaskingKeyAsync_m55556B927ED46A8B6DBF4120FF246FEC9F899EEC,
	WebSocketFrame_readPayloadDataAsync_m46FD6BD9FBF1AE3F99692D6372FA29D2A7A56855,
	WebSocketFrame_CreateCloseFrame_mA6E4B1350A7988A2AB79E1ACFD5D8B23304A958F,
	WebSocketFrame_CreatePingFrame_m3FB0CE5FF9D1CCCE2F7A8472EE4C088E6C52E58A,
	WebSocketFrame_CreatePongFrame_m00D2B6F42FDD53B9351E8C50DF9EBF5AD96D565B,
	WebSocketFrame_ReadFrameAsync_mA06C978446F5D203FF1AE5C2C962E3CEE0896936,
	WebSocketFrame_Unmask_m0EA4C16389BA326A4142C7E17F9BD7085FAC687D,
	WebSocketFrame_GetEnumerator_m9D52C4ADD5619EEC50A9D022597512CFA217B09D,
	WebSocketFrame_PrintToString_m5F1FCCBAB22F279510375C499ED9EE8C1EFFBC53,
	WebSocketFrame_ToArray_m2AE8E02852C05805AEE803976DE8925BD0200216,
	WebSocketFrame_ToString_mAE49EC095A893DF67E98500D155483A5B7555D7F,
	WebSocketFrame_System_Collections_IEnumerable_GetEnumerator_mF06569C6B3356E840353E6D0A642287012C3AE27,
	U3CU3Ec__DisplayClass67_0__ctor_mE00BB4C75CFC4B069EB56EC123723D08BDFE0109,
	U3CU3Ec__DisplayClass67_0_U3CdumpU3Eb__0_m6C28C82464A5DCA553F618A7CF04A0D6284F2E8B,
	U3CU3Ec__DisplayClass67_1__ctor_m6CE0C0F6A75EAD4B74FAB16EB64697D2B3B61960,
	U3CU3Ec__DisplayClass67_1_U3CdumpU3Eb__1_mFAF2D2813869DA529C60C068F2454B276E99154B,
	U3CU3Ec__DisplayClass71_0__ctor_mD3AA75D4732FC00F7D5B4EC2857B2834616A441E,
	U3CU3Ec__DisplayClass71_0_U3CreadExtendedPayloadLengthAsyncU3Eb__0_m2A4D9D5BDCC2549492E68DB69AE431B742080B79,
	U3CU3Ec__DisplayClass73_0__ctor_m10DEB3715DC0E3041BF6917B38FDF2ABA5CCCBA1,
	U3CU3Ec__DisplayClass73_0_U3CreadHeaderAsyncU3Eb__0_m8ADEB89BC5F24E2BA37202E2F2C66663760C5F7F,
	U3CU3Ec__DisplayClass75_0__ctor_m722882ABF39E1C9C32A0AD8F955CE15B71585BF6,
	U3CU3Ec__DisplayClass75_0_U3CreadMaskingKeyAsyncU3Eb__0_m1FA8526D4569F4CDBF68E22E49E976DFD6DEF74B,
	U3CU3Ec__DisplayClass77_0__ctor_m71633A74BC77F4805788F9B7EA2C503C2EA135FD,
	U3CU3Ec__DisplayClass77_0_U3CreadPayloadDataAsyncU3Eb__0_mEF050408B66A26DF43426F5197C3D8755367F8CA,
	U3CU3Ec__DisplayClass83_0__ctor_m3FE3BF4B184324E2BD408DCA69536D055E015DCE,
	U3CU3Ec__DisplayClass83_0_U3CReadFrameAsyncU3Eb__0_mBAB123EE861B5E23364AFF45A884317A61636DE7,
	U3CU3Ec__DisplayClass83_0_U3CReadFrameAsyncU3Eb__1_m70888344F2C292E44292A1CA350BC89432147EE2,
	U3CU3Ec__DisplayClass83_0_U3CReadFrameAsyncU3Eb__2_m7E2A49A965E3FE851420B1F90A7DB240E819B61C,
	U3CU3Ec__DisplayClass83_0_U3CReadFrameAsyncU3Eb__3_mCB0E3FB6164F7E98AD566D2CF8868E4145C2B2B8,
	U3CGetEnumeratorU3Ed__85__ctor_mB81744CE82D2D830E6E3271167B58BBEEEA122B8,
	U3CGetEnumeratorU3Ed__85_System_IDisposable_Dispose_mCE929B1D1E5378276B9CEC19201A079022860EA0,
	U3CGetEnumeratorU3Ed__85_MoveNext_mD7591509CEE1E5E2622D3D8F14F867519AC79F00,
	U3CGetEnumeratorU3Ed__85_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mA058D4C46AC01F3ED57D8F130C576D2A0450B147,
	U3CGetEnumeratorU3Ed__85_System_Collections_IEnumerator_Reset_m88AFA5520518B5E282A8DD1F5AC7F8DE991EAA3D,
	U3CGetEnumeratorU3Ed__85_System_Collections_IEnumerator_get_Current_mC46248ABC03D68B751A1165D6D1801CA942A28F1,
	AuthenticationBase__ctor_m719CF21F3BFF3F429E7025B7C2049ABA596D0C8B,
	AuthenticationBase_get_Scheme_m26C6B857FB79FC8C2A74C9B52E804AF9D03CCC4D,
	AuthenticationBase_CreateNonceValue_m0473BFEC6705EB7EFA0F70FB4C8193BF17727365,
	AuthenticationBase_ParseParameters_m2A84314878B8DBA4E1C35E0BEA353E1C78474D56,
	NULL,
	NULL,
	AuthenticationBase_ToString_m5689DDBCA3030BF5BA04D25A318F4D74522D56D1,
	AuthenticationChallenge__ctor_m07FAA064832B53A14F56160443981C820CA61D14,
	AuthenticationChallenge_Parse_mE6BE547CF0858C03CCC274B002D657F388207107,
	AuthenticationChallenge_ToBasicString_mACF66C3A24E93DD8DB7E3D86A7F3E66D56839038,
	AuthenticationChallenge_ToDigestString_mBF05247679713904FDD81AB25530955D9C96071C,
	AuthenticationResponse__ctor_m672EC81772111AFA6E4B978CA97684ABBED3CFB0,
	AuthenticationResponse__ctor_m869489970AE4E949A1660C51F5943831BB35BDC5,
	AuthenticationResponse__ctor_m817F13011DD037624BAA38C3A28A9F64B4E66D2A,
	AuthenticationResponse_get_NonceCount_mC37856627D8AC0034866D7B5873504E3155151E0,
	AuthenticationResponse_createA1_m930598A2558EE9057AE7E4148BEF6F44D9D14E9E,
	AuthenticationResponse_createA1_m0A12270C9B70954CD3BE27BD1CD24D7DD9C7790F,
	AuthenticationResponse_createA2_m2CF18A05359D77F2E5C26D15D490451520DF549B,
	AuthenticationResponse_createA2_m101DEB56DCD6A94A5F06F3390292726F4B5ADC27,
	AuthenticationResponse_hash_m5DA157091F6578CBDBF643C6736886500973C6FD,
	AuthenticationResponse_initAsDigest_m281CF0A2A62561E3D5336AFCEE676C803103FB7D,
	AuthenticationResponse_CreateRequestDigest_m8072EFE98268C79EDCF8B7A84BBF94FDB0D90FF3,
	AuthenticationResponse_ToBasicString_m642B92065B362155EF35A93C8F0EB7678906F411,
	AuthenticationResponse_ToDigestString_m91A757B5671A3AD26450B53DBD481DA694FCEEB0,
	U3CU3Ec__cctor_m5980C2748EA6FDEAEF14D1C9E4925FEA20C47D4F,
	U3CU3Ec__ctor_m1BED1BD78197F1961B86C40A23C67D503937476B,
	U3CU3Ec_U3CinitAsDigestU3Eb__24_0_m152DA1BDDC55720E7B906D5F2AAF46ED55F8E2F6,
	ClientSslConfiguration__ctor_mCBF26DE444D5AD2FA4CBE5E81060582DE780C6AE,
	ClientSslConfiguration_get_CheckCertificateRevocation_m8ECD5FE571D91965243EB13B194A7F59894E7205,
	ClientSslConfiguration_get_ClientCertificates_m60D8F8C2A4AE8CFEE04C1EE887E5DB56F38B99C0,
	ClientSslConfiguration_get_ClientCertificateSelectionCallback_mC3E0B75D22C38C7E5AB279CD387D447E259CA643,
	ClientSslConfiguration_get_EnabledSslProtocols_m3035221E5891CAF90533EB9808EF6E548BAA6DA3,
	ClientSslConfiguration_get_ServerCertificateValidationCallback_m82BDB7652E43B49B8F5844327651744522CE2C68,
	ClientSslConfiguration_get_TargetHost_m1C551257F6A289C9727180CF19AA9376AAA32481,
	ClientSslConfiguration_defaultSelectClientCertificate_m236368F92729EB43A653E24B55A735F3580F91FE,
	ClientSslConfiguration_defaultValidateServerCertificate_m91920AAAE8CC6001430DC0727FD09292F4B25892,
	Cookie__cctor_m1ED7F429EA40CF260CBC3964497393D0C79FACB1,
	Cookie__ctor_m1A4A428AA9EC21F5CE57BA58FD167E420363ADDF,
	Cookie__ctor_m4F2DA31492A07608B56F4066A7A15FA8D14D83C0,
	Cookie_set_ExactDomain_mBDC214FF8F6EDE24455AE63CB7975DC36358B99F,
	Cookie_set_Comment_mEA8412196AD3F76BD12B8D315C85593A313AA9AB,
	Cookie_set_CommentUri_m9985739E7B2B898DBC6464F08311039E709B9B34,
	Cookie_set_Discard_m5910E5B4923BB88B011B2F02A16D14CAAD1CF56A,
	Cookie_get_Domain_mD086A052BD15CE6D8F68F1F2477ED90327A87BA0,
	Cookie_set_Domain_mBDBBF96CA4907CF1FE17F86E9B523118CF4D85E4,
	Cookie_get_Expired_m05B70D70DFCA4169F62BAA8342A3833E4663E088,
	Cookie_get_Expires_m1FC39B8F37FF57E8736E90CD196D4768786B0B3F,
	Cookie_set_Expires_mB26AFCBD09F82EFF3ECFDC7F00C0BDB48BA42413,
	Cookie_set_HttpOnly_m34EA7082993D5BFBFB6EED23906B3C05AF3FA325,
	Cookie_get_Name_m26CDC1525FEDA4582056E7F9DDB1329A615377F5,
	Cookie_set_Name_m3F1720C9C2815DCCE9FEFA76E5021028AC95A4EC,
	Cookie_get_Path_m0B0081B0F45D35027D572A59D5BFD564D1E590E6,
	Cookie_set_Path_m7255F98171BBB11FC7BA978C4BF47D4C3CA9EF25,
	Cookie_set_Port_mE6F01D55B2C5FFA0AAAD4CBE6DDB44A98AC5E3AC,
	Cookie_set_Secure_mB1ADA1037764FAC4D6118C836CEB1B4241DFECD3,
	Cookie_get_Value_mD41B70CD874C713867836E2405CA0452BEE6E4B6,
	Cookie_set_Value_mB7BF66CBC56CC23725B2221D96309F3EB7587160,
	Cookie_get_Version_m2E8EB6E47B18C879A9FEB64ED40A4F7AC990B6C8,
	Cookie_set_Version_m833E55458B950054D119DB63612D5A2D264D8AFC,
	Cookie_canSetName_mBF6A452767AFDEEB274D5C2BEB4F1E9BD725DB89,
	Cookie_canSetValue_m1175576929984276131C29C45F6483BFF307DFBD,
	Cookie_hash_m9A6FBE2BBC3024545B03B14C1F1C925C5BD47635,
	Cookie_tryCreatePorts_mEA2441AAC5688489AEDC1CA968E1300610E5359F,
	Cookie_ToRequestString_m81ECADAD3DEF47AE5E4FDA7B3906D111DDF2D101,
	Cookie_Equals_m03D9CA4051EEE118E81C41BEFC27F94803050F12,
	Cookie_GetHashCode_mF28D50CEE926E77A1541859EDE028D4A8E42ECC2,
	Cookie_ToString_mF559BDD9263682D1D22813987E6249E38F8EC514,
	CookieCollection__ctor_m54C41243018D62688D7C0CADBBC5DA43750ACC02,
	CookieCollection_get_Sorted_m071DAFCE9CC929A22F344E6A47B99C5BAB7BABFC,
	CookieCollection_get_Count_m2ACD93FB3ABD4E56726DA01971277F5667E2ABD6,
	CookieCollection_get_SyncRoot_mACAC8073C3A621504C6BF3C7498E707DA89306C1,
	CookieCollection_compareCookieWithinSorted_m923F73F9A8DE6E3E91AD74B6D4FBDD69F617286A,
	CookieCollection_parseRequest_m757769653163B49268F0667169DB0DF0176042E9,
	CookieCollection_parseResponse_m9B3863F46E2FCB99E33632F69A3E27A43DF88248,
	CookieCollection_searchCookie_m27A468721EAD0DDB9A92EACD8AF88B7A4F69424E,
	CookieCollection_splitCookieHeaderValue_mCD642353A8FB66CDC0BEFE384AB3A762BAECC54A,
	CookieCollection_Parse_m4E8976C034E31989C92C1F17018F52DF01BB1E28,
	CookieCollection_SetOrRemove_m9B84ED23BABADA2098478C5CD74570FDEB370D91,
	CookieCollection_SetOrRemove_mA5E6FEDE87E5A8ECA670031ADF052321D94921E7,
	CookieCollection_Add_m2C3DD1525813F20BF04541B38665A6EFE2FA5D28,
	CookieCollection_CopyTo_mCE61EC5F2D2F4953DC1A050B2FB75C60E66C7630,
	CookieCollection_GetEnumerator_m22C09F58301BAC307E8C427E6E949BA4E39FF1DA,
	CookieException__ctor_m01D86763ECD655F7EEE609A04ACCB4CC7F9A2DF0,
	CookieException__ctor_m305757AEC6FA4420F4687DC5EC0B38254E1D722D,
	CookieException__ctor_m531ED4C19F012C2399E614EAD6D2F50352978706,
	CookieException_GetObjectData_m85443CA6F016E458870E9FD6FB01755211C54093,
	CookieException_System_Runtime_Serialization_ISerializable_GetObjectData_m337280E4451AB47F40D2986753935482F74A988A,
	HttpHeaderInfo__ctor_m281F160CF393469778DF205B99F12A36D93D7C23,
	HttpHeaderInfo_get_IsMultiValueInRequest_mA69B870D55ED1FA381163B5323D4B74B17DF8AB3,
	HttpHeaderInfo_get_IsMultiValueInResponse_mF20B7AC4DDD5B994AEBFF800D937AC1488707D04,
	HttpHeaderInfo_get_IsRequest_m030F743EBE63A850212C177D4A54303C24B0CD0F,
	HttpHeaderInfo_get_IsResponse_m54DA601AA4A3AFDFEEAE177F53C8AB29CFD31AFC,
	HttpHeaderInfo_get_Name_mE7BBDFE2CA445921165E316011BC4C061F16789E,
	HttpHeaderInfo_IsMultiValue_m4093D0DB9B9416C938652D48FE3F2EB8CEBF4F33,
	HttpHeaderInfo_IsRestricted_m0C0708DFCA70E14A25CC9C599C94B753A409540C,
	HttpUtility_getChar_m5389D4953093A1CB95AC68F2CCFE448122A33EB5,
	HttpUtility_getInt_m1DC3CA2F12A9D3691233B30E523CAA7835C2C7FA,
	HttpUtility_writeCharBytes_m31B40192FF9FC1A1D3025B341BF6F278BA3DEB82,
	HttpUtility_GetEncoding_m2CCFD337B16C286FE96D825EB9278C1729DCBCD6,
	HttpUtility_UrlDecode_m2CE9422EC86118AADFD271F37D16B10E544A5322,
	HttpUtility_UrlDecode_m059F125D5096947C004415B3D2A7C06BC05D086B,
	HttpUtility__cctor_mC4CE0809D18F4E44087560E4E62CF2AF2A263204,
	HttpVersion__cctor_mDFA7D3B3DABB6036002A7965092C012D45E4BA02,
	NetworkCredential__cctor_m60EA31C6A3158B7ED95B5352FD519132B35BE787,
	NetworkCredential_get_Domain_mABA9B27319E30196C9A90B3605B7020FC83E12C7,
	NetworkCredential_get_Password_m6140B2E28F58FBE6E56D0DA042DC62CAFE1CE0E5,
	NetworkCredential_get_Username_mCB3221FA0B0B5DCAB661439D479BB730691B86EE,
	WebHeaderCollection__cctor_mF98F246DC0339E465B8A668D8CB00C14CFCA8036,
	WebHeaderCollection__ctor_m67BF55950F010C692FFE161BDCA6E24FBB618931,
	WebHeaderCollection__ctor_m6FC350D811657D0C4745A6197D82D52B589C499F,
	WebHeaderCollection_get_AllKeys_m9EA04ADEE79DBA67A4D5D0BD38631720479D5AEA,
	WebHeaderCollection_get_Count_m642E490EA9B73C2437BB9E10C3995F7D66E67F29,
	WebHeaderCollection_add_mEE83B961C22C63F59C26A0680AE661F95C2AB213,
	WebHeaderCollection_addWithoutCheckingName_m11D6BDC48B12E3F4E8FDAD452CD478205A5D5AC2,
	WebHeaderCollection_addWithoutCheckingNameAndRestricted_mF8233AE8E7A9BC7297992835CD4253996AA3CB3D,
	WebHeaderCollection_checkColonSeparated_m42FFF81C337EE815DDD91FD897B7D6E09BC5531C,
	WebHeaderCollection_checkHeaderType_m0F9B78FD24EFDC8A609E1A1DF4264E0E2F838395,
	WebHeaderCollection_checkName_m577CC8D9A41FC3CBAB7EB122A4A0E72F5B92B51A,
	WebHeaderCollection_checkRestricted_mC7134DB53200F1CD60CAB9204A50F3BA8F068535,
	WebHeaderCollection_checkState_m8C5FB8A4481804E040E7DA7606859102824DD916,
	WebHeaderCollection_checkValue_m75012EA09FD616151FD055730AF41417719ED6D3,
	WebHeaderCollection_doWithCheckingState_mC1F8EB9DCC0BF6B138E3BBA0B5D089AD1D8943A5,
	WebHeaderCollection_doWithCheckingState_mB22F01202D9C55DF7BCD2362A9B77DB556B61876,
	WebHeaderCollection_doWithoutCheckingName_mF2CA5BFC7D0598F2802B30AAE8CC9157392B87C5,
	WebHeaderCollection_getHeaderInfo_m5AFB4EDBEF6BA047A5384C0B1A89FA16C67750DD,
	WebHeaderCollection_isRestricted_mCE08BE9CE40B1C72CB02134FE5C7A3BACDD1A0F5,
	WebHeaderCollection_setWithoutCheckingName_mDFF9B915832FB9923D64207DC621FA3CEA55A3B5,
	WebHeaderCollection_InternalSet_m684592CCF23E05E13CCD792AE7B9AC61BEC0E0A1,
	WebHeaderCollection_InternalSet_m96EA3C273D0C1EE76BFEC690B9D8F7E5103B23BB,
	WebHeaderCollection_IsHeaderName_mA7C3CDC84B2CBAB462C925EE3DFB1342E98C1112,
	WebHeaderCollection_IsHeaderValue_mDDE6E6EBFE0CD0F634BCC650EC92798902D15668,
	WebHeaderCollection_IsMultiValue_m1F005BD3A897E0949BBCBF4436E89B1322F60CD9,
	WebHeaderCollection_Add_m37F68EDB6B2B64D2816BD6B27F85E3F1D614B69C,
	WebHeaderCollection_Get_m0943472FCD6EF1F9115F62694E28A6E9ABA8125A,
	WebHeaderCollection_Get_mA611FABEBAEF99224BD73593ACFB49F05297A13A,
	WebHeaderCollection_GetEnumerator_m66EBC0FD0E1AE790C9D757033708635FBA49D8ED,
	WebHeaderCollection_GetKey_mB29F5963D092F4639B1928E249F4AA224646EFDA,
	WebHeaderCollection_GetObjectData_mBA6849A9370646667CFDA41F9EBF4817E299C3C7,
	WebHeaderCollection_OnDeserialization_m8E0BC14512788D22F637388F1EC9C57DB6ED98B5,
	WebHeaderCollection_Set_m6B784E717DFECE6E8917FFEC9A13306B3F4D94BD,
	WebHeaderCollection_ToString_mFDE4C43EE1C9A223B99761A3BF7782941AE8B07A,
	WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_mC4315258B3EFB94DFB7AC77963C8EF930BC8302A,
	U3CU3Ec__DisplayClass59_0__ctor_mC9D3705C68754C36CDF371645A46281B1DB3BE40,
	U3CU3Ec__DisplayClass59_0_U3CGetObjectDataU3Eb__0_m29762E3C0845575A73F823664B4085129DD93C1B,
	U3CU3Ec__DisplayClass70_0__ctor_m8580E1DD36158EA301972891D06AC3468CD111AC,
	U3CU3Ec__DisplayClass70_0_U3CToStringU3Eb__0_mA775B88B7A20B424EDAC37F083ED08B9241FA713,
	NULL,
};
static const int32_t s_InvokerIndices[434] = 
{
	1187,
	1204,
	785,
	2160,
	2160,
	2160,
	2160,
	1936,
	1950,
	-1,
	2183,
	1950,
	1950,
	1811,
	2160,
	2156,
	1946,
	1780,
	1935,
	1941,
	2002,
	2184,
	2184,
	2184,
	2179,
	2184,
	2183,
	2183,
	1947,
	1784,
	1701,
	1560,
	-1,
	1949,
	2160,
	1958,
	1900,
	1923,
	1814,
	1998,
	2160,
	2160,
	2160,
	1865,
	2001,
	2001,
	1820,
	1867,
	-1,
	1950,
	1999,
	2180,
	2183,
	2183,
	2183,
	-1,
	-1,
	2053,
	1947,
	-1,
	2160,
	2160,
	2258,
	1379,
	1047,
	1379,
	1187,
	1379,
	1179,
	1379,
	1187,
	1178,
	1379,
	1366,
	1347,
	1379,
	1347,
	1347,
	1347,
	-1,
	-1,
	785,
	1347,
	1347,
	1347,
	1949,
	1947,
	-1,
	1347,
	1379,
	1178,
	-1,
	-1,
	357,
	785,
	2160,
	2160,
	611,
	1187,
	1347,
	357,
	1347,
	1366,
	1366,
	1366,
	1366,
	1366,
	1347,
	2160,
	1347,
	505,
	1347,
	1379,
	505,
	2063,
	782,
	2063,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	795,
	1347,
	1379,
	2258,
	1379,
	1187,
	783,
	697,
	1335,
	1366,
	1347,
	1337,
	1187,
	1347,
	1347,
	1347,
	1347,
	1178,
	1379,
	1366,
	1366,
	1379,
	1347,
	2258,
	785,
	1366,
	1347,
	1347,
	1187,
	1187,
	648,
	173,
	55,
	1998,
	648,
	697,
	366,
	311,
	1366,
	1347,
	1347,
	1379,
	1187,
	785,
	785,
	781,
	781,
	1379,
	1379,
	1187,
	1379,
	1058,
	1187,
	1058,
	1058,
	1058,
	1058,
	1058,
	1187,
	1058,
	1379,
	1379,
	1379,
	1379,
	661,
	483,
	313,
	1058,
	1347,
	611,
	1379,
	1379,
	1379,
	1058,
	1058,
	1058,
	1058,
	2244,
	2160,
	1379,
	1187,
	1379,
	1187,
	1187,
	2258,
	1379,
	1058,
	1379,
	1379,
	1187,
	1379,
	1058,
	1379,
	1058,
	1187,
	1177,
	785,
	697,
	697,
	496,
	1335,
	2258,
	1379,
	206,
	206,
	1336,
	1337,
	1366,
	1366,
	1366,
	1366,
	1366,
	1366,
	1366,
	1366,
	1366,
	1366,
	1337,
	1366,
	1347,
	1366,
	1366,
	2244,
	2160,
	2160,
	2160,
	1708,
	1867,
	1708,
	1708,
	1950,
	2165,
	1950,
	1713,
	1379,
	1347,
	968,
	1347,
	1347,
	1347,
	1379,
	1347,
	1379,
	357,
	1379,
	1187,
	1379,
	1187,
	1379,
	1187,
	1379,
	1187,
	1379,
	1187,
	1187,
	1187,
	1187,
	1178,
	1379,
	1366,
	1366,
	1379,
	1347,
	738,
	1336,
	2244,
	2160,
	1347,
	1347,
	1347,
	738,
	2160,
	1347,
	1347,
	1187,
	529,
	336,
	1336,
	1789,
	1525,
	1949,
	1789,
	2160,
	1379,
	2160,
	1347,
	1347,
	2258,
	1379,
	1058,
	1187,
	1366,
	1347,
	1347,
	1336,
	1347,
	1347,
	1525,
	1675,
	2258,
	1379,
	785,
	1204,
	1187,
	1187,
	1204,
	1347,
	1187,
	1366,
	1327,
	1168,
	1204,
	1347,
	1187,
	1347,
	1187,
	1187,
	1204,
	1347,
	1187,
	1336,
	1178,
	1998,
	1998,
	1491,
	1814,
	967,
	1058,
	1336,
	1347,
	1379,
	1347,
	1336,
	1347,
	1914,
	2160,
	2160,
	913,
	2160,
	1950,
	1187,
	1187,
	1187,
	782,
	1347,
	1187,
	789,
	1379,
	789,
	789,
	782,
	1366,
	1366,
	1366,
	1366,
	1347,
	1074,
	1074,
	1743,
	2124,
	1846,
	2160,
	2160,
	1949,
	2258,
	2258,
	2258,
	1347,
	1347,
	1347,
	2258,
	789,
	1379,
	1347,
	1336,
	531,
	785,
	785,
	2123,
	2123,
	2160,
	1187,
	1204,
	2160,
	358,
	196,
	530,
	2160,
	2002,
	785,
	787,
	531,
	2183,
	2183,
	2002,
	785,
	964,
	967,
	1347,
	964,
	789,
	1187,
	785,
	1347,
	789,
	1379,
	1178,
	1379,
	1178,
	1347,
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x0200000D, { 11, 1 } },
	{ 0x0600000A, { 0, 3 } },
	{ 0x06000021, { 3, 1 } },
	{ 0x06000031, { 4, 1 } },
	{ 0x06000038, { 5, 1 } },
	{ 0x06000039, { 6, 1 } },
	{ 0x0600003C, { 7, 4 } },
	{ 0x06000058, { 12, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[17] = 
{
	{ (Il2CppRGCTXDataType)2, 865 },
	{ (Il2CppRGCTXDataType)2, 907 },
	{ (Il2CppRGCTXDataType)3, 2516 },
	{ (Il2CppRGCTXDataType)2, 1829 },
	{ (Il2CppRGCTXDataType)3, 2438 },
	{ (Il2CppRGCTXDataType)2, 1830 },
	{ (Il2CppRGCTXDataType)2, 1831 },
	{ (Il2CppRGCTXDataType)2, 400 },
	{ (Il2CppRGCTXDataType)3, 17 },
	{ (Il2CppRGCTXDataType)3, 18 },
	{ (Il2CppRGCTXDataType)2, 85 },
	{ (Il2CppRGCTXDataType)2, 283 },
	{ (Il2CppRGCTXDataType)2, 401 },
	{ (Il2CppRGCTXDataType)3, 21 },
	{ (Il2CppRGCTXDataType)3, 22 },
	{ (Il2CppRGCTXDataType)3, 2523 },
	{ (Il2CppRGCTXDataType)2, 99 },
};
extern const CustomAttributesCacheGenerator g_websocketU2Dsharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_websocketU2Dsharp_CodeGenModule;
const Il2CppCodeGenModule g_websocketU2Dsharp_CodeGenModule = 
{
	"websocket-sharp.dll",
	434,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	8,
	s_rgctxIndices,
	17,
	s_rgctxValues,
	NULL,
	g_websocketU2Dsharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
