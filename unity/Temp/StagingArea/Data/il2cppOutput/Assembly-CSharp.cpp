﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<WebSocketSharp.MessageEventArgs>
struct Action_1_t715674B98F93D6C1C2C2EF5F638B70CE047B31B9;
// System.EventHandler`1<WebSocketSharp.CloseEventArgs>
struct EventHandler_1_t36F607B09B5F9A711346F2AF00EF3C6881F94446;
// System.EventHandler`1<WebSocketSharp.ErrorEventArgs>
struct EventHandler_1_tBD1E780AE74FB5A7F72179606D77FF0BDF0C5179;
// System.EventHandler`1<WebSocketSharp.MessageEventArgs>
struct EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6;
// System.EventHandler`1<System.Object>
struct EventHandler_1_tFA1C30E54FA1061D79E711F65F9A174BFBD8CDCB;
// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String>
struct Func_2_tFC1AEC46364D8B309E93499FB5EE4E8F24C8E11C;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181;
// System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>
struct Queue_1_t938D568900761CA7F7E7B98B7B3FCE6EA8FC15FC;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// System.Collections.ArrayList
struct ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// WebSocketSharp.Net.AuthenticationChallenge
struct AuthenticationChallenge_t1EC25A662B83CD875495EA711A9C5BF2AD116985;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// WebSocketSharp.Net.ClientSslConfiguration
struct ClientSslConfiguration_t8F870FAD51C116D6E675E7F256D6CD3BDF3AD58B;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t89ECD07A71921A03F3EB5B9AB70096E59F9495EC;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// System.EventHandler
struct EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// WebSocketSharp.Logger
struct Logger_t40E3322D8E86399C03B46E0F6C2925918B77A20B;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t9E2ED486907E3A16122ED4E946534E4DD6B5A7BA;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// System.IO.MemoryStream
struct MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// WebSocketSharp.MessageEventArgs
struct MessageEventArgs_t644AAD070C0BA6114FE7C4BD44A31BF3A9FA19D2;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// WebSocketSharp.Net.NetworkCredential
struct NetworkCredential_t2141776593D76A04BF41A10E1BDC9D7E25FEF378;
// NewBehaviourScript
struct NewBehaviourScript_t5ED8D4BD4668DBECFEFD841BD2A8CC1D8FCF38CB;
// Player
struct Player_t5689617909B48F7640EA0892D85C92C13CC22C6F;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// System.IO.Stream
struct Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB;
// System.String
struct String_t;
// System.Net.Sockets.TcpClient
struct TcpClient_t0EEB05EA031F6AFD93D46116F5E33A9C4E3350EE;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// System.Uri
struct Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612;
// System.UriParser
struct UriParser_t6DEBE5C6CDC3C29C9019CD951C7ECEBD6A5D3E3A;
// User
struct User_t73C2C8C9B5B5C1F356AA134EC0B50FC072360F77;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// WS_Client
struct WS_Client_tF30FD0EC57F32A91C0F9A49A618FDCF2106FF041;
// WebSocketSharp.WebSocket
struct WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC;
// WebSocketSharp.Net.WebSockets.WebSocketContext
struct WebSocketContext_tB3BFB4B432DCBE64949BF59EBC3ED9D16EBE75B1;
// bloc
struct bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7;
// bodyCtrl
struct bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D;
// button
struct button_t4F901F446B79F86CB9593995276166F7ADD407E6;
// cell
struct cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF;
// currCell
struct currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42;
// food
struct food_t03A1F65F99660B06E1CB5BF29E080CD11F687652;
// gameEnd
struct gameEnd_t0BD1FD2C540A61239AC275D8EB7F722AAC1A7DCA;
// level0Config
struct level0Config_t922848EFFCF7A30778900D21F26B5E498E142360;
// level1Config
struct level1Config_t0953E2DCDD13E635D78702D7EE04DB61CE0CD534;
// level2Config
struct level2Config_t3AE5B1517E64CF7064471F2A15A0D043104F6074;
// level3Config
struct level3Config_tF052783C756D2BB48C5C9F7FD394EC7A37688853;
// level4Config
struct level4Config_t89718633F1C441AE885A0CA12B4D08C247CAC673;
// level5Config
struct level5Config_tC75394A13F0C653B2A28C5C7B7E4EFA690CB7CD2;
// mainMenu
struct mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339;
// mapGen
struct mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8;
// playerControl
struct playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21;
// setScoresToZero
struct setScoresToZero_t83CA487B984141BA8548B2DB3F6A09827A4F9B2C;
// snekOrg
struct snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1;
// tailCtrl
struct tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// System.Uri/UriInfo
struct UriInfo_tCB2302A896132D1F70E47C3895FAB9A0F2A6EE45;
// WS_Client/<>c
struct U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7;

IL2CPP_EXTERN_C RuntimeClass* ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral01B87E63030BCFCDE47CC49F301933D8B6BA312A;
IL2CPP_EXTERN_C String_t* _stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305;
IL2CPP_EXTERN_C String_t* _stringLiteral2B60501C4A64D5EC88692DA33B5432CCC71C434D;
IL2CPP_EXTERN_C String_t* _stringLiteral322DFDD498B4441E023BB01DAA343BF820A06210;
IL2CPP_EXTERN_C String_t* _stringLiteral3959E7C25DB570E5997B285C2E5C61F96FF05445;
IL2CPP_EXTERN_C String_t* _stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4;
IL2CPP_EXTERN_C String_t* _stringLiteral68AD5F6C7F659E84E37BEB4108167B9C334CF234;
IL2CPP_EXTERN_C String_t* _stringLiteral6AF708CCE5D643FF4CFD2CC4E65F4A36B1F1C499;
IL2CPP_EXTERN_C String_t* _stringLiteral75B9C2589F235DD3C81AB7074613877A21D6F1F6;
IL2CPP_EXTERN_C String_t* _stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2;
IL2CPP_EXTERN_C String_t* _stringLiteralAB4F9FB3969821142A073A0FFCBA0619032580A0;
IL2CPP_EXTERN_C String_t* _stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF;
IL2CPP_EXTERN_C String_t* _stringLiteralB3CA5033F9F4CC8909C704BC3E6BDC7A7991A427;
IL2CPP_EXTERN_C String_t* _stringLiteralE12F82F2AF27C8BFF5B2C886B542D7A59A46BF96;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Empty_TisString_t_m224DA90A7384ACF7EBE2F94D2DFDE2F310D1E77D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventHandler_1__ctor_m6E3771CC1A1EFE63D59637DE6D0CB689DEFAF943_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_Tisbutton_t4F901F446B79F86CB9593995276166F7ADD407E6_m85F68D44BCA088B03954BB7ADE6D419C2B05AFEC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TismapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_m8D19FCEE1886F87EFB5F27938BFB6398F419E027_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m6C4CDF2E6D3B2347704D0BBC108D0893C1ABBEAE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_set_Item_m0E40C7E017BB8ADBADBD6DE8947884FA4DEA2DE5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CStartU3Eb__2_0_mCFC48F6EEA3302A6173CFB18D550F6447FEB0BFD_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object


// System.EmptyArray`1<System.Object>
struct EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4  : public RuntimeObject
{
public:

public:
};

struct EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4_StaticFields
{
public:
	// T[] System.EmptyArray`1::Value
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4_StaticFields, ___Value_0)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_Value_0() const { return ___Value_0; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_0), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____items_1)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_StaticFields, ____emptyArray_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get__emptyArray_5() const { return ____emptyArray_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____items_1)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9_StaticFields, ____emptyArray_5)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____items_1)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.ArrayList
struct ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575  : public RuntimeObject
{
public:
	// System.Object[] System.Collections.ArrayList::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_0;
	// System.Int32 System.Collections.ArrayList::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.ArrayList::_version
	int32_t ____version_2;
	// System.Object System.Collections.ArrayList::_syncRoot
	RuntimeObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__items_0() { return static_cast<int32_t>(offsetof(ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575, ____items_0)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_0() const { return ____items_0; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_0() { return &____items_0; }
	inline void set__items_0(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_0), (void*)value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_3), (void*)value);
	}
};

struct ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575_StaticFields
{
public:
	// System.Object[] System.Collections.ArrayList::emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___emptyArray_5;

public:
	inline static int32_t get_offset_of_emptyArray_5() { return static_cast<int32_t>(offsetof(ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575_StaticFields, ___emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_emptyArray_5() const { return ___emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_emptyArray_5() { return &___emptyArray_5; }
	inline void set_emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___emptyArray_5), (void*)value);
	}
};


// System.EventArgs
struct EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA  : public RuntimeObject
{
public:

public:
};

struct EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_StaticFields, ___Empty_0)); }
	inline EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_0), (void*)value);
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// WS_Client/<>c
struct U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_StaticFields
{
public:
	// WS_Client/<>c WS_Client/<>c::<>9
	U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7 * ___U3CU3E9_0;
	// System.EventHandler`1<WebSocketSharp.MessageEventArgs> WS_Client/<>c::<>9__2_0
	EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 * ___U3CU3E9__2_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_StaticFields, ___U3CU3E9__2_0_1)); }
	inline EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__2_0_1), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.SceneManagement.Scene
struct Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// WebSocketSharp.CompressionMethod
struct CompressionMethod_t9827B539439C1C434A72D2D0EDEAA7E5CD55BDA3 
{
public:
	// System.Byte WebSocketSharp.CompressionMethod::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompressionMethod_t9827B539439C1C434A72D2D0EDEAA7E5CD55BDA3, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.KeyCode
struct KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// WebSocketSharp.Opcode
struct Opcode_tDC9FB545CC805E78741539C9F5C4DA0B1367CBE2 
{
public:
	// System.Byte WebSocketSharp.Opcode::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Opcode_tDC9FB545CC805E78741539C9F5C4DA0B1367CBE2, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// System.TimeSpan
struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___Zero_19)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};


// System.UriFormat
struct UriFormat_t25C936463BDE737B16A8EC3DA05091FC31F1A71F 
{
public:
	// System.Int32 System.UriFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriFormat_t25C936463BDE737B16A8EC3DA05091FC31F1A71F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.UriIdnScope
struct UriIdnScope_tBA22B992BA582F68F2B98CDEBCB24299F249DE4D 
{
public:
	// System.Int32 System.UriIdnScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriIdnScope_tBA22B992BA582F68F2B98CDEBCB24299F249DE4D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.UriKind
struct UriKind_tFC16ACC1842283AAE2C7F50C9C70EFBF6550B3FC 
{
public:
	// System.Int32 System.UriKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriKind_tFC16ACC1842283AAE2C7F50C9C70EFBF6550B3FC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// WebSocketSharp.WebSocketState
struct WebSocketState_tDD99CEE8123269945C40E0979E6A9284FF9E7AFA 
{
public:
	// System.UInt16 WebSocketSharp.WebSocketState::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebSocketState_tDD99CEE8123269945C40E0979E6A9284FF9E7AFA, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};


// System.Uri/Flags
struct Flags_t72C622DF5C3ED762F55AB36EC2CCDDF3AF56B8D4 
{
public:
	// System.UInt64 System.Uri/Flags::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_t72C622DF5C3ED762F55AB36EC2CCDDF3AF56B8D4, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// WebSocketSharp.MessageEventArgs
struct MessageEventArgs_t644AAD070C0BA6114FE7C4BD44A31BF3A9FA19D2  : public EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA
{
public:
	// System.String WebSocketSharp.MessageEventArgs::_data
	String_t* ____data_1;
	// System.Boolean WebSocketSharp.MessageEventArgs::_dataSet
	bool ____dataSet_2;
	// WebSocketSharp.Opcode WebSocketSharp.MessageEventArgs::_opcode
	uint8_t ____opcode_3;
	// System.Byte[] WebSocketSharp.MessageEventArgs::_rawData
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ____rawData_4;

public:
	inline static int32_t get_offset_of__data_1() { return static_cast<int32_t>(offsetof(MessageEventArgs_t644AAD070C0BA6114FE7C4BD44A31BF3A9FA19D2, ____data_1)); }
	inline String_t* get__data_1() const { return ____data_1; }
	inline String_t** get_address_of__data_1() { return &____data_1; }
	inline void set__data_1(String_t* value)
	{
		____data_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_1), (void*)value);
	}

	inline static int32_t get_offset_of__dataSet_2() { return static_cast<int32_t>(offsetof(MessageEventArgs_t644AAD070C0BA6114FE7C4BD44A31BF3A9FA19D2, ____dataSet_2)); }
	inline bool get__dataSet_2() const { return ____dataSet_2; }
	inline bool* get_address_of__dataSet_2() { return &____dataSet_2; }
	inline void set__dataSet_2(bool value)
	{
		____dataSet_2 = value;
	}

	inline static int32_t get_offset_of__opcode_3() { return static_cast<int32_t>(offsetof(MessageEventArgs_t644AAD070C0BA6114FE7C4BD44A31BF3A9FA19D2, ____opcode_3)); }
	inline uint8_t get__opcode_3() const { return ____opcode_3; }
	inline uint8_t* get_address_of__opcode_3() { return &____opcode_3; }
	inline void set__opcode_3(uint8_t value)
	{
		____opcode_3 = value;
	}

	inline static int32_t get_offset_of__rawData_4() { return static_cast<int32_t>(offsetof(MessageEventArgs_t644AAD070C0BA6114FE7C4BD44A31BF3A9FA19D2, ____rawData_4)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get__rawData_4() const { return ____rawData_4; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of__rawData_4() { return &____rawData_4; }
	inline void set__rawData_4(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		____rawData_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rawData_4), (void*)value);
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.Uri
struct Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612  : public RuntimeObject
{
public:
	// System.String System.Uri::m_String
	String_t* ___m_String_16;
	// System.String System.Uri::m_originalUnicodeString
	String_t* ___m_originalUnicodeString_17;
	// System.UriParser System.Uri::m_Syntax
	UriParser_t6DEBE5C6CDC3C29C9019CD951C7ECEBD6A5D3E3A * ___m_Syntax_18;
	// System.String System.Uri::m_DnsSafeHost
	String_t* ___m_DnsSafeHost_19;
	// System.Uri/Flags System.Uri::m_Flags
	uint64_t ___m_Flags_20;
	// System.Uri/UriInfo System.Uri::m_Info
	UriInfo_tCB2302A896132D1F70E47C3895FAB9A0F2A6EE45 * ___m_Info_21;
	// System.Boolean System.Uri::m_iriParsing
	bool ___m_iriParsing_22;

public:
	inline static int32_t get_offset_of_m_String_16() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612, ___m_String_16)); }
	inline String_t* get_m_String_16() const { return ___m_String_16; }
	inline String_t** get_address_of_m_String_16() { return &___m_String_16; }
	inline void set_m_String_16(String_t* value)
	{
		___m_String_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_String_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_originalUnicodeString_17() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612, ___m_originalUnicodeString_17)); }
	inline String_t* get_m_originalUnicodeString_17() const { return ___m_originalUnicodeString_17; }
	inline String_t** get_address_of_m_originalUnicodeString_17() { return &___m_originalUnicodeString_17; }
	inline void set_m_originalUnicodeString_17(String_t* value)
	{
		___m_originalUnicodeString_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_originalUnicodeString_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_Syntax_18() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612, ___m_Syntax_18)); }
	inline UriParser_t6DEBE5C6CDC3C29C9019CD951C7ECEBD6A5D3E3A * get_m_Syntax_18() const { return ___m_Syntax_18; }
	inline UriParser_t6DEBE5C6CDC3C29C9019CD951C7ECEBD6A5D3E3A ** get_address_of_m_Syntax_18() { return &___m_Syntax_18; }
	inline void set_m_Syntax_18(UriParser_t6DEBE5C6CDC3C29C9019CD951C7ECEBD6A5D3E3A * value)
	{
		___m_Syntax_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Syntax_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_DnsSafeHost_19() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612, ___m_DnsSafeHost_19)); }
	inline String_t* get_m_DnsSafeHost_19() const { return ___m_DnsSafeHost_19; }
	inline String_t** get_address_of_m_DnsSafeHost_19() { return &___m_DnsSafeHost_19; }
	inline void set_m_DnsSafeHost_19(String_t* value)
	{
		___m_DnsSafeHost_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DnsSafeHost_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_Flags_20() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612, ___m_Flags_20)); }
	inline uint64_t get_m_Flags_20() const { return ___m_Flags_20; }
	inline uint64_t* get_address_of_m_Flags_20() { return &___m_Flags_20; }
	inline void set_m_Flags_20(uint64_t value)
	{
		___m_Flags_20 = value;
	}

	inline static int32_t get_offset_of_m_Info_21() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612, ___m_Info_21)); }
	inline UriInfo_tCB2302A896132D1F70E47C3895FAB9A0F2A6EE45 * get_m_Info_21() const { return ___m_Info_21; }
	inline UriInfo_tCB2302A896132D1F70E47C3895FAB9A0F2A6EE45 ** get_address_of_m_Info_21() { return &___m_Info_21; }
	inline void set_m_Info_21(UriInfo_tCB2302A896132D1F70E47C3895FAB9A0F2A6EE45 * value)
	{
		___m_Info_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Info_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_iriParsing_22() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612, ___m_iriParsing_22)); }
	inline bool get_m_iriParsing_22() const { return ___m_iriParsing_22; }
	inline bool* get_address_of_m_iriParsing_22() { return &___m_iriParsing_22; }
	inline void set_m_iriParsing_22(bool value)
	{
		___m_iriParsing_22 = value;
	}
};

struct Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields
{
public:
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_0;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_1;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_2;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_3;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_4;
	// System.String System.Uri::UriSchemeWs
	String_t* ___UriSchemeWs_5;
	// System.String System.Uri::UriSchemeWss
	String_t* ___UriSchemeWss_6;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_7;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_8;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_9;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_10;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_11;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_12;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitialized
	bool ___s_ConfigInitialized_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitializing
	bool ___s_ConfigInitializing_24;
	// System.UriIdnScope modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IdnScope
	int32_t ___s_IdnScope_25;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IriParsing
	bool ___s_IriParsing_26;
	// System.Boolean System.Uri::useDotNetRelativeOrAbsolute
	bool ___useDotNetRelativeOrAbsolute_27;
	// System.Boolean System.Uri::IsWindowsFileSystem
	bool ___IsWindowsFileSystem_29;
	// System.Object System.Uri::s_initLock
	RuntimeObject * ___s_initLock_30;
	// System.Char[] System.Uri::HexLowerChars
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___HexLowerChars_34;
	// System.Char[] System.Uri::_WSchars
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ____WSchars_35;

public:
	inline static int32_t get_offset_of_UriSchemeFile_0() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeFile_0)); }
	inline String_t* get_UriSchemeFile_0() const { return ___UriSchemeFile_0; }
	inline String_t** get_address_of_UriSchemeFile_0() { return &___UriSchemeFile_0; }
	inline void set_UriSchemeFile_0(String_t* value)
	{
		___UriSchemeFile_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeFile_0), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_1() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeFtp_1)); }
	inline String_t* get_UriSchemeFtp_1() const { return ___UriSchemeFtp_1; }
	inline String_t** get_address_of_UriSchemeFtp_1() { return &___UriSchemeFtp_1; }
	inline void set_UriSchemeFtp_1(String_t* value)
	{
		___UriSchemeFtp_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeFtp_1), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_2() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeGopher_2)); }
	inline String_t* get_UriSchemeGopher_2() const { return ___UriSchemeGopher_2; }
	inline String_t** get_address_of_UriSchemeGopher_2() { return &___UriSchemeGopher_2; }
	inline void set_UriSchemeGopher_2(String_t* value)
	{
		___UriSchemeGopher_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeGopher_2), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_3() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeHttp_3)); }
	inline String_t* get_UriSchemeHttp_3() const { return ___UriSchemeHttp_3; }
	inline String_t** get_address_of_UriSchemeHttp_3() { return &___UriSchemeHttp_3; }
	inline void set_UriSchemeHttp_3(String_t* value)
	{
		___UriSchemeHttp_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeHttp_3), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_4() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeHttps_4)); }
	inline String_t* get_UriSchemeHttps_4() const { return ___UriSchemeHttps_4; }
	inline String_t** get_address_of_UriSchemeHttps_4() { return &___UriSchemeHttps_4; }
	inline void set_UriSchemeHttps_4(String_t* value)
	{
		___UriSchemeHttps_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeHttps_4), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeWs_5() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeWs_5)); }
	inline String_t* get_UriSchemeWs_5() const { return ___UriSchemeWs_5; }
	inline String_t** get_address_of_UriSchemeWs_5() { return &___UriSchemeWs_5; }
	inline void set_UriSchemeWs_5(String_t* value)
	{
		___UriSchemeWs_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeWs_5), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeWss_6() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeWss_6)); }
	inline String_t* get_UriSchemeWss_6() const { return ___UriSchemeWss_6; }
	inline String_t** get_address_of_UriSchemeWss_6() { return &___UriSchemeWss_6; }
	inline void set_UriSchemeWss_6(String_t* value)
	{
		___UriSchemeWss_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeWss_6), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_7() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeMailto_7)); }
	inline String_t* get_UriSchemeMailto_7() const { return ___UriSchemeMailto_7; }
	inline String_t** get_address_of_UriSchemeMailto_7() { return &___UriSchemeMailto_7; }
	inline void set_UriSchemeMailto_7(String_t* value)
	{
		___UriSchemeMailto_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeMailto_7), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_8() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeNews_8)); }
	inline String_t* get_UriSchemeNews_8() const { return ___UriSchemeNews_8; }
	inline String_t** get_address_of_UriSchemeNews_8() { return &___UriSchemeNews_8; }
	inline void set_UriSchemeNews_8(String_t* value)
	{
		___UriSchemeNews_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNews_8), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_9() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeNntp_9)); }
	inline String_t* get_UriSchemeNntp_9() const { return ___UriSchemeNntp_9; }
	inline String_t** get_address_of_UriSchemeNntp_9() { return &___UriSchemeNntp_9; }
	inline void set_UriSchemeNntp_9(String_t* value)
	{
		___UriSchemeNntp_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNntp_9), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_10() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeNetTcp_10)); }
	inline String_t* get_UriSchemeNetTcp_10() const { return ___UriSchemeNetTcp_10; }
	inline String_t** get_address_of_UriSchemeNetTcp_10() { return &___UriSchemeNetTcp_10; }
	inline void set_UriSchemeNetTcp_10(String_t* value)
	{
		___UriSchemeNetTcp_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNetTcp_10), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_11() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeNetPipe_11)); }
	inline String_t* get_UriSchemeNetPipe_11() const { return ___UriSchemeNetPipe_11; }
	inline String_t** get_address_of_UriSchemeNetPipe_11() { return &___UriSchemeNetPipe_11; }
	inline void set_UriSchemeNetPipe_11(String_t* value)
	{
		___UriSchemeNetPipe_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNetPipe_11), (void*)value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_12() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___SchemeDelimiter_12)); }
	inline String_t* get_SchemeDelimiter_12() const { return ___SchemeDelimiter_12; }
	inline String_t** get_address_of_SchemeDelimiter_12() { return &___SchemeDelimiter_12; }
	inline void set_SchemeDelimiter_12(String_t* value)
	{
		___SchemeDelimiter_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SchemeDelimiter_12), (void*)value);
	}

	inline static int32_t get_offset_of_s_ConfigInitialized_23() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___s_ConfigInitialized_23)); }
	inline bool get_s_ConfigInitialized_23() const { return ___s_ConfigInitialized_23; }
	inline bool* get_address_of_s_ConfigInitialized_23() { return &___s_ConfigInitialized_23; }
	inline void set_s_ConfigInitialized_23(bool value)
	{
		___s_ConfigInitialized_23 = value;
	}

	inline static int32_t get_offset_of_s_ConfigInitializing_24() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___s_ConfigInitializing_24)); }
	inline bool get_s_ConfigInitializing_24() const { return ___s_ConfigInitializing_24; }
	inline bool* get_address_of_s_ConfigInitializing_24() { return &___s_ConfigInitializing_24; }
	inline void set_s_ConfigInitializing_24(bool value)
	{
		___s_ConfigInitializing_24 = value;
	}

	inline static int32_t get_offset_of_s_IdnScope_25() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___s_IdnScope_25)); }
	inline int32_t get_s_IdnScope_25() const { return ___s_IdnScope_25; }
	inline int32_t* get_address_of_s_IdnScope_25() { return &___s_IdnScope_25; }
	inline void set_s_IdnScope_25(int32_t value)
	{
		___s_IdnScope_25 = value;
	}

	inline static int32_t get_offset_of_s_IriParsing_26() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___s_IriParsing_26)); }
	inline bool get_s_IriParsing_26() const { return ___s_IriParsing_26; }
	inline bool* get_address_of_s_IriParsing_26() { return &___s_IriParsing_26; }
	inline void set_s_IriParsing_26(bool value)
	{
		___s_IriParsing_26 = value;
	}

	inline static int32_t get_offset_of_useDotNetRelativeOrAbsolute_27() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___useDotNetRelativeOrAbsolute_27)); }
	inline bool get_useDotNetRelativeOrAbsolute_27() const { return ___useDotNetRelativeOrAbsolute_27; }
	inline bool* get_address_of_useDotNetRelativeOrAbsolute_27() { return &___useDotNetRelativeOrAbsolute_27; }
	inline void set_useDotNetRelativeOrAbsolute_27(bool value)
	{
		___useDotNetRelativeOrAbsolute_27 = value;
	}

	inline static int32_t get_offset_of_IsWindowsFileSystem_29() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___IsWindowsFileSystem_29)); }
	inline bool get_IsWindowsFileSystem_29() const { return ___IsWindowsFileSystem_29; }
	inline bool* get_address_of_IsWindowsFileSystem_29() { return &___IsWindowsFileSystem_29; }
	inline void set_IsWindowsFileSystem_29(bool value)
	{
		___IsWindowsFileSystem_29 = value;
	}

	inline static int32_t get_offset_of_s_initLock_30() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___s_initLock_30)); }
	inline RuntimeObject * get_s_initLock_30() const { return ___s_initLock_30; }
	inline RuntimeObject ** get_address_of_s_initLock_30() { return &___s_initLock_30; }
	inline void set_s_initLock_30(RuntimeObject * value)
	{
		___s_initLock_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_initLock_30), (void*)value);
	}

	inline static int32_t get_offset_of_HexLowerChars_34() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___HexLowerChars_34)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_HexLowerChars_34() const { return ___HexLowerChars_34; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_HexLowerChars_34() { return &___HexLowerChars_34; }
	inline void set_HexLowerChars_34(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___HexLowerChars_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HexLowerChars_34), (void*)value);
	}

	inline static int32_t get_offset_of__WSchars_35() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ____WSchars_35)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get__WSchars_35() const { return ____WSchars_35; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of__WSchars_35() { return &____WSchars_35; }
	inline void set__WSchars_35(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		____WSchars_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WSchars_35), (void*)value);
	}
};


// WebSocketSharp.WebSocket
struct WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.AuthenticationChallenge WebSocketSharp.WebSocket::_authChallenge
	AuthenticationChallenge_t1EC25A662B83CD875495EA711A9C5BF2AD116985 * ____authChallenge_0;
	// System.String WebSocketSharp.WebSocket::_base64Key
	String_t* ____base64Key_1;
	// System.Boolean WebSocketSharp.WebSocket::_client
	bool ____client_2;
	// System.Action WebSocketSharp.WebSocket::_closeContext
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ____closeContext_3;
	// WebSocketSharp.CompressionMethod WebSocketSharp.WebSocket::_compression
	uint8_t ____compression_4;
	// WebSocketSharp.Net.WebSockets.WebSocketContext WebSocketSharp.WebSocket::_context
	WebSocketContext_tB3BFB4B432DCBE64949BF59EBC3ED9D16EBE75B1 * ____context_5;
	// WebSocketSharp.Net.CookieCollection WebSocketSharp.WebSocket::_cookies
	CookieCollection_t89ECD07A71921A03F3EB5B9AB70096E59F9495EC * ____cookies_6;
	// WebSocketSharp.Net.NetworkCredential WebSocketSharp.WebSocket::_credentials
	NetworkCredential_t2141776593D76A04BF41A10E1BDC9D7E25FEF378 * ____credentials_7;
	// System.Boolean WebSocketSharp.WebSocket::_emitOnPing
	bool ____emitOnPing_8;
	// System.Boolean WebSocketSharp.WebSocket::_enableRedirection
	bool ____enableRedirection_9;
	// System.String WebSocketSharp.WebSocket::_extensions
	String_t* ____extensions_10;
	// System.Boolean WebSocketSharp.WebSocket::_extensionsRequested
	bool ____extensionsRequested_11;
	// System.Object WebSocketSharp.WebSocket::_forMessageEventQueue
	RuntimeObject * ____forMessageEventQueue_12;
	// System.Object WebSocketSharp.WebSocket::_forPing
	RuntimeObject * ____forPing_13;
	// System.Object WebSocketSharp.WebSocket::_forSend
	RuntimeObject * ____forSend_14;
	// System.Object WebSocketSharp.WebSocket::_forState
	RuntimeObject * ____forState_15;
	// System.IO.MemoryStream WebSocketSharp.WebSocket::_fragmentsBuffer
	MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * ____fragmentsBuffer_16;
	// System.Boolean WebSocketSharp.WebSocket::_fragmentsCompressed
	bool ____fragmentsCompressed_17;
	// WebSocketSharp.Opcode WebSocketSharp.WebSocket::_fragmentsOpcode
	uint8_t ____fragmentsOpcode_18;
	// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String> WebSocketSharp.WebSocket::_handshakeRequestChecker
	Func_2_tFC1AEC46364D8B309E93499FB5EE4E8F24C8E11C * ____handshakeRequestChecker_20;
	// System.Boolean WebSocketSharp.WebSocket::_ignoreExtensions
	bool ____ignoreExtensions_21;
	// System.Boolean WebSocketSharp.WebSocket::_inContinuation
	bool ____inContinuation_22;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.WebSocket::_inMessage
	bool ____inMessage_23;
	// WebSocketSharp.Logger modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.WebSocket::_logger
	Logger_t40E3322D8E86399C03B46E0F6C2925918B77A20B * ____logger_24;
	// System.Action`1<WebSocketSharp.MessageEventArgs> WebSocketSharp.WebSocket::_message
	Action_1_t715674B98F93D6C1C2C2EF5F638B70CE047B31B9 * ____message_26;
	// System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs> WebSocketSharp.WebSocket::_messageEventQueue
	Queue_1_t938D568900761CA7F7E7B98B7B3FCE6EA8FC15FC * ____messageEventQueue_27;
	// System.UInt32 WebSocketSharp.WebSocket::_nonceCount
	uint32_t ____nonceCount_28;
	// System.String WebSocketSharp.WebSocket::_origin
	String_t* ____origin_29;
	// System.Threading.ManualResetEvent WebSocketSharp.WebSocket::_pongReceived
	ManualResetEvent_t9E2ED486907E3A16122ED4E946534E4DD6B5A7BA * ____pongReceived_30;
	// System.Boolean WebSocketSharp.WebSocket::_preAuth
	bool ____preAuth_31;
	// System.String WebSocketSharp.WebSocket::_protocol
	String_t* ____protocol_32;
	// System.String[] WebSocketSharp.WebSocket::_protocols
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____protocols_33;
	// System.Boolean WebSocketSharp.WebSocket::_protocolsRequested
	bool ____protocolsRequested_34;
	// WebSocketSharp.Net.NetworkCredential WebSocketSharp.WebSocket::_proxyCredentials
	NetworkCredential_t2141776593D76A04BF41A10E1BDC9D7E25FEF378 * ____proxyCredentials_35;
	// System.Uri WebSocketSharp.WebSocket::_proxyUri
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ____proxyUri_36;
	// WebSocketSharp.WebSocketState modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.WebSocket::_readyState
	uint16_t ____readyState_37;
	// System.Threading.ManualResetEvent WebSocketSharp.WebSocket::_receivingExited
	ManualResetEvent_t9E2ED486907E3A16122ED4E946534E4DD6B5A7BA * ____receivingExited_38;
	// System.Int32 WebSocketSharp.WebSocket::_retryCountForConnect
	int32_t ____retryCountForConnect_39;
	// System.Boolean WebSocketSharp.WebSocket::_secure
	bool ____secure_40;
	// WebSocketSharp.Net.ClientSslConfiguration WebSocketSharp.WebSocket::_sslConfig
	ClientSslConfiguration_t8F870FAD51C116D6E675E7F256D6CD3BDF3AD58B * ____sslConfig_41;
	// System.IO.Stream WebSocketSharp.WebSocket::_stream
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ____stream_42;
	// System.Net.Sockets.TcpClient WebSocketSharp.WebSocket::_tcpClient
	TcpClient_t0EEB05EA031F6AFD93D46116F5E33A9C4E3350EE * ____tcpClient_43;
	// System.Uri WebSocketSharp.WebSocket::_uri
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ____uri_44;
	// System.TimeSpan WebSocketSharp.WebSocket::_waitTime
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ____waitTime_46;
	// System.EventHandler`1<WebSocketSharp.CloseEventArgs> WebSocketSharp.WebSocket::OnClose
	EventHandler_1_t36F607B09B5F9A711346F2AF00EF3C6881F94446 * ___OnClose_50;
	// System.EventHandler`1<WebSocketSharp.ErrorEventArgs> WebSocketSharp.WebSocket::OnError
	EventHandler_1_tBD1E780AE74FB5A7F72179606D77FF0BDF0C5179 * ___OnError_51;
	// System.EventHandler`1<WebSocketSharp.MessageEventArgs> WebSocketSharp.WebSocket::OnMessage
	EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 * ___OnMessage_52;
	// System.EventHandler WebSocketSharp.WebSocket::OnOpen
	EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___OnOpen_53;

public:
	inline static int32_t get_offset_of__authChallenge_0() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____authChallenge_0)); }
	inline AuthenticationChallenge_t1EC25A662B83CD875495EA711A9C5BF2AD116985 * get__authChallenge_0() const { return ____authChallenge_0; }
	inline AuthenticationChallenge_t1EC25A662B83CD875495EA711A9C5BF2AD116985 ** get_address_of__authChallenge_0() { return &____authChallenge_0; }
	inline void set__authChallenge_0(AuthenticationChallenge_t1EC25A662B83CD875495EA711A9C5BF2AD116985 * value)
	{
		____authChallenge_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____authChallenge_0), (void*)value);
	}

	inline static int32_t get_offset_of__base64Key_1() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____base64Key_1)); }
	inline String_t* get__base64Key_1() const { return ____base64Key_1; }
	inline String_t** get_address_of__base64Key_1() { return &____base64Key_1; }
	inline void set__base64Key_1(String_t* value)
	{
		____base64Key_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____base64Key_1), (void*)value);
	}

	inline static int32_t get_offset_of__client_2() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____client_2)); }
	inline bool get__client_2() const { return ____client_2; }
	inline bool* get_address_of__client_2() { return &____client_2; }
	inline void set__client_2(bool value)
	{
		____client_2 = value;
	}

	inline static int32_t get_offset_of__closeContext_3() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____closeContext_3)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get__closeContext_3() const { return ____closeContext_3; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of__closeContext_3() { return &____closeContext_3; }
	inline void set__closeContext_3(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		____closeContext_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____closeContext_3), (void*)value);
	}

	inline static int32_t get_offset_of__compression_4() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____compression_4)); }
	inline uint8_t get__compression_4() const { return ____compression_4; }
	inline uint8_t* get_address_of__compression_4() { return &____compression_4; }
	inline void set__compression_4(uint8_t value)
	{
		____compression_4 = value;
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____context_5)); }
	inline WebSocketContext_tB3BFB4B432DCBE64949BF59EBC3ED9D16EBE75B1 * get__context_5() const { return ____context_5; }
	inline WebSocketContext_tB3BFB4B432DCBE64949BF59EBC3ED9D16EBE75B1 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(WebSocketContext_tB3BFB4B432DCBE64949BF59EBC3ED9D16EBE75B1 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____context_5), (void*)value);
	}

	inline static int32_t get_offset_of__cookies_6() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____cookies_6)); }
	inline CookieCollection_t89ECD07A71921A03F3EB5B9AB70096E59F9495EC * get__cookies_6() const { return ____cookies_6; }
	inline CookieCollection_t89ECD07A71921A03F3EB5B9AB70096E59F9495EC ** get_address_of__cookies_6() { return &____cookies_6; }
	inline void set__cookies_6(CookieCollection_t89ECD07A71921A03F3EB5B9AB70096E59F9495EC * value)
	{
		____cookies_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cookies_6), (void*)value);
	}

	inline static int32_t get_offset_of__credentials_7() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____credentials_7)); }
	inline NetworkCredential_t2141776593D76A04BF41A10E1BDC9D7E25FEF378 * get__credentials_7() const { return ____credentials_7; }
	inline NetworkCredential_t2141776593D76A04BF41A10E1BDC9D7E25FEF378 ** get_address_of__credentials_7() { return &____credentials_7; }
	inline void set__credentials_7(NetworkCredential_t2141776593D76A04BF41A10E1BDC9D7E25FEF378 * value)
	{
		____credentials_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____credentials_7), (void*)value);
	}

	inline static int32_t get_offset_of__emitOnPing_8() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____emitOnPing_8)); }
	inline bool get__emitOnPing_8() const { return ____emitOnPing_8; }
	inline bool* get_address_of__emitOnPing_8() { return &____emitOnPing_8; }
	inline void set__emitOnPing_8(bool value)
	{
		____emitOnPing_8 = value;
	}

	inline static int32_t get_offset_of__enableRedirection_9() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____enableRedirection_9)); }
	inline bool get__enableRedirection_9() const { return ____enableRedirection_9; }
	inline bool* get_address_of__enableRedirection_9() { return &____enableRedirection_9; }
	inline void set__enableRedirection_9(bool value)
	{
		____enableRedirection_9 = value;
	}

	inline static int32_t get_offset_of__extensions_10() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____extensions_10)); }
	inline String_t* get__extensions_10() const { return ____extensions_10; }
	inline String_t** get_address_of__extensions_10() { return &____extensions_10; }
	inline void set__extensions_10(String_t* value)
	{
		____extensions_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____extensions_10), (void*)value);
	}

	inline static int32_t get_offset_of__extensionsRequested_11() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____extensionsRequested_11)); }
	inline bool get__extensionsRequested_11() const { return ____extensionsRequested_11; }
	inline bool* get_address_of__extensionsRequested_11() { return &____extensionsRequested_11; }
	inline void set__extensionsRequested_11(bool value)
	{
		____extensionsRequested_11 = value;
	}

	inline static int32_t get_offset_of__forMessageEventQueue_12() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____forMessageEventQueue_12)); }
	inline RuntimeObject * get__forMessageEventQueue_12() const { return ____forMessageEventQueue_12; }
	inline RuntimeObject ** get_address_of__forMessageEventQueue_12() { return &____forMessageEventQueue_12; }
	inline void set__forMessageEventQueue_12(RuntimeObject * value)
	{
		____forMessageEventQueue_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____forMessageEventQueue_12), (void*)value);
	}

	inline static int32_t get_offset_of__forPing_13() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____forPing_13)); }
	inline RuntimeObject * get__forPing_13() const { return ____forPing_13; }
	inline RuntimeObject ** get_address_of__forPing_13() { return &____forPing_13; }
	inline void set__forPing_13(RuntimeObject * value)
	{
		____forPing_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____forPing_13), (void*)value);
	}

	inline static int32_t get_offset_of__forSend_14() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____forSend_14)); }
	inline RuntimeObject * get__forSend_14() const { return ____forSend_14; }
	inline RuntimeObject ** get_address_of__forSend_14() { return &____forSend_14; }
	inline void set__forSend_14(RuntimeObject * value)
	{
		____forSend_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____forSend_14), (void*)value);
	}

	inline static int32_t get_offset_of__forState_15() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____forState_15)); }
	inline RuntimeObject * get__forState_15() const { return ____forState_15; }
	inline RuntimeObject ** get_address_of__forState_15() { return &____forState_15; }
	inline void set__forState_15(RuntimeObject * value)
	{
		____forState_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____forState_15), (void*)value);
	}

	inline static int32_t get_offset_of__fragmentsBuffer_16() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____fragmentsBuffer_16)); }
	inline MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * get__fragmentsBuffer_16() const { return ____fragmentsBuffer_16; }
	inline MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C ** get_address_of__fragmentsBuffer_16() { return &____fragmentsBuffer_16; }
	inline void set__fragmentsBuffer_16(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * value)
	{
		____fragmentsBuffer_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fragmentsBuffer_16), (void*)value);
	}

	inline static int32_t get_offset_of__fragmentsCompressed_17() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____fragmentsCompressed_17)); }
	inline bool get__fragmentsCompressed_17() const { return ____fragmentsCompressed_17; }
	inline bool* get_address_of__fragmentsCompressed_17() { return &____fragmentsCompressed_17; }
	inline void set__fragmentsCompressed_17(bool value)
	{
		____fragmentsCompressed_17 = value;
	}

	inline static int32_t get_offset_of__fragmentsOpcode_18() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____fragmentsOpcode_18)); }
	inline uint8_t get__fragmentsOpcode_18() const { return ____fragmentsOpcode_18; }
	inline uint8_t* get_address_of__fragmentsOpcode_18() { return &____fragmentsOpcode_18; }
	inline void set__fragmentsOpcode_18(uint8_t value)
	{
		____fragmentsOpcode_18 = value;
	}

	inline static int32_t get_offset_of__handshakeRequestChecker_20() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____handshakeRequestChecker_20)); }
	inline Func_2_tFC1AEC46364D8B309E93499FB5EE4E8F24C8E11C * get__handshakeRequestChecker_20() const { return ____handshakeRequestChecker_20; }
	inline Func_2_tFC1AEC46364D8B309E93499FB5EE4E8F24C8E11C ** get_address_of__handshakeRequestChecker_20() { return &____handshakeRequestChecker_20; }
	inline void set__handshakeRequestChecker_20(Func_2_tFC1AEC46364D8B309E93499FB5EE4E8F24C8E11C * value)
	{
		____handshakeRequestChecker_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____handshakeRequestChecker_20), (void*)value);
	}

	inline static int32_t get_offset_of__ignoreExtensions_21() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____ignoreExtensions_21)); }
	inline bool get__ignoreExtensions_21() const { return ____ignoreExtensions_21; }
	inline bool* get_address_of__ignoreExtensions_21() { return &____ignoreExtensions_21; }
	inline void set__ignoreExtensions_21(bool value)
	{
		____ignoreExtensions_21 = value;
	}

	inline static int32_t get_offset_of__inContinuation_22() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____inContinuation_22)); }
	inline bool get__inContinuation_22() const { return ____inContinuation_22; }
	inline bool* get_address_of__inContinuation_22() { return &____inContinuation_22; }
	inline void set__inContinuation_22(bool value)
	{
		____inContinuation_22 = value;
	}

	inline static int32_t get_offset_of__inMessage_23() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____inMessage_23)); }
	inline bool get__inMessage_23() const { return ____inMessage_23; }
	inline bool* get_address_of__inMessage_23() { return &____inMessage_23; }
	inline void set__inMessage_23(bool value)
	{
		____inMessage_23 = value;
	}

	inline static int32_t get_offset_of__logger_24() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____logger_24)); }
	inline Logger_t40E3322D8E86399C03B46E0F6C2925918B77A20B * get__logger_24() const { return ____logger_24; }
	inline Logger_t40E3322D8E86399C03B46E0F6C2925918B77A20B ** get_address_of__logger_24() { return &____logger_24; }
	inline void set__logger_24(Logger_t40E3322D8E86399C03B46E0F6C2925918B77A20B * value)
	{
		____logger_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____logger_24), (void*)value);
	}

	inline static int32_t get_offset_of__message_26() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____message_26)); }
	inline Action_1_t715674B98F93D6C1C2C2EF5F638B70CE047B31B9 * get__message_26() const { return ____message_26; }
	inline Action_1_t715674B98F93D6C1C2C2EF5F638B70CE047B31B9 ** get_address_of__message_26() { return &____message_26; }
	inline void set__message_26(Action_1_t715674B98F93D6C1C2C2EF5F638B70CE047B31B9 * value)
	{
		____message_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_26), (void*)value);
	}

	inline static int32_t get_offset_of__messageEventQueue_27() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____messageEventQueue_27)); }
	inline Queue_1_t938D568900761CA7F7E7B98B7B3FCE6EA8FC15FC * get__messageEventQueue_27() const { return ____messageEventQueue_27; }
	inline Queue_1_t938D568900761CA7F7E7B98B7B3FCE6EA8FC15FC ** get_address_of__messageEventQueue_27() { return &____messageEventQueue_27; }
	inline void set__messageEventQueue_27(Queue_1_t938D568900761CA7F7E7B98B7B3FCE6EA8FC15FC * value)
	{
		____messageEventQueue_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____messageEventQueue_27), (void*)value);
	}

	inline static int32_t get_offset_of__nonceCount_28() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____nonceCount_28)); }
	inline uint32_t get__nonceCount_28() const { return ____nonceCount_28; }
	inline uint32_t* get_address_of__nonceCount_28() { return &____nonceCount_28; }
	inline void set__nonceCount_28(uint32_t value)
	{
		____nonceCount_28 = value;
	}

	inline static int32_t get_offset_of__origin_29() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____origin_29)); }
	inline String_t* get__origin_29() const { return ____origin_29; }
	inline String_t** get_address_of__origin_29() { return &____origin_29; }
	inline void set__origin_29(String_t* value)
	{
		____origin_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____origin_29), (void*)value);
	}

	inline static int32_t get_offset_of__pongReceived_30() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____pongReceived_30)); }
	inline ManualResetEvent_t9E2ED486907E3A16122ED4E946534E4DD6B5A7BA * get__pongReceived_30() const { return ____pongReceived_30; }
	inline ManualResetEvent_t9E2ED486907E3A16122ED4E946534E4DD6B5A7BA ** get_address_of__pongReceived_30() { return &____pongReceived_30; }
	inline void set__pongReceived_30(ManualResetEvent_t9E2ED486907E3A16122ED4E946534E4DD6B5A7BA * value)
	{
		____pongReceived_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pongReceived_30), (void*)value);
	}

	inline static int32_t get_offset_of__preAuth_31() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____preAuth_31)); }
	inline bool get__preAuth_31() const { return ____preAuth_31; }
	inline bool* get_address_of__preAuth_31() { return &____preAuth_31; }
	inline void set__preAuth_31(bool value)
	{
		____preAuth_31 = value;
	}

	inline static int32_t get_offset_of__protocol_32() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____protocol_32)); }
	inline String_t* get__protocol_32() const { return ____protocol_32; }
	inline String_t** get_address_of__protocol_32() { return &____protocol_32; }
	inline void set__protocol_32(String_t* value)
	{
		____protocol_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____protocol_32), (void*)value);
	}

	inline static int32_t get_offset_of__protocols_33() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____protocols_33)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__protocols_33() const { return ____protocols_33; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__protocols_33() { return &____protocols_33; }
	inline void set__protocols_33(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____protocols_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____protocols_33), (void*)value);
	}

	inline static int32_t get_offset_of__protocolsRequested_34() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____protocolsRequested_34)); }
	inline bool get__protocolsRequested_34() const { return ____protocolsRequested_34; }
	inline bool* get_address_of__protocolsRequested_34() { return &____protocolsRequested_34; }
	inline void set__protocolsRequested_34(bool value)
	{
		____protocolsRequested_34 = value;
	}

	inline static int32_t get_offset_of__proxyCredentials_35() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____proxyCredentials_35)); }
	inline NetworkCredential_t2141776593D76A04BF41A10E1BDC9D7E25FEF378 * get__proxyCredentials_35() const { return ____proxyCredentials_35; }
	inline NetworkCredential_t2141776593D76A04BF41A10E1BDC9D7E25FEF378 ** get_address_of__proxyCredentials_35() { return &____proxyCredentials_35; }
	inline void set__proxyCredentials_35(NetworkCredential_t2141776593D76A04BF41A10E1BDC9D7E25FEF378 * value)
	{
		____proxyCredentials_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____proxyCredentials_35), (void*)value);
	}

	inline static int32_t get_offset_of__proxyUri_36() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____proxyUri_36)); }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * get__proxyUri_36() const { return ____proxyUri_36; }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 ** get_address_of__proxyUri_36() { return &____proxyUri_36; }
	inline void set__proxyUri_36(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * value)
	{
		____proxyUri_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____proxyUri_36), (void*)value);
	}

	inline static int32_t get_offset_of__readyState_37() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____readyState_37)); }
	inline uint16_t get__readyState_37() const { return ____readyState_37; }
	inline uint16_t* get_address_of__readyState_37() { return &____readyState_37; }
	inline void set__readyState_37(uint16_t value)
	{
		____readyState_37 = value;
	}

	inline static int32_t get_offset_of__receivingExited_38() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____receivingExited_38)); }
	inline ManualResetEvent_t9E2ED486907E3A16122ED4E946534E4DD6B5A7BA * get__receivingExited_38() const { return ____receivingExited_38; }
	inline ManualResetEvent_t9E2ED486907E3A16122ED4E946534E4DD6B5A7BA ** get_address_of__receivingExited_38() { return &____receivingExited_38; }
	inline void set__receivingExited_38(ManualResetEvent_t9E2ED486907E3A16122ED4E946534E4DD6B5A7BA * value)
	{
		____receivingExited_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____receivingExited_38), (void*)value);
	}

	inline static int32_t get_offset_of__retryCountForConnect_39() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____retryCountForConnect_39)); }
	inline int32_t get__retryCountForConnect_39() const { return ____retryCountForConnect_39; }
	inline int32_t* get_address_of__retryCountForConnect_39() { return &____retryCountForConnect_39; }
	inline void set__retryCountForConnect_39(int32_t value)
	{
		____retryCountForConnect_39 = value;
	}

	inline static int32_t get_offset_of__secure_40() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____secure_40)); }
	inline bool get__secure_40() const { return ____secure_40; }
	inline bool* get_address_of__secure_40() { return &____secure_40; }
	inline void set__secure_40(bool value)
	{
		____secure_40 = value;
	}

	inline static int32_t get_offset_of__sslConfig_41() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____sslConfig_41)); }
	inline ClientSslConfiguration_t8F870FAD51C116D6E675E7F256D6CD3BDF3AD58B * get__sslConfig_41() const { return ____sslConfig_41; }
	inline ClientSslConfiguration_t8F870FAD51C116D6E675E7F256D6CD3BDF3AD58B ** get_address_of__sslConfig_41() { return &____sslConfig_41; }
	inline void set__sslConfig_41(ClientSslConfiguration_t8F870FAD51C116D6E675E7F256D6CD3BDF3AD58B * value)
	{
		____sslConfig_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sslConfig_41), (void*)value);
	}

	inline static int32_t get_offset_of__stream_42() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____stream_42)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get__stream_42() const { return ____stream_42; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of__stream_42() { return &____stream_42; }
	inline void set__stream_42(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		____stream_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stream_42), (void*)value);
	}

	inline static int32_t get_offset_of__tcpClient_43() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____tcpClient_43)); }
	inline TcpClient_t0EEB05EA031F6AFD93D46116F5E33A9C4E3350EE * get__tcpClient_43() const { return ____tcpClient_43; }
	inline TcpClient_t0EEB05EA031F6AFD93D46116F5E33A9C4E3350EE ** get_address_of__tcpClient_43() { return &____tcpClient_43; }
	inline void set__tcpClient_43(TcpClient_t0EEB05EA031F6AFD93D46116F5E33A9C4E3350EE * value)
	{
		____tcpClient_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____tcpClient_43), (void*)value);
	}

	inline static int32_t get_offset_of__uri_44() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____uri_44)); }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * get__uri_44() const { return ____uri_44; }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 ** get_address_of__uri_44() { return &____uri_44; }
	inline void set__uri_44(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * value)
	{
		____uri_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____uri_44), (void*)value);
	}

	inline static int32_t get_offset_of__waitTime_46() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ____waitTime_46)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get__waitTime_46() const { return ____waitTime_46; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of__waitTime_46() { return &____waitTime_46; }
	inline void set__waitTime_46(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		____waitTime_46 = value;
	}

	inline static int32_t get_offset_of_OnClose_50() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ___OnClose_50)); }
	inline EventHandler_1_t36F607B09B5F9A711346F2AF00EF3C6881F94446 * get_OnClose_50() const { return ___OnClose_50; }
	inline EventHandler_1_t36F607B09B5F9A711346F2AF00EF3C6881F94446 ** get_address_of_OnClose_50() { return &___OnClose_50; }
	inline void set_OnClose_50(EventHandler_1_t36F607B09B5F9A711346F2AF00EF3C6881F94446 * value)
	{
		___OnClose_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnClose_50), (void*)value);
	}

	inline static int32_t get_offset_of_OnError_51() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ___OnError_51)); }
	inline EventHandler_1_tBD1E780AE74FB5A7F72179606D77FF0BDF0C5179 * get_OnError_51() const { return ___OnError_51; }
	inline EventHandler_1_tBD1E780AE74FB5A7F72179606D77FF0BDF0C5179 ** get_address_of_OnError_51() { return &___OnError_51; }
	inline void set_OnError_51(EventHandler_1_tBD1E780AE74FB5A7F72179606D77FF0BDF0C5179 * value)
	{
		___OnError_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnError_51), (void*)value);
	}

	inline static int32_t get_offset_of_OnMessage_52() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ___OnMessage_52)); }
	inline EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 * get_OnMessage_52() const { return ___OnMessage_52; }
	inline EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 ** get_address_of_OnMessage_52() { return &___OnMessage_52; }
	inline void set_OnMessage_52(EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 * value)
	{
		___OnMessage_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnMessage_52), (void*)value);
	}

	inline static int32_t get_offset_of_OnOpen_53() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC, ___OnOpen_53)); }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * get_OnOpen_53() const { return ___OnOpen_53; }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B ** get_address_of_OnOpen_53() { return &___OnOpen_53; }
	inline void set_OnOpen_53(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * value)
	{
		___OnOpen_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnOpen_53), (void*)value);
	}
};

struct WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_StaticFields
{
public:
	// System.Int32 WebSocketSharp.WebSocket::_maxRetryCountForConnect
	int32_t ____maxRetryCountForConnect_25;
	// System.Byte[] WebSocketSharp.WebSocket::EmptyBytes
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___EmptyBytes_47;
	// System.Int32 WebSocketSharp.WebSocket::FragmentLength
	int32_t ___FragmentLength_48;
	// System.Security.Cryptography.RandomNumberGenerator WebSocketSharp.WebSocket::RandomNumber
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ___RandomNumber_49;

public:
	inline static int32_t get_offset_of__maxRetryCountForConnect_25() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_StaticFields, ____maxRetryCountForConnect_25)); }
	inline int32_t get__maxRetryCountForConnect_25() const { return ____maxRetryCountForConnect_25; }
	inline int32_t* get_address_of__maxRetryCountForConnect_25() { return &____maxRetryCountForConnect_25; }
	inline void set__maxRetryCountForConnect_25(int32_t value)
	{
		____maxRetryCountForConnect_25 = value;
	}

	inline static int32_t get_offset_of_EmptyBytes_47() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_StaticFields, ___EmptyBytes_47)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_EmptyBytes_47() const { return ___EmptyBytes_47; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_EmptyBytes_47() { return &___EmptyBytes_47; }
	inline void set_EmptyBytes_47(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___EmptyBytes_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyBytes_47), (void*)value);
	}

	inline static int32_t get_offset_of_FragmentLength_48() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_StaticFields, ___FragmentLength_48)); }
	inline int32_t get_FragmentLength_48() const { return ___FragmentLength_48; }
	inline int32_t* get_address_of_FragmentLength_48() { return &___FragmentLength_48; }
	inline void set_FragmentLength_48(int32_t value)
	{
		___FragmentLength_48 = value;
	}

	inline static int32_t get_offset_of_RandomNumber_49() { return static_cast<int32_t>(offsetof(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_StaticFields, ___RandomNumber_49)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get_RandomNumber_49() const { return ___RandomNumber_49; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of_RandomNumber_49() { return &___RandomNumber_49; }
	inline void set_RandomNumber_49(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		___RandomNumber_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RandomNumber_49), (void*)value);
	}
};


// System.EventHandler`1<WebSocketSharp.MessageEventArgs>
struct EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// NewBehaviourScript
struct NewBehaviourScript_t5ED8D4BD4668DBECFEFD841BD2A8CC1D8FCF38CB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Player
struct Player_t5689617909B48F7640EA0892D85C92C13CC22C6F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String Player::id
	String_t* ___id_4;
	// System.String Player::username
	String_t* ___username_5;

public:
	inline static int32_t get_offset_of_id_4() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___id_4)); }
	inline String_t* get_id_4() const { return ___id_4; }
	inline String_t** get_address_of_id_4() { return &___id_4; }
	inline void set_id_4(String_t* value)
	{
		___id_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_4), (void*)value);
	}

	inline static int32_t get_offset_of_username_5() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___username_5)); }
	inline String_t* get_username_5() const { return ___username_5; }
	inline String_t** get_address_of_username_5() { return &___username_5; }
	inline void set_username_5(String_t* value)
	{
		___username_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___username_5), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// User
struct User_t73C2C8C9B5B5C1F356AA134EC0B50FC072360F77  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String User::id
	String_t* ___id_4;
	// System.String User::username
	String_t* ___username_5;

public:
	inline static int32_t get_offset_of_id_4() { return static_cast<int32_t>(offsetof(User_t73C2C8C9B5B5C1F356AA134EC0B50FC072360F77, ___id_4)); }
	inline String_t* get_id_4() const { return ___id_4; }
	inline String_t** get_address_of_id_4() { return &___id_4; }
	inline void set_id_4(String_t* value)
	{
		___id_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_4), (void*)value);
	}

	inline static int32_t get_offset_of_username_5() { return static_cast<int32_t>(offsetof(User_t73C2C8C9B5B5C1F356AA134EC0B50FC072360F77, ___username_5)); }
	inline String_t* get_username_5() const { return ___username_5; }
	inline String_t** get_address_of_username_5() { return &___username_5; }
	inline void set_username_5(String_t* value)
	{
		___username_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___username_5), (void*)value);
	}
};


// WS_Client
struct WS_Client_tF30FD0EC57F32A91C0F9A49A618FDCF2106FF041  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// WebSocketSharp.WebSocket WS_Client::ws
	WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * ___ws_4;
	// Player WS_Client::user
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * ___user_5;

public:
	inline static int32_t get_offset_of_ws_4() { return static_cast<int32_t>(offsetof(WS_Client_tF30FD0EC57F32A91C0F9A49A618FDCF2106FF041, ___ws_4)); }
	inline WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * get_ws_4() const { return ___ws_4; }
	inline WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC ** get_address_of_ws_4() { return &___ws_4; }
	inline void set_ws_4(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * value)
	{
		___ws_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ws_4), (void*)value);
	}

	inline static int32_t get_offset_of_user_5() { return static_cast<int32_t>(offsetof(WS_Client_tF30FD0EC57F32A91C0F9A49A618FDCF2106FF041, ___user_5)); }
	inline Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * get_user_5() const { return ___user_5; }
	inline Player_t5689617909B48F7640EA0892D85C92C13CC22C6F ** get_address_of_user_5() { return &___user_5; }
	inline void set_user_5(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * value)
	{
		___user_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___user_5), (void*)value);
	}
};


// bloc
struct bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// mainMenu bloc::mainMenu
	mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339 * ___mainMenu_4;
	// UnityEngine.SpriteRenderer bloc::spriteRen
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___spriteRen_5;
	// UnityEngine.Sprite bloc::Ren0
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___Ren0_6;
	// UnityEngine.Sprite bloc::Ren1
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___Ren1_7;
	// System.Boolean bloc::isSet
	bool ___isSet_8;

public:
	inline static int32_t get_offset_of_mainMenu_4() { return static_cast<int32_t>(offsetof(bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7, ___mainMenu_4)); }
	inline mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339 * get_mainMenu_4() const { return ___mainMenu_4; }
	inline mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339 ** get_address_of_mainMenu_4() { return &___mainMenu_4; }
	inline void set_mainMenu_4(mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339 * value)
	{
		___mainMenu_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainMenu_4), (void*)value);
	}

	inline static int32_t get_offset_of_spriteRen_5() { return static_cast<int32_t>(offsetof(bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7, ___spriteRen_5)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_spriteRen_5() const { return ___spriteRen_5; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_spriteRen_5() { return &___spriteRen_5; }
	inline void set_spriteRen_5(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___spriteRen_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteRen_5), (void*)value);
	}

	inline static int32_t get_offset_of_Ren0_6() { return static_cast<int32_t>(offsetof(bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7, ___Ren0_6)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_Ren0_6() const { return ___Ren0_6; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_Ren0_6() { return &___Ren0_6; }
	inline void set_Ren0_6(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___Ren0_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Ren0_6), (void*)value);
	}

	inline static int32_t get_offset_of_Ren1_7() { return static_cast<int32_t>(offsetof(bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7, ___Ren1_7)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_Ren1_7() const { return ___Ren1_7; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_Ren1_7() { return &___Ren1_7; }
	inline void set_Ren1_7(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___Ren1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Ren1_7), (void*)value);
	}

	inline static int32_t get_offset_of_isSet_8() { return static_cast<int32_t>(offsetof(bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7, ___isSet_8)); }
	inline bool get_isSet_8() const { return ___isSet_8; }
	inline bool* get_address_of_isSet_8() { return &___isSet_8; }
	inline void set_isSet_8(bool value)
	{
		___isSet_8 = value;
	}
};


// bodyCtrl
struct bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 bodyCtrl::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_4;
	// System.Single bodyCtrl::rotation
	float ___rotation_5;
	// System.Single bodyCtrl::rotVar
	float ___rotVar_6;
	// System.Boolean bodyCtrl::isAngle
	bool ___isAngle_7;
	// UnityEngine.SpriteRenderer bodyCtrl::spriteRen
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___spriteRen_8;
	// UnityEngine.Sprite bodyCtrl::straightSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___straightSprite_9;
	// UnityEngine.Sprite bodyCtrl::angleSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___angleSprite_10;
	// mapGen bodyCtrl::stdGrid
	mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * ___stdGrid_11;

public:
	inline static int32_t get_offset_of_position_4() { return static_cast<int32_t>(offsetof(bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D, ___position_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_4() const { return ___position_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_4() { return &___position_4; }
	inline void set_position_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_4 = value;
	}

	inline static int32_t get_offset_of_rotation_5() { return static_cast<int32_t>(offsetof(bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D, ___rotation_5)); }
	inline float get_rotation_5() const { return ___rotation_5; }
	inline float* get_address_of_rotation_5() { return &___rotation_5; }
	inline void set_rotation_5(float value)
	{
		___rotation_5 = value;
	}

	inline static int32_t get_offset_of_rotVar_6() { return static_cast<int32_t>(offsetof(bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D, ___rotVar_6)); }
	inline float get_rotVar_6() const { return ___rotVar_6; }
	inline float* get_address_of_rotVar_6() { return &___rotVar_6; }
	inline void set_rotVar_6(float value)
	{
		___rotVar_6 = value;
	}

	inline static int32_t get_offset_of_isAngle_7() { return static_cast<int32_t>(offsetof(bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D, ___isAngle_7)); }
	inline bool get_isAngle_7() const { return ___isAngle_7; }
	inline bool* get_address_of_isAngle_7() { return &___isAngle_7; }
	inline void set_isAngle_7(bool value)
	{
		___isAngle_7 = value;
	}

	inline static int32_t get_offset_of_spriteRen_8() { return static_cast<int32_t>(offsetof(bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D, ___spriteRen_8)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_spriteRen_8() const { return ___spriteRen_8; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_spriteRen_8() { return &___spriteRen_8; }
	inline void set_spriteRen_8(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___spriteRen_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteRen_8), (void*)value);
	}

	inline static int32_t get_offset_of_straightSprite_9() { return static_cast<int32_t>(offsetof(bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D, ___straightSprite_9)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_straightSprite_9() const { return ___straightSprite_9; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_straightSprite_9() { return &___straightSprite_9; }
	inline void set_straightSprite_9(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___straightSprite_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___straightSprite_9), (void*)value);
	}

	inline static int32_t get_offset_of_angleSprite_10() { return static_cast<int32_t>(offsetof(bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D, ___angleSprite_10)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_angleSprite_10() const { return ___angleSprite_10; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_angleSprite_10() { return &___angleSprite_10; }
	inline void set_angleSprite_10(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___angleSprite_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___angleSprite_10), (void*)value);
	}

	inline static int32_t get_offset_of_stdGrid_11() { return static_cast<int32_t>(offsetof(bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D, ___stdGrid_11)); }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * get_stdGrid_11() const { return ___stdGrid_11; }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 ** get_address_of_stdGrid_11() { return &___stdGrid_11; }
	inline void set_stdGrid_11(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * value)
	{
		___stdGrid_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stdGrid_11), (void*)value);
	}
};


// button
struct button_t4F901F446B79F86CB9593995276166F7ADD407E6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 button::scene
	int32_t ___scene_4;
	// UnityEngine.SpriteRenderer button::spriteRen
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___spriteRen_5;
	// UnityEngine.Sprite button::Ren0
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___Ren0_6;
	// UnityEngine.Sprite button::Ren1
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___Ren1_7;
	// System.Boolean button::isSet
	bool ___isSet_8;

public:
	inline static int32_t get_offset_of_scene_4() { return static_cast<int32_t>(offsetof(button_t4F901F446B79F86CB9593995276166F7ADD407E6, ___scene_4)); }
	inline int32_t get_scene_4() const { return ___scene_4; }
	inline int32_t* get_address_of_scene_4() { return &___scene_4; }
	inline void set_scene_4(int32_t value)
	{
		___scene_4 = value;
	}

	inline static int32_t get_offset_of_spriteRen_5() { return static_cast<int32_t>(offsetof(button_t4F901F446B79F86CB9593995276166F7ADD407E6, ___spriteRen_5)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_spriteRen_5() const { return ___spriteRen_5; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_spriteRen_5() { return &___spriteRen_5; }
	inline void set_spriteRen_5(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___spriteRen_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteRen_5), (void*)value);
	}

	inline static int32_t get_offset_of_Ren0_6() { return static_cast<int32_t>(offsetof(button_t4F901F446B79F86CB9593995276166F7ADD407E6, ___Ren0_6)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_Ren0_6() const { return ___Ren0_6; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_Ren0_6() { return &___Ren0_6; }
	inline void set_Ren0_6(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___Ren0_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Ren0_6), (void*)value);
	}

	inline static int32_t get_offset_of_Ren1_7() { return static_cast<int32_t>(offsetof(button_t4F901F446B79F86CB9593995276166F7ADD407E6, ___Ren1_7)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_Ren1_7() const { return ___Ren1_7; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_Ren1_7() { return &___Ren1_7; }
	inline void set_Ren1_7(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___Ren1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Ren1_7), (void*)value);
	}

	inline static int32_t get_offset_of_isSet_8() { return static_cast<int32_t>(offsetof(button_t4F901F446B79F86CB9593995276166F7ADD407E6, ___isSet_8)); }
	inline bool get_isSet_8() const { return ___isSet_8; }
	inline bool* get_address_of_isSet_8() { return &___isSet_8; }
	inline void set_isSet_8(bool value)
	{
		___isSet_8 = value;
	}
};


// cell
struct cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean cell::isObstacle
	bool ___isObstacle_4;
	// System.Boolean cell::isCellFood
	bool ___isCellFood_5;
	// System.Boolean cell::isCellSnek
	bool ___isCellSnek_6;
	// System.Int32 cell::mapX
	int32_t ___mapX_7;
	// System.Int32 cell::mapY
	int32_t ___mapY_8;
	// UnityEngine.SpriteRenderer cell::spriteRen
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___spriteRen_9;
	// UnityEngine.Sprite cell::obstSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___obstSprite_10;
	// UnityEngine.Vector2 cell::coords
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___coords_11;

public:
	inline static int32_t get_offset_of_isObstacle_4() { return static_cast<int32_t>(offsetof(cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF, ___isObstacle_4)); }
	inline bool get_isObstacle_4() const { return ___isObstacle_4; }
	inline bool* get_address_of_isObstacle_4() { return &___isObstacle_4; }
	inline void set_isObstacle_4(bool value)
	{
		___isObstacle_4 = value;
	}

	inline static int32_t get_offset_of_isCellFood_5() { return static_cast<int32_t>(offsetof(cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF, ___isCellFood_5)); }
	inline bool get_isCellFood_5() const { return ___isCellFood_5; }
	inline bool* get_address_of_isCellFood_5() { return &___isCellFood_5; }
	inline void set_isCellFood_5(bool value)
	{
		___isCellFood_5 = value;
	}

	inline static int32_t get_offset_of_isCellSnek_6() { return static_cast<int32_t>(offsetof(cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF, ___isCellSnek_6)); }
	inline bool get_isCellSnek_6() const { return ___isCellSnek_6; }
	inline bool* get_address_of_isCellSnek_6() { return &___isCellSnek_6; }
	inline void set_isCellSnek_6(bool value)
	{
		___isCellSnek_6 = value;
	}

	inline static int32_t get_offset_of_mapX_7() { return static_cast<int32_t>(offsetof(cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF, ___mapX_7)); }
	inline int32_t get_mapX_7() const { return ___mapX_7; }
	inline int32_t* get_address_of_mapX_7() { return &___mapX_7; }
	inline void set_mapX_7(int32_t value)
	{
		___mapX_7 = value;
	}

	inline static int32_t get_offset_of_mapY_8() { return static_cast<int32_t>(offsetof(cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF, ___mapY_8)); }
	inline int32_t get_mapY_8() const { return ___mapY_8; }
	inline int32_t* get_address_of_mapY_8() { return &___mapY_8; }
	inline void set_mapY_8(int32_t value)
	{
		___mapY_8 = value;
	}

	inline static int32_t get_offset_of_spriteRen_9() { return static_cast<int32_t>(offsetof(cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF, ___spriteRen_9)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_spriteRen_9() const { return ___spriteRen_9; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_spriteRen_9() { return &___spriteRen_9; }
	inline void set_spriteRen_9(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___spriteRen_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteRen_9), (void*)value);
	}

	inline static int32_t get_offset_of_obstSprite_10() { return static_cast<int32_t>(offsetof(cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF, ___obstSprite_10)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_obstSprite_10() const { return ___obstSprite_10; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_obstSprite_10() { return &___obstSprite_10; }
	inline void set_obstSprite_10(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___obstSprite_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___obstSprite_10), (void*)value);
	}

	inline static int32_t get_offset_of_coords_11() { return static_cast<int32_t>(offsetof(cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF, ___coords_11)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_coords_11() const { return ___coords_11; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_coords_11() { return &___coords_11; }
	inline void set_coords_11(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___coords_11 = value;
	}
};


// currCell
struct currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject currCell::stdGrid
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___stdGrid_4;
	// cell currCell::actual
	cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * ___actual_5;
	// mapGen currCell::temp
	mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * ___temp_6;
	// playerControl currCell::controller
	playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * ___controller_7;

public:
	inline static int32_t get_offset_of_stdGrid_4() { return static_cast<int32_t>(offsetof(currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42, ___stdGrid_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_stdGrid_4() const { return ___stdGrid_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_stdGrid_4() { return &___stdGrid_4; }
	inline void set_stdGrid_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___stdGrid_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stdGrid_4), (void*)value);
	}

	inline static int32_t get_offset_of_actual_5() { return static_cast<int32_t>(offsetof(currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42, ___actual_5)); }
	inline cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * get_actual_5() const { return ___actual_5; }
	inline cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF ** get_address_of_actual_5() { return &___actual_5; }
	inline void set_actual_5(cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * value)
	{
		___actual_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___actual_5), (void*)value);
	}

	inline static int32_t get_offset_of_temp_6() { return static_cast<int32_t>(offsetof(currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42, ___temp_6)); }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * get_temp_6() const { return ___temp_6; }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 ** get_address_of_temp_6() { return &___temp_6; }
	inline void set_temp_6(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * value)
	{
		___temp_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___temp_6), (void*)value);
	}

	inline static int32_t get_offset_of_controller_7() { return static_cast<int32_t>(offsetof(currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42, ___controller_7)); }
	inline playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * get_controller_7() const { return ___controller_7; }
	inline playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 ** get_address_of_controller_7() { return &___controller_7; }
	inline void set_controller_7(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * value)
	{
		___controller_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controller_7), (void*)value);
	}
};


// food
struct food_t03A1F65F99660B06E1CB5BF29E080CD11F687652  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean food::eaten
	bool ___eaten_4;
	// System.Int32 food::foodX
	int32_t ___foodX_5;
	// System.Int32 food::foodY
	int32_t ___foodY_6;
	// System.Int32 food::mealsCount
	int32_t ___mealsCount_7;
	// System.Boolean food::isFoodOn
	bool ___isFoodOn_8;
	// mapGen food::temp
	mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * ___temp_9;
	// cell food::apple
	cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * ___apple_10;
	// UnityEngine.UI.Text food::txt
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txt_11;

public:
	inline static int32_t get_offset_of_eaten_4() { return static_cast<int32_t>(offsetof(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652, ___eaten_4)); }
	inline bool get_eaten_4() const { return ___eaten_4; }
	inline bool* get_address_of_eaten_4() { return &___eaten_4; }
	inline void set_eaten_4(bool value)
	{
		___eaten_4 = value;
	}

	inline static int32_t get_offset_of_foodX_5() { return static_cast<int32_t>(offsetof(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652, ___foodX_5)); }
	inline int32_t get_foodX_5() const { return ___foodX_5; }
	inline int32_t* get_address_of_foodX_5() { return &___foodX_5; }
	inline void set_foodX_5(int32_t value)
	{
		___foodX_5 = value;
	}

	inline static int32_t get_offset_of_foodY_6() { return static_cast<int32_t>(offsetof(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652, ___foodY_6)); }
	inline int32_t get_foodY_6() const { return ___foodY_6; }
	inline int32_t* get_address_of_foodY_6() { return &___foodY_6; }
	inline void set_foodY_6(int32_t value)
	{
		___foodY_6 = value;
	}

	inline static int32_t get_offset_of_mealsCount_7() { return static_cast<int32_t>(offsetof(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652, ___mealsCount_7)); }
	inline int32_t get_mealsCount_7() const { return ___mealsCount_7; }
	inline int32_t* get_address_of_mealsCount_7() { return &___mealsCount_7; }
	inline void set_mealsCount_7(int32_t value)
	{
		___mealsCount_7 = value;
	}

	inline static int32_t get_offset_of_isFoodOn_8() { return static_cast<int32_t>(offsetof(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652, ___isFoodOn_8)); }
	inline bool get_isFoodOn_8() const { return ___isFoodOn_8; }
	inline bool* get_address_of_isFoodOn_8() { return &___isFoodOn_8; }
	inline void set_isFoodOn_8(bool value)
	{
		___isFoodOn_8 = value;
	}

	inline static int32_t get_offset_of_temp_9() { return static_cast<int32_t>(offsetof(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652, ___temp_9)); }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * get_temp_9() const { return ___temp_9; }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 ** get_address_of_temp_9() { return &___temp_9; }
	inline void set_temp_9(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * value)
	{
		___temp_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___temp_9), (void*)value);
	}

	inline static int32_t get_offset_of_apple_10() { return static_cast<int32_t>(offsetof(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652, ___apple_10)); }
	inline cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * get_apple_10() const { return ___apple_10; }
	inline cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF ** get_address_of_apple_10() { return &___apple_10; }
	inline void set_apple_10(cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * value)
	{
		___apple_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___apple_10), (void*)value);
	}

	inline static int32_t get_offset_of_txt_11() { return static_cast<int32_t>(offsetof(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652, ___txt_11)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txt_11() const { return ___txt_11; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txt_11() { return &___txt_11; }
	inline void set_txt_11(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txt_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txt_11), (void*)value);
	}
};

struct food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_StaticFields
{
public:
	// System.Int32 food::score
	int32_t ___score_12;

public:
	inline static int32_t get_offset_of_score_12() { return static_cast<int32_t>(offsetof(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_StaticFields, ___score_12)); }
	inline int32_t get_score_12() const { return ___score_12; }
	inline int32_t* get_address_of_score_12() { return &___score_12; }
	inline void set_score_12(int32_t value)
	{
		___score_12 = value;
	}
};


// gameEnd
struct gameEnd_t0BD1FD2C540A61239AC275D8EB7F722AAC1A7DCA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject gameEnd::endBloc
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___endBloc_4;
	// UnityEngine.UI.Text gameEnd::txt
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txt_5;
	// UnityEngine.UI.Text gameEnd::txt2
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txt2_6;
	// System.String gameEnd::highScore
	String_t* ___highScore_7;

public:
	inline static int32_t get_offset_of_endBloc_4() { return static_cast<int32_t>(offsetof(gameEnd_t0BD1FD2C540A61239AC275D8EB7F722AAC1A7DCA, ___endBloc_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_endBloc_4() const { return ___endBloc_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_endBloc_4() { return &___endBloc_4; }
	inline void set_endBloc_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___endBloc_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___endBloc_4), (void*)value);
	}

	inline static int32_t get_offset_of_txt_5() { return static_cast<int32_t>(offsetof(gameEnd_t0BD1FD2C540A61239AC275D8EB7F722AAC1A7DCA, ___txt_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txt_5() const { return ___txt_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txt_5() { return &___txt_5; }
	inline void set_txt_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txt_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txt_5), (void*)value);
	}

	inline static int32_t get_offset_of_txt2_6() { return static_cast<int32_t>(offsetof(gameEnd_t0BD1FD2C540A61239AC275D8EB7F722AAC1A7DCA, ___txt2_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txt2_6() const { return ___txt2_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txt2_6() { return &___txt2_6; }
	inline void set_txt2_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txt2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txt2_6), (void*)value);
	}

	inline static int32_t get_offset_of_highScore_7() { return static_cast<int32_t>(offsetof(gameEnd_t0BD1FD2C540A61239AC275D8EB7F722AAC1A7DCA, ___highScore_7)); }
	inline String_t* get_highScore_7() const { return ___highScore_7; }
	inline String_t** get_address_of_highScore_7() { return &___highScore_7; }
	inline void set_highScore_7(String_t* value)
	{
		___highScore_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___highScore_7), (void*)value);
	}
};


// level0Config
struct level0Config_t922848EFFCF7A30778900D21F26B5E498E142360  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// mapGen level0Config::gridGen
	mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * ___gridGen_4;
	// UnityEngine.GameObject level0Config::cellGen
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cellGen_5;

public:
	inline static int32_t get_offset_of_gridGen_4() { return static_cast<int32_t>(offsetof(level0Config_t922848EFFCF7A30778900D21F26B5E498E142360, ___gridGen_4)); }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * get_gridGen_4() const { return ___gridGen_4; }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 ** get_address_of_gridGen_4() { return &___gridGen_4; }
	inline void set_gridGen_4(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * value)
	{
		___gridGen_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gridGen_4), (void*)value);
	}

	inline static int32_t get_offset_of_cellGen_5() { return static_cast<int32_t>(offsetof(level0Config_t922848EFFCF7A30778900D21F26B5E498E142360, ___cellGen_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cellGen_5() const { return ___cellGen_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cellGen_5() { return &___cellGen_5; }
	inline void set_cellGen_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cellGen_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cellGen_5), (void*)value);
	}
};


// level1Config
struct level1Config_t0953E2DCDD13E635D78702D7EE04DB61CE0CD534  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// mapGen level1Config::gridGen
	mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * ___gridGen_4;
	// UnityEngine.GameObject level1Config::cellGen
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cellGen_5;

public:
	inline static int32_t get_offset_of_gridGen_4() { return static_cast<int32_t>(offsetof(level1Config_t0953E2DCDD13E635D78702D7EE04DB61CE0CD534, ___gridGen_4)); }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * get_gridGen_4() const { return ___gridGen_4; }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 ** get_address_of_gridGen_4() { return &___gridGen_4; }
	inline void set_gridGen_4(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * value)
	{
		___gridGen_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gridGen_4), (void*)value);
	}

	inline static int32_t get_offset_of_cellGen_5() { return static_cast<int32_t>(offsetof(level1Config_t0953E2DCDD13E635D78702D7EE04DB61CE0CD534, ___cellGen_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cellGen_5() const { return ___cellGen_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cellGen_5() { return &___cellGen_5; }
	inline void set_cellGen_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cellGen_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cellGen_5), (void*)value);
	}
};


// level2Config
struct level2Config_t3AE5B1517E64CF7064471F2A15A0D043104F6074  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// mapGen level2Config::gridGen
	mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * ___gridGen_4;
	// UnityEngine.GameObject level2Config::cellGen
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cellGen_5;

public:
	inline static int32_t get_offset_of_gridGen_4() { return static_cast<int32_t>(offsetof(level2Config_t3AE5B1517E64CF7064471F2A15A0D043104F6074, ___gridGen_4)); }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * get_gridGen_4() const { return ___gridGen_4; }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 ** get_address_of_gridGen_4() { return &___gridGen_4; }
	inline void set_gridGen_4(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * value)
	{
		___gridGen_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gridGen_4), (void*)value);
	}

	inline static int32_t get_offset_of_cellGen_5() { return static_cast<int32_t>(offsetof(level2Config_t3AE5B1517E64CF7064471F2A15A0D043104F6074, ___cellGen_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cellGen_5() const { return ___cellGen_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cellGen_5() { return &___cellGen_5; }
	inline void set_cellGen_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cellGen_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cellGen_5), (void*)value);
	}
};


// level3Config
struct level3Config_tF052783C756D2BB48C5C9F7FD394EC7A37688853  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// mapGen level3Config::gridGen
	mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * ___gridGen_4;
	// UnityEngine.GameObject level3Config::cellGen
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cellGen_5;

public:
	inline static int32_t get_offset_of_gridGen_4() { return static_cast<int32_t>(offsetof(level3Config_tF052783C756D2BB48C5C9F7FD394EC7A37688853, ___gridGen_4)); }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * get_gridGen_4() const { return ___gridGen_4; }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 ** get_address_of_gridGen_4() { return &___gridGen_4; }
	inline void set_gridGen_4(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * value)
	{
		___gridGen_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gridGen_4), (void*)value);
	}

	inline static int32_t get_offset_of_cellGen_5() { return static_cast<int32_t>(offsetof(level3Config_tF052783C756D2BB48C5C9F7FD394EC7A37688853, ___cellGen_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cellGen_5() const { return ___cellGen_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cellGen_5() { return &___cellGen_5; }
	inline void set_cellGen_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cellGen_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cellGen_5), (void*)value);
	}
};


// level4Config
struct level4Config_t89718633F1C441AE885A0CA12B4D08C247CAC673  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// mapGen level4Config::gridGen
	mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * ___gridGen_4;
	// UnityEngine.GameObject level4Config::cellGen
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cellGen_5;

public:
	inline static int32_t get_offset_of_gridGen_4() { return static_cast<int32_t>(offsetof(level4Config_t89718633F1C441AE885A0CA12B4D08C247CAC673, ___gridGen_4)); }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * get_gridGen_4() const { return ___gridGen_4; }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 ** get_address_of_gridGen_4() { return &___gridGen_4; }
	inline void set_gridGen_4(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * value)
	{
		___gridGen_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gridGen_4), (void*)value);
	}

	inline static int32_t get_offset_of_cellGen_5() { return static_cast<int32_t>(offsetof(level4Config_t89718633F1C441AE885A0CA12B4D08C247CAC673, ___cellGen_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cellGen_5() const { return ___cellGen_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cellGen_5() { return &___cellGen_5; }
	inline void set_cellGen_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cellGen_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cellGen_5), (void*)value);
	}
};


// level5Config
struct level5Config_tC75394A13F0C653B2A28C5C7B7E4EFA690CB7CD2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// mapGen level5Config::gridGen
	mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * ___gridGen_4;
	// UnityEngine.GameObject level5Config::cellGen
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cellGen_5;

public:
	inline static int32_t get_offset_of_gridGen_4() { return static_cast<int32_t>(offsetof(level5Config_tC75394A13F0C653B2A28C5C7B7E4EFA690CB7CD2, ___gridGen_4)); }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * get_gridGen_4() const { return ___gridGen_4; }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 ** get_address_of_gridGen_4() { return &___gridGen_4; }
	inline void set_gridGen_4(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * value)
	{
		___gridGen_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gridGen_4), (void*)value);
	}

	inline static int32_t get_offset_of_cellGen_5() { return static_cast<int32_t>(offsetof(level5Config_tC75394A13F0C653B2A28C5C7B7E4EFA690CB7CD2, ___cellGen_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cellGen_5() const { return ___cellGen_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cellGen_5() { return &___cellGen_5; }
	inline void set_cellGen_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cellGen_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cellGen_5), (void*)value);
	}
};


// mainMenu
struct mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// bloc mainMenu::bloc
	bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7 * ___bloc_4;
	// UnityEngine.GameObject mainMenu::contBox
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___contBox_5;
	// UnityEngine.GameObject mainMenu::survBox
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___survBox_6;
	// UnityEngine.GameObject mainMenu::advBox
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___advBox_7;
	// UnityEngine.GameObject mainMenu::lvlsBox
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___lvlsBox_8;
	// UnityEngine.GameObject mainMenu::diffBox
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___diffBox_9;
	// UnityEngine.GameObject mainMenu::back
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___back_10;

public:
	inline static int32_t get_offset_of_bloc_4() { return static_cast<int32_t>(offsetof(mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339, ___bloc_4)); }
	inline bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7 * get_bloc_4() const { return ___bloc_4; }
	inline bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7 ** get_address_of_bloc_4() { return &___bloc_4; }
	inline void set_bloc_4(bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7 * value)
	{
		___bloc_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bloc_4), (void*)value);
	}

	inline static int32_t get_offset_of_contBox_5() { return static_cast<int32_t>(offsetof(mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339, ___contBox_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_contBox_5() const { return ___contBox_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_contBox_5() { return &___contBox_5; }
	inline void set_contBox_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___contBox_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contBox_5), (void*)value);
	}

	inline static int32_t get_offset_of_survBox_6() { return static_cast<int32_t>(offsetof(mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339, ___survBox_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_survBox_6() const { return ___survBox_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_survBox_6() { return &___survBox_6; }
	inline void set_survBox_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___survBox_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___survBox_6), (void*)value);
	}

	inline static int32_t get_offset_of_advBox_7() { return static_cast<int32_t>(offsetof(mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339, ___advBox_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_advBox_7() const { return ___advBox_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_advBox_7() { return &___advBox_7; }
	inline void set_advBox_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___advBox_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___advBox_7), (void*)value);
	}

	inline static int32_t get_offset_of_lvlsBox_8() { return static_cast<int32_t>(offsetof(mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339, ___lvlsBox_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_lvlsBox_8() const { return ___lvlsBox_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_lvlsBox_8() { return &___lvlsBox_8; }
	inline void set_lvlsBox_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___lvlsBox_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lvlsBox_8), (void*)value);
	}

	inline static int32_t get_offset_of_diffBox_9() { return static_cast<int32_t>(offsetof(mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339, ___diffBox_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_diffBox_9() const { return ___diffBox_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_diffBox_9() { return &___diffBox_9; }
	inline void set_diffBox_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___diffBox_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___diffBox_9), (void*)value);
	}

	inline static int32_t get_offset_of_back_10() { return static_cast<int32_t>(offsetof(mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339, ___back_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_back_10() const { return ___back_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_back_10() { return &___back_10; }
	inline void set_back_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___back_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___back_10), (void*)value);
	}
};


// mapGen
struct mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single mapGen::floatPosX
	float ___floatPosX_7;
	// System.Single mapGen::floatPosY
	float ___floatPosY_8;
	// System.Collections.ArrayList mapGen::map
	ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * ___map_10;

public:
	inline static int32_t get_offset_of_floatPosX_7() { return static_cast<int32_t>(offsetof(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8, ___floatPosX_7)); }
	inline float get_floatPosX_7() const { return ___floatPosX_7; }
	inline float* get_address_of_floatPosX_7() { return &___floatPosX_7; }
	inline void set_floatPosX_7(float value)
	{
		___floatPosX_7 = value;
	}

	inline static int32_t get_offset_of_floatPosY_8() { return static_cast<int32_t>(offsetof(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8, ___floatPosY_8)); }
	inline float get_floatPosY_8() const { return ___floatPosY_8; }
	inline float* get_address_of_floatPosY_8() { return &___floatPosY_8; }
	inline void set_floatPosY_8(float value)
	{
		___floatPosY_8 = value;
	}

	inline static int32_t get_offset_of_map_10() { return static_cast<int32_t>(offsetof(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8, ___map_10)); }
	inline ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * get_map_10() const { return ___map_10; }
	inline ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 ** get_address_of_map_10() { return &___map_10; }
	inline void set_map_10(ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * value)
	{
		___map_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___map_10), (void*)value);
	}
};

struct mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields
{
public:
	// System.Int32 mapGen::sizeX
	int32_t ___sizeX_4;
	// System.Int32 mapGen::sizeY
	int32_t ___sizeY_5;
	// UnityEngine.Vector2 mapGen::spawn
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___spawn_6;
	// System.Boolean mapGen::mapGenerated
	bool ___mapGenerated_9;

public:
	inline static int32_t get_offset_of_sizeX_4() { return static_cast<int32_t>(offsetof(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields, ___sizeX_4)); }
	inline int32_t get_sizeX_4() const { return ___sizeX_4; }
	inline int32_t* get_address_of_sizeX_4() { return &___sizeX_4; }
	inline void set_sizeX_4(int32_t value)
	{
		___sizeX_4 = value;
	}

	inline static int32_t get_offset_of_sizeY_5() { return static_cast<int32_t>(offsetof(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields, ___sizeY_5)); }
	inline int32_t get_sizeY_5() const { return ___sizeY_5; }
	inline int32_t* get_address_of_sizeY_5() { return &___sizeY_5; }
	inline void set_sizeY_5(int32_t value)
	{
		___sizeY_5 = value;
	}

	inline static int32_t get_offset_of_spawn_6() { return static_cast<int32_t>(offsetof(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields, ___spawn_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_spawn_6() const { return ___spawn_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_spawn_6() { return &___spawn_6; }
	inline void set_spawn_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___spawn_6 = value;
	}

	inline static int32_t get_offset_of_mapGenerated_9() { return static_cast<int32_t>(offsetof(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields, ___mapGenerated_9)); }
	inline bool get_mapGenerated_9() const { return ___mapGenerated_9; }
	inline bool* get_address_of_mapGenerated_9() { return &___mapGenerated_9; }
	inline void set_mapGenerated_9(bool value)
	{
		___mapGenerated_9 = value;
	}
};


// playerControl
struct playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 playerControl::moveSpeed
	int32_t ___moveSpeed_4;
	// System.Int32 playerControl::currCellX
	int32_t ___currCellX_5;
	// System.Int32 playerControl::currCellY
	int32_t ___currCellY_6;
	// System.Boolean playerControl::moveReq
	bool ___moveReq_7;
	// System.Boolean playerControl::isRotating
	bool ___isRotating_9;
	// UnityEngine.Vector3 playerControl::numericMove
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___numericMove_10;
	// UnityEngine.Vector3 playerControl::prevNumMove
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___prevNumMove_11;
	// System.Single playerControl::hRotation
	float ___hRotation_12;
	// System.Single playerControl::realRot
	float ___realRot_13;
	// UnityEngine.Vector3 playerControl::realMove
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___realMove_14;
	// UnityEngine.Vector3 playerControl::cubeMove
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___cubeMove_15;
	// UnityEngine.Vector3 playerControl::finalMove
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___finalMove_16;
	// mapGen playerControl::stdGrid
	mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * ___stdGrid_17;
	// currCell playerControl::currCell
	currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * ___currCell_18;
	// food playerControl::food
	food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * ___food_19;

public:
	inline static int32_t get_offset_of_moveSpeed_4() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___moveSpeed_4)); }
	inline int32_t get_moveSpeed_4() const { return ___moveSpeed_4; }
	inline int32_t* get_address_of_moveSpeed_4() { return &___moveSpeed_4; }
	inline void set_moveSpeed_4(int32_t value)
	{
		___moveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_currCellX_5() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___currCellX_5)); }
	inline int32_t get_currCellX_5() const { return ___currCellX_5; }
	inline int32_t* get_address_of_currCellX_5() { return &___currCellX_5; }
	inline void set_currCellX_5(int32_t value)
	{
		___currCellX_5 = value;
	}

	inline static int32_t get_offset_of_currCellY_6() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___currCellY_6)); }
	inline int32_t get_currCellY_6() const { return ___currCellY_6; }
	inline int32_t* get_address_of_currCellY_6() { return &___currCellY_6; }
	inline void set_currCellY_6(int32_t value)
	{
		___currCellY_6 = value;
	}

	inline static int32_t get_offset_of_moveReq_7() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___moveReq_7)); }
	inline bool get_moveReq_7() const { return ___moveReq_7; }
	inline bool* get_address_of_moveReq_7() { return &___moveReq_7; }
	inline void set_moveReq_7(bool value)
	{
		___moveReq_7 = value;
	}

	inline static int32_t get_offset_of_isRotating_9() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___isRotating_9)); }
	inline bool get_isRotating_9() const { return ___isRotating_9; }
	inline bool* get_address_of_isRotating_9() { return &___isRotating_9; }
	inline void set_isRotating_9(bool value)
	{
		___isRotating_9 = value;
	}

	inline static int32_t get_offset_of_numericMove_10() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___numericMove_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_numericMove_10() const { return ___numericMove_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_numericMove_10() { return &___numericMove_10; }
	inline void set_numericMove_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___numericMove_10 = value;
	}

	inline static int32_t get_offset_of_prevNumMove_11() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___prevNumMove_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_prevNumMove_11() const { return ___prevNumMove_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_prevNumMove_11() { return &___prevNumMove_11; }
	inline void set_prevNumMove_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___prevNumMove_11 = value;
	}

	inline static int32_t get_offset_of_hRotation_12() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___hRotation_12)); }
	inline float get_hRotation_12() const { return ___hRotation_12; }
	inline float* get_address_of_hRotation_12() { return &___hRotation_12; }
	inline void set_hRotation_12(float value)
	{
		___hRotation_12 = value;
	}

	inline static int32_t get_offset_of_realRot_13() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___realRot_13)); }
	inline float get_realRot_13() const { return ___realRot_13; }
	inline float* get_address_of_realRot_13() { return &___realRot_13; }
	inline void set_realRot_13(float value)
	{
		___realRot_13 = value;
	}

	inline static int32_t get_offset_of_realMove_14() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___realMove_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_realMove_14() const { return ___realMove_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_realMove_14() { return &___realMove_14; }
	inline void set_realMove_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___realMove_14 = value;
	}

	inline static int32_t get_offset_of_cubeMove_15() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___cubeMove_15)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_cubeMove_15() const { return ___cubeMove_15; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_cubeMove_15() { return &___cubeMove_15; }
	inline void set_cubeMove_15(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___cubeMove_15 = value;
	}

	inline static int32_t get_offset_of_finalMove_16() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___finalMove_16)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_finalMove_16() const { return ___finalMove_16; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_finalMove_16() { return &___finalMove_16; }
	inline void set_finalMove_16(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___finalMove_16 = value;
	}

	inline static int32_t get_offset_of_stdGrid_17() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___stdGrid_17)); }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * get_stdGrid_17() const { return ___stdGrid_17; }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 ** get_address_of_stdGrid_17() { return &___stdGrid_17; }
	inline void set_stdGrid_17(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * value)
	{
		___stdGrid_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stdGrid_17), (void*)value);
	}

	inline static int32_t get_offset_of_currCell_18() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___currCell_18)); }
	inline currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * get_currCell_18() const { return ___currCell_18; }
	inline currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 ** get_address_of_currCell_18() { return &___currCell_18; }
	inline void set_currCell_18(currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * value)
	{
		___currCell_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currCell_18), (void*)value);
	}

	inline static int32_t get_offset_of_food_19() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21, ___food_19)); }
	inline food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * get_food_19() const { return ___food_19; }
	inline food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 ** get_address_of_food_19() { return &___food_19; }
	inline void set_food_19(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * value)
	{
		___food_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___food_19), (void*)value);
	}
};

struct playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_StaticFields
{
public:
	// System.Boolean playerControl::alive
	bool ___alive_8;

public:
	inline static int32_t get_offset_of_alive_8() { return static_cast<int32_t>(offsetof(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_StaticFields, ___alive_8)); }
	inline bool get_alive_8() const { return ___alive_8; }
	inline bool* get_address_of_alive_8() { return &___alive_8; }
	inline void set_alive_8(bool value)
	{
		___alive_8 = value;
	}
};


// setScoresToZero
struct setScoresToZero_t83CA487B984141BA8548B2DB3F6A09827A4F9B2C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// snekOrg
struct snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> snekOrg::snekParts
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___snekParts_4;
	// UnityEngine.GameObject snekOrg::Head
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Head_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> snekOrg::partsPositions
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___partsPositions_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> snekOrg::partsRotations
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___partsRotations_7;
	// playerControl snekOrg::controller
	playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * ___controller_8;
	// mapGen snekOrg::stdGrid
	mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * ___stdGrid_9;
	// UnityEngine.GameObject snekOrg::Body0
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Body0_10;
	// UnityEngine.GameObject snekOrg::Body1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Body1_11;
	// UnityEngine.GameObject snekOrg::Tail
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Tail_12;
	// food snekOrg::food
	food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * ___food_13;
	// System.Int32 snekOrg::nbBodies
	int32_t ___nbBodies_14;
	// System.Int32 snekOrg::latestMealsCount
	int32_t ___latestMealsCount_15;

public:
	inline static int32_t get_offset_of_snekParts_4() { return static_cast<int32_t>(offsetof(snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1, ___snekParts_4)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_snekParts_4() const { return ___snekParts_4; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_snekParts_4() { return &___snekParts_4; }
	inline void set_snekParts_4(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___snekParts_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___snekParts_4), (void*)value);
	}

	inline static int32_t get_offset_of_Head_5() { return static_cast<int32_t>(offsetof(snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1, ___Head_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Head_5() const { return ___Head_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Head_5() { return &___Head_5; }
	inline void set_Head_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Head_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Head_5), (void*)value);
	}

	inline static int32_t get_offset_of_partsPositions_6() { return static_cast<int32_t>(offsetof(snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1, ___partsPositions_6)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_partsPositions_6() const { return ___partsPositions_6; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_partsPositions_6() { return &___partsPositions_6; }
	inline void set_partsPositions_6(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___partsPositions_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___partsPositions_6), (void*)value);
	}

	inline static int32_t get_offset_of_partsRotations_7() { return static_cast<int32_t>(offsetof(snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1, ___partsRotations_7)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_partsRotations_7() const { return ___partsRotations_7; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_partsRotations_7() { return &___partsRotations_7; }
	inline void set_partsRotations_7(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___partsRotations_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___partsRotations_7), (void*)value);
	}

	inline static int32_t get_offset_of_controller_8() { return static_cast<int32_t>(offsetof(snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1, ___controller_8)); }
	inline playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * get_controller_8() const { return ___controller_8; }
	inline playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 ** get_address_of_controller_8() { return &___controller_8; }
	inline void set_controller_8(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * value)
	{
		___controller_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controller_8), (void*)value);
	}

	inline static int32_t get_offset_of_stdGrid_9() { return static_cast<int32_t>(offsetof(snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1, ___stdGrid_9)); }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * get_stdGrid_9() const { return ___stdGrid_9; }
	inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 ** get_address_of_stdGrid_9() { return &___stdGrid_9; }
	inline void set_stdGrid_9(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * value)
	{
		___stdGrid_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stdGrid_9), (void*)value);
	}

	inline static int32_t get_offset_of_Body0_10() { return static_cast<int32_t>(offsetof(snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1, ___Body0_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Body0_10() const { return ___Body0_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Body0_10() { return &___Body0_10; }
	inline void set_Body0_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Body0_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Body0_10), (void*)value);
	}

	inline static int32_t get_offset_of_Body1_11() { return static_cast<int32_t>(offsetof(snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1, ___Body1_11)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Body1_11() const { return ___Body1_11; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Body1_11() { return &___Body1_11; }
	inline void set_Body1_11(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Body1_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Body1_11), (void*)value);
	}

	inline static int32_t get_offset_of_Tail_12() { return static_cast<int32_t>(offsetof(snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1, ___Tail_12)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Tail_12() const { return ___Tail_12; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Tail_12() { return &___Tail_12; }
	inline void set_Tail_12(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Tail_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Tail_12), (void*)value);
	}

	inline static int32_t get_offset_of_food_13() { return static_cast<int32_t>(offsetof(snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1, ___food_13)); }
	inline food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * get_food_13() const { return ___food_13; }
	inline food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 ** get_address_of_food_13() { return &___food_13; }
	inline void set_food_13(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * value)
	{
		___food_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___food_13), (void*)value);
	}

	inline static int32_t get_offset_of_nbBodies_14() { return static_cast<int32_t>(offsetof(snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1, ___nbBodies_14)); }
	inline int32_t get_nbBodies_14() const { return ___nbBodies_14; }
	inline int32_t* get_address_of_nbBodies_14() { return &___nbBodies_14; }
	inline void set_nbBodies_14(int32_t value)
	{
		___nbBodies_14 = value;
	}

	inline static int32_t get_offset_of_latestMealsCount_15() { return static_cast<int32_t>(offsetof(snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1, ___latestMealsCount_15)); }
	inline int32_t get_latestMealsCount_15() const { return ___latestMealsCount_15; }
	inline int32_t* get_address_of_latestMealsCount_15() { return &___latestMealsCount_15; }
	inline void set_latestMealsCount_15(int32_t value)
	{
		___latestMealsCount_15 = value;
	}
};


// tailCtrl
struct tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 tailCtrl::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_4;
	// UnityEngine.Vector3 tailCtrl::movement
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___movement_5;
	// playerControl tailCtrl::playermov
	playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * ___playermov_6;
	// snekOrg tailCtrl::SNEK
	snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1 * ___SNEK_7;
	// System.Single tailCtrl::rotation
	float ___rotation_8;
	// System.Single tailCtrl::rotVar
	float ___rotVar_9;

public:
	inline static int32_t get_offset_of_position_4() { return static_cast<int32_t>(offsetof(tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0, ___position_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_4() const { return ___position_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_4() { return &___position_4; }
	inline void set_position_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_4 = value;
	}

	inline static int32_t get_offset_of_movement_5() { return static_cast<int32_t>(offsetof(tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0, ___movement_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_movement_5() const { return ___movement_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_movement_5() { return &___movement_5; }
	inline void set_movement_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___movement_5 = value;
	}

	inline static int32_t get_offset_of_playermov_6() { return static_cast<int32_t>(offsetof(tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0, ___playermov_6)); }
	inline playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * get_playermov_6() const { return ___playermov_6; }
	inline playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 ** get_address_of_playermov_6() { return &___playermov_6; }
	inline void set_playermov_6(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * value)
	{
		___playermov_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playermov_6), (void*)value);
	}

	inline static int32_t get_offset_of_SNEK_7() { return static_cast<int32_t>(offsetof(tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0, ___SNEK_7)); }
	inline snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1 * get_SNEK_7() const { return ___SNEK_7; }
	inline snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1 ** get_address_of_SNEK_7() { return &___SNEK_7; }
	inline void set_SNEK_7(snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1 * value)
	{
		___SNEK_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SNEK_7), (void*)value);
	}

	inline static int32_t get_offset_of_rotation_8() { return static_cast<int32_t>(offsetof(tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0, ___rotation_8)); }
	inline float get_rotation_8() const { return ___rotation_8; }
	inline float* get_address_of_rotation_8() { return &___rotation_8; }
	inline void set_rotation_8(float value)
	{
		___rotation_8 = value;
	}

	inline static int32_t get_offset_of_rotVar_9() { return static_cast<int32_t>(offsetof(tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0, ___rotVar_9)); }
	inline float get_rotVar_9() const { return ___rotVar_9; }
	inline float* get_address_of_rotVar_9() { return &___rotVar_9; }
	inline void set_rotVar_9(float value)
	{
		___rotVar_9 = value;
	}
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  m_Items[1];

public:
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  m_Items[1];

public:
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		m_Items[index] = value;
	}
};


// !!0[] System.Array::Empty<System.Object>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Array_Empty_TisRuntimeObject_m1FBC21243DF3542384C523801E8CA8A97606C747_gshared_inline (const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler_1__ctor_m55B15D6747B269625FC10375E6008AA99BD498B4_gshared (EventHandler_1_tFA1C30E54FA1061D79E711F65F9A174BFBD8CDCB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m4039C8E65629D33E1EC84D2505BF1D5DDC936622_gshared (RuntimeObject * ___original0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m6C4CDF2E6D3B2347704D0BBC108D0893C1ABBEAE_gshared (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Item(System.Int32,!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_set_Item_m0E40C7E017BB8ADBADBD6DE8947884FA4DEA2DE5_gshared (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, int32_t ___index0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_gshared_inline (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.String>()
inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* Array_Empty_TisString_t_m224DA90A7384ACF7EBE2F94D2DFDE2F310D1E77D_inline (const RuntimeMethod* method)
{
	return ((  StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* (*) (const RuntimeMethod*))Array_Empty_TisRuntimeObject_m1FBC21243DF3542384C523801E8CA8A97606C747_gshared_inline)(method);
}
// System.Void WebSocketSharp.WebSocket::.ctor(System.String,System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebSocket__ctor_m175FB52D7114C1BADE9B416A1A8FEA68DDB598D2 (WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * __this, String_t* ___url0, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___protocols1, const RuntimeMethod* method);
// System.Void System.EventHandler`1<WebSocketSharp.MessageEventArgs>::.ctor(System.Object,System.IntPtr)
inline void EventHandler_1__ctor_m6E3771CC1A1EFE63D59637DE6D0CB689DEFAF943 (EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 *, RuntimeObject *, intptr_t, const RuntimeMethod*))EventHandler_1__ctor_m55B15D6747B269625FC10375E6008AA99BD498B4_gshared)(__this, ___object0, ___method1, method);
}
// System.Void WebSocketSharp.WebSocket::add_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebSocket_add_OnMessage_mF0B54AE34AAFCE31A08400721B0A5E090917A217 (WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * __this, EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 * ___value0, const RuntimeMethod* method);
// System.Void WebSocketSharp.WebSocket::Connect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebSocket_Connect_m786CF51F671040752A008871832C29E4D9E416A5 (WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220 (int32_t ___key0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.String UnityEngine.JsonUtility::ToJson(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* JsonUtility_ToJson_mF4F097C9AEC7699970E3E7E99EF8FF2F44DA1B5C (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void WebSocketSharp.WebSocket::Send(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebSocket_Send_mB2A549377FA705334E19181213272F70DB8C0786 (WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * __this, String_t* ___data0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_eulerAngles_mFDCBC6282E4B1196AA26BF01008B2AAA2DD2969E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5 (int32_t ___sceneBuildIndex0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Application::get_loadedLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Application_get_loadedLevel_m0E346B638F8E10D4FD5D4C883FAA39C34CF41C1E (const RuntimeMethod* method);
// System.Void UnityEngine.Application::LoadLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_LoadLevel_mB8D52CB842A98DDBB611D39C80709F5233AF6AAB (int32_t ___index0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method);
// System.Boolean cell::getObst()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool cell_getObst_m01A4AB2FC92883533D633DFB00B0293688033197_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_sortingOrder_mAABE4F8F9B158068C8A1582ACE0BFEA3CF499139 (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, int32_t ___value0, const RuntimeMethod* method);
// cell mapGen::getCellAt(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method);
// System.Boolean cell::getSnek()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool cell_getSnek_m0571C8B3866F69B57E862C8E5F0151E587D7D5E0_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method);
// System.Boolean cell::getFood()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool cell_getFood_mC65A1DA2CC3B72B729F408DF4CE5E1D40A8A6FEC_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<mapGen>()
inline mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * GameObject_GetComponent_TismapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_m8D19FCEE1886F87EFB5F27938BFB6398F419E027 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void currCell::setCell(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void currCell_setCell_m5D8D0D987DE89C4AE98A94E95AA6660A7513C957 (currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method);
// System.Int32 mapGen::getPosXn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method);
// System.Int32 mapGen::getPosYn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70 (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method);
// System.Single playerControl::getRealRot()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float playerControl_getRealRot_m196ED94BADA5895078952015608913BDBEE5ED2F_inline (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method);
// System.Void cell::isFood(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void cell_isFood_m213F08D7ABE5B167DB7707B44A1B176941FBDDED_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, bool ___b0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A (int32_t ___minInclusive0, int32_t ___maxExclusive1, const RuntimeMethod* method);
// System.Int32 cell::getCellXn()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t cell_getCellXn_m290BAFE1347F788D453D34BDC5240DAC865E8590_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method);
// System.Int32 cell::getCellYn()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t cell_getCellYn_m706FEF38E9A39770DC1C68C83A58AB266A032060_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4 (const RuntimeMethod* method);
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Scene_get_buildIndex_mE32CE766EA0790E4636A351BA353A7FD71A11DA4 (Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986 (String_t* ___key0, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_SetInt_m0C5C977E960B9CA8F9AB73AF4129C3DCABD067B6 (String_t* ___key0, int32_t ___value1, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m4039C8E65629D33E1EC84D2505BF1D5DDC936622_gshared)(___original0, method);
}
// System.Collections.ArrayList mapGen::getMap()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * mapGen_getMap_m627656B6E78F2C9C4FC13EBADD4DB886ABC7FE96_inline (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<cell>()
inline cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void cell::isObst(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, bool ___b0, const RuntimeMethod* method);
// System.Void cell::mapping(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void cell_mapping_m07D3B600FAE7384B84085F0A1EBAD07A1FF56212 (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method);
// System.Void cell::setCoords(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void cell_setCoords_m56B7F6A8D5A742DE7D8A2CC1BCA0C6BEE606ED9D (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, float ___i0, float ___j1, const RuntimeMethod* method);
// System.Void mapGen::setPos(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void mapGen_setPos_mFEED8A85AE9BBE2FB29248E997B90CBB0ABE73B0 (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void bloc::setBloc(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void bloc_setBloc_m63AAC7F1ECD6DD0D60B5E9DD66B1E8A39A03C186_inline (bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7 * __this, bool ___b0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<button>()
inline button_t4F901F446B79F86CB9593995276166F7ADD407E6 * GameObject_GetComponent_Tisbutton_t4F901F446B79F86CB9593995276166F7ADD407E6_m85F68D44BCA088B03954BB7ADE6D419C2B05AFEC (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  button_t4F901F446B79F86CB9593995276166F7ADD407E6 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Boolean button::isBtnSet()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool button_isBtnSet_mA09EFC2279D396533426BC9D0B19401285896182_inline (button_t4F901F446B79F86CB9593995276166F7ADD407E6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.ArrayList::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArrayList__ctor_m6847CFECD6BDC2AD10A4AC9852A572B88B8D6B1B (ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m9D556E728119BEB64AA624EBE82931367B6573A5 (String_t* ___name0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Vector3_op_Inequality_m15190A795B416EB699E69E6190DE6F1C1F208710 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method);
// System.Void food::putFood()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void food_putFood_m49DD3492104F98A1AB7C3D18633F8815D10F44F9 (food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * __this, const RuntimeMethod* method);
// System.Boolean currCell::getActObst()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool currCell_getActObst_m09D37F4D6B041A5037A62C4D73478B4DAE7AC970 (currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * __this, const RuntimeMethod* method);
// System.Boolean currCell::getActSnek()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool currCell_getActSnek_m316EAD81A633D817C5D143F5384EEBBF9F3248E3 (currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * __this, const RuntimeMethod* method);
// System.Boolean currCell::getActFood()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool currCell_getActFood_mFF88646F43125C65462C569D6905737E166F340D (currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * __this, const RuntimeMethod* method);
// System.Void food::eat()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void food_eat_m903D542867241A92061EE6DBAE43516D4EEF67CA (food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * __this, const RuntimeMethod* method);
// System.Single mapGen::getPosXf()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float mapGen_getPosXf_mD0B5B3B19A9D74B82799A8AE52BD112189CB5063_inline (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method);
// System.Single mapGen::getPosYf()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float mapGen_getPosYf_mF6A253CF42326AD207A29EF6A03EEF2E511D527C_inline (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_m027A155054DDC4206F679EFB86BE0960D45C33A7 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___eulers0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, int32_t, const RuntimeMethod*))List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0)
inline void List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3 (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
inline void List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59 (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , const RuntimeMethod*))List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_gshared)(__this, ___item0, method);
}
// !!0 UnityEngine.GameObject::GetComponent<bodyCtrl>()
inline bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void bodyCtrl::setPartPos(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void bodyCtrl_setPartPos_mF2E73FC83E1E868B997B2BEA397F8DDFED0AE581 (bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___X0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<tailCtrl>()
inline tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void tailCtrl::setTailPos(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void tailCtrl_setTailPos_mEEACFF413EBB022438C4B1B2698C82BFEE770CCB (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___X0, const RuntimeMethod* method);
// System.Void tailCtrl::setTailMov(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void tailCtrl_setTailMov_mE01124E941868160F86C540239A3D6BA8C852F3D (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___X0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0)
inline void List_1_Add_m6C4CDF2E6D3B2347704D0BBC108D0893C1ABBEAE (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , const RuntimeMethod*))List_1_Add_m6C4CDF2E6D3B2347704D0BBC108D0893C1ABBEAE_gshared)(__this, ___item0, method);
}
// System.Single playerControl::getRotVar()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float playerControl_getRotVar_mBFEA83CCFACBEE74025DDAAF2CC76DD5A7351492_inline (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Item(System.Int32,!0)
inline void List_1_set_Item_m0E40C7E017BB8ADBADBD6DE8947884FA4DEA2DE5 (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, int32_t ___index0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value1, const RuntimeMethod* method)
{
	((  void (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, int32_t, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , const RuntimeMethod*))List_1_set_Item_m0E40C7E017BB8ADBADBD6DE8947884FA4DEA2DE5_gshared)(__this, ___index0, ___value1, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32)
inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_inline (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, int32_t, const RuntimeMethod*))List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_gshared_inline)(__this, ___index0, method);
}
// System.Void bodyCtrl::setPartRot(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void bodyCtrl_setPartRot_mBC7E7CFF8FB896D4BC0509D48B14AE566E9E1205 (bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___X0, const RuntimeMethod* method);
// UnityEngine.Vector3 playerControl::getCurrCell()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  playerControl_getCurrCell_m55DE57C3FFE3237AACE857B1843ED79C8A85E798 (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method);
// System.Int32 food::getMealsCount()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t food_getMealsCount_m2FC192A4AFEF2441E3A95C6746D52D446497CCA4_inline (food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 bodyCtrl::getPartPos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  bodyCtrl_getPartPos_m0D3736BCBE106245967F152BDCB41711B45E2A8D (bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 tailCtrl::getTailRot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  tailCtrl_getTailRot_mF96B56AA79CC6E7284796797F55193F648284768 (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, const RuntimeMethod* method);
// System.Void tailCtrl::setTailRot(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void tailCtrl_setTailRot_m750C26637F78E88A6A761762BDBB6293A1F70E89 (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___X0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,!0)
inline void List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406 (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value1, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, int32_t, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , const RuntimeMethod*))List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406_gshared)(__this, ___index0, ___value1, method);
}
// System.Void cell::isSnek(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void cell_isSnek_mCF5381441AFAA8846A05EB36C5011A9A634AC4A8_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, bool ___b0, const RuntimeMethod* method);
// UnityEngine.Vector3 tailCtrl::getTailPos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  tailCtrl_getTailPos_m23E21B9AA7E968D91E76B28D51A9ADB68A11580D (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
inline void List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8 (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// UnityEngine.Vector3 tailCtrl::getTailNumMove(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  tailCtrl_getTailNumMove_mA639704C8C2829692EA74BCC1D19AD429C7616DC (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___X0, const RuntimeMethod* method);
// System.Int32 playerControl::getMoveSpeed()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t playerControl_getMoveSpeed_m80AEB733464773149D656FB2C35DFA1AC6B40679_inline (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 snekOrg::getLastBod()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  snekOrg_getLastBod_m5FBE0B56E39516461FB42890B7E0350CC9099528 (snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1 * __this, const RuntimeMethod* method);
// System.Void WS_Client/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mC98D4D4F3BC2DBE2D21B3CE34E2F186BD837AD64 (U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Uri WebSocketSharp.WebSocket::get_Url()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * WebSocket_get_Url_mBC9629B05E6D0F63B87A76E8E079DFDD78DFC788 (WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * __this, const RuntimeMethod* method);
// System.String WebSocketSharp.MessageEventArgs::get_Data()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* MessageEventArgs_get_Data_mD9D53F8250BB093F649A6279D1EB19D408901A52 (MessageEventArgs_t644AAD070C0BA6114FE7C4BD44A31BF3A9FA19D2 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78 (String_t* ___str00, String_t* ___str11, String_t* ___str22, String_t* ___str33, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NewBehaviourScript::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NewBehaviourScript_Start_m783F84A617DADC4574B0BF1524481E6B96C65661 (NewBehaviourScript_t5ED8D4BD4668DBECFEFD841BD2A8CC1D8FCF38CB * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void NewBehaviourScript::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NewBehaviourScript_Update_m411C4D5C2D993FD70092FDA0FE2AC4786F8AC001 (NewBehaviourScript_t5ED8D4BD4668DBECFEFD841BD2A8CC1D8FCF38CB * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void NewBehaviourScript::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NewBehaviourScript__ctor_m437970EA37D66BDF32972F4CC0F65B95E5961FAA (NewBehaviourScript_t5ED8D4BD4668DBECFEFD841BD2A8CC1D8FCF38CB * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Player::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Player::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Player::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void User::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void User__ctor_m78396ED1791ACA3495EA0478E6735C0AEB0D90C6 (User_t73C2C8C9B5B5C1F356AA134EC0B50FC072360F77 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WS_Client::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WS_Client_Start_mF0C78A1A6D0BDFC43444A2D8E5ED1ECEF4D91108 (WS_Client_tF30FD0EC57F32A91C0F9A49A618FDCF2106FF041 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Empty_TisString_t_m224DA90A7384ACF7EBE2F94D2DFDE2F310D1E77D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1__ctor_m6E3771CC1A1EFE63D59637DE6D0CB689DEFAF943_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CStartU3Eb__2_0_mCFC48F6EEA3302A6173CFB18D550F6447FEB0BFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3CA5033F9F4CC8909C704BC3E6BDC7A7991A427);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 * G_B2_0 = NULL;
	WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * G_B2_1 = NULL;
	EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 * G_B1_0 = NULL;
	WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * G_B1_1 = NULL;
	{
		// ws = new WebSocket("ws://localhost:8000");
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0;
		L_0 = Array_Empty_TisString_t_m224DA90A7384ACF7EBE2F94D2DFDE2F310D1E77D_inline(/*hidden argument*/Array_Empty_TisString_t_m224DA90A7384ACF7EBE2F94D2DFDE2F310D1E77D_RuntimeMethod_var);
		WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * L_1 = (WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC *)il2cpp_codegen_object_new(WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_il2cpp_TypeInfo_var);
		WebSocket__ctor_m175FB52D7114C1BADE9B416A1A8FEA68DDB598D2(L_1, _stringLiteralB3CA5033F9F4CC8909C704BC3E6BDC7A7991A427, L_0, /*hidden argument*/NULL);
		__this->set_ws_4(L_1);
		//  ws.OnMessage += (sender, e) => {
		//     Debug.Log("Message received from " + ((WebSocket)sender).Url + ", Data : " + e.Data);
		// };
		WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * L_2 = __this->get_ws_4();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_il2cpp_TypeInfo_var);
		EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 * L_3 = ((U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_il2cpp_TypeInfo_var))->get_U3CU3E9__2_0_1();
		EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 * L_4 = L_3;
		G_B1_0 = L_4;
		G_B1_1 = L_2;
		if (L_4)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_2;
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_il2cpp_TypeInfo_var);
		U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7 * L_5 = ((U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 * L_6 = (EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 *)il2cpp_codegen_object_new(EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m6E3771CC1A1EFE63D59637DE6D0CB689DEFAF943(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec_U3CStartU3Eb__2_0_mCFC48F6EEA3302A6173CFB18D550F6447FEB0BFD_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_m6E3771CC1A1EFE63D59637DE6D0CB689DEFAF943_RuntimeMethod_var);
		EventHandler_1_t9DD8E50C02E452F3F4A54FCA0EE598A960080DA6 * L_7 = L_6;
		((U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_il2cpp_TypeInfo_var))->set_U3CU3E9__2_0_1(L_7);
		G_B2_0 = L_7;
		G_B2_1 = G_B1_1;
	}

IL_003a:
	{
		WebSocket_add_OnMessage_mF0B54AE34AAFCE31A08400721B0A5E090917A217(G_B2_1, G_B2_0, /*hidden argument*/NULL);
		// ws.Connect();
		WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * L_8 = __this->get_ws_4();
		WebSocket_Connect_m786CF51F671040752A008871832C29E4D9E416A5(L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void WS_Client::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WS_Client_Update_mABD58A13EF3A421E764BBD559BCC90EC62D53CFB (WS_Client_tF30FD0EC57F32A91C0F9A49A618FDCF2106FF041 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01B87E63030BCFCDE47CC49F301933D8B6BA312A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral322DFDD498B4441E023BB01DAA343BF820A06210);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAB4F9FB3969821142A073A0FFCBA0619032580A0);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// if(ws == null){
		WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * L_0 = __this->get_ws_4();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// if(Input.GetKeyDown(KeyCode.Space)){
		bool L_1;
		L_1 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)32), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0054;
		}
	}
	{
		// Debug.Log("sending message");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral01B87E63030BCFCDE47CC49F301933D8B6BA312A, /*hidden argument*/NULL);
		// user.id = "00000000000";
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_2 = __this->get_user_5();
		L_2->set_id_4(_stringLiteralAB4F9FB3969821142A073A0FFCBA0619032580A0);
		// user.username = "johndoe";
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_3 = __this->get_user_5();
		L_3->set_username_5(_stringLiteral322DFDD498B4441E023BB01DAA343BF820A06210);
		// string json = JsonUtility.ToJson(user);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_4 = __this->get_user_5();
		String_t* L_5;
		L_5 = JsonUtility_ToJson_mF4F097C9AEC7699970E3E7E99EF8FF2F44DA1B5C(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// ws.Send(json);
		WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC * L_6 = __this->get_ws_4();
		String_t* L_7 = V_0;
		WebSocket_Send_mB2A549377FA705334E19181213272F70DB8C0786(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0054:
	{
		// }
		return;
	}
}
// System.Void WS_Client::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WS_Client__ctor_m8EE950404D8FFDCA6E47D91CE696B66027304521 (WS_Client_tF30FD0EC57F32A91C0F9A49A618FDCF2106FF041 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void bloc::setBloc(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void bloc_setBloc_m63AAC7F1ECD6DD0D60B5E9DD66B1E8A39A03C186 (bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7 * __this, bool ___b0, const RuntimeMethod* method)
{
	{
		// isSet=b;
		bool L_0 = ___b0;
		__this->set_isSet_8(L_0);
		// }
		return;
	}
}
// System.Void bloc::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void bloc_Start_m9409F428FA026F7366D24C3D7B033EFA899AD27B (bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// spriteRen=this.GetComponent<SpriteRenderer>();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0;
		L_0 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		__this->set_spriteRen_5(L_0);
		// spriteRen.sprite = Ren0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1 = __this->get_spriteRen_5();
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_2 = __this->get_Ren0_6();
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void bloc::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void bloc_Update_mFE86B0F4B89BC9830A2D3B5685B97041B2C34130 (bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7 * __this, const RuntimeMethod* method)
{
	{
		// if(isSet) spriteRen.sprite=Ren1;
		bool L_0 = __this->get_isSet_8();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		// if(isSet) spriteRen.sprite=Ren1;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1 = __this->get_spriteRen_5();
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_2 = __this->get_Ren1_7();
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// }
		return;
	}
}
// System.Void bloc::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void bloc__ctor_m38FA0DC5511CF4D7D9EF62B28E11E4A12D17A032 (bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void bodyCtrl::setAngle(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void bodyCtrl_setAngle_m6C48C52E769DCF5A45172C1D4C91C45065820007 (bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * __this, bool ___b0, const RuntimeMethod* method)
{
	{
		// isAngle=b;
		bool L_0 = ___b0;
		__this->set_isAngle_7(L_0);
		// }
		return;
	}
}
// System.Void bodyCtrl::setPartPos(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void bodyCtrl_setPartPos_mF2E73FC83E1E868B997B2BEA397F8DDFED0AE581 (bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___X0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// position.x = X.x-mapGen.sizeX/2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_0 = __this->get_address_of_position_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = ___X0;
		float L_2 = L_1.get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_3 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		L_0->set_x_2(((float)il2cpp_codegen_subtract((float)L_2, (float)((float)((float)((int32_t)((int32_t)L_3/(int32_t)2)))))));
		// position.y = X.y-mapGen.sizeY/2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_4 = __this->get_address_of_position_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = ___X0;
		float L_6 = L_5.get_y_3();
		int32_t L_7 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		L_4->set_y_3(((float)il2cpp_codegen_subtract((float)L_6, (float)((float)((float)((int32_t)((int32_t)L_7/(int32_t)2)))))));
		// position.z=0f;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_8 = __this->get_address_of_position_4();
		L_8->set_z_4((0.0f));
		// }
		return;
	}
}
// System.Void bodyCtrl::setPartRot(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void bodyCtrl_setPartRot_mBC7E7CFF8FB896D4BC0509D48B14AE566E9E1205 (bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___X0, const RuntimeMethod* method)
{
	{
		// rotation=X.x;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___X0;
		float L_1 = L_0.get_x_0();
		__this->set_rotation_5(L_1);
		// rotVar=X.y;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___X0;
		float L_3 = L_2.get_y_1();
		__this->set_rotVar_6(L_3);
		// }
		return;
	}
}
// UnityEngine.Vector3 bodyCtrl::getPartPos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  bodyCtrl_getPartPos_m0D3736BCBE106245967F152BDCB41711B45E2A8D (bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return new Vector3(position.x+mapGen.sizeX/2,position.y+mapGen.sizeY/2,0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_0 = __this->get_address_of_position_4();
		float L_1 = L_0->get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_2 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_3 = __this->get_address_of_position_4();
		float L_4 = L_3->get_y_3();
		int32_t L_5 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), ((float)il2cpp_codegen_add((float)L_1, (float)((float)((float)((int32_t)((int32_t)L_2/(int32_t)2)))))), ((float)il2cpp_codegen_add((float)L_4, (float)((float)((float)((int32_t)((int32_t)L_5/(int32_t)2)))))), (0.0f), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void bodyCtrl::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void bodyCtrl_Start_m8630E5803A08441DBD5A87C8651624E00C6E928E (bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void bodyCtrl::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void bodyCtrl_Update_m75472F0B4C17B34B596F1BA4EFCA05B31607A7E3 (bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * __this, const RuntimeMethod* method)
{
	{
		// transform.position=position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = __this->get_position_4();
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_0, L_1, /*hidden argument*/NULL);
		// if (rotVar!=0f){
		float L_2 = __this->get_rotVar_6();
		if ((((float)L_2) == ((float)(0.0f))))
		{
			goto IL_0091;
		}
	}
	{
		// spriteRen.sprite = angleSprite;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_3 = __this->get_spriteRen_8();
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_4 = __this->get_angleSprite_10();
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_3, L_4, /*hidden argument*/NULL);
		// if(rotVar==90f) transform.eulerAngles=new Vector3(0,0,rotation+rotVar);
		float L_5 = __this->get_rotVar_6();
		if ((!(((float)L_5) == ((float)(90.0f)))))
		{
			goto IL_0063;
		}
	}
	{
		// if(rotVar==90f) transform.eulerAngles=new Vector3(0,0,rotation+rotVar);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_7 = __this->get_rotation_5();
		float L_8 = __this->get_rotVar_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), (0.0f), (0.0f), ((float)il2cpp_codegen_add((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		Transform_set_eulerAngles_mFDCBC6282E4B1196AA26BF01008B2AAA2DD2969E(L_6, L_9, /*hidden argument*/NULL);
	}

IL_0063:
	{
		// if(rotVar==-90f) transform.eulerAngles=new Vector3(0,0,rotation);
		float L_10 = __this->get_rotVar_6();
		if ((!(((float)L_10) == ((float)(-90.0f)))))
		{
			goto IL_00c2;
		}
	}
	{
		// if(rotVar==-90f) transform.eulerAngles=new Vector3(0,0,rotation);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_12 = __this->get_rotation_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		memset((&L_13), 0, sizeof(L_13));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_13), (0.0f), (0.0f), L_12, /*hidden argument*/NULL);
		Transform_set_eulerAngles_mFDCBC6282E4B1196AA26BF01008B2AAA2DD2969E(L_11, L_13, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0091:
	{
		// transform.eulerAngles=new Vector3(0,0,rotation);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14;
		L_14 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_15 = __this->get_rotation_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		memset((&L_16), 0, sizeof(L_16));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_16), (0.0f), (0.0f), L_15, /*hidden argument*/NULL);
		Transform_set_eulerAngles_mFDCBC6282E4B1196AA26BF01008B2AAA2DD2969E(L_14, L_16, /*hidden argument*/NULL);
		// spriteRen.sprite = straightSprite;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_17 = __this->get_spriteRen_8();
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_18 = __this->get_straightSprite_9();
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_17, L_18, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		// }
		return;
	}
}
// System.Void bodyCtrl::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void bodyCtrl__ctor_mA94A8C76EB07C8097513AD90A9D3D6492AA7EE21 (bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * __this, const RuntimeMethod* method)
{
	{
		// float rotation=-90f;
		__this->set_rotation_5((-90.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean button::isBtnSet()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool button_isBtnSet_mA09EFC2279D396533426BC9D0B19401285896182 (button_t4F901F446B79F86CB9593995276166F7ADD407E6 * __this, const RuntimeMethod* method)
{
	{
		// return isSet;
		bool L_0 = __this->get_isSet_8();
		return L_0;
	}
}
// System.Void button::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void button_Start_m92908B4D022C6C1F10D6F4A9A87EF49D5BA96835 (button_t4F901F446B79F86CB9593995276166F7ADD407E6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// spriteRen=this.GetComponent<SpriteRenderer>();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0;
		L_0 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		__this->set_spriteRen_5(L_0);
		// spriteRen.sprite = Ren0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1 = __this->get_spriteRen_5();
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_2 = __this->get_Ren0_6();
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_1, L_2, /*hidden argument*/NULL);
		// isSet=false;
		__this->set_isSet_8((bool)0);
		// }
		return;
	}
}
// System.Void button::OnMouseUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void button_OnMouseUp_m3681602D44D5221BBB029F2C24BBCFB24FE07615 (button_t4F901F446B79F86CB9593995276166F7ADD407E6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// isSet=true;
		__this->set_isSet_8((bool)1);
		// if (scene==-1){}
		int32_t L_0 = __this->get_scene_4();
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_003f;
		}
	}
	{
		// else if(scene==0)
		int32_t L_1 = __this->get_scene_4();
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		// SceneManager.LoadScene(0);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(0, /*hidden argument*/NULL);
		return;
	}

IL_001f:
	{
		// else if(scene==-2)
		int32_t L_2 = __this->get_scene_4();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0034;
		}
	}
	{
		// Application.LoadLevel(Application.loadedLevel);
		int32_t L_3;
		L_3 = Application_get_loadedLevel_m0E346B638F8E10D4FD5D4C883FAA39C34CF41C1E(/*hidden argument*/NULL);
		Application_LoadLevel_mB8D52CB842A98DDBB611D39C80709F5233AF6AAB(L_3, /*hidden argument*/NULL);
		return;
	}

IL_0034:
	{
		// Application.LoadLevel(scene);
		int32_t L_4 = __this->get_scene_4();
		Application_LoadLevel_mB8D52CB842A98DDBB611D39C80709F5233AF6AAB(L_4, /*hidden argument*/NULL);
	}

IL_003f:
	{
		// }
		return;
	}
}
// System.Void button::OnMouseEnter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void button_OnMouseEnter_m3D24048B82CE3BEF5F363F284B995EFEA379FA0D (button_t4F901F446B79F86CB9593995276166F7ADD407E6 * __this, const RuntimeMethod* method)
{
	{
		// spriteRen.sprite = Ren1;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0 = __this->get_spriteRen_5();
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_1 = __this->get_Ren1_7();
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void button::OnMouseExit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void button_OnMouseExit_m237B00A5A698418EFF024E1784DFE9F914F953B2 (button_t4F901F446B79F86CB9593995276166F7ADD407E6 * __this, const RuntimeMethod* method)
{
	{
		// spriteRen.sprite = Ren0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0 = __this->get_spriteRen_5();
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_1 = __this->get_Ren0_6();
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void button::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void button_Update_mEAF4ECF2DBEDA31CBEEEC2A48289910AD82E4885 (button_t4F901F446B79F86CB9593995276166F7ADD407E6 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void button::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void button__ctor_mF868E2D674E6EF28B24D21B8DD5096A2B800E8D5 (button_t4F901F446B79F86CB9593995276166F7ADD407E6 * __this, const RuntimeMethod* method)
{
	{
		// public int scene=-1;
		__this->set_scene_4((-1));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void cell::mapping(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void cell_mapping_m07D3B600FAE7384B84085F0A1EBAD07A1FF56212 (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method)
{
	{
		// this.mapX=i;
		int32_t L_0 = ___i0;
		__this->set_mapX_7(L_0);
		// this.mapY=j;
		int32_t L_1 = ___j1;
		__this->set_mapY_8(L_1);
		// }
		return;
	}
}
// System.Boolean cell::getObst()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool cell_getObst_m01A4AB2FC92883533D633DFB00B0293688033197 (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	{
		// return this.isObstacle;
		bool L_0 = __this->get_isObstacle_4();
		return L_0;
	}
}
// System.Void cell::isObst(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24 (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, bool ___b0, const RuntimeMethod* method)
{
	{
		// this.isObstacle=b;
		bool L_0 = ___b0;
		__this->set_isObstacle_4(L_0);
		// }
		return;
	}
}
// System.Boolean cell::getFood()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool cell_getFood_mC65A1DA2CC3B72B729F408DF4CE5E1D40A8A6FEC (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	{
		// return this.isCellFood;
		bool L_0 = __this->get_isCellFood_5();
		return L_0;
	}
}
// System.Void cell::isFood(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void cell_isFood_m213F08D7ABE5B167DB7707B44A1B176941FBDDED (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, bool ___b0, const RuntimeMethod* method)
{
	{
		// this.isCellFood=b;
		bool L_0 = ___b0;
		__this->set_isCellFood_5(L_0);
		// }
		return;
	}
}
// System.Boolean cell::getSnek()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool cell_getSnek_m0571C8B3866F69B57E862C8E5F0151E587D7D5E0 (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	{
		// return this.isCellSnek;
		bool L_0 = __this->get_isCellSnek_6();
		return L_0;
	}
}
// System.Void cell::isSnek(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void cell_isSnek_mCF5381441AFAA8846A05EB36C5011A9A634AC4A8 (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, bool ___b0, const RuntimeMethod* method)
{
	{
		// this.isCellSnek=b;
		bool L_0 = ___b0;
		__this->set_isCellSnek_6(L_0);
		// }
		return;
	}
}
// System.Void cell::setCoords(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void cell_setCoords_m56B7F6A8D5A742DE7D8A2CC1BCA0C6BEE606ED9D (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, float ___i0, float ___j1, const RuntimeMethod* method)
{
	{
		// this.coords=new Vector2(i,j);
		float L_0 = ___i0;
		float L_1 = ___j1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		__this->set_coords_11(L_2);
		// }
		return;
	}
}
// System.Single cell::getCellXf()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float cell_getCellXf_mCE31A20DBED83D9A405FB12FB16C399201FDFBD2 (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	{
		// return coords.x;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_0 = __this->get_address_of_coords_11();
		float L_1 = L_0->get_x_0();
		return L_1;
	}
}
// System.Single cell::getCellYf()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float cell_getCellYf_mC3A1FAA71ABCF09DBCAD1B1C0796424E87385E6E (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	{
		// return coords.y;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_0 = __this->get_address_of_coords_11();
		float L_1 = L_0->get_y_1();
		return L_1;
	}
}
// System.Int32 cell::getCellXn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t cell_getCellXn_m290BAFE1347F788D453D34BDC5240DAC865E8590 (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	{
		// return mapX;
		int32_t L_0 = __this->get_mapX_7();
		return L_0;
	}
}
// System.Int32 cell::getCellYn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t cell_getCellYn_m706FEF38E9A39770DC1C68C83A58AB266A032060 (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	{
		// return mapY;
		int32_t L_0 = __this->get_mapY_8();
		return L_0;
	}
}
// System.Void cell::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void cell_Start_mC04AF155231F1790FD7053320128547F6C0CE3D6 (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// transform.position=coords;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1 = __this->get_coords_11();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline(L_1, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_0, L_2, /*hidden argument*/NULL);
		// spriteRen = this.GetComponent<SpriteRenderer>();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_3;
		L_3 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		__this->set_spriteRen_9(L_3);
		// isCellFood=false;
		__this->set_isCellFood_5((bool)0);
		// isCellSnek=false;
		__this->set_isCellSnek_6((bool)0);
		// }
		return;
	}
}
// System.Void cell::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void cell_Update_m1709BB2B5DA2EED8A4D46E7E80475AF044051D53 (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	{
		// if (getObst()){
		bool L_0;
		L_0 = cell_getObst_m01A4AB2FC92883533D633DFB00B0293688033197_inline(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		// spriteRen.sprite = obstSprite;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1 = __this->get_spriteRen_9();
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_2 = __this->get_obstSprite_10();
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_1, L_2, /*hidden argument*/NULL);
		// spriteRen.sortingOrder = 3;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_3 = __this->get_spriteRen_9();
		Renderer_set_sortingOrder_mAABE4F8F9B158068C8A1582ACE0BFEA3CF499139(L_3, 3, /*hidden argument*/NULL);
	}

IL_0025:
	{
		// }
		return;
	}
}
// System.Void cell::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void cell__ctor_mC65406EDBB408F35ADD4175274AA52AC4D212D4B (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void currCell::setCell(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void currCell_setCell_m5D8D0D987DE89C4AE98A94E95AA6660A7513C957 (currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(playerControl.alive && 0<=i && i<=mapGen.sizeX && 0<=j && j<=mapGen.sizeY){
		bool L_0 = ((playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_StaticFields*)il2cpp_codegen_static_fields_for(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_il2cpp_TypeInfo_var))->get_alive_8();
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_1 = ___i0;
		if ((((int32_t)0) > ((int32_t)L_1)))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_2 = ___i0;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_3 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_2) > ((int32_t)L_3)))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_4 = ___j1;
		if ((((int32_t)0) > ((int32_t)L_4)))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_5 = ___j1;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_6 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_5) > ((int32_t)L_6)))
		{
			goto IL_0032;
		}
	}
	{
		// actual = temp.getCellAt(i,j);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_7 = __this->get_temp_6();
		int32_t L_8 = ___i0;
		int32_t L_9 = ___j1;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_10;
		L_10 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_7, L_8, L_9, /*hidden argument*/NULL);
		__this->set_actual_5(L_10);
	}

IL_0032:
	{
		// }
		return;
	}
}
// System.Boolean currCell::getActObst()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool currCell_getActObst_m09D37F4D6B041A5037A62C4D73478B4DAE7AC970 (currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * __this, const RuntimeMethod* method)
{
	{
		// return actual.getObst();
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_0 = __this->get_actual_5();
		bool L_1;
		L_1 = cell_getObst_m01A4AB2FC92883533D633DFB00B0293688033197_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean currCell::getActSnek()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool currCell_getActSnek_m316EAD81A633D817C5D143F5384EEBBF9F3248E3 (currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * __this, const RuntimeMethod* method)
{
	{
		// return actual.getSnek();
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_0 = __this->get_actual_5();
		bool L_1;
		L_1 = cell_getSnek_m0571C8B3866F69B57E862C8E5F0151E587D7D5E0_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean currCell::getActFood()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool currCell_getActFood_mFF88646F43125C65462C569D6905737E166F340D (currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * __this, const RuntimeMethod* method)
{
	{
		// return actual.getFood();
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_0 = __this->get_actual_5();
		bool L_1;
		L_1 = cell_getFood_mC65A1DA2CC3B72B729F408DF4CE5E1D40A8A6FEC_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// cell currCell::getActual()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * currCell_getActual_m07E1D44AB0D8F1CE85AFA5CA39FB46D88C0B5E30 (currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * __this, const RuntimeMethod* method)
{
	{
		// return actual;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_0 = __this->get_actual_5();
		return L_0;
	}
}
// System.Void currCell::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void currCell_Start_m937EB078E7155F714AC1252A95929003BB2A6D4A (currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TismapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_m8D19FCEE1886F87EFB5F27938BFB6398F419E027_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// temp = stdGrid.GetComponent<mapGen>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_stdGrid_4();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_1;
		L_1 = GameObject_GetComponent_TismapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_m8D19FCEE1886F87EFB5F27938BFB6398F419E027(L_0, /*hidden argument*/GameObject_GetComponent_TismapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_m8D19FCEE1886F87EFB5F27938BFB6398F419E027_RuntimeMethod_var);
		__this->set_temp_6(L_1);
		// setCell((int)mapGen.spawn.x,(int)mapGen.spawn.y);
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		float L_2 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_x_0();
		float L_3 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_y_1();
		currCell_setCell_m5D8D0D987DE89C4AE98A94E95AA6660A7513C957(__this, il2cpp_codegen_cast_double_to_int<int32_t>(L_2), il2cpp_codegen_cast_double_to_int<int32_t>(L_3), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void currCell::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void currCell_Update_m2D0446DD20A1696EA744DF3C26F22E22B3F95898 (currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// setCell(temp.getPosXn(),temp.getPosYn());
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_0 = __this->get_temp_6();
		int32_t L_1;
		L_1 = mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F(L_0, /*hidden argument*/NULL);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_2 = __this->get_temp_6();
		int32_t L_3;
		L_3 = mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70(L_2, /*hidden argument*/NULL);
		currCell_setCell_m5D8D0D987DE89C4AE98A94E95AA6660A7513C957(__this, L_1, L_3, /*hidden argument*/NULL);
		// transform.position = new Vector3(temp.getPosXn()-mapGen.sizeX/2,temp.getPosYn()-mapGen.sizeY/2,0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_5 = __this->get_temp_6();
		int32_t L_6;
		L_6 = mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_7 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_8 = __this->get_temp_6();
		int32_t L_9;
		L_9 = mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70(L_8, /*hidden argument*/NULL);
		int32_t L_10 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		memset((&L_11), 0, sizeof(L_11));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_11), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)((int32_t)((int32_t)L_7/(int32_t)2)))))), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)((int32_t)((int32_t)L_10/(int32_t)2)))))), (0.0f), /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_4, L_11, /*hidden argument*/NULL);
		// transform.eulerAngles = new Vector3(0,0,controller.getRealRot());
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12;
		L_12 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * L_13 = __this->get_controller_7();
		float L_14;
		L_14 = playerControl_getRealRot_m196ED94BADA5895078952015608913BDBEE5ED2F_inline(L_13, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_15), (0.0f), (0.0f), L_14, /*hidden argument*/NULL);
		Transform_set_eulerAngles_mFDCBC6282E4B1196AA26BF01008B2AAA2DD2969E(L_12, L_15, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void currCell::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void currCell__ctor_mB3E23B1F8DA01B9A80737AC5DAF27DB8DDED3213 (currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void food::putFood()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void food_putFood_m49DD3492104F98A1AB7C3D18633F8815D10F44F9 (food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * __this, const RuntimeMethod* method)
{
	{
		// isFoodOn = true;
		__this->set_isFoodOn_8((bool)1);
		// }
		return;
	}
}
// System.Int32 food::getFoodX()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t food_getFoodX_m4DA6F476200719BED4975FAC3F2551B7FB987135 (food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * __this, const RuntimeMethod* method)
{
	{
		// return foodX;
		int32_t L_0 = __this->get_foodX_5();
		return L_0;
	}
}
// System.Int32 food::getFoodY()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t food_getFoodY_m0B4167F5475F2D6ACC3BFB5E482AD09D47B63A8C (food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * __this, const RuntimeMethod* method)
{
	{
		// return foodY;
		int32_t L_0 = __this->get_foodY_6();
		return L_0;
	}
}
// System.Boolean food::isEaten()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool food_isEaten_m91282D74A7311AD208B9625C567489C16029EDA4 (food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * __this, const RuntimeMethod* method)
{
	{
		// return (eaten && isFoodOn);
		bool L_0 = __this->get_eaten_4();
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		bool L_1 = __this->get_isFoodOn_8();
		return L_1;
	}

IL_000f:
	{
		return (bool)0;
	}
}
// System.Void food::eat()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void food_eat_m903D542867241A92061EE6DBAE43516D4EEF67CA (food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * __this, const RuntimeMethod* method)
{
	{
		// temp.getCellAt(foodX,foodY).isFood(false);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_0 = __this->get_temp_9();
		int32_t L_1 = __this->get_foodX_5();
		int32_t L_2 = __this->get_foodY_6();
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_3;
		L_3 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_0, L_1, L_2, /*hidden argument*/NULL);
		cell_isFood_m213F08D7ABE5B167DB7707B44A1B176941FBDDED_inline(L_3, (bool)0, /*hidden argument*/NULL);
		// eaten=true;
		__this->set_eaten_4((bool)1);
		// mealsCount++;
		int32_t L_4 = __this->get_mealsCount_7();
		__this->set_mealsCount_7(((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)));
		// }
		return;
	}
}
// System.Int32 food::getMealsCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t food_getMealsCount_m2FC192A4AFEF2441E3A95C6746D52D446497CCA4 (food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * __this, const RuntimeMethod* method)
{
	{
		// return mealsCount;
		int32_t L_0 = __this->get_mealsCount_7();
		return L_0;
	}
}
// System.Void food::putFoodHere()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void food_putFoodHere_mDFC90E1E8AECBD14DB870CF46ACF7B9E251D7B15 (food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * __this, const RuntimeMethod* method)
{
	{
		// void putFoodHere(){}
		return;
	}
}
// System.Void food::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void food_Start_m57ED37224057ACCD38E22B18CD1FCDDBE350ACBE (food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * __this, const RuntimeMethod* method)
{
	{
		// eaten=true;
		__this->set_eaten_4((bool)1);
		// isFoodOn=true;
		__this->set_isFoodOn_8((bool)1);
		// }
		return;
	}
}
// System.Void food::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void food_Update_mFB7524EB9A759BA35B851CDA3EF8A0F92FE9E1B2 (food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral68AD5F6C7F659E84E37BEB4108167B9C334CF234);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (eaten && isFoodOn){
		bool L_0 = __this->get_eaten_4();
		if (!L_0)
		{
			goto IL_011a;
		}
	}
	{
		bool L_1 = __this->get_isFoodOn_8();
		if (!L_1)
		{
			goto IL_011a;
		}
	}
	{
		// foodX = Random.Range(0,mapGen.sizeX);
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_2 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		int32_t L_3;
		L_3 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, L_2, /*hidden argument*/NULL);
		__this->set_foodX_5(L_3);
		// foodY = Random.Range(0,mapGen.sizeY);
		int32_t L_4 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		int32_t L_5;
		L_5 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, L_4, /*hidden argument*/NULL);
		__this->set_foodY_6(L_5);
		// apple = (cell) temp.getCellAt(foodX,foodY);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_6 = __this->get_temp_9();
		int32_t L_7 = __this->get_foodX_5();
		int32_t L_8 = __this->get_foodY_6();
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_9;
		L_9 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->set_apple_10(L_9);
		goto IL_0096;
	}

IL_0057:
	{
		// foodX = Random.Range(0,mapGen.sizeX);
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_10 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		int32_t L_11;
		L_11 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, L_10, /*hidden argument*/NULL);
		__this->set_foodX_5(L_11);
		// foodY = Random.Range(0,mapGen.sizeY);
		int32_t L_12 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		int32_t L_13;
		L_13 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, L_12, /*hidden argument*/NULL);
		__this->set_foodY_6(L_13);
		// apple = (cell) temp.getCellAt(foodX,foodY);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_14 = __this->get_temp_9();
		int32_t L_15 = __this->get_foodX_5();
		int32_t L_16 = __this->get_foodY_6();
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_17;
		L_17 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_14, L_15, L_16, /*hidden argument*/NULL);
		__this->set_apple_10(L_17);
	}

IL_0096:
	{
		// while(apple.getObst() || apple.getSnek() || (temp.getPosXn()==foodX && temp.getPosYn()==foodY)){
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_18 = __this->get_apple_10();
		bool L_19;
		L_19 = cell_getObst_m01A4AB2FC92883533D633DFB00B0293688033197_inline(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0057;
		}
	}
	{
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_20 = __this->get_apple_10();
		bool L_21;
		L_21 = cell_getSnek_m0571C8B3866F69B57E862C8E5F0151E587D7D5E0_inline(L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_0057;
		}
	}
	{
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_22 = __this->get_temp_9();
		int32_t L_23;
		L_23 = mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F(L_22, /*hidden argument*/NULL);
		int32_t L_24 = __this->get_foodX_5();
		if ((!(((uint32_t)L_23) == ((uint32_t)L_24))))
		{
			goto IL_00d6;
		}
	}
	{
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_25 = __this->get_temp_9();
		int32_t L_26;
		L_26 = mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70(L_25, /*hidden argument*/NULL);
		int32_t L_27 = __this->get_foodY_6();
		if ((((int32_t)L_26) == ((int32_t)L_27)))
		{
			goto IL_0057;
		}
	}

IL_00d6:
	{
		// this.transform.position = new Vector3(apple.getCellXn()-mapGen.sizeX/2,apple.getCellYn()-mapGen.sizeY/2,0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_28;
		L_28 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_29 = __this->get_apple_10();
		int32_t L_30;
		L_30 = cell_getCellXn_m290BAFE1347F788D453D34BDC5240DAC865E8590_inline(L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_31 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_32 = __this->get_apple_10();
		int32_t L_33;
		L_33 = cell_getCellYn_m706FEF38E9A39770DC1C68C83A58AB266A032060_inline(L_32, /*hidden argument*/NULL);
		int32_t L_34 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35;
		memset((&L_35), 0, sizeof(L_35));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_35), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_30, (int32_t)((int32_t)((int32_t)L_31/(int32_t)2)))))), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_33, (int32_t)((int32_t)((int32_t)L_34/(int32_t)2)))))), (0.0f), /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_28, L_35, /*hidden argument*/NULL);
		// eaten=false;
		__this->set_eaten_4((bool)0);
	}

IL_011a:
	{
		// ((cell) temp.getCellAt(foodX,foodY)).isFood(true);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_36 = __this->get_temp_9();
		int32_t L_37 = __this->get_foodX_5();
		int32_t L_38 = __this->get_foodY_6();
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_39;
		L_39 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_36, L_37, L_38, /*hidden argument*/NULL);
		cell_isFood_m213F08D7ABE5B167DB7707B44A1B176941FBDDED_inline(L_39, (bool)1, /*hidden argument*/NULL);
		// score = mealsCount * 10;
		int32_t L_40 = __this->get_mealsCount_7();
		((food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_StaticFields*)il2cpp_codegen_static_fields_for(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_il2cpp_TypeInfo_var))->set_score_12(((int32_t)il2cpp_codegen_multiply((int32_t)L_40, (int32_t)((int32_t)10))));
		// txt.text = "Score=" + score;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_41 = __this->get_txt_11();
		String_t* L_42;
		L_42 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(((food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_StaticFields*)il2cpp_codegen_static_fields_for(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_il2cpp_TypeInfo_var))->get_address_of_score_12()), /*hidden argument*/NULL);
		String_t* L_43;
		L_43 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral68AD5F6C7F659E84E37BEB4108167B9C334CF234, L_42, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_41, L_43);
		// }
		return;
	}
}
// System.Void food::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void food__ctor_mEB0D1FBE84A7074E81C87B80E09CA0DC50E5414C (food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void gameEnd::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void gameEnd_Start_m5FB82ADDFCBD2FA9A2C0B8B9443269B56E1D02DC (gameEnd_t0BD1FD2C540A61239AC275D8EB7F722AAC1A7DCA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral75B9C2589F235DD3C81AB7074613877A21D6F1F6);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		// highScore="Highscore"+(SceneManager.GetActiveScene().buildIndex-1);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  L_0;
		L_0 = SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4(/*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1;
		L_1 = Scene_get_buildIndex_mE32CE766EA0790E4636A351BA353A7FD71A11DA4((Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE *)(&V_0), /*hidden argument*/NULL);
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1));
		String_t* L_2;
		L_2 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_3;
		L_3 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral75B9C2589F235DD3C81AB7074613877A21D6F1F6, L_2, /*hidden argument*/NULL);
		__this->set_highScore_7(L_3);
		// }
		return;
	}
}
// System.Void gameEnd::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void gameEnd_Update_m84882BA196F80522A977E1722833CF410C0E0075 (gameEnd_t0BD1FD2C540A61239AC275D8EB7F722AAC1A7DCA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2B60501C4A64D5EC88692DA33B5432CCC71C434D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6AF708CCE5D643FF4CFD2CC4E65F4A36B1F1C499);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if(!playerControl.alive){
		bool L_0 = ((playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_StaticFields*)il2cpp_codegen_static_fields_for(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_il2cpp_TypeInfo_var))->get_alive_8();
		if (L_0)
		{
			goto IL_007c;
		}
	}
	{
		// endBloc.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_endBloc_4();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)1, /*hidden argument*/NULL);
		// txt.text = "Score=\n" + food.score;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2 = __this->get_txt_5();
		String_t* L_3;
		L_3 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(((food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_StaticFields*)il2cpp_codegen_static_fields_for(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_il2cpp_TypeInfo_var))->get_address_of_score_12()), /*hidden argument*/NULL);
		String_t* L_4;
		L_4 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral2B60501C4A64D5EC88692DA33B5432CCC71C434D, L_3, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_4);
		// txt2.text = "Highscore=\n" + PlayerPrefs.GetInt(highScore);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_5 = __this->get_txt2_6();
		String_t* L_6 = __this->get_highScore_7();
		int32_t L_7;
		L_7 = PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		String_t* L_8;
		L_8 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		String_t* L_9;
		L_9 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral6AF708CCE5D643FF4CFD2CC4E65F4A36B1F1C499, L_8, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_9);
		// if(PlayerPrefs.GetInt(highScore) < food.score)
		String_t* L_10 = __this->get_highScore_7();
		int32_t L_11;
		L_11 = PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986(L_10, /*hidden argument*/NULL);
		int32_t L_12 = ((food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_StaticFields*)il2cpp_codegen_static_fields_for(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_il2cpp_TypeInfo_var))->get_score_12();
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_007c;
		}
	}
	{
		// PlayerPrefs.SetInt(highScore, food.score);
		String_t* L_13 = __this->get_highScore_7();
		int32_t L_14 = ((food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_StaticFields*)il2cpp_codegen_static_fields_for(food_t03A1F65F99660B06E1CB5BF29E080CD11F687652_il2cpp_TypeInfo_var))->get_score_12();
		PlayerPrefs_SetInt_m0C5C977E960B9CA8F9AB73AF4129C3DCABD067B6(L_13, L_14, /*hidden argument*/NULL);
	}

IL_007c:
	{
		// }
		return;
	}
}
// System.Void gameEnd::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void gameEnd__ctor_mD7529F3D3BAE816E35158AC973E3420F139EB72B (gameEnd_t0BD1FD2C540A61239AC275D8EB7F722AAC1A7DCA * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void level0Config::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level0Config_Start_m996B1819CD1A2971B92BBC0217DE7F426494198C (level0Config_t922848EFFCF7A30778900D21F26B5E498E142360 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		// mapGen.sizeX=30;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_sizeX_4(((int32_t)30));
		// mapGen.sizeY=20;
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_sizeY_5(((int32_t)20));
		// mapGen.spawn=new Vector2(15f,10f);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_0), (15.0f), (10.0f), /*hidden argument*/NULL);
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_spawn_6(L_0);
		// for (int i=0; i < mapGen.sizeX; i++)
		V_0 = 0;
		goto IL_005d;
	}

IL_0026:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		V_1 = 0;
		goto IL_0051;
	}

IL_002a:
	{
		// GameObject tempCell=Instantiate(cellGen) as GameObject;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_cellGen_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_1, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		V_2 = L_2;
		// gridGen.getMap().Add(tempCell.GetComponent<cell>());
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_3 = __this->get_gridGen_4();
		ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * L_4;
		L_4 = mapGen_getMap_m627656B6E78F2C9C4FC13EBADD4DB886ABC7FE96_inline(L_3, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = V_2;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_6;
		L_6 = GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C(L_5, /*hidden argument*/GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C_RuntimeMethod_var);
		int32_t L_7;
		L_7 = VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(25 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_4, L_6);
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0051:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_10 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_002a;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_005d:
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_13 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0026;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		V_3 = 0;
		goto IL_0114;
	}

IL_006c:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		V_4 = 0;
		goto IL_0104;
	}

IL_0074:
	{
		// if(i==0 || j==0 || i== mapGen.sizeX-1 || j== mapGen.sizeY-1){
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_15 = V_4;
		if (!L_15)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_16 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_17 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_16) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)1)))))
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_18 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_19 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1))))))
		{
			goto IL_00a6;
		}
	}

IL_0090:
	{
		// gridGen.getCellAt(i,j).isObst(true);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_20 = __this->get_gridGen_4();
		int32_t L_21 = V_3;
		int32_t L_22 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_23;
		L_23 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_20, L_21, L_22, /*hidden argument*/NULL);
		cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24_inline(L_23, (bool)1, /*hidden argument*/NULL);
		// }
		goto IL_00ba;
	}

IL_00a6:
	{
		// gridGen.getCellAt(i,j).isObst(false);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_24 = __this->get_gridGen_4();
		int32_t L_25 = V_3;
		int32_t L_26 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_27;
		L_27 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_24, L_25, L_26, /*hidden argument*/NULL);
		cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24_inline(L_27, (bool)0, /*hidden argument*/NULL);
	}

IL_00ba:
	{
		// gridGen.getCellAt(i,j).mapping(i,j);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_28 = __this->get_gridGen_4();
		int32_t L_29 = V_3;
		int32_t L_30 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_31;
		L_31 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_28, L_29, L_30, /*hidden argument*/NULL);
		int32_t L_32 = V_3;
		int32_t L_33 = V_4;
		cell_mapping_m07D3B600FAE7384B84085F0A1EBAD07A1FF56212(L_31, L_32, L_33, /*hidden argument*/NULL);
		// gridGen.getCellAt(i,j).setCoords(i-mapGen.spawn.x,j-mapGen.spawn.y);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_34 = __this->get_gridGen_4();
		int32_t L_35 = V_3;
		int32_t L_36 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_37;
		L_37 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_34, L_35, L_36, /*hidden argument*/NULL);
		int32_t L_38 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		float L_39 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_x_0();
		int32_t L_40 = V_4;
		float L_41 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_y_1();
		cell_setCoords_m56B7F6A8D5A742DE7D8A2CC1BCA0C6BEE606ED9D(L_37, ((float)il2cpp_codegen_subtract((float)((float)((float)L_38)), (float)L_39)), ((float)il2cpp_codegen_subtract((float)((float)((float)L_40)), (float)L_41)), /*hidden argument*/NULL);
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_42 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)1));
	}

IL_0104:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_43 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_44 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_43) < ((int32_t)L_44)))
		{
			goto IL_0074;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_45 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)1));
	}

IL_0114:
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_46 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_47 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_46) < ((int32_t)L_47)))
		{
			goto IL_006c;
		}
	}
	{
		// }
		return;
	}
}
// System.Void level0Config::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level0Config_Update_m80C7F8877613164F86341A4E492974B5654B8165 (level0Config_t922848EFFCF7A30778900D21F26B5E498E142360 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void level0Config::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level0Config__ctor_m18A090FB16ECFCAC1085610A83C367BD3581DD98 (level0Config_t922848EFFCF7A30778900D21F26B5E498E142360 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void level1Config::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level1Config_Start_m49859E3C964DD7B6AEF7152EE160349555C87489 (level1Config_t0953E2DCDD13E635D78702D7EE04DB61CE0CD534 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		// mapGen.sizeX=30;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_sizeX_4(((int32_t)30));
		// mapGen.sizeY=20;
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_sizeY_5(((int32_t)20));
		// mapGen.spawn=new Vector2(15f,6f);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_0), (15.0f), (6.0f), /*hidden argument*/NULL);
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_spawn_6(L_0);
		// for (int i=0; i < mapGen.sizeX; i++)
		V_0 = 0;
		goto IL_005d;
	}

IL_0026:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		V_1 = 0;
		goto IL_0051;
	}

IL_002a:
	{
		// GameObject tempCell=Instantiate(cellGen) as GameObject;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_cellGen_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_1, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		V_2 = L_2;
		// gridGen.getMap().Add(tempCell.GetComponent<cell>());
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_3 = __this->get_gridGen_4();
		ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * L_4;
		L_4 = mapGen_getMap_m627656B6E78F2C9C4FC13EBADD4DB886ABC7FE96_inline(L_3, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = V_2;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_6;
		L_6 = GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C(L_5, /*hidden argument*/GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C_RuntimeMethod_var);
		int32_t L_7;
		L_7 = VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(25 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_4, L_6);
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0051:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_10 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_002a;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_005d:
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_13 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0026;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		V_3 = 0;
		goto IL_0123;
	}

IL_006c:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		V_4 = 0;
		goto IL_0113;
	}

IL_0074:
	{
		// if(i==0 || j==0 || i== mapGen.sizeX-1 || j== mapGen.sizeY-1 || (i>=12 && i<=17 && j>=7 && j<=12)){
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_15 = V_4;
		if (!L_15)
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_16 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_17 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_16) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)1)))))
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_18 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_19 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_18) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1)))))
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_20 = V_3;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)12))))
		{
			goto IL_00bb;
		}
	}
	{
		int32_t L_21 = V_3;
		if ((((int32_t)L_21) > ((int32_t)((int32_t)17))))
		{
			goto IL_00bb;
		}
	}
	{
		int32_t L_22 = V_4;
		if ((((int32_t)L_22) < ((int32_t)7)))
		{
			goto IL_00bb;
		}
	}
	{
		int32_t L_23 = V_4;
		if ((((int32_t)L_23) > ((int32_t)((int32_t)12))))
		{
			goto IL_00bb;
		}
	}

IL_00a5:
	{
		// gridGen.getCellAt(i,j).isObst(true);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_24 = __this->get_gridGen_4();
		int32_t L_25 = V_3;
		int32_t L_26 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_27;
		L_27 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_24, L_25, L_26, /*hidden argument*/NULL);
		cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24_inline(L_27, (bool)1, /*hidden argument*/NULL);
		// }
		goto IL_00cf;
	}

IL_00bb:
	{
		// gridGen.getCellAt(i,j).isObst(false);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_28 = __this->get_gridGen_4();
		int32_t L_29 = V_3;
		int32_t L_30 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_31;
		L_31 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_28, L_29, L_30, /*hidden argument*/NULL);
		cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24_inline(L_31, (bool)0, /*hidden argument*/NULL);
	}

IL_00cf:
	{
		// gridGen.getCellAt(i,j).mapping(i,j);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_32 = __this->get_gridGen_4();
		int32_t L_33 = V_3;
		int32_t L_34 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_35;
		L_35 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_32, L_33, L_34, /*hidden argument*/NULL);
		int32_t L_36 = V_3;
		int32_t L_37 = V_4;
		cell_mapping_m07D3B600FAE7384B84085F0A1EBAD07A1FF56212(L_35, L_36, L_37, /*hidden argument*/NULL);
		// gridGen.getCellAt(i,j).setCoords(i-mapGen.sizeX/2,j-mapGen.sizeY/2);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_38 = __this->get_gridGen_4();
		int32_t L_39 = V_3;
		int32_t L_40 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_41;
		L_41 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_38, L_39, L_40, /*hidden argument*/NULL);
		int32_t L_42 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_43 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		int32_t L_44 = V_4;
		int32_t L_45 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		cell_setCoords_m56B7F6A8D5A742DE7D8A2CC1BCA0C6BEE606ED9D(L_41, ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_42, (int32_t)((int32_t)((int32_t)L_43/(int32_t)2)))))), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_44, (int32_t)((int32_t)((int32_t)L_45/(int32_t)2)))))), /*hidden argument*/NULL);
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_46 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)1));
	}

IL_0113:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_47 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_48 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_47) < ((int32_t)L_48)))
		{
			goto IL_0074;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_49 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)1));
	}

IL_0123:
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_50 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_51 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_50) < ((int32_t)L_51)))
		{
			goto IL_006c;
		}
	}
	{
		// }
		return;
	}
}
// System.Void level1Config::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level1Config_Update_mD54222741E271F7DAAA845D9442178D540E9CE50 (level1Config_t0953E2DCDD13E635D78702D7EE04DB61CE0CD534 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void level1Config::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level1Config__ctor_mE888C59971E05B535943D9EB81AA9FA176ECA7D7 (level1Config_t0953E2DCDD13E635D78702D7EE04DB61CE0CD534 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void level2Config::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level2Config_Start_m887169D2B97F875EFCAFA2C8C7403355A7D67941 (level2Config_t3AE5B1517E64CF7064471F2A15A0D043104F6074 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		// mapGen.sizeX=30;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_sizeX_4(((int32_t)30));
		// mapGen.sizeY=20;
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_sizeY_5(((int32_t)20));
		// mapGen.spawn=new Vector2(15f,10f);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_0), (15.0f), (10.0f), /*hidden argument*/NULL);
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_spawn_6(L_0);
		// for (int i=0; i < mapGen.sizeX; i++)
		V_0 = 0;
		goto IL_005d;
	}

IL_0026:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		V_1 = 0;
		goto IL_0051;
	}

IL_002a:
	{
		// GameObject tempCell=Instantiate(cellGen) as GameObject;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_cellGen_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_1, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		V_2 = L_2;
		// gridGen.getMap().Add(tempCell.GetComponent<cell>());
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_3 = __this->get_gridGen_4();
		ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * L_4;
		L_4 = mapGen_getMap_m627656B6E78F2C9C4FC13EBADD4DB886ABC7FE96_inline(L_3, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = V_2;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_6;
		L_6 = GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C(L_5, /*hidden argument*/GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C_RuntimeMethod_var);
		int32_t L_7;
		L_7 = VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(25 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_4, L_6);
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0051:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_10 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_002a;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_005d:
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_13 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0026;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		V_3 = 0;
		goto IL_0123;
	}

IL_006c:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		V_4 = 0;
		goto IL_0113;
	}

IL_0074:
	{
		// if(i==0 || j==0 || i== mapGen.sizeX-1 || j== mapGen.sizeY-1 || (i>=11 && i<=18 && (j==7 || j==12))){
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_15 = V_4;
		if (!L_15)
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_16 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_17 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_16) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)1)))))
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_18 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_19 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_18) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1)))))
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_20 = V_3;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)11))))
		{
			goto IL_00bb;
		}
	}
	{
		int32_t L_21 = V_3;
		if ((((int32_t)L_21) > ((int32_t)((int32_t)18))))
		{
			goto IL_00bb;
		}
	}
	{
		int32_t L_22 = V_4;
		if ((((int32_t)L_22) == ((int32_t)7)))
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_23 = V_4;
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_00bb;
		}
	}

IL_00a5:
	{
		// gridGen.getCellAt(i,j).isObst(true);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_24 = __this->get_gridGen_4();
		int32_t L_25 = V_3;
		int32_t L_26 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_27;
		L_27 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_24, L_25, L_26, /*hidden argument*/NULL);
		cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24_inline(L_27, (bool)1, /*hidden argument*/NULL);
		// }
		goto IL_00cf;
	}

IL_00bb:
	{
		// gridGen.getCellAt(i,j).isObst(false);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_28 = __this->get_gridGen_4();
		int32_t L_29 = V_3;
		int32_t L_30 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_31;
		L_31 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_28, L_29, L_30, /*hidden argument*/NULL);
		cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24_inline(L_31, (bool)0, /*hidden argument*/NULL);
	}

IL_00cf:
	{
		// gridGen.getCellAt(i,j).mapping(i,j);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_32 = __this->get_gridGen_4();
		int32_t L_33 = V_3;
		int32_t L_34 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_35;
		L_35 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_32, L_33, L_34, /*hidden argument*/NULL);
		int32_t L_36 = V_3;
		int32_t L_37 = V_4;
		cell_mapping_m07D3B600FAE7384B84085F0A1EBAD07A1FF56212(L_35, L_36, L_37, /*hidden argument*/NULL);
		// gridGen.getCellAt(i,j).setCoords(i-mapGen.sizeX/2,j-mapGen.sizeY/2);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_38 = __this->get_gridGen_4();
		int32_t L_39 = V_3;
		int32_t L_40 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_41;
		L_41 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_38, L_39, L_40, /*hidden argument*/NULL);
		int32_t L_42 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_43 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		int32_t L_44 = V_4;
		int32_t L_45 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		cell_setCoords_m56B7F6A8D5A742DE7D8A2CC1BCA0C6BEE606ED9D(L_41, ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_42, (int32_t)((int32_t)((int32_t)L_43/(int32_t)2)))))), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_44, (int32_t)((int32_t)((int32_t)L_45/(int32_t)2)))))), /*hidden argument*/NULL);
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_46 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)1));
	}

IL_0113:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_47 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_48 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_47) < ((int32_t)L_48)))
		{
			goto IL_0074;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_49 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)1));
	}

IL_0123:
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_50 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_51 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_50) < ((int32_t)L_51)))
		{
			goto IL_006c;
		}
	}
	{
		// gridGen.setPos(mapGen.spawn.x,mapGen.spawn.y);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_52 = __this->get_gridGen_4();
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		float L_53 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_x_0();
		float L_54 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_y_1();
		mapGen_setPos_mFEED8A85AE9BBE2FB29248E997B90CBB0ABE73B0(L_52, L_53, L_54, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void level2Config::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level2Config_Update_m6EEE856EBBF7EB4470EC24B4373F0A61C138F0CA (level2Config_t3AE5B1517E64CF7064471F2A15A0D043104F6074 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void level2Config::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level2Config__ctor_m1A7E3E66C4275D48F940F9FF0E477EFC0902481C (level2Config_t3AE5B1517E64CF7064471F2A15A0D043104F6074 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void level3Config::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level3Config_Start_mA03FEE0BDC2BB18BFF0B000FCC4DE65658DF4A6E (level3Config_tF052783C756D2BB48C5C9F7FD394EC7A37688853 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		// mapGen.sizeX=30;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_sizeX_4(((int32_t)30));
		// mapGen.sizeY=20;
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_sizeY_5(((int32_t)20));
		// mapGen.spawn=new Vector2(5f,21f);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_0), (5.0f), (21.0f), /*hidden argument*/NULL);
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_spawn_6(L_0);
		// for (int i=0; i < mapGen.sizeX; i++)
		V_0 = 0;
		goto IL_005d;
	}

IL_0026:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		V_1 = 0;
		goto IL_0051;
	}

IL_002a:
	{
		// GameObject tempCell=Instantiate(cellGen) as GameObject;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_cellGen_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_1, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		V_2 = L_2;
		// gridGen.getMap().Add(tempCell.GetComponent<cell>());
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_3 = __this->get_gridGen_4();
		ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * L_4;
		L_4 = mapGen_getMap_m627656B6E78F2C9C4FC13EBADD4DB886ABC7FE96_inline(L_3, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = V_2;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_6;
		L_6 = GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C(L_5, /*hidden argument*/GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C_RuntimeMethod_var);
		int32_t L_7;
		L_7 = VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(25 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_4, L_6);
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0051:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_10 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_002a;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_005d:
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_13 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0026;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		V_3 = 0;
		goto IL_012c;
	}

IL_006c:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		V_4 = 0;
		goto IL_011c;
	}

IL_0074:
	{
		// if(i==0 || j==0 || i== mapGen.sizeX-1 || j== mapGen.sizeY-1 || (j>=6 && j<=13 && (i<=5 || i>=24 || (i>=12 && i<=17)))){
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_00ae;
		}
	}
	{
		int32_t L_15 = V_4;
		if (!L_15)
		{
			goto IL_00ae;
		}
	}
	{
		int32_t L_16 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_17 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_16) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)1)))))
		{
			goto IL_00ae;
		}
	}
	{
		int32_t L_18 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_19 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_18) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1)))))
		{
			goto IL_00ae;
		}
	}
	{
		int32_t L_20 = V_4;
		if ((((int32_t)L_20) < ((int32_t)6)))
		{
			goto IL_00c4;
		}
	}
	{
		int32_t L_21 = V_4;
		if ((((int32_t)L_21) > ((int32_t)((int32_t)13))))
		{
			goto IL_00c4;
		}
	}
	{
		int32_t L_22 = V_3;
		if ((((int32_t)L_22) <= ((int32_t)5)))
		{
			goto IL_00ae;
		}
	}
	{
		int32_t L_23 = V_3;
		if ((((int32_t)L_23) >= ((int32_t)((int32_t)24))))
		{
			goto IL_00ae;
		}
	}
	{
		int32_t L_24 = V_3;
		if ((((int32_t)L_24) < ((int32_t)((int32_t)12))))
		{
			goto IL_00c4;
		}
	}
	{
		int32_t L_25 = V_3;
		if ((((int32_t)L_25) > ((int32_t)((int32_t)17))))
		{
			goto IL_00c4;
		}
	}

IL_00ae:
	{
		// gridGen.getCellAt(i,j).isObst(true);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_26 = __this->get_gridGen_4();
		int32_t L_27 = V_3;
		int32_t L_28 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_29;
		L_29 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_26, L_27, L_28, /*hidden argument*/NULL);
		cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24_inline(L_29, (bool)1, /*hidden argument*/NULL);
		// }
		goto IL_00d8;
	}

IL_00c4:
	{
		// gridGen.getCellAt(i,j).isObst(false);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_30 = __this->get_gridGen_4();
		int32_t L_31 = V_3;
		int32_t L_32 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_33;
		L_33 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_30, L_31, L_32, /*hidden argument*/NULL);
		cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24_inline(L_33, (bool)0, /*hidden argument*/NULL);
	}

IL_00d8:
	{
		// gridGen.getCellAt(i,j).mapping(i,j);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_34 = __this->get_gridGen_4();
		int32_t L_35 = V_3;
		int32_t L_36 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_37;
		L_37 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_34, L_35, L_36, /*hidden argument*/NULL);
		int32_t L_38 = V_3;
		int32_t L_39 = V_4;
		cell_mapping_m07D3B600FAE7384B84085F0A1EBAD07A1FF56212(L_37, L_38, L_39, /*hidden argument*/NULL);
		// gridGen.getCellAt(i,j).setCoords(i-mapGen.sizeX/2,j-mapGen.sizeY/2);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_40 = __this->get_gridGen_4();
		int32_t L_41 = V_3;
		int32_t L_42 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_43;
		L_43 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_40, L_41, L_42, /*hidden argument*/NULL);
		int32_t L_44 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_45 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		int32_t L_46 = V_4;
		int32_t L_47 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		cell_setCoords_m56B7F6A8D5A742DE7D8A2CC1BCA0C6BEE606ED9D(L_43, ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_44, (int32_t)((int32_t)((int32_t)L_45/(int32_t)2)))))), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_46, (int32_t)((int32_t)((int32_t)L_47/(int32_t)2)))))), /*hidden argument*/NULL);
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_48 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_48, (int32_t)1));
	}

IL_011c:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_49 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_50 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_49) < ((int32_t)L_50)))
		{
			goto IL_0074;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_51 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_51, (int32_t)1));
	}

IL_012c:
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_52 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_53 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_52) < ((int32_t)L_53)))
		{
			goto IL_006c;
		}
	}
	{
		// gridGen.setPos(mapGen.spawn.x,mapGen.spawn.y);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_54 = __this->get_gridGen_4();
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		float L_55 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_x_0();
		float L_56 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_y_1();
		mapGen_setPos_mFEED8A85AE9BBE2FB29248E997B90CBB0ABE73B0(L_54, L_55, L_56, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void level3Config::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level3Config_Update_m3E3748B4A69B4463B3B5BB703AE725146F3F6952 (level3Config_tF052783C756D2BB48C5C9F7FD394EC7A37688853 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void level3Config::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level3Config__ctor_m85D1C9383B161CB33A5570E49EEFC8F8292C925C (level3Config_tF052783C756D2BB48C5C9F7FD394EC7A37688853 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void level4Config::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level4Config_Start_m303B05EA63DF5A6641B1BB3E1B4F5B9F945FB904 (level4Config_t89718633F1C441AE885A0CA12B4D08C247CAC673 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		// mapGen.sizeX=32;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_sizeX_4(((int32_t)32));
		// mapGen.sizeY=32;
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_sizeY_5(((int32_t)32));
		// mapGen.spawn=new Vector2(12f,16f);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_0), (12.0f), (16.0f), /*hidden argument*/NULL);
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_spawn_6(L_0);
		// for (int i=0; i < mapGen.sizeX; i++)
		V_0 = 0;
		goto IL_005d;
	}

IL_0026:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		V_1 = 0;
		goto IL_0051;
	}

IL_002a:
	{
		// GameObject tempCell=Instantiate(cellGen) as GameObject;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_cellGen_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_1, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		V_2 = L_2;
		// gridGen.getMap().Add(tempCell.GetComponent<cell>());
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_3 = __this->get_gridGen_4();
		ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * L_4;
		L_4 = mapGen_getMap_m627656B6E78F2C9C4FC13EBADD4DB886ABC7FE96_inline(L_3, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = V_2;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_6;
		L_6 = GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C(L_5, /*hidden argument*/GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C_RuntimeMethod_var);
		int32_t L_7;
		L_7 = VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(25 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_4, L_6);
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0051:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_10 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_002a;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_005d:
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_13 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0026;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		V_3 = 0;
		goto IL_0138;
	}

IL_006c:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		V_4 = 0;
		goto IL_0128;
	}

IL_0074:
	{
		// if(i==0 || j==0 || i== mapGen.sizeX-1 || j== mapGen.sizeY-1 || (j>=12 && j<=19 && (i<=8 || i>=23)) || (i>=12 && i<=19 && (j<=8 || j>=23))){
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_15 = V_4;
		if (!L_15)
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_16 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_17 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_16) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)1)))))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_18 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_19 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_18) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1)))))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_20 = V_4;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)12))))
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_21 = V_4;
		if ((((int32_t)L_21) > ((int32_t)((int32_t)19))))
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_22 = V_3;
		if ((((int32_t)L_22) <= ((int32_t)8)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_23 = V_3;
		if ((((int32_t)L_23) >= ((int32_t)((int32_t)23))))
		{
			goto IL_00ba;
		}
	}

IL_00a5:
	{
		int32_t L_24 = V_3;
		if ((((int32_t)L_24) < ((int32_t)((int32_t)12))))
		{
			goto IL_00d0;
		}
	}
	{
		int32_t L_25 = V_3;
		if ((((int32_t)L_25) > ((int32_t)((int32_t)19))))
		{
			goto IL_00d0;
		}
	}
	{
		int32_t L_26 = V_4;
		if ((((int32_t)L_26) <= ((int32_t)8)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_27 = V_4;
		if ((((int32_t)L_27) < ((int32_t)((int32_t)23))))
		{
			goto IL_00d0;
		}
	}

IL_00ba:
	{
		// gridGen.getCellAt(i,j).isObst(true);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_28 = __this->get_gridGen_4();
		int32_t L_29 = V_3;
		int32_t L_30 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_31;
		L_31 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_28, L_29, L_30, /*hidden argument*/NULL);
		cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24_inline(L_31, (bool)1, /*hidden argument*/NULL);
		// }
		goto IL_00e4;
	}

IL_00d0:
	{
		// gridGen.getCellAt(i,j).isObst(false);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_32 = __this->get_gridGen_4();
		int32_t L_33 = V_3;
		int32_t L_34 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_35;
		L_35 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_32, L_33, L_34, /*hidden argument*/NULL);
		cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24_inline(L_35, (bool)0, /*hidden argument*/NULL);
	}

IL_00e4:
	{
		// gridGen.getCellAt(i,j).mapping(i,j);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_36 = __this->get_gridGen_4();
		int32_t L_37 = V_3;
		int32_t L_38 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_39;
		L_39 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_36, L_37, L_38, /*hidden argument*/NULL);
		int32_t L_40 = V_3;
		int32_t L_41 = V_4;
		cell_mapping_m07D3B600FAE7384B84085F0A1EBAD07A1FF56212(L_39, L_40, L_41, /*hidden argument*/NULL);
		// gridGen.getCellAt(i,j).setCoords(i-mapGen.sizeX/2,j-mapGen.sizeY/2);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_42 = __this->get_gridGen_4();
		int32_t L_43 = V_3;
		int32_t L_44 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_45;
		L_45 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_42, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_46 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_47 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		int32_t L_48 = V_4;
		int32_t L_49 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		cell_setCoords_m56B7F6A8D5A742DE7D8A2CC1BCA0C6BEE606ED9D(L_45, ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_46, (int32_t)((int32_t)((int32_t)L_47/(int32_t)2)))))), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_48, (int32_t)((int32_t)((int32_t)L_49/(int32_t)2)))))), /*hidden argument*/NULL);
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_50 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_50, (int32_t)1));
	}

IL_0128:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_51 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_52 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_51) < ((int32_t)L_52)))
		{
			goto IL_0074;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_53 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_53, (int32_t)1));
	}

IL_0138:
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_54 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_55 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_54) < ((int32_t)L_55)))
		{
			goto IL_006c;
		}
	}
	{
		// gridGen.setPos(mapGen.spawn.x,mapGen.spawn.y);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_56 = __this->get_gridGen_4();
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		float L_57 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_x_0();
		float L_58 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_y_1();
		mapGen_setPos_mFEED8A85AE9BBE2FB29248E997B90CBB0ABE73B0(L_56, L_57, L_58, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void level4Config::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level4Config_Update_m27369AA08EB2F605793899F3FC1313062207F8DB (level4Config_t89718633F1C441AE885A0CA12B4D08C247CAC673 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void level4Config::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level4Config__ctor_m0374F7C5AB219A0F2C266C33672F83DEE77EFA8B (level4Config_t89718633F1C441AE885A0CA12B4D08C247CAC673 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void level5Config::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level5Config_Start_m1C4E872F79EC978714CFF487D6BE48DB5AD3987E (level5Config_tC75394A13F0C653B2A28C5C7B7E4EFA690CB7CD2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		// mapGen.sizeX=33;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_sizeX_4(((int32_t)33));
		// mapGen.sizeY=33;
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_sizeY_5(((int32_t)33));
		// mapGen.spawn=new Vector2(3f,13f);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_0), (3.0f), (13.0f), /*hidden argument*/NULL);
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_spawn_6(L_0);
		// for (int i=0; i < mapGen.sizeX; i++)
		V_0 = 0;
		goto IL_005d;
	}

IL_0026:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		V_1 = 0;
		goto IL_0051;
	}

IL_002a:
	{
		// GameObject tempCell=Instantiate(cellGen) as GameObject;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_cellGen_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_1, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		V_2 = L_2;
		// gridGen.getMap().Add(tempCell.GetComponent<cell>());
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_3 = __this->get_gridGen_4();
		ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * L_4;
		L_4 = mapGen_getMap_m627656B6E78F2C9C4FC13EBADD4DB886ABC7FE96_inline(L_3, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = V_2;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_6;
		L_6 = GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C(L_5, /*hidden argument*/GameObject_GetComponent_Tiscell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_m7BB4B8A8579392FCAFFC5F5A3E9D0635B5CD492C_RuntimeMethod_var);
		int32_t L_7;
		L_7 = VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(25 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_4, L_6);
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0051:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_10 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_002a;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_005d:
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_13 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0026;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		V_3 = 0;
		goto IL_0157;
	}

IL_006c:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		V_4 = 0;
		goto IL_0147;
	}

IL_0074:
	{
		// if(i==0 || j==0 || i== mapGen.sizeX-1 || j== mapGen.sizeY-1 || (j>=8 && j<=24 && i==16) || (i<=24 && i>=8 && j==16) || ((i==11 || i==21) && (j<=8 || j>=24)) || ((j==11 || j==21) && (i<=8 || i>=24))){
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_15 = V_4;
		if (!L_15)
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_16 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_17 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_16) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)1)))))
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_18 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_19 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_18) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1)))))
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_20 = V_4;
		if ((((int32_t)L_20) < ((int32_t)8)))
		{
			goto IL_00a0;
		}
	}
	{
		int32_t L_21 = V_4;
		if ((((int32_t)L_21) > ((int32_t)((int32_t)24))))
		{
			goto IL_00a0;
		}
	}
	{
		int32_t L_22 = V_3;
		if ((((int32_t)L_22) == ((int32_t)((int32_t)16))))
		{
			goto IL_00d9;
		}
	}

IL_00a0:
	{
		int32_t L_23 = V_3;
		if ((((int32_t)L_23) > ((int32_t)((int32_t)24))))
		{
			goto IL_00af;
		}
	}
	{
		int32_t L_24 = V_3;
		if ((((int32_t)L_24) < ((int32_t)8)))
		{
			goto IL_00af;
		}
	}
	{
		int32_t L_25 = V_4;
		if ((((int32_t)L_25) == ((int32_t)((int32_t)16))))
		{
			goto IL_00d9;
		}
	}

IL_00af:
	{
		int32_t L_26 = V_3;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)11))))
		{
			goto IL_00b9;
		}
	}
	{
		int32_t L_27 = V_3;
		if ((!(((uint32_t)L_27) == ((uint32_t)((int32_t)21)))))
		{
			goto IL_00c4;
		}
	}

IL_00b9:
	{
		int32_t L_28 = V_4;
		if ((((int32_t)L_28) <= ((int32_t)8)))
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_29 = V_4;
		if ((((int32_t)L_29) >= ((int32_t)((int32_t)24))))
		{
			goto IL_00d9;
		}
	}

IL_00c4:
	{
		int32_t L_30 = V_4;
		if ((((int32_t)L_30) == ((int32_t)((int32_t)11))))
		{
			goto IL_00d0;
		}
	}
	{
		int32_t L_31 = V_4;
		if ((!(((uint32_t)L_31) == ((uint32_t)((int32_t)21)))))
		{
			goto IL_00ef;
		}
	}

IL_00d0:
	{
		int32_t L_32 = V_3;
		if ((((int32_t)L_32) <= ((int32_t)8)))
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_33 = V_3;
		if ((((int32_t)L_33) < ((int32_t)((int32_t)24))))
		{
			goto IL_00ef;
		}
	}

IL_00d9:
	{
		// gridGen.getCellAt(i,j).isObst(true);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_34 = __this->get_gridGen_4();
		int32_t L_35 = V_3;
		int32_t L_36 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_37;
		L_37 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_34, L_35, L_36, /*hidden argument*/NULL);
		cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24_inline(L_37, (bool)1, /*hidden argument*/NULL);
		// }
		goto IL_0103;
	}

IL_00ef:
	{
		// gridGen.getCellAt(i,j).isObst(false);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_38 = __this->get_gridGen_4();
		int32_t L_39 = V_3;
		int32_t L_40 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_41;
		L_41 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_38, L_39, L_40, /*hidden argument*/NULL);
		cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24_inline(L_41, (bool)0, /*hidden argument*/NULL);
	}

IL_0103:
	{
		// gridGen.getCellAt(i,j).mapping(i,j);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_42 = __this->get_gridGen_4();
		int32_t L_43 = V_3;
		int32_t L_44 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_45;
		L_45 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_42, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_46 = V_3;
		int32_t L_47 = V_4;
		cell_mapping_m07D3B600FAE7384B84085F0A1EBAD07A1FF56212(L_45, L_46, L_47, /*hidden argument*/NULL);
		// gridGen.getCellAt(i,j).setCoords(i-mapGen.sizeX/2,j-mapGen.sizeY/2);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_48 = __this->get_gridGen_4();
		int32_t L_49 = V_3;
		int32_t L_50 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_51;
		L_51 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_48, L_49, L_50, /*hidden argument*/NULL);
		int32_t L_52 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_53 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		int32_t L_54 = V_4;
		int32_t L_55 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		cell_setCoords_m56B7F6A8D5A742DE7D8A2CC1BCA0C6BEE606ED9D(L_51, ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_52, (int32_t)((int32_t)((int32_t)L_53/(int32_t)2)))))), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_54, (int32_t)((int32_t)((int32_t)L_55/(int32_t)2)))))), /*hidden argument*/NULL);
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_56 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_56, (int32_t)1));
	}

IL_0147:
	{
		// for (int j=0; j < mapGen.sizeY; j++){
		int32_t L_57 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_58 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_57) < ((int32_t)L_58)))
		{
			goto IL_0074;
		}
	}
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_59 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_59, (int32_t)1));
	}

IL_0157:
	{
		// for (int i=0; i < mapGen.sizeX; i++)
		int32_t L_60 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_61 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_60) < ((int32_t)L_61)))
		{
			goto IL_006c;
		}
	}
	{
		// gridGen.setPos(mapGen.spawn.x,mapGen.spawn.y);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_62 = __this->get_gridGen_4();
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		float L_63 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_x_0();
		float L_64 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_y_1();
		mapGen_setPos_mFEED8A85AE9BBE2FB29248E997B90CBB0ABE73B0(L_62, L_63, L_64, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void level5Config::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level5Config_Update_m29E597507509797854EEE62BC9DC206385F8D893 (level5Config_tC75394A13F0C653B2A28C5C7B7E4EFA690CB7CD2 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void level5Config::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void level5Config__ctor_m036275E1861748127B238C25AD00CF405BB1B373 (level5Config_tC75394A13F0C653B2A28C5C7B7E4EFA690CB7CD2 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void mainMenu::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void mainMenu_Start_m3409ECFCEA4CA479FCD8643BF891EC0FF0D4DCBC (mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339 * __this, const RuntimeMethod* method)
{
	{
		// bloc.setBloc(false);
		bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7 * L_0 = __this->get_bloc_4();
		bloc_setBloc_m63AAC7F1ECD6DD0D60B5E9DD66B1E8A39A03C186_inline(L_0, (bool)0, /*hidden argument*/NULL);
		// contBox.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_contBox_5();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)1, /*hidden argument*/NULL);
		// survBox.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_survBox_6();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
		// advBox.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_advBox_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// lvlsBox.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_lvlsBox_8();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, (bool)0, /*hidden argument*/NULL);
		// diffBox.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_diffBox_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void mainMenu::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void mainMenu_Update_m5D44F29640B47BD5A96523B2DF21656B31D13830 (mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_Tisbutton_t4F901F446B79F86CB9593995276166F7ADD407E6_m85F68D44BCA088B03954BB7ADE6D419C2B05AFEC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(contBox.GetComponent<button>().isBtnSet()){
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_contBox_5();
		button_t4F901F446B79F86CB9593995276166F7ADD407E6 * L_1;
		L_1 = GameObject_GetComponent_Tisbutton_t4F901F446B79F86CB9593995276166F7ADD407E6_m85F68D44BCA088B03954BB7ADE6D419C2B05AFEC(L_0, /*hidden argument*/GameObject_GetComponent_Tisbutton_t4F901F446B79F86CB9593995276166F7ADD407E6_m85F68D44BCA088B03954BB7ADE6D419C2B05AFEC_RuntimeMethod_var);
		bool L_2;
		L_2 = button_isBtnSet_mA09EFC2279D396533426BC9D0B19401285896182_inline(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004e;
		}
	}
	{
		// bloc.setBloc(true);
		bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7 * L_3 = __this->get_bloc_4();
		bloc_setBloc_m63AAC7F1ECD6DD0D60B5E9DD66B1E8A39A03C186_inline(L_3, (bool)1, /*hidden argument*/NULL);
		// contBox.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_contBox_5();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, (bool)0, /*hidden argument*/NULL);
		// survBox.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_survBox_6();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)1, /*hidden argument*/NULL);
		// advBox.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = __this->get_advBox_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_6, (bool)1, /*hidden argument*/NULL);
		// back.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = __this->get_back_10();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_7, (bool)1, /*hidden argument*/NULL);
	}

IL_004e:
	{
		// if(advBox.GetComponent<button>().isBtnSet()){
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = __this->get_advBox_7();
		button_t4F901F446B79F86CB9593995276166F7ADD407E6 * L_9;
		L_9 = GameObject_GetComponent_Tisbutton_t4F901F446B79F86CB9593995276166F7ADD407E6_m85F68D44BCA088B03954BB7ADE6D419C2B05AFEC(L_8, /*hidden argument*/GameObject_GetComponent_Tisbutton_t4F901F446B79F86CB9593995276166F7ADD407E6_m85F68D44BCA088B03954BB7ADE6D419C2B05AFEC_RuntimeMethod_var);
		bool L_10;
		L_10 = button_isBtnSet_mA09EFC2279D396533426BC9D0B19401285896182_inline(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0084;
		}
	}
	{
		// survBox.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11 = __this->get_survBox_6();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_11, (bool)0, /*hidden argument*/NULL);
		// advBox.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12 = __this->get_advBox_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_12, (bool)0, /*hidden argument*/NULL);
		// lvlsBox.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13 = __this->get_lvlsBox_8();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_13, (bool)1, /*hidden argument*/NULL);
	}

IL_0084:
	{
		// if(survBox.GetComponent<button>().isBtnSet()){
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14 = __this->get_survBox_6();
		button_t4F901F446B79F86CB9593995276166F7ADD407E6 * L_15;
		L_15 = GameObject_GetComponent_Tisbutton_t4F901F446B79F86CB9593995276166F7ADD407E6_m85F68D44BCA088B03954BB7ADE6D419C2B05AFEC(L_14, /*hidden argument*/GameObject_GetComponent_Tisbutton_t4F901F446B79F86CB9593995276166F7ADD407E6_m85F68D44BCA088B03954BB7ADE6D419C2B05AFEC_RuntimeMethod_var);
		bool L_16;
		L_16 = button_isBtnSet_mA09EFC2279D396533426BC9D0B19401285896182_inline(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00ba;
		}
	}
	{
		// survBox.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17 = __this->get_survBox_6();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_17, (bool)0, /*hidden argument*/NULL);
		// advBox.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_18 = __this->get_advBox_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_18, (bool)0, /*hidden argument*/NULL);
		// diffBox.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19 = __this->get_diffBox_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_19, (bool)1, /*hidden argument*/NULL);
	}

IL_00ba:
	{
		// if(back.GetComponent<button>().isBtnSet()){
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_20 = __this->get_back_10();
		button_t4F901F446B79F86CB9593995276166F7ADD407E6 * L_21;
		L_21 = GameObject_GetComponent_Tisbutton_t4F901F446B79F86CB9593995276166F7ADD407E6_m85F68D44BCA088B03954BB7ADE6D419C2B05AFEC(L_20, /*hidden argument*/GameObject_GetComponent_Tisbutton_t4F901F446B79F86CB9593995276166F7ADD407E6_m85F68D44BCA088B03954BB7ADE6D419C2B05AFEC_RuntimeMethod_var);
		bool L_22;
		L_22 = button_isBtnSet_mA09EFC2279D396533426BC9D0B19401285896182_inline(L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0120;
		}
	}
	{
		// bloc.setBloc(false);
		bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7 * L_23 = __this->get_bloc_4();
		bloc_setBloc_m63AAC7F1ECD6DD0D60B5E9DD66B1E8A39A03C186_inline(L_23, (bool)0, /*hidden argument*/NULL);
		// contBox.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_24 = __this->get_contBox_5();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_24, (bool)1, /*hidden argument*/NULL);
		// survBox.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_25 = __this->get_survBox_6();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_25, (bool)0, /*hidden argument*/NULL);
		// advBox.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_26 = __this->get_advBox_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_26, (bool)0, /*hidden argument*/NULL);
		// lvlsBox.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_27 = __this->get_lvlsBox_8();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_27, (bool)0, /*hidden argument*/NULL);
		// diffBox.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_28 = __this->get_diffBox_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_28, (bool)0, /*hidden argument*/NULL);
		// back.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_29 = __this->get_back_10();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_29, (bool)0, /*hidden argument*/NULL);
	}

IL_0120:
	{
		// }
		return;
	}
}
// System.Void mainMenu::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void mainMenu__ctor_mE3659A1D9717CE7DB207A80A81E608028AF0ECD0 (mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean mapGen::isMapGen()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool mapGen_isMapGen_m6EFD323A2CFFE926A72DF9C68B4573A241AAC32C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return mapGenerated;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		bool L_0 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_mapGenerated_9();
		return L_0;
	}
}
// cell mapGen::getCellAt(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return (cell) getMap()[i*sizeY+j];
		ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * L_0;
		L_0 = mapGen_getMap_m627656B6E78F2C9C4FC13EBADD4DB886ABC7FE96_inline(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___i0;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_2 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		int32_t L_3 = ___j1;
		RuntimeObject * L_4;
		L_4 = VirtFuncInvoker1< RuntimeObject *, int32_t >::Invoke(23 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_2)), (int32_t)L_3)));
		return ((cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF *)CastclassClass((RuntimeObject*)L_4, cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF_il2cpp_TypeInfo_var));
	}
}
// System.Void mapGen::setPos(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void mapGen_setPos_mFEED8A85AE9BBE2FB29248E997B90CBB0ABE73B0 (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(0<=x && x<=sizeX && 0<=y && y<=sizeY){
		float L_0 = ___x0;
		if ((!(((float)(0.0f)) <= ((float)L_0))))
		{
			goto IL_0030;
		}
	}
	{
		float L_1 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_2 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((!(((float)L_1) <= ((float)((float)((float)L_2))))))
		{
			goto IL_0030;
		}
	}
	{
		float L_3 = ___y1;
		if ((!(((float)(0.0f)) <= ((float)L_3))))
		{
			goto IL_0030;
		}
	}
	{
		float L_4 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_5 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((!(((float)L_4) <= ((float)((float)((float)L_5))))))
		{
			goto IL_0030;
		}
	}
	{
		// this.floatPosX=x;
		float L_6 = ___x0;
		__this->set_floatPosX_7(L_6);
		// this.floatPosY=y;
		float L_7 = ___y1;
		__this->set_floatPosY_8(L_7);
	}

IL_0030:
	{
		// }
		return;
	}
}
// System.Single mapGen::getPosXf()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float mapGen_getPosXf_mD0B5B3B19A9D74B82799A8AE52BD112189CB5063 (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method)
{
	{
		// return this.floatPosX;
		float L_0 = __this->get_floatPosX_7();
		return L_0;
	}
}
// System.Single mapGen::getPosYf()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float mapGen_getPosYf_mF6A253CF42326AD207A29EF6A03EEF2E511D527C (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method)
{
	{
		// return this.floatPosY;
		float L_0 = __this->get_floatPosY_8();
		return L_0;
	}
}
// System.Int32 mapGen::getPosXn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method)
{
	{
		// return (int)this.floatPosX;
		float L_0 = __this->get_floatPosX_7();
		return il2cpp_codegen_cast_double_to_int<int32_t>(L_0);
	}
}
// System.Int32 mapGen::getPosYn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70 (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method)
{
	{
		// return (int)this.floatPosY;
		float L_0 = __this->get_floatPosY_8();
		return il2cpp_codegen_cast_double_to_int<int32_t>(L_0);
	}
}
// System.Collections.ArrayList mapGen::getMap()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * mapGen_getMap_m627656B6E78F2C9C4FC13EBADD4DB886ABC7FE96 (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method)
{
	{
		// return map;
		ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * L_0 = __this->get_map_10();
		return L_0;
	}
}
// System.Void mapGen::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void mapGen_Start_m09E313F8A5E1B86EC8071629A16C3B02B352EC6B (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// mapGenerated=true;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->set_mapGenerated_9((bool)1);
		// }
		return;
	}
}
// System.Void mapGen::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void mapGen_Update_m525A09BBD62FE59927D6997ED01F2C504A9F9EBA (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void mapGen::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void mapGen__ctor_mAA30D2E02E10B512B6C2C73A4F79061E4956FA53 (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public ArrayList map = new ArrayList();
		ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * L_0 = (ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 *)il2cpp_codegen_object_new(ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575_il2cpp_TypeInfo_var);
		ArrayList__ctor_m6847CFECD6BDC2AD10A4AC9852A572B88B8D6B1B(L_0, /*hidden argument*/NULL);
		__this->set_map_10(L_0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void mapGen::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void mapGen__cctor_mAC555C23DAE6DCB3296421F9AC024D72A8FA5A13 (const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 playerControl::getMoveSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t playerControl_getMoveSpeed_m80AEB733464773149D656FB2C35DFA1AC6B40679 (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method)
{
	{
		// return moveSpeed;
		int32_t L_0 = __this->get_moveSpeed_4();
		return L_0;
	}
}
// System.Single playerControl::getRotVar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float playerControl_getRotVar_mBFEA83CCFACBEE74025DDAAF2CC76DD5A7351492 (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method)
{
	{
		// return hRotation;
		float L_0 = __this->get_hRotation_12();
		return L_0;
	}
}
// System.Single playerControl::getRealRot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float playerControl_getRealRot_m196ED94BADA5895078952015608913BDBEE5ED2F (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method)
{
	{
		// return realRot;
		float L_0 = __this->get_realRot_13();
		return L_0;
	}
}
// UnityEngine.Vector3 playerControl::getNumMove()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  playerControl_getNumMove_mC2EF16C61A32C01B1E71FC5E3AF336F943F21523 (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method)
{
	{
		// return numericMove;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = __this->get_numericMove_10();
		return L_0;
	}
}
// UnityEngine.Vector3 playerControl::getCurrCell()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  playerControl_getCurrCell_m55DE57C3FFE3237AACE857B1843ED79C8A85E798 (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method)
{
	{
		// return new Vector3(currCellX,currCellY,0);
		int32_t L_0 = __this->get_currCellX_5();
		int32_t L_1 = __this->get_currCellY_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_2), ((float)((float)L_0)), ((float)((float)L_1)), (0.0f), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void playerControl::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void playerControl_Start_m79478EFE9B7FB692A35A44758A032EEAB36B2479 (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// alive=true;
		((playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_StaticFields*)il2cpp_codegen_static_fields_for(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_il2cpp_TypeInfo_var))->set_alive_8((bool)1);
		// numericMove = new Vector3(1, 0, 0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_0), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_numericMove_10(L_0);
		// realMove= new Vector3( moveSpeed * Time.deltaTime,0,0);
		int32_t L_1 = __this->get_moveSpeed_4();
		float L_2;
		L_2 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_3), ((float)il2cpp_codegen_multiply((float)((float)((float)L_1)), (float)L_2)), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_realMove_14(L_3);
		// currCellX = stdGrid.getPosXn();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_4 = __this->get_stdGrid_17();
		int32_t L_5;
		L_5 = mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F(L_4, /*hidden argument*/NULL);
		__this->set_currCellX_5(L_5);
		// currCellY = stdGrid.getPosYn();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_6 = __this->get_stdGrid_17();
		int32_t L_7;
		L_7 = mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70(L_6, /*hidden argument*/NULL);
		__this->set_currCellY_6(L_7);
		// this.transform.position = new Vector3(currCellX-mapGen.sizeX/2,currCellY-mapGen.sizeY/2,0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		int32_t L_9 = __this->get_currCellX_5();
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_10 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		int32_t L_11 = __this->get_currCellY_6();
		int32_t L_12 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		memset((&L_13), 0, sizeof(L_13));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_13), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)((int32_t)((int32_t)L_10/(int32_t)2)))))), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)((int32_t)((int32_t)L_12/(int32_t)2)))))), (0.0f), /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_8, L_13, /*hidden argument*/NULL);
		// prevNumMove=numericMove;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = __this->get_numericMove_10();
		__this->set_prevNumMove_11(L_14);
		// }
		return;
	}
}
// System.Void playerControl::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void playerControl_Update_mB0198FAC403D75B14E36B61FB6AD3F70AB3854B6 (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (moveReq & alive){
		bool L_0 = __this->get_moveReq_7();
		bool L_1 = ((playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_StaticFields*)il2cpp_codegen_static_fields_for(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_il2cpp_TypeInfo_var))->get_alive_8();
		if (!((int32_t)((int32_t)L_0&(int32_t)L_1)))
		{
			goto IL_01d1;
		}
	}
	{
		// if (Input.GetKeyDown("up") && numericMove.y != -1){
		bool L_2;
		L_2 = Input_GetKeyDown_m9D556E728119BEB64AA624EBE82931367B6573A5(_stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0083;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_3 = __this->get_address_of_numericMove_10();
		float L_4 = L_3->get_y_3();
		if ((((float)L_4) == ((float)(-1.0f))))
		{
			goto IL_0083;
		}
	}
	{
		// prevNumMove=numericMove;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = __this->get_numericMove_10();
		__this->set_prevNumMove_11(L_5);
		// numericMove = new Vector3(0, 1, 0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_numericMove_10(L_6);
		// currCellX = stdGrid.getPosXn();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_7 = __this->get_stdGrid_17();
		int32_t L_8;
		L_8 = mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F(L_7, /*hidden argument*/NULL);
		__this->set_currCellX_5(L_8);
		// currCellY = stdGrid.getPosYn();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_9 = __this->get_stdGrid_17();
		int32_t L_10;
		L_10 = mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70(L_9, /*hidden argument*/NULL);
		__this->set_currCellY_6(L_10);
		// moveReq = false;
		__this->set_moveReq_7((bool)0);
		// }
		goto IL_01d1;
	}

IL_0083:
	{
		// else if (Input.GetKeyDown("down") && numericMove.y != 1){
		bool L_11;
		L_11 = Input_GetKeyDown_m9D556E728119BEB64AA624EBE82931367B6573A5(_stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00f5;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_12 = __this->get_address_of_numericMove_10();
		float L_13 = L_12->get_y_3();
		if ((((float)L_13) == ((float)(1.0f))))
		{
			goto IL_00f5;
		}
	}
	{
		// prevNumMove=numericMove;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = __this->get_numericMove_10();
		__this->set_prevNumMove_11(L_14);
		// numericMove = new Vector3(0, -1, 0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_15), (0.0f), (-1.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_numericMove_10(L_15);
		// currCellX = stdGrid.getPosXn();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_16 = __this->get_stdGrid_17();
		int32_t L_17;
		L_17 = mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F(L_16, /*hidden argument*/NULL);
		__this->set_currCellX_5(L_17);
		// currCellY = stdGrid.getPosYn();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_18 = __this->get_stdGrid_17();
		int32_t L_19;
		L_19 = mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70(L_18, /*hidden argument*/NULL);
		__this->set_currCellY_6(L_19);
		// moveReq = false;
		__this->set_moveReq_7((bool)0);
		// }
		goto IL_01d1;
	}

IL_00f5:
	{
		// else if (Input.GetKeyDown("right") && numericMove.x != -1){
		bool L_20;
		L_20 = Input_GetKeyDown_m9D556E728119BEB64AA624EBE82931367B6573A5(_stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0164;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_21 = __this->get_address_of_numericMove_10();
		float L_22 = L_21->get_x_2();
		if ((((float)L_22) == ((float)(-1.0f))))
		{
			goto IL_0164;
		}
	}
	{
		// prevNumMove=numericMove;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23 = __this->get_numericMove_10();
		__this->set_prevNumMove_11(L_23);
		// numericMove = new Vector3(1, 0, 0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24;
		memset((&L_24), 0, sizeof(L_24));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_24), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_numericMove_10(L_24);
		// currCellX = stdGrid.getPosXn();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_25 = __this->get_stdGrid_17();
		int32_t L_26;
		L_26 = mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F(L_25, /*hidden argument*/NULL);
		__this->set_currCellX_5(L_26);
		// currCellY = stdGrid.getPosYn();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_27 = __this->get_stdGrid_17();
		int32_t L_28;
		L_28 = mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70(L_27, /*hidden argument*/NULL);
		__this->set_currCellY_6(L_28);
		// moveReq = false;
		__this->set_moveReq_7((bool)0);
		// }
		goto IL_01d1;
	}

IL_0164:
	{
		// else if (Input.GetKeyDown("left") && numericMove.x != 1){
		bool L_29;
		L_29 = Input_GetKeyDown_m9D556E728119BEB64AA624EBE82931367B6573A5(_stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_01d1;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_30 = __this->get_address_of_numericMove_10();
		float L_31 = L_30->get_x_2();
		if ((((float)L_31) == ((float)(1.0f))))
		{
			goto IL_01d1;
		}
	}
	{
		// prevNumMove=numericMove;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32 = __this->get_numericMove_10();
		__this->set_prevNumMove_11(L_32);
		// numericMove = new Vector3(-1, 0, 0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33;
		memset((&L_33), 0, sizeof(L_33));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_33), (-1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_numericMove_10(L_33);
		// currCellX = stdGrid.getPosXn();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_34 = __this->get_stdGrid_17();
		int32_t L_35;
		L_35 = mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F(L_34, /*hidden argument*/NULL);
		__this->set_currCellX_5(L_35);
		// currCellY = stdGrid.getPosYn();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_36 = __this->get_stdGrid_17();
		int32_t L_37;
		L_37 = mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70(L_36, /*hidden argument*/NULL);
		__this->set_currCellY_6(L_37);
		// moveReq = false;
		__this->set_moveReq_7((bool)0);
	}

IL_01d1:
	{
		// realMove= numericMove * moveSpeed * Time.deltaTime;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38 = __this->get_numericMove_10();
		int32_t L_39 = __this->get_moveSpeed_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_40;
		L_40 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_38, ((float)((float)L_39)), /*hidden argument*/NULL);
		float L_41;
		L_41 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_42;
		L_42 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_40, L_41, /*hidden argument*/NULL);
		__this->set_realMove_14(L_42);
		// if (stdGrid.getPosXn()!=currCellX || stdGrid.getPosYn()!=currCellY){
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_43 = __this->get_stdGrid_17();
		int32_t L_44;
		L_44 = mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F(L_43, /*hidden argument*/NULL);
		int32_t L_45 = __this->get_currCellX_5();
		if ((!(((uint32_t)L_44) == ((uint32_t)L_45))))
		{
			goto IL_021c;
		}
	}
	{
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_46 = __this->get_stdGrid_17();
		int32_t L_47;
		L_47 = mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70(L_46, /*hidden argument*/NULL);
		int32_t L_48 = __this->get_currCellY_6();
		if ((((int32_t)L_47) == ((int32_t)L_48)))
		{
			goto IL_0400;
		}
	}

IL_021c:
	{
		// currCellX = stdGrid.getPosXn();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_49 = __this->get_stdGrid_17();
		int32_t L_50;
		L_50 = mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F(L_49, /*hidden argument*/NULL);
		__this->set_currCellX_5(L_50);
		// currCellY = stdGrid.getPosYn();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_51 = __this->get_stdGrid_17();
		int32_t L_52;
		L_52 = mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70(L_51, /*hidden argument*/NULL);
		__this->set_currCellY_6(L_52);
		// this.transform.position = new Vector3(currCellX-mapGen.sizeX/2,currCellY-mapGen.sizeY/2,0) - numericMove;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_53;
		L_53 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		int32_t L_54 = __this->get_currCellX_5();
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_55 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		int32_t L_56 = __this->get_currCellY_6();
		int32_t L_57 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_58;
		memset((&L_58), 0, sizeof(L_58));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_58), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_54, (int32_t)((int32_t)((int32_t)L_55/(int32_t)2)))))), ((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_56, (int32_t)((int32_t)((int32_t)L_57/(int32_t)2)))))), (0.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_59 = __this->get_numericMove_10();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_60;
		L_60 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_58, L_59, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_53, L_60, /*hidden argument*/NULL);
		// transform.eulerAngles=new Vector3(0,0,realRot);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_61;
		L_61 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_62 = __this->get_realRot_13();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_63;
		memset((&L_63), 0, sizeof(L_63));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_63), (0.0f), (0.0f), L_62, /*hidden argument*/NULL);
		Transform_set_eulerAngles_mFDCBC6282E4B1196AA26BF01008B2AAA2DD2969E(L_61, L_63, /*hidden argument*/NULL);
		// if(!moveReq){
		bool L_64 = __this->get_moveReq_7();
		if (L_64)
		{
			goto IL_02b7;
		}
	}
	{
		// cubeMove=realMove;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_65 = __this->get_realMove_14();
		__this->set_cubeMove_15(L_65);
		// moveReq=true;
		__this->set_moveReq_7((bool)1);
	}

IL_02b7:
	{
		// if(isRotating){
		bool L_66 = __this->get_isRotating_9();
		if (!L_66)
		{
			goto IL_02d1;
		}
	}
	{
		// isRotating=false;
		__this->set_isRotating_9((bool)0);
		// hRotation=0;
		__this->set_hRotation_12((0.0f));
	}

IL_02d1:
	{
		// if(prevNumMove!=numericMove){
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_67 = __this->get_prevNumMove_11();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_68 = __this->get_numericMove_10();
		bool L_69;
		L_69 = Vector3_op_Inequality_m15190A795B416EB699E69E6190DE6F1C1F208710(L_67, L_68, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_0397;
		}
	}
	{
		// if((prevNumMove.x==numericMove.y && numericMove.y!=0) || (prevNumMove.y==1 && numericMove.x==-1) || (prevNumMove.y==-1 && numericMove.x==1))
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_70 = __this->get_address_of_prevNumMove_11();
		float L_71 = L_70->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_72 = __this->get_address_of_numericMove_10();
		float L_73 = L_72->get_y_3();
		if ((!(((float)L_71) == ((float)L_73))))
		{
			goto IL_0311;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_74 = __this->get_address_of_numericMove_10();
		float L_75 = L_74->get_y_3();
		if ((!(((float)L_75) == ((float)(0.0f)))))
		{
			goto IL_0359;
		}
	}

IL_0311:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_76 = __this->get_address_of_prevNumMove_11();
		float L_77 = L_76->get_y_3();
		if ((!(((float)L_77) == ((float)(1.0f)))))
		{
			goto IL_0335;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_78 = __this->get_address_of_numericMove_10();
		float L_79 = L_78->get_x_2();
		if ((((float)L_79) == ((float)(-1.0f))))
		{
			goto IL_0359;
		}
	}

IL_0335:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_80 = __this->get_address_of_prevNumMove_11();
		float L_81 = L_80->get_y_3();
		if ((!(((float)L_81) == ((float)(-1.0f)))))
		{
			goto IL_0366;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_82 = __this->get_address_of_numericMove_10();
		float L_83 = L_82->get_x_2();
		if ((!(((float)L_83) == ((float)(1.0f)))))
		{
			goto IL_0366;
		}
	}

IL_0359:
	{
		// hRotation=90f;
		__this->set_hRotation_12((90.0f));
		goto IL_0371;
	}

IL_0366:
	{
		// hRotation=-90f;
		__this->set_hRotation_12((-90.0f));
	}

IL_0371:
	{
		// realRot+=hRotation;
		float L_84 = __this->get_realRot_13();
		float L_85 = __this->get_hRotation_12();
		__this->set_realRot_13(((float)il2cpp_codegen_add((float)L_84, (float)L_85)));
		// isRotating=true;
		__this->set_isRotating_9((bool)1);
		// prevNumMove=numericMove;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_86 = __this->get_numericMove_10();
		__this->set_prevNumMove_11(L_86);
	}

IL_0397:
	{
		// food.putFood();
		food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * L_87 = __this->get_food_19();
		food_putFood_m49DD3492104F98A1AB7C3D18633F8815D10F44F9(L_87, /*hidden argument*/NULL);
		// if (currCell.getActObst() || currCell.getActSnek()){
		currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * L_88 = __this->get_currCell_18();
		bool L_89;
		L_89 = currCell_getActObst_m09D37F4D6B041A5037A62C4D73478B4DAE7AC970(L_88, /*hidden argument*/NULL);
		if (L_89)
		{
			goto IL_03bc;
		}
	}
	{
		currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * L_90 = __this->get_currCell_18();
		bool L_91;
		L_91 = currCell_getActSnek_m316EAD81A633D817C5D143F5384EEBBF9F3248E3(L_90, /*hidden argument*/NULL);
		if (!L_91)
		{
			goto IL_03e8;
		}
	}

IL_03bc:
	{
		// alive=false;
		((playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_StaticFields*)il2cpp_codegen_static_fields_for(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_il2cpp_TypeInfo_var))->set_alive_8((bool)0);
		// realMove = new Vector3(0,0,0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_92;
		memset((&L_92), 0, sizeof(L_92));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_92), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_realMove_14(L_92);
		// cubeMove=realMove;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_93 = __this->get_realMove_14();
		__this->set_cubeMove_15(L_93);
	}

IL_03e8:
	{
		// if (currCell.getActFood())
		currCell_t2F39B86B55ED3774C8F2939955581681C1C7BF42 * L_94 = __this->get_currCell_18();
		bool L_95;
		L_95 = currCell_getActFood_mFF88646F43125C65462C569D6905737E166F340D(L_94, /*hidden argument*/NULL);
		if (!L_95)
		{
			goto IL_0400;
		}
	}
	{
		// food.eat();
		food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * L_96 = __this->get_food_19();
		food_eat_m903D542867241A92061EE6DBAE43516D4EEF67CA(L_96, /*hidden argument*/NULL);
	}

IL_0400:
	{
		// if (alive)
		bool L_97 = ((playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_StaticFields*)il2cpp_codegen_static_fields_for(playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21_il2cpp_TypeInfo_var))->get_alive_8();
		if (!L_97)
		{
			goto IL_0440;
		}
	}
	{
		// stdGrid.setPos(stdGrid.getPosXf()+realMove.x,stdGrid.getPosYf()+realMove.y);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_98 = __this->get_stdGrid_17();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_99 = __this->get_stdGrid_17();
		float L_100;
		L_100 = mapGen_getPosXf_mD0B5B3B19A9D74B82799A8AE52BD112189CB5063_inline(L_99, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_101 = __this->get_address_of_realMove_14();
		float L_102 = L_101->get_x_2();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_103 = __this->get_stdGrid_17();
		float L_104;
		L_104 = mapGen_getPosYf_mF6A253CF42326AD207A29EF6A03EEF2E511D527C_inline(L_103, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_105 = __this->get_address_of_realMove_14();
		float L_106 = L_105->get_y_3();
		mapGen_setPos_mFEED8A85AE9BBE2FB29248E997B90CBB0ABE73B0(L_98, ((float)il2cpp_codegen_add((float)L_100, (float)L_102)), ((float)il2cpp_codegen_add((float)L_104, (float)L_106)), /*hidden argument*/NULL);
	}

IL_0440:
	{
		// if (((cubeMove.x>0 && this.transform.position.x>stdGrid.getPosXn()-mapGen.sizeX/2) || (cubeMove.x<0 && this.transform.position.x<stdGrid.getPosXn()-mapGen.sizeX/2)) || ((cubeMove.y>0 && this.transform.position.y>stdGrid.getPosYn()-mapGen.sizeY/2) || (cubeMove.y<0 && this.transform.position.y<stdGrid.getPosYn()-mapGen.sizeY/2)))
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_107 = __this->get_address_of_cubeMove_15();
		float L_108 = L_107->get_x_2();
		if ((!(((float)L_108) > ((float)(0.0f)))))
		{
			goto IL_047b;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_109;
		L_109 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_110;
		L_110 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_109, /*hidden argument*/NULL);
		float L_111 = L_110.get_x_2();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_112 = __this->get_stdGrid_17();
		int32_t L_113;
		L_113 = mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F(L_112, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_114 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((float)L_111) > ((float)((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_113, (int32_t)((int32_t)((int32_t)L_114/(int32_t)2)))))))))
		{
			goto IL_0523;
		}
	}

IL_047b:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_115 = __this->get_address_of_cubeMove_15();
		float L_116 = L_115->get_x_2();
		if ((!(((float)L_116) < ((float)(0.0f)))))
		{
			goto IL_04b3;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_117;
		L_117 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_118;
		L_118 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_117, /*hidden argument*/NULL);
		float L_119 = L_118.get_x_2();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_120 = __this->get_stdGrid_17();
		int32_t L_121;
		L_121 = mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F(L_120, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_122 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((float)L_119) < ((float)((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_121, (int32_t)((int32_t)((int32_t)L_122/(int32_t)2)))))))))
		{
			goto IL_0523;
		}
	}

IL_04b3:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_123 = __this->get_address_of_cubeMove_15();
		float L_124 = L_123->get_y_3();
		if ((!(((float)L_124) > ((float)(0.0f)))))
		{
			goto IL_04eb;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_125;
		L_125 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_126;
		L_126 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_125, /*hidden argument*/NULL);
		float L_127 = L_126.get_y_3();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_128 = __this->get_stdGrid_17();
		int32_t L_129;
		L_129 = mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70(L_128, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_130 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((float)L_127) > ((float)((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_129, (int32_t)((int32_t)((int32_t)L_130/(int32_t)2)))))))))
		{
			goto IL_0523;
		}
	}

IL_04eb:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_131 = __this->get_address_of_cubeMove_15();
		float L_132 = L_131->get_y_3();
		if ((!(((float)L_132) < ((float)(0.0f)))))
		{
			goto IL_053f;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_133;
		L_133 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_134;
		L_134 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_133, /*hidden argument*/NULL);
		float L_135 = L_134.get_y_3();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_136 = __this->get_stdGrid_17();
		int32_t L_137;
		L_137 = mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70(L_136, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_138 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((!(((float)L_135) < ((float)((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_137, (int32_t)((int32_t)((int32_t)L_138/(int32_t)2))))))))))
		{
			goto IL_053f;
		}
	}

IL_0523:
	{
		// finalMove=new Vector3(0,0,0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_139;
		memset((&L_139), 0, sizeof(L_139));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_139), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_finalMove_16(L_139);
		goto IL_054b;
	}

IL_053f:
	{
		// finalMove=cubeMove;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_140 = __this->get_cubeMove_15();
		__this->set_finalMove_16(L_140);
	}

IL_054b:
	{
		// this.transform.position += finalMove;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_141;
		L_141 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_142 = L_141;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_143;
		L_143 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_142, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_144 = __this->get_finalMove_16();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_145;
		L_145 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_143, L_144, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_142, L_145, /*hidden argument*/NULL);
		// transform.Rotate(new Vector3(0,0,hRotation * Time.deltaTime * moveSpeed));
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_146;
		L_146 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_147 = __this->get_hRotation_12();
		float L_148;
		L_148 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		int32_t L_149 = __this->get_moveSpeed_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_150;
		memset((&L_150), 0, sizeof(L_150));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_150), (0.0f), (0.0f), ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_147, (float)L_148)), (float)((float)((float)L_149)))), /*hidden argument*/NULL);
		Transform_Rotate_m027A155054DDC4206F679EFB86BE0960D45C33A7(L_146, L_150, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void playerControl::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void playerControl__ctor_m8AAF24DEBD6672820A41122CDD1A459AC76B46E8 (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public int moveSpeed=1;
		__this->set_moveSpeed_4(1);
		// int currCellX=(int)mapGen.spawn.x;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		float L_0 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_x_0();
		__this->set_currCellX_5(il2cpp_codegen_cast_double_to_int<int32_t>(L_0));
		// int currCellY=(int)mapGen.spawn.y;
		float L_1 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_y_1();
		__this->set_currCellY_6(il2cpp_codegen_cast_double_to_int<int32_t>(L_1));
		// Vector3 numericMove=new Vector3(1,0,0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_2), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_numericMove_10(L_2);
		// Vector3 prevNumMove=new Vector3(1,0,0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_3), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_prevNumMove_11(L_3);
		// float realRot=-90f;
		__this->set_realRot_13((-90.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void setScoresToZero::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void setScoresToZero_Start_m92207368B58A9239FBBE0FCB65123CB9481E0800 (setScoresToZero_t83CA487B984141BA8548B2DB3F6A09827A4F9B2C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral75B9C2589F235DD3C81AB7074613877A21D6F1F6);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for(int i=0;i<=7;i++)
		V_0 = 0;
		goto IL_001f;
	}

IL_0004:
	{
		// PlayerPrefs.SetInt("Highscore"+(i), 0);
		String_t* L_0;
		L_0 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		String_t* L_1;
		L_1 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral75B9C2589F235DD3C81AB7074613877A21D6F1F6, L_0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m0C5C977E960B9CA8F9AB73AF4129C3DCABD067B6(L_1, 0, /*hidden argument*/NULL);
		// for(int i=0;i<=7;i++)
		int32_t L_2 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1));
	}

IL_001f:
	{
		// for(int i=0;i<=7;i++)
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) <= ((int32_t)7)))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void setScoresToZero::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void setScoresToZero_Update_m324BCDE20983EF1375362663BE612D0FA7406CF3 (setScoresToZero_t83CA487B984141BA8548B2DB3F6A09827A4F9B2C * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void setScoresToZero::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void setScoresToZero__ctor_m5470F41CE5743E09082E906ABBFD78E164E73EB8 (setScoresToZero_t83CA487B984141BA8548B2DB3F6A09827A4F9B2C * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2 snekOrg::getLastBod()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  snekOrg_getLastBod_m5FBE0B56E39516461FB42890B7E0350CC9099528 (snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return new Vector2(partsPositions[nbBodies].x-mapGen.spawn.x,partsPositions[nbBodies].y-mapGen.spawn.y);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_0 = __this->get_partsPositions_6();
		int32_t L_1 = __this->get_nbBodies_14();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		float L_3 = L_2.get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		float L_4 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_x_0();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_5 = __this->get_partsPositions_6();
		int32_t L_6 = __this->get_nbBodies_14();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_5, L_6, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		float L_8 = L_7.get_y_3();
		float L_9 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_10), ((float)il2cpp_codegen_subtract((float)L_3, (float)L_4)), ((float)il2cpp_codegen_subtract((float)L_8, (float)L_9)), /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void snekOrg::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void snekOrg_Start_m0E98D6FB1204B882629BC2AE96CE921B973C2A30 (snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m6C4CDF2E6D3B2347704D0BBC108D0893C1ABBEAE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		// snekParts.Add(Head);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_0 = __this->get_snekParts_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_Head_5();
		List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3(L_0, L_1, /*hidden argument*/List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		// Vector3 pos0=new Vector3(mapGen.spawn.x,mapGen.spawn.y,0);
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		float L_2 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_x_0();
		float L_3 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_y_1();
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), L_2, L_3, (0.0f), /*hidden argument*/NULL);
		// partsPositions.Add(pos0);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_4 = __this->get_partsPositions_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_0;
		List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59(L_4, L_5, /*hidden argument*/List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		// snekParts.Add(Body0);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_6 = __this->get_snekParts_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = __this->get_Body0_10();
		List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3(L_6, L_7, /*hidden argument*/List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		// Vector3 pos1=new Vector3(mapGen.spawn.x-1f,mapGen.spawn.y,0);
		float L_8 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_x_0();
		float L_9 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_y_1();
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_1), ((float)il2cpp_codegen_subtract((float)L_8, (float)(1.0f))), L_9, (0.0f), /*hidden argument*/NULL);
		// partsPositions.Add(pos1);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_10 = __this->get_partsPositions_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = V_1;
		List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59(L_10, L_11, /*hidden argument*/List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		// snekParts.Add(Body1);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_12 = __this->get_snekParts_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13 = __this->get_Body1_11();
		List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3(L_12, L_13, /*hidden argument*/List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		// Vector3 pos2=new Vector3(mapGen.spawn.x-1f,mapGen.spawn.y,0);
		float L_14 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_x_0();
		float L_15 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_y_1();
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_2), ((float)il2cpp_codegen_subtract((float)L_14, (float)(1.0f))), L_15, (0.0f), /*hidden argument*/NULL);
		// partsPositions.Add(pos2);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_16 = __this->get_partsPositions_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17 = V_2;
		List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59(L_16, L_17, /*hidden argument*/List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		// Body0.GetComponent<bodyCtrl>().setPartPos(partsPositions[1]);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_18 = __this->get_Body0_10();
		bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * L_19;
		L_19 = GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678(L_18, /*hidden argument*/GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678_RuntimeMethod_var);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_20 = __this->get_partsPositions_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_20, 1, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		bodyCtrl_setPartPos_mF2E73FC83E1E868B997B2BEA397F8DDFED0AE581(L_19, L_21, /*hidden argument*/NULL);
		// Body1.GetComponent<bodyCtrl>().setPartPos(partsPositions[2]);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_22 = __this->get_Body1_11();
		bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * L_23;
		L_23 = GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678(L_22, /*hidden argument*/GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678_RuntimeMethod_var);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_24 = __this->get_partsPositions_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_24, 2, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		bodyCtrl_setPartPos_mF2E73FC83E1E868B997B2BEA397F8DDFED0AE581(L_23, L_25, /*hidden argument*/NULL);
		// Vector3 pos3=new Vector3(mapGen.spawn.x-2f,mapGen.spawn.y,0);
		float L_26 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_x_0();
		float L_27 = (((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_address_of_spawn_6())->get_y_1();
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_3), ((float)il2cpp_codegen_subtract((float)L_26, (float)(2.0f))), L_27, (0.0f), /*hidden argument*/NULL);
		// Tail.GetComponent<tailCtrl>().setTailPos(pos3);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_28 = __this->get_Tail_12();
		tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * L_29;
		L_29 = GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF(L_28, /*hidden argument*/GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30 = V_3;
		tailCtrl_setTailPos_mEEACFF413EBB022438C4B1B2698C82BFEE770CCB(L_29, L_30, /*hidden argument*/NULL);
		// Tail.GetComponent<tailCtrl>().setTailMov(partsPositions[2]);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_31 = __this->get_Tail_12();
		tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * L_32;
		L_32 = GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF(L_31, /*hidden argument*/GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF_RuntimeMethod_var);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_33 = __this->get_partsPositions_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34;
		L_34 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_33, 2, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		tailCtrl_setTailMov_mE01124E941868160F86C540239A3D6BA8C852F3D(L_32, L_34, /*hidden argument*/NULL);
		// partsRotations.Add(new Vector2(-90f,0f));
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_35 = __this->get_partsRotations_7();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_36;
		memset((&L_36), 0, sizeof(L_36));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_36), (-90.0f), (0.0f), /*hidden argument*/NULL);
		List_1_Add_m6C4CDF2E6D3B2347704D0BBC108D0893C1ABBEAE(L_35, L_36, /*hidden argument*/List_1_Add_m6C4CDF2E6D3B2347704D0BBC108D0893C1ABBEAE_RuntimeMethod_var);
		// partsRotations.Add(new Vector2(-90f,0f));
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_37 = __this->get_partsRotations_7();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_38;
		memset((&L_38), 0, sizeof(L_38));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_38), (-90.0f), (0.0f), /*hidden argument*/NULL);
		List_1_Add_m6C4CDF2E6D3B2347704D0BBC108D0893C1ABBEAE(L_37, L_38, /*hidden argument*/List_1_Add_m6C4CDF2E6D3B2347704D0BBC108D0893C1ABBEAE_RuntimeMethod_var);
		// partsRotations.Add(new Vector2(-90f,0f));
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_39 = __this->get_partsRotations_7();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_40;
		memset((&L_40), 0, sizeof(L_40));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_40), (-90.0f), (0.0f), /*hidden argument*/NULL);
		List_1_Add_m6C4CDF2E6D3B2347704D0BBC108D0893C1ABBEAE(L_39, L_40, /*hidden argument*/List_1_Add_m6C4CDF2E6D3B2347704D0BBC108D0893C1ABBEAE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void snekOrg::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void snekOrg_Update_m5235BFE252820DF50228481B21552148824281AC (snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m6C4CDF2E6D3B2347704D0BBC108D0893C1ABBEAE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_set_Item_m0E40C7E017BB8ADBADBD6DE8947884FA4DEA2DE5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_0 = NULL;
	bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		// partsRotations[1]=new Vector2(controller.getRealRot(),controller.getRotVar());
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_0 = __this->get_partsRotations_7();
		playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * L_1 = __this->get_controller_8();
		float L_2;
		L_2 = playerControl_getRealRot_m196ED94BADA5895078952015608913BDBEE5ED2F_inline(L_1, /*hidden argument*/NULL);
		playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * L_3 = __this->get_controller_8();
		float L_4;
		L_4 = playerControl_getRotVar_mBFEA83CCFACBEE74025DDAAF2CC76DD5A7351492_inline(L_3, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_5), L_2, L_4, /*hidden argument*/NULL);
		List_1_set_Item_m0E40C7E017BB8ADBADBD6DE8947884FA4DEA2DE5(L_0, 1, L_5, /*hidden argument*/List_1_set_Item_m0E40C7E017BB8ADBADBD6DE8947884FA4DEA2DE5_RuntimeMethod_var);
		// snekParts[1].GetComponent<bodyCtrl>().setPartRot(partsRotations[1]);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_6 = __this->get_snekParts_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7;
		L_7 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_6, 1, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * L_8;
		L_8 = GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678(L_7, /*hidden argument*/GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678_RuntimeMethod_var);
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_9 = __this->get_partsRotations_7();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_10;
		L_10 = List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_inline(L_9, 1, /*hidden argument*/List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_RuntimeMethod_var);
		bodyCtrl_setPartRot_mBC7E7CFF8FB896D4BC0509D48B14AE566E9E1205(L_8, L_10, /*hidden argument*/NULL);
		// if ((stdGrid.getPosXn()!=(controller.getCurrCell()).x || stdGrid.getPosYn()!=(controller.getCurrCell()).y)){
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_11 = __this->get_stdGrid_9();
		int32_t L_12;
		L_12 = mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F(L_11, /*hidden argument*/NULL);
		playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * L_13 = __this->get_controller_8();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = playerControl_getCurrCell_m55DE57C3FFE3237AACE857B1843ED79C8A85E798(L_13, /*hidden argument*/NULL);
		float L_15 = L_14.get_x_2();
		if ((!(((float)((float)((float)L_12))) == ((float)L_15))))
		{
			goto IL_0088;
		}
	}
	{
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_16 = __this->get_stdGrid_9();
		int32_t L_17;
		L_17 = mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70(L_16, /*hidden argument*/NULL);
		playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * L_18 = __this->get_controller_8();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = playerControl_getCurrCell_m55DE57C3FFE3237AACE857B1843ED79C8A85E798(L_18, /*hidden argument*/NULL);
		float L_20 = L_19.get_y_3();
		if ((((float)((float)((float)L_17))) == ((float)L_20)))
		{
			goto IL_0385;
		}
	}

IL_0088:
	{
		// if(food.getMealsCount()!=latestMealsCount){
		food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * L_21 = __this->get_food_13();
		int32_t L_22;
		L_22 = food_getMealsCount_m2FC192A4AFEF2441E3A95C6746D52D446497CCA4_inline(L_21, /*hidden argument*/NULL);
		int32_t L_23 = __this->get_latestMealsCount_15();
		if ((((int32_t)L_22) == ((int32_t)L_23)))
		{
			goto IL_01b3;
		}
	}
	{
		// GameObject tempBody=Instantiate(Body1) as GameObject;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_24 = __this->get_Body1_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_25;
		L_25 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_24, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		V_0 = L_25;
		// nbBodies++;
		int32_t L_26 = __this->get_nbBodies_14();
		__this->set_nbBodies_14(((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)1)));
		// bodyCtrl temp = tempBody.GetComponent<bodyCtrl>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_27 = V_0;
		bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * L_28;
		L_28 = GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678(L_27, /*hidden argument*/GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678_RuntimeMethod_var);
		V_1 = L_28;
		// temp.setPartPos(partsPositions[nbBodies-1]);
		bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * L_29 = V_1;
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_30 = __this->get_partsPositions_6();
		int32_t L_31 = __this->get_nbBodies_14();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_30, ((int32_t)il2cpp_codegen_subtract((int32_t)L_31, (int32_t)1)), /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		bodyCtrl_setPartPos_mF2E73FC83E1E868B997B2BEA397F8DDFED0AE581(L_29, L_32, /*hidden argument*/NULL);
		// temp.setPartRot(partsRotations[nbBodies-1]);
		bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * L_33 = V_1;
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_34 = __this->get_partsRotations_7();
		int32_t L_35 = __this->get_nbBodies_14();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_36;
		L_36 = List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_inline(L_34, ((int32_t)il2cpp_codegen_subtract((int32_t)L_35, (int32_t)1)), /*hidden argument*/List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_RuntimeMethod_var);
		bodyCtrl_setPartRot_mBC7E7CFF8FB896D4BC0509D48B14AE566E9E1205(L_33, L_36, /*hidden argument*/NULL);
		// partsPositions.Add(temp.getPartPos());
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_37 = __this->get_partsPositions_6();
		bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * L_38 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39;
		L_39 = bodyCtrl_getPartPos_m0D3736BCBE106245967F152BDCB41711B45E2A8D(L_38, /*hidden argument*/NULL);
		List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59(L_37, L_39, /*hidden argument*/List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		// partsRotations.Add(partsRotations[nbBodies-1]);
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_40 = __this->get_partsRotations_7();
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_41 = __this->get_partsRotations_7();
		int32_t L_42 = __this->get_nbBodies_14();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_43;
		L_43 = List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_inline(L_41, ((int32_t)il2cpp_codegen_subtract((int32_t)L_42, (int32_t)1)), /*hidden argument*/List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_RuntimeMethod_var);
		List_1_Add_m6C4CDF2E6D3B2347704D0BBC108D0893C1ABBEAE(L_40, L_43, /*hidden argument*/List_1_Add_m6C4CDF2E6D3B2347704D0BBC108D0893C1ABBEAE_RuntimeMethod_var);
		// snekParts.Add(tempBody);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_44 = __this->get_snekParts_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_45 = V_0;
		List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3(L_44, L_45, /*hidden argument*/List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		// Tail.transform.position=partsPositions[nbBodies];
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_46 = __this->get_Tail_12();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_47;
		L_47 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_46, /*hidden argument*/NULL);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_48 = __this->get_partsPositions_6();
		int32_t L_49 = __this->get_nbBodies_14();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_50;
		L_50 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_48, L_49, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_47, L_50, /*hidden argument*/NULL);
		// Tail.GetComponent<tailCtrl>().setTailMov(new Vector3(0,0,0));
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_51 = __this->get_Tail_12();
		tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * L_52;
		L_52 = GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF(L_51, /*hidden argument*/GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_53;
		memset((&L_53), 0, sizeof(L_53));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_53), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		tailCtrl_setTailMov_mE01124E941868160F86C540239A3D6BA8C852F3D(L_52, L_53, /*hidden argument*/NULL);
		// Tail.GetComponent<tailCtrl>().setTailRot(new Vector2(Tail.GetComponent<tailCtrl>().getTailRot().x,0f));
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_54 = __this->get_Tail_12();
		tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * L_55;
		L_55 = GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF(L_54, /*hidden argument*/GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF_RuntimeMethod_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_56 = __this->get_Tail_12();
		tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * L_57;
		L_57 = GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF(L_56, /*hidden argument*/GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF_RuntimeMethod_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_58;
		L_58 = tailCtrl_getTailRot_mF96B56AA79CC6E7284796797F55193F648284768(L_57, /*hidden argument*/NULL);
		float L_59 = L_58.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_60;
		memset((&L_60), 0, sizeof(L_60));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_60), L_59, (0.0f), /*hidden argument*/NULL);
		tailCtrl_setTailRot_m750C26637F78E88A6A761762BDBB6293A1F70E89(L_55, L_60, /*hidden argument*/NULL);
		// latestMealsCount++;
		int32_t L_61 = __this->get_latestMealsCount_15();
		__this->set_latestMealsCount_15(((int32_t)il2cpp_codegen_add((int32_t)L_61, (int32_t)1)));
		// }
		goto IL_02be;
	}

IL_01b3:
	{
		// Tail.GetComponent<tailCtrl>().setTailPos(partsPositions[nbBodies]);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_62 = __this->get_Tail_12();
		tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * L_63;
		L_63 = GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF(L_62, /*hidden argument*/GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF_RuntimeMethod_var);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_64 = __this->get_partsPositions_6();
		int32_t L_65 = __this->get_nbBodies_14();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_66;
		L_66 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_64, L_65, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		tailCtrl_setTailPos_mEEACFF413EBB022438C4B1B2698C82BFEE770CCB(L_63, L_66, /*hidden argument*/NULL);
		// Tail.GetComponent<tailCtrl>().setTailMov(partsPositions[nbBodies-1]);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_67 = __this->get_Tail_12();
		tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * L_68;
		L_68 = GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF(L_67, /*hidden argument*/GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF_RuntimeMethod_var);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_69 = __this->get_partsPositions_6();
		int32_t L_70 = __this->get_nbBodies_14();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_71;
		L_71 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_69, ((int32_t)il2cpp_codegen_subtract((int32_t)L_70, (int32_t)1)), /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		tailCtrl_setTailMov_mE01124E941868160F86C540239A3D6BA8C852F3D(L_68, L_71, /*hidden argument*/NULL);
		// Tail.GetComponent<tailCtrl>().setTailRot(partsRotations[nbBodies-1]);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_72 = __this->get_Tail_12();
		tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * L_73;
		L_73 = GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF(L_72, /*hidden argument*/GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF_RuntimeMethod_var);
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_74 = __this->get_partsRotations_7();
		int32_t L_75 = __this->get_nbBodies_14();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_76;
		L_76 = List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_inline(L_74, ((int32_t)il2cpp_codegen_subtract((int32_t)L_75, (int32_t)1)), /*hidden argument*/List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_RuntimeMethod_var);
		tailCtrl_setTailRot_m750C26637F78E88A6A761762BDBB6293A1F70E89(L_73, L_76, /*hidden argument*/NULL);
		// snekParts[nbBodies].GetComponent<bodyCtrl>().setPartPos(partsPositions[nbBodies-1]);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_77 = __this->get_snekParts_4();
		int32_t L_78 = __this->get_nbBodies_14();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_79;
		L_79 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_77, L_78, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * L_80;
		L_80 = GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678(L_79, /*hidden argument*/GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678_RuntimeMethod_var);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_81 = __this->get_partsPositions_6();
		int32_t L_82 = __this->get_nbBodies_14();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_83;
		L_83 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_81, ((int32_t)il2cpp_codegen_subtract((int32_t)L_82, (int32_t)1)), /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		bodyCtrl_setPartPos_mF2E73FC83E1E868B997B2BEA397F8DDFED0AE581(L_80, L_83, /*hidden argument*/NULL);
		// partsPositions[nbBodies]=partsPositions[nbBodies-1];
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_84 = __this->get_partsPositions_6();
		int32_t L_85 = __this->get_nbBodies_14();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_86 = __this->get_partsPositions_6();
		int32_t L_87 = __this->get_nbBodies_14();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_88;
		L_88 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_86, ((int32_t)il2cpp_codegen_subtract((int32_t)L_87, (int32_t)1)), /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406(L_84, L_85, L_88, /*hidden argument*/List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406_RuntimeMethod_var);
		// partsRotations[nbBodies]=partsRotations[nbBodies-1];
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_89 = __this->get_partsRotations_7();
		int32_t L_90 = __this->get_nbBodies_14();
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_91 = __this->get_partsRotations_7();
		int32_t L_92 = __this->get_nbBodies_14();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_93;
		L_93 = List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_inline(L_91, ((int32_t)il2cpp_codegen_subtract((int32_t)L_92, (int32_t)1)), /*hidden argument*/List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_RuntimeMethod_var);
		List_1_set_Item_m0E40C7E017BB8ADBADBD6DE8947884FA4DEA2DE5(L_89, L_90, L_93, /*hidden argument*/List_1_set_Item_m0E40C7E017BB8ADBADBD6DE8947884FA4DEA2DE5_RuntimeMethod_var);
		// snekParts[nbBodies].GetComponent<bodyCtrl>().setPartRot(partsRotations[nbBodies-1]);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_94 = __this->get_snekParts_4();
		int32_t L_95 = __this->get_nbBodies_14();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_96;
		L_96 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_94, L_95, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * L_97;
		L_97 = GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678(L_96, /*hidden argument*/GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678_RuntimeMethod_var);
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_98 = __this->get_partsRotations_7();
		int32_t L_99 = __this->get_nbBodies_14();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_100;
		L_100 = List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_inline(L_98, ((int32_t)il2cpp_codegen_subtract((int32_t)L_99, (int32_t)1)), /*hidden argument*/List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_RuntimeMethod_var);
		bodyCtrl_setPartRot_mBC7E7CFF8FB896D4BC0509D48B14AE566E9E1205(L_97, L_100, /*hidden argument*/NULL);
	}

IL_02be:
	{
		// for(int i=nbBodies-2; i>=0;i--){
		int32_t L_101 = __this->get_nbBodies_14();
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_101, (int32_t)2));
		goto IL_0350;
	}

IL_02cc:
	{
		// snekParts[i+1].GetComponent<bodyCtrl>().setPartPos(partsPositions[i]);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_102 = __this->get_snekParts_4();
		int32_t L_103 = V_2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_104;
		L_104 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_102, ((int32_t)il2cpp_codegen_add((int32_t)L_103, (int32_t)1)), /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * L_105;
		L_105 = GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678(L_104, /*hidden argument*/GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678_RuntimeMethod_var);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_106 = __this->get_partsPositions_6();
		int32_t L_107 = V_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_108;
		L_108 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_106, L_107, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		bodyCtrl_setPartPos_mF2E73FC83E1E868B997B2BEA397F8DDFED0AE581(L_105, L_108, /*hidden argument*/NULL);
		// partsPositions[i+1]=partsPositions[i];
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_109 = __this->get_partsPositions_6();
		int32_t L_110 = V_2;
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_111 = __this->get_partsPositions_6();
		int32_t L_112 = V_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_113;
		L_113 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_111, L_112, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406(L_109, ((int32_t)il2cpp_codegen_add((int32_t)L_110, (int32_t)1)), L_113, /*hidden argument*/List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406_RuntimeMethod_var);
		// if(i>0){
		int32_t L_114 = V_2;
		if ((((int32_t)L_114) <= ((int32_t)0)))
		{
			goto IL_034c;
		}
	}
	{
		// partsRotations[i+1]=partsRotations[i];
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_115 = __this->get_partsRotations_7();
		int32_t L_116 = V_2;
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_117 = __this->get_partsRotations_7();
		int32_t L_118 = V_2;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_119;
		L_119 = List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_inline(L_117, L_118, /*hidden argument*/List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_RuntimeMethod_var);
		List_1_set_Item_m0E40C7E017BB8ADBADBD6DE8947884FA4DEA2DE5(L_115, ((int32_t)il2cpp_codegen_add((int32_t)L_116, (int32_t)1)), L_119, /*hidden argument*/List_1_set_Item_m0E40C7E017BB8ADBADBD6DE8947884FA4DEA2DE5_RuntimeMethod_var);
		// snekParts[i+1].GetComponent<bodyCtrl>().setPartRot(partsRotations[i]);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_120 = __this->get_snekParts_4();
		int32_t L_121 = V_2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_122;
		L_122 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_120, ((int32_t)il2cpp_codegen_add((int32_t)L_121, (int32_t)1)), /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		bodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D * L_123;
		L_123 = GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678(L_122, /*hidden argument*/GameObject_GetComponent_TisbodyCtrl_tB2399BE54966F51CD24F29FD11D37D41F5FFDC2D_mFCDB53D411B3E2648CF0ED3907BDD83FADDF1678_RuntimeMethod_var);
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_124 = __this->get_partsRotations_7();
		int32_t L_125 = V_2;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_126;
		L_126 = List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_inline(L_124, L_125, /*hidden argument*/List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_RuntimeMethod_var);
		bodyCtrl_setPartRot_mBC7E7CFF8FB896D4BC0509D48B14AE566E9E1205(L_123, L_126, /*hidden argument*/NULL);
	}

IL_034c:
	{
		// for(int i=nbBodies-2; i>=0;i--){
		int32_t L_127 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_127, (int32_t)1));
	}

IL_0350:
	{
		// for(int i=nbBodies-2; i>=0;i--){
		int32_t L_128 = V_2;
		if ((((int32_t)L_128) >= ((int32_t)0)))
		{
			goto IL_02cc;
		}
	}
	{
		// partsPositions[0]=new Vector3(stdGrid.getPosXn(),stdGrid.getPosYn(),0);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_129 = __this->get_partsPositions_6();
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_130 = __this->get_stdGrid_9();
		int32_t L_131;
		L_131 = mapGen_getPosXn_m7DE63FB6D969A1BF9523551A9BE741AD9C4E500F(L_130, /*hidden argument*/NULL);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_132 = __this->get_stdGrid_9();
		int32_t L_133;
		L_133 = mapGen_getPosYn_mF82B4557C5B32302B1F1A1B1151AFEBD798B5F70(L_132, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_134;
		memset((&L_134), 0, sizeof(L_134));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_134), ((float)((float)L_131)), ((float)((float)L_133)), (0.0f), /*hidden argument*/NULL);
		List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406(L_129, 0, L_134, /*hidden argument*/List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406_RuntimeMethod_var);
	}

IL_0385:
	{
		// for(int i=0; i<mapGen.sizeX; i++)
		V_3 = 0;
		goto IL_03b5;
	}

IL_0389:
	{
		// for(int j=0; j<mapGen.sizeY; j++)
		V_4 = 0;
		goto IL_03a8;
	}

IL_038e:
	{
		// ((cell) stdGrid.getCellAt(i,j)).isSnek(false);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_135 = __this->get_stdGrid_9();
		int32_t L_136 = V_3;
		int32_t L_137 = V_4;
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_138;
		L_138 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_135, L_136, L_137, /*hidden argument*/NULL);
		cell_isSnek_mCF5381441AFAA8846A05EB36C5011A9A634AC4A8_inline(L_138, (bool)0, /*hidden argument*/NULL);
		// for(int j=0; j<mapGen.sizeY; j++)
		int32_t L_139 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_139, (int32_t)1));
	}

IL_03a8:
	{
		// for(int j=0; j<mapGen.sizeY; j++)
		int32_t L_140 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_141 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		if ((((int32_t)L_140) < ((int32_t)L_141)))
		{
			goto IL_038e;
		}
	}
	{
		// for(int i=0; i<mapGen.sizeX; i++)
		int32_t L_142 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_142, (int32_t)1));
	}

IL_03b5:
	{
		// for(int i=0; i<mapGen.sizeX; i++)
		int32_t L_143 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_144 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		if ((((int32_t)L_143) < ((int32_t)L_144)))
		{
			goto IL_0389;
		}
	}
	{
		// for (int i=1; i<=nbBodies;i++)
		V_5 = 1;
		goto IL_03ff;
	}

IL_03c2:
	{
		// ((cell) stdGrid.getCellAt((int)partsPositions[i].x,(int)partsPositions[i].y)).isSnek(true);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_145 = __this->get_stdGrid_9();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_146 = __this->get_partsPositions_6();
		int32_t L_147 = V_5;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_148;
		L_148 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_146, L_147, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		float L_149 = L_148.get_x_2();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_150 = __this->get_partsPositions_6();
		int32_t L_151 = V_5;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_152;
		L_152 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_150, L_151, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		float L_153 = L_152.get_y_3();
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_154;
		L_154 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_145, il2cpp_codegen_cast_double_to_int<int32_t>(L_149), il2cpp_codegen_cast_double_to_int<int32_t>(L_153), /*hidden argument*/NULL);
		cell_isSnek_mCF5381441AFAA8846A05EB36C5011A9A634AC4A8_inline(L_154, (bool)1, /*hidden argument*/NULL);
		// for (int i=1; i<=nbBodies;i++)
		int32_t L_155 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_155, (int32_t)1));
	}

IL_03ff:
	{
		// for (int i=1; i<=nbBodies;i++)
		int32_t L_156 = V_5;
		int32_t L_157 = __this->get_nbBodies_14();
		if ((((int32_t)L_156) <= ((int32_t)L_157)))
		{
			goto IL_03c2;
		}
	}
	{
		// ((cell) stdGrid.getCellAt((int)Tail.GetComponent<tailCtrl>().getTailPos().x,(int)Tail.GetComponent<tailCtrl>().getTailPos().y)).isSnek(true);
		mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * L_158 = __this->get_stdGrid_9();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_159 = __this->get_Tail_12();
		tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * L_160;
		L_160 = GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF(L_159, /*hidden argument*/GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_161;
		L_161 = tailCtrl_getTailPos_m23E21B9AA7E968D91E76B28D51A9ADB68A11580D(L_160, /*hidden argument*/NULL);
		float L_162 = L_161.get_x_2();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_163 = __this->get_Tail_12();
		tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * L_164;
		L_164 = GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF(L_163, /*hidden argument*/GameObject_GetComponent_TistailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0_m56ECF7B62BE9B9F91CB1E5B806F2420440336FFF_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_165;
		L_165 = tailCtrl_getTailPos_m23E21B9AA7E968D91E76B28D51A9ADB68A11580D(L_164, /*hidden argument*/NULL);
		float L_166 = L_165.get_y_3();
		cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * L_167;
		L_167 = mapGen_getCellAt_m080C1A1D1FC027A8F6370C51CF53FBA193A30D2A(L_158, il2cpp_codegen_cast_double_to_int<int32_t>(L_162), il2cpp_codegen_cast_double_to_int<int32_t>(L_166), /*hidden argument*/NULL);
		cell_isSnek_mCF5381441AFAA8846A05EB36C5011A9A634AC4A8_inline(L_167, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void snekOrg::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void snekOrg__ctor_m659B105E543F7F7C33128A164722ED9CBCD86744 (snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<GameObject> snekParts = new List<GameObject>();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_0 = (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *)il2cpp_codegen_object_new(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8(L_0, /*hidden argument*/List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		__this->set_snekParts_4(L_0);
		// public int nbBodies=2;
		__this->set_nbBodies_14(2);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 tailCtrl::getTailPos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  tailCtrl_getTailPos_m23E21B9AA7E968D91E76B28D51A9ADB68A11580D (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return new Vector3(position.x+mapGen.sizeX/2,position.y+mapGen.sizeY/2,0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_0 = __this->get_address_of_position_4();
		float L_1 = L_0->get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_2 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_3 = __this->get_address_of_position_4();
		float L_4 = L_3->get_y_3();
		int32_t L_5 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), ((float)il2cpp_codegen_add((float)L_1, (float)((float)((float)((int32_t)((int32_t)L_2/(int32_t)2)))))), ((float)il2cpp_codegen_add((float)L_4, (float)((float)((float)((int32_t)((int32_t)L_5/(int32_t)2)))))), (0.0f), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void tailCtrl::setTailPos(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void tailCtrl_setTailPos_mEEACFF413EBB022438C4B1B2698C82BFEE770CCB (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___X0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// position = new Vector3(X.x-mapGen.sizeX/2,X.y-mapGen.sizeY/2,0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___X0;
		float L_1 = L_0.get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_2 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___X0;
		float L_4 = L_3.get_y_3();
		int32_t L_5 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), ((float)il2cpp_codegen_subtract((float)L_1, (float)((float)((float)((int32_t)((int32_t)L_2/(int32_t)2)))))), ((float)il2cpp_codegen_subtract((float)L_4, (float)((float)((float)((int32_t)((int32_t)L_5/(int32_t)2)))))), (0.0f), /*hidden argument*/NULL);
		__this->set_position_4(L_6);
		// transform.position=position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = __this->get_position_4();
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_7, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Vector2 tailCtrl::getTailRot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  tailCtrl_getTailRot_mF96B56AA79CC6E7284796797F55193F648284768 (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, const RuntimeMethod* method)
{
	{
		// return new Vector2(rotation,rotVar);
		float L_0 = __this->get_rotation_8();
		float L_1 = __this->get_rotVar_9();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void tailCtrl::setTailRot(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void tailCtrl_setTailRot_m750C26637F78E88A6A761762BDBB6293A1F70E89 (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___X0, const RuntimeMethod* method)
{
	{
		// rotation=X.x;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___X0;
		float L_1 = L_0.get_x_0();
		__this->set_rotation_8(L_1);
		// rotVar=X.y;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___X0;
		float L_3 = L_2.get_y_1();
		__this->set_rotVar_9(L_3);
		// }
		return;
	}
}
// UnityEngine.Vector3 tailCtrl::getTailNumMove(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  tailCtrl_getTailNumMove_mA639704C8C2829692EA74BCC1D19AD429C7616DC (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___X0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return new Vector3((Mathf.Round(X.x-mapGen.sizeX/2-position.x)),(Mathf.Round(X.y-mapGen.sizeY/2-position.y)),0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___X0;
		float L_1 = L_0.get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var);
		int32_t L_2 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeX_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_3 = __this->get_address_of_position_4();
		float L_4 = L_3->get_x_2();
		float L_5;
		L_5 = bankers_roundf(((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)L_1, (float)((float)((float)((int32_t)((int32_t)L_2/(int32_t)2)))))), (float)L_4)));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___X0;
		float L_7 = L_6.get_y_3();
		int32_t L_8 = ((mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_StaticFields*)il2cpp_codegen_static_fields_for(mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8_il2cpp_TypeInfo_var))->get_sizeY_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_9 = __this->get_address_of_position_4();
		float L_10 = L_9->get_y_3();
		float L_11;
		L_11 = bankers_roundf(((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)L_7, (float)((float)((float)((int32_t)((int32_t)L_8/(int32_t)2)))))), (float)L_10)));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), L_5, L_11, (0.0f), /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void tailCtrl::setTailMov(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void tailCtrl_setTailMov_mE01124E941868160F86C540239A3D6BA8C852F3D (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___X0, const RuntimeMethod* method)
{
	{
		// movement = getTailNumMove(X)*playermov.getMoveSpeed()*Time.deltaTime;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___X0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = tailCtrl_getTailNumMove_mA639704C8C2829692EA74BCC1D19AD429C7616DC(__this, L_0, /*hidden argument*/NULL);
		playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * L_2 = __this->get_playermov_6();
		int32_t L_3;
		L_3 = playerControl_getMoveSpeed_m80AEB733464773149D656FB2C35DFA1AC6B40679_inline(L_2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_1, ((float)((float)L_3)), /*hidden argument*/NULL);
		float L_5;
		L_5 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_4, L_5, /*hidden argument*/NULL);
		__this->set_movement_5(L_6);
		// }
		return;
	}
}
// System.Void tailCtrl::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void tailCtrl_Start_m1D83791053C6657814A2822871A2A0274AAD533C (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void tailCtrl::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void tailCtrl_Update_mBAC245F71878294B1A0DB6AF2C0A2518136E7FFF (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, const RuntimeMethod* method)
{
	{
		// if ((movement.x>0 && this.transform.position.x<=SNEK.getLastBod().x) || (movement.x<0 && this.transform.position.x>=SNEK.getLastBod().x) || (movement.y>0 && this.transform.position.y<=SNEK.getLastBod().y) || (movement.y<0 && this.transform.position.y>=SNEK.getLastBod().y))
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_0 = __this->get_address_of_movement_5();
		float L_1 = L_0->get_x_2();
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_0037;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		float L_4 = L_3.get_x_2();
		snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1 * L_5 = __this->get_SNEK_7();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		L_6 = snekOrg_getLastBod_m5FBE0B56E39516461FB42890B7E0350CC9099528(L_5, /*hidden argument*/NULL);
		float L_7 = L_6.get_x_0();
		if ((((float)L_4) <= ((float)L_7)))
		{
			goto IL_00d3;
		}
	}

IL_0037:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_8 = __this->get_address_of_movement_5();
		float L_9 = L_8->get_x_2();
		if ((!(((float)L_9) < ((float)(0.0f)))))
		{
			goto IL_006b;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_10, /*hidden argument*/NULL);
		float L_12 = L_11.get_x_2();
		snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1 * L_13 = __this->get_SNEK_7();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_14;
		L_14 = snekOrg_getLastBod_m5FBE0B56E39516461FB42890B7E0350CC9099528(L_13, /*hidden argument*/NULL);
		float L_15 = L_14.get_x_0();
		if ((((float)L_12) >= ((float)L_15)))
		{
			goto IL_00d3;
		}
	}

IL_006b:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_16 = __this->get_address_of_movement_5();
		float L_17 = L_16->get_y_3();
		if ((!(((float)L_17) > ((float)(0.0f)))))
		{
			goto IL_009f;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18;
		L_18 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_18, /*hidden argument*/NULL);
		float L_20 = L_19.get_y_3();
		snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1 * L_21 = __this->get_SNEK_7();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_22;
		L_22 = snekOrg_getLastBod_m5FBE0B56E39516461FB42890B7E0350CC9099528(L_21, /*hidden argument*/NULL);
		float L_23 = L_22.get_y_1();
		if ((((float)L_20) <= ((float)L_23)))
		{
			goto IL_00d3;
		}
	}

IL_009f:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_24 = __this->get_address_of_movement_5();
		float L_25 = L_24->get_y_3();
		if ((!(((float)L_25) < ((float)(0.0f)))))
		{
			goto IL_00ef;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_26;
		L_26 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
		L_27 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_26, /*hidden argument*/NULL);
		float L_28 = L_27.get_y_3();
		snekOrg_t825FF5F7F1DEF3F23C243353A06F0622F700A3E1 * L_29 = __this->get_SNEK_7();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_30;
		L_30 = snekOrg_getLastBod_m5FBE0B56E39516461FB42890B7E0350CC9099528(L_29, /*hidden argument*/NULL);
		float L_31 = L_30.get_y_1();
		if ((!(((float)L_28) >= ((float)L_31))))
		{
			goto IL_00ef;
		}
	}

IL_00d3:
	{
		// transform.position +=movement;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_32;
		L_32 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_33 = L_32;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34;
		L_34 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_33, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35 = __this->get_movement_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_36;
		L_36 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_34, L_35, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_33, L_36, /*hidden argument*/NULL);
	}

IL_00ef:
	{
		// if(rotVar==0)
		float L_37 = __this->get_rotVar_9();
		if ((!(((float)L_37) == ((float)(0.0f)))))
		{
			goto IL_011d;
		}
	}
	{
		// transform.eulerAngles=new Vector3(0,0,rotation);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_38;
		L_38 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_39 = __this->get_rotation_8();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_40;
		memset((&L_40), 0, sizeof(L_40));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_40), (0.0f), (0.0f), L_39, /*hidden argument*/NULL);
		Transform_set_eulerAngles_mFDCBC6282E4B1196AA26BF01008B2AAA2DD2969E(L_38, L_40, /*hidden argument*/NULL);
		return;
	}

IL_011d:
	{
		// transform.Rotate(new Vector3(0,0,rotVar * Time.deltaTime * playermov.getMoveSpeed()));
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_41;
		L_41 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_42 = __this->get_rotVar_9();
		float L_43;
		L_43 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * L_44 = __this->get_playermov_6();
		int32_t L_45;
		L_45 = playerControl_getMoveSpeed_m80AEB733464773149D656FB2C35DFA1AC6B40679_inline(L_44, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_46;
		memset((&L_46), 0, sizeof(L_46));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_46), (0.0f), (0.0f), ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_42, (float)L_43)), (float)((float)((float)L_45)))), /*hidden argument*/NULL);
		Transform_Rotate_m027A155054DDC4206F679EFB86BE0960D45C33A7(L_41, L_46, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void tailCtrl::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void tailCtrl__ctor_m475755F15D43F40B1531EB8AD7647D1B05126D7E (tailCtrl_t71CE805627994DC4C7B1D0A22968DF4DF49205A0 * __this, const RuntimeMethod* method)
{
	{
		// float rotation=-90f;
		__this->set_rotation_8((-90.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WS_Client/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mC9FCDA33F9592287831C1A277EF1A67B98729D25 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7 * L_0 = (U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7 *)il2cpp_codegen_object_new(U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mC98D4D4F3BC2DBE2D21B3CE34E2F186BD837AD64(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void WS_Client/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mC98D4D4F3BC2DBE2D21B3CE34E2F186BD837AD64 (U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WS_Client/<>c::<Start>b__2_0(System.Object,WebSocketSharp.MessageEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStartU3Eb__2_0_mCFC48F6EEA3302A6173CFB18D550F6447FEB0BFD (U3CU3Ec_tC2B4D6404FD7C067BAF5B26BA4EA8936926B37D7 * __this, RuntimeObject * ___sender0, MessageEventArgs_t644AAD070C0BA6114FE7C4BD44A31BF3A9FA19D2 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3959E7C25DB570E5997B285C2E5C61F96FF05445);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE12F82F2AF27C8BFF5B2C886B542D7A59A46BF96);
		s_Il2CppMethodInitialized = true;
	}
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	{
		// Debug.Log("Message received from " + ((WebSocket)sender).Url + ", Data : " + e.Data);
		RuntimeObject * L_0 = ___sender0;
		Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * L_1;
		L_1 = WebSocket_get_Url_mBC9629B05E6D0F63B87A76E8E079DFDD78DFC788(((WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC *)CastclassClass((RuntimeObject*)L_0, WebSocket_t8A996D528D2852CEFE9FB719BDD39719BAA377AC_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = _stringLiteral3959E7C25DB570E5997B285C2E5C61F96FF05445;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = _stringLiteral3959E7C25DB570E5997B285C2E5C61F96FF05445;
			goto IL_0017;
		}
	}
	{
		G_B3_0 = ((String_t*)(NULL));
		G_B3_1 = G_B1_1;
		goto IL_001c;
	}

IL_0017:
	{
		String_t* L_3;
		L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, G_B2_0);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_1;
	}

IL_001c:
	{
		MessageEventArgs_t644AAD070C0BA6114FE7C4BD44A31BF3A9FA19D2 * L_4 = ___e1;
		String_t* L_5;
		L_5 = MessageEventArgs_get_Data_mD9D53F8250BB093F649A6279D1EB19D408901A52(L_4, /*hidden argument*/NULL);
		String_t* L_6;
		L_6 = String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78(G_B3_1, G_B3_0, _stringLiteralE12F82F2AF27C8BFF5B2C886B542D7A59A46BF96, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_6, /*hidden argument*/NULL);
		// };
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_4), L_1, L_3, (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001a;
	}

IL_001a:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool cell_getObst_m01A4AB2FC92883533D633DFB00B0293688033197_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	{
		// return this.isObstacle;
		bool L_0 = __this->get_isObstacle_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool cell_getSnek_m0571C8B3866F69B57E862C8E5F0151E587D7D5E0_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	{
		// return this.isCellSnek;
		bool L_0 = __this->get_isCellSnek_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool cell_getFood_mC65A1DA2CC3B72B729F408DF4CE5E1D40A8A6FEC_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	{
		// return this.isCellFood;
		bool L_0 = __this->get_isCellFood_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float playerControl_getRealRot_m196ED94BADA5895078952015608913BDBEE5ED2F_inline (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method)
{
	{
		// return realRot;
		float L_0 = __this->get_realRot_13();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void cell_isFood_m213F08D7ABE5B167DB7707B44A1B176941FBDDED_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, bool ___b0, const RuntimeMethod* method)
{
	{
		// this.isCellFood=b;
		bool L_0 = ___b0;
		__this->set_isCellFood_5(L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t cell_getCellXn_m290BAFE1347F788D453D34BDC5240DAC865E8590_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	{
		// return mapX;
		int32_t L_0 = __this->get_mapX_7();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t cell_getCellYn_m706FEF38E9A39770DC1C68C83A58AB266A032060_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, const RuntimeMethod* method)
{
	{
		// return mapY;
		int32_t L_0 = __this->get_mapY_8();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * mapGen_getMap_m627656B6E78F2C9C4FC13EBADD4DB886ABC7FE96_inline (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method)
{
	{
		// return map;
		ArrayList_t6C1A49839DC1F0D568E8E11FA1626FCF0EC06575 * L_0 = __this->get_map_10();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void cell_isObst_m82CCE920ED8127788B1D9D0AD12C78D337138A24_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, bool ___b0, const RuntimeMethod* method)
{
	{
		// this.isObstacle=b;
		bool L_0 = ___b0;
		__this->set_isObstacle_4(L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void bloc_setBloc_m63AAC7F1ECD6DD0D60B5E9DD66B1E8A39A03C186_inline (bloc_t2B5AE9427EBFE7091A74C782916DCECC15AF12E7 * __this, bool ___b0, const RuntimeMethod* method)
{
	{
		// isSet=b;
		bool L_0 = ___b0;
		__this->set_isSet_8(L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool button_isBtnSet_mA09EFC2279D396533426BC9D0B19401285896182_inline (button_t4F901F446B79F86CB9593995276166F7ADD407E6 * __this, const RuntimeMethod* method)
{
	{
		// return isSet;
		bool L_0 = __this->get_isSet_8();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float mapGen_getPosXf_mD0B5B3B19A9D74B82799A8AE52BD112189CB5063_inline (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method)
{
	{
		// return this.floatPosX;
		float L_0 = __this->get_floatPosX_7();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float mapGen_getPosYf_mF6A253CF42326AD207A29EF6A03EEF2E511D527C_inline (mapGen_t5AF095A5FA9F185122797BA62776F021CD9038B8 * __this, const RuntimeMethod* method)
{
	{
		// return this.floatPosY;
		float L_0 = __this->get_floatPosY_8();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float playerControl_getRotVar_mBFEA83CCFACBEE74025DDAAF2CC76DD5A7351492_inline (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method)
{
	{
		// return hRotation;
		float L_0 = __this->get_hRotation_12();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t food_getMealsCount_m2FC192A4AFEF2441E3A95C6746D52D446497CCA4_inline (food_t03A1F65F99660B06E1CB5BF29E080CD11F687652 * __this, const RuntimeMethod* method)
{
	{
		// return mealsCount;
		int32_t L_0 = __this->get_mealsCount_7();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void cell_isSnek_mCF5381441AFAA8846A05EB36C5011A9A634AC4A8_inline (cell_tA81DFAAE3D5131A73E0CA3E659E2425F315117EF * __this, bool ___b0, const RuntimeMethod* method)
{
	{
		// this.isCellSnek=b;
		bool L_0 = ___b0;
		__this->set_isCellSnek_6(L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t playerControl_getMoveSpeed_m80AEB733464773149D656FB2C35DFA1AC6B40679_inline (playerControl_t9DDFE48E907DD2109E602231ACE7E5E4F5BCAB21 * __this, const RuntimeMethod* method)
{
	{
		// return moveSpeed;
		int32_t L_0 = __this->get_moveSpeed_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Array_Empty_TisRuntimeObject_m1FBC21243DF3542384C523801E8CA8A97606C747_gshared_inline (const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = ((EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Value_0();
		return (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_2 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)__this->get__items_1();
		int32_t L_3 = ___index0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)L_2, (int32_t)L_3);
		return (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  List_1_get_Item_m3E1AEDD64868D9F6901AFBF0FA6B0A7A0001BA1E_gshared_inline (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_2 = (Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA*)__this->get__items_1();
		int32_t L_3 = ___index0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA*)L_2, (int32_t)L_3);
		return (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_4;
	}
}
